<?php 
return array (
  'version' => '2016-04-09 08:36:15',
  'content' => 
  array (
    '足球' => 
    array (
      3 => 
      array (
        'id' => 1,
        'name' => '3人制',
        'value' => '3',
        'min' => 3,
        'max' => 10,
      ),
      5 => 
      array (
        'id' => 2,
        'name' => '5人制',
        'value' => '5',
        'min' => 5,
        'max' => 10,
      ),
      7 => 
      array (
        'id' => 3,
        'name' => '7人制',
        'value' => '7',
        'min' => 7,
        'max' => 15,
      ),
      8 => 
      array (
        'id' => 4,
        'name' => '8人制',
        'value' => '8',
        'min' => 8,
        'max' => 15,
      ),
      11 => 
      array (
        'id' => 5,
        'name' => '11人制',
        'value' => '11',
        'min' => 11,
        'max' => 18,
      ),
    ),
  ),
)
;