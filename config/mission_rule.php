<?php 
return array (
  'user' => 
  array (
    'once_register' => 
    array (
      'id' => 1,
      'title' => '注册',
      'key' => 'once_register',
      'credit' => 100,
      'exp' => 2,
      'type' => 'once',
    ),
    'once_improve' => 
    array (
      'id' => 2,
      'title' => '完善资料',
      'key' => 'once_improve',
      'credit' => 20,
      'exp' => 2,
      'type' => 'once',
    ),
    'once_real_name_auth' => 
    array (
      'id' => 3,
      'title' => '实名认证',
      'key' => 'once_real_name_auth',
      'credit' => 30,
      'exp' => 2,
      'type' => 'once',
    ),
    'once_corps_create' => 
    array (
      'id' => 4,
      'title' => '创建战队',
      'key' => 'once_corps_create',
      'credit' => 20,
      'exp' => 1,
      'type' => 'once',
    ),
    'once_corps_reunit' => 
    array (
      'id' => 5,
      'title' => '加入战队',
      'key' => 'once_corps_reunit',
      'credit' => 20,
      'exp' => 1,
      'type' => 'once',
    ),
    'once_share_app' => 
    array (
      'id' => 6,
      'title' => '分享',
      'key' => 'once_share_app',
      'credit' => 10,
      'exp' => 2,
      'type' => 'once',
    ),
    'once_recharge' => 
    array (
      'id' => 7,
      'title' => '首充',
      'key' => 'once_recharge',
      'credit' => 100,
      'exp' => 2,
      'type' => 'once',
    ),
    'daily_sign' => 
    array (
      'id' => 8,
      'title' => '每日签到',
      'key' => 'daily_sign',
      'credit' => 2,
      'exp' => 1,
      'type' => 'common',
    ),
    'daily_share' => 
    array (
      'id' => 9,
      'title' => '每日分享',
      'key' => 'daily_share',
      'credit' => 10,
      'exp' => 5,
      'type' => 'common',
    ),
    'weekly_week_count' => 
    array (
      'id' => 10,
      'title' => '周踢球达两次',
      'key' => 'weekly_week_count',
      'credit' => 50,
      'exp' => 5,
      'type' => 'common',
    ),
    'long_join_corps' => 
    array (
      'id' => 11,
      'title' => '加入5支战队',
      'key' => 'long_join_corps',
      'credit' => 20,
      'exp' => 2,
      'type' => 'common',
    ),
    'long_register' => 
    array (
      'id' => 12,
      'title' => '邀请好友注册',
      'key' => 'long_register',
      'credit' => 100,
      'exp' => 2,
      'type' => 'common',
    ),
  ),
  'corps' => 
  array (
    'corps_create' => 
    array (
      'id' => 13,
      'title' => '创建战队',
      'key' => 'corps_create',
      'credit' => 1000,
      'exp' => 0,
      'type' => 'once',
    ),
    'corps_team_count' => 
    array (
      'id' => 14,
      'title' => '战队达到10人',
      'key' => 'corps_team_count',
      'credit' => 100,
      'exp' => 10,
      'type' => 'once',
    ),
    'corps_secretary' => 
    array (
      'id' => 15,
      'title' => '战队达到15人送球秘',
      'key' => 'corps_secretary',
      'credit' => 0,
      'exp' => 0,
      'type' => 'once',
    ),
    'corps_match' => 
    array (
      'id' => 16,
      'title' => '约战/应战一次',
      'key' => 'corps_match',
      'credit' => 100,
      'exp' => 20,
      'type' => 'once',
    ),
    'corps_match_count' => 
    array (
      'id' => 17,
      'title' => '周踢球达两次',
      'key' => 'corps_match_count',
      'credit' => 200,
      'exp' => 60,
      'type' => 'once',
    ),
  ),
)
;