<?php 
return array (
  'version' => '2016-04-09 08:36:15',
  'content' => 
  array (
    '足球' => 
    array (
      0 => 
      array (
        'id' => 1,
        'name' => '前锋',
        'value' => '前锋',
      ),
      1 => 
      array (
        'id' => 2,
        'name' => '中锋',
        'value' => '中锋',
      ),
      2 => 
      array (
        'id' => 3,
        'name' => '前卫',
        'value' => '前卫',
      ),
      3 => 
      array (
        'id' => 4,
        'name' => '后腰',
        'value' => '后腰',
      ),
      4 => 
      array (
        'id' => 5,
        'name' => '前腰',
        'value' => '前腰',
      ),
      5 => 
      array (
        'id' => 6,
        'name' => '后卫',
        'value' => '后卫',
      ),
      6 => 
      array (
        'id' => 7,
        'name' => '中卫',
        'value' => '中卫',
      ),
      7 => 
      array (
        'id' => 8,
        'name' => '守门员',
        'value' => '守门员',
      ),
    ),
  ),
)
;