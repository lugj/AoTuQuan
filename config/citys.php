<?php 
return array (
  'version' => '2016-04-09 10:12:34',
  'content' => 
  array (
    0 => 
    array (
      'title' => '热门城市',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 20,
          'name' => '北京',
          'first_spell' => 'bj',
          'all_spell' => 'beijing',
        ),
        1 => 
        array (
          'id' => 53,
          'name' => '常州',
          'first_spell' => 'cz',
          'all_spell' => 'changzhou',
        ),
        2 => 
        array (
          'id' => 92,
          'name' => '广州',
          'first_spell' => 'gz',
          'all_spell' => 'guangzhou',
        ),
        3 => 
        array (
          'id' => 196,
          'name' => '南京',
          'first_spell' => 'nj',
          'all_spell' => 'nanjing',
        ),
        4 => 
        array (
          'id' => 229,
          'name' => '深圳',
          'first_spell' => 'sz',
          'all_spell' => 'shenzhen',
        ),
        5 => 
        array (
          'id' => 232,
          'name' => '上海',
          'first_spell' => 'sh',
          'all_spell' => 'shanghai',
        ),
        6 => 
        array (
          'id' => 256,
          'name' => '苏州',
          'first_spell' => 'sz',
          'all_spell' => 'suzhou',
        ),
        7 => 
        array (
          'id' => 265,
          'name' => '天津',
          'first_spell' => 'tj',
          'all_spell' => 'tianjin',
        ),
        8 => 
        array (
          'id' => 290,
          'name' => '无锡',
          'first_spell' => 'wx',
          'all_spell' => 'wuxi',
        ),
      ),
    ),
    1 => 
    array (
      'title' => 'A',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 1,
          'name' => '阿坝',
          'first_spell' => 'ab',
          'all_spell' => 'aba',
        ),
        1 => 
        array (
          'id' => 2,
          'name' => '安康',
          'first_spell' => 'ak',
          'all_spell' => 'ankang',
        ),
        2 => 
        array (
          'id' => 3,
          'name' => '阿克苏',
          'first_spell' => 'aks',
          'all_spell' => 'akesu',
        ),
        3 => 
        array (
          'id' => 4,
          'name' => '阿里',
          'first_spell' => 'al',
          'all_spell' => 'ali',
        ),
        4 => 
        array (
          'id' => 5,
          'name' => '阿拉尔',
          'first_spell' => 'ale',
          'all_spell' => 'alaer',
        ),
        5 => 
        array (
          'id' => 6,
          'name' => '阿拉善盟',
          'first_spell' => 'alsm',
          'all_spell' => 'alashanmeng',
        ),
        6 => 
        array (
          'id' => 7,
          'name' => '阿勒泰',
          'first_spell' => 'alt',
          'all_spell' => 'aletai',
        ),
        7 => 
        array (
          'id' => 8,
          'name' => '澳门',
          'first_spell' => 'am',
          'all_spell' => 'aomen',
        ),
        8 => 
        array (
          'id' => 9,
          'name' => '安庆',
          'first_spell' => 'aq',
          'all_spell' => 'anqing',
        ),
        9 => 
        array (
          'id' => 10,
          'name' => '鞍山',
          'first_spell' => 'as',
          'all_spell' => 'anshan',
        ),
        10 => 
        array (
          'id' => 11,
          'name' => '安顺',
          'first_spell' => 'as',
          'all_spell' => 'anshun',
        ),
        11 => 
        array (
          'id' => 12,
          'name' => '安阳',
          'first_spell' => 'ay',
          'all_spell' => 'anyang',
        ),
      ),
    ),
    2 => 
    array (
      'title' => 'B',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 13,
          'name' => '蚌埠',
          'first_spell' => 'bb',
          'all_spell' => 'bangbu',
        ),
        1 => 
        array (
          'id' => 14,
          'name' => '白城',
          'first_spell' => 'bc',
          'all_spell' => 'baicheng',
        ),
        2 => 
        array (
          'id' => 15,
          'name' => '保定',
          'first_spell' => 'bd',
          'all_spell' => 'baoding',
        ),
        3 => 
        array (
          'id' => 16,
          'name' => '博尔塔拉',
          'first_spell' => 'betl',
          'all_spell' => 'boertala',
        ),
        4 => 
        array (
          'id' => 17,
          'name' => '北海',
          'first_spell' => 'bh',
          'all_spell' => 'beihai',
        ),
        5 => 
        array (
          'id' => 18,
          'name' => '毕节',
          'first_spell' => 'bj',
          'all_spell' => 'bijie',
        ),
        6 => 
        array (
          'id' => 19,
          'name' => '宝鸡',
          'first_spell' => 'bj',
          'all_spell' => 'baoji',
        ),
        7 => 
        array (
          'id' => 20,
          'name' => '北京',
          'first_spell' => 'bj',
          'all_spell' => 'beijing',
        ),
        8 => 
        array (
          'id' => 21,
          'name' => '白沙',
          'first_spell' => 'bs',
          'all_spell' => 'baisha',
        ),
        9 => 
        array (
          'id' => 22,
          'name' => '百色',
          'first_spell' => 'bs',
          'all_spell' => 'baise',
        ),
        10 => 
        array (
          'id' => 23,
          'name' => '白山',
          'first_spell' => 'bs',
          'all_spell' => 'baishan',
        ),
        11 => 
        array (
          'id' => 24,
          'name' => '保山',
          'first_spell' => 'bs',
          'all_spell' => 'baoshan',
        ),
        12 => 
        array (
          'id' => 25,
          'name' => '包头',
          'first_spell' => 'bt',
          'all_spell' => 'baotou',
        ),
        13 => 
        array (
          'id' => 26,
          'name' => '保亭',
          'first_spell' => 'bt',
          'all_spell' => 'baoting',
        ),
        14 => 
        array (
          'id' => 27,
          'name' => '本溪',
          'first_spell' => 'bx',
          'all_spell' => 'benxi',
        ),
        15 => 
        array (
          'id' => 28,
          'name' => '白银',
          'first_spell' => 'by',
          'all_spell' => 'baiyin',
        ),
        16 => 
        array (
          'id' => 29,
          'name' => '巴音郭楞',
          'first_spell' => 'bygl',
          'all_spell' => 'bayinguoleng',
        ),
        17 => 
        array (
          'id' => 30,
          'name' => '巴彦淖尔',
          'first_spell' => 'byne',
          'all_spell' => 'bayannaoer',
        ),
        18 => 
        array (
          'id' => 31,
          'name' => '巴中',
          'first_spell' => 'bz',
          'all_spell' => 'bazhong',
        ),
        19 => 
        array (
          'id' => 32,
          'name' => '滨州',
          'first_spell' => 'bz',
          'all_spell' => 'binzhou',
        ),
      ),
    ),
    3 => 
    array (
      'title' => 'C',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 33,
          'name' => '长春',
          'first_spell' => 'cc',
          'all_spell' => 'changchun',
        ),
        1 => 
        array (
          'id' => 34,
          'name' => '成都',
          'first_spell' => 'cd',
          'all_spell' => 'chengdu',
        ),
        2 => 
        array (
          'id' => 35,
          'name' => '昌都',
          'first_spell' => 'cd',
          'all_spell' => 'changdu',
        ),
        3 => 
        array (
          'id' => 36,
          'name' => '承德',
          'first_spell' => 'cd',
          'all_spell' => 'chengde',
        ),
        4 => 
        array (
          'id' => 37,
          'name' => '常德',
          'first_spell' => 'cd',
          'all_spell' => 'changde',
        ),
        5 => 
        array (
          'id' => 38,
          'name' => '赤峰',
          'first_spell' => 'cf',
          'all_spell' => 'chifeng',
        ),
        6 => 
        array (
          'id' => 39,
          'name' => '巢湖',
          'first_spell' => 'ch',
          'all_spell' => 'chaohu',
        ),
        7 => 
        array (
          'id' => 40,
          'name' => '昌吉',
          'first_spell' => 'cj',
          'all_spell' => 'changji',
        ),
        8 => 
        array (
          'id' => 41,
          'name' => '昌江',
          'first_spell' => 'cj',
          'all_spell' => 'changjiang',
        ),
        9 => 
        array (
          'id' => 42,
          'name' => '澄迈',
          'first_spell' => 'cm',
          'all_spell' => 'chengmai',
        ),
        10 => 
        array (
          'id' => 43,
          'name' => '长沙',
          'first_spell' => 'cs',
          'all_spell' => 'changsha',
        ),
        11 => 
        array (
          'id' => 44,
          'name' => '楚雄',
          'first_spell' => 'cx',
          'all_spell' => 'chuxiong',
        ),
        12 => 
        array (
          'id' => 45,
          'name' => '朝阳',
          'first_spell' => 'cy',
          'all_spell' => 'chaoyang',
        ),
        13 => 
        array (
          'id' => 46,
          'name' => '长治',
          'first_spell' => 'cz',
          'all_spell' => 'changzhi',
        ),
        14 => 
        array (
          'id' => 47,
          'name' => '沧州',
          'first_spell' => 'cz',
          'all_spell' => 'cangzhou',
        ),
        15 => 
        array (
          'id' => 48,
          'name' => '潮州',
          'first_spell' => 'cz',
          'all_spell' => 'chaozhou',
        ),
        16 => 
        array (
          'id' => 49,
          'name' => '滁州',
          'first_spell' => 'cz',
          'all_spell' => 'chuzhou',
        ),
        17 => 
        array (
          'id' => 50,
          'name' => '池州',
          'first_spell' => 'cz',
          'all_spell' => 'chizhou',
        ),
        18 => 
        array (
          'id' => 51,
          'name' => '郴州',
          'first_spell' => 'cz',
          'all_spell' => 'chenzhou',
        ),
        19 => 
        array (
          'id' => 52,
          'name' => '崇左',
          'first_spell' => 'cz',
          'all_spell' => 'chongzuo',
        ),
        20 => 
        array (
          'id' => 53,
          'name' => '常州',
          'first_spell' => 'cz',
          'all_spell' => 'changzhou',
        ),
      ),
    ),
    4 => 
    array (
      'title' => 'D',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 54,
          'name' => '东莞',
          'first_spell' => 'd',
          'all_spell' => 'dong',
        ),
        1 => 
        array (
          'id' => 55,
          'name' => '定安',
          'first_spell' => 'da',
          'all_spell' => 'dingan',
        ),
        2 => 
        array (
          'id' => 56,
          'name' => '丹东',
          'first_spell' => 'dd',
          'all_spell' => 'dandong',
        ),
        3 => 
        array (
          'id' => 57,
          'name' => '东方',
          'first_spell' => 'df',
          'all_spell' => 'dongfang',
        ),
        4 => 
        array (
          'id' => 58,
          'name' => '德宏',
          'first_spell' => 'dh',
          'all_spell' => 'dehong',
        ),
        5 => 
        array (
          'id' => 59,
          'name' => '大连',
          'first_spell' => 'dl',
          'all_spell' => 'dalian',
        ),
        6 => 
        array (
          'id' => 60,
          'name' => '大理',
          'first_spell' => 'dl',
          'all_spell' => 'dali',
        ),
        7 => 
        array (
          'id' => 61,
          'name' => '迪庆',
          'first_spell' => 'dq',
          'all_spell' => 'diqing',
        ),
        8 => 
        array (
          'id' => 62,
          'name' => '大庆',
          'first_spell' => 'dq',
          'all_spell' => 'daqing',
        ),
        9 => 
        array (
          'id' => 63,
          'name' => '大同',
          'first_spell' => 'dt',
          'all_spell' => 'datong',
        ),
        10 => 
        array (
          'id' => 64,
          'name' => '定西',
          'first_spell' => 'dx',
          'all_spell' => 'dingxi',
        ),
        11 => 
        array (
          'id' => 65,
          'name' => '大兴安岭',
          'first_spell' => 'dxal',
          'all_spell' => 'daxinganling',
        ),
        12 => 
        array (
          'id' => 66,
          'name' => '德阳',
          'first_spell' => 'dy',
          'all_spell' => 'deyang',
        ),
        13 => 
        array (
          'id' => 67,
          'name' => '东营',
          'first_spell' => 'dy',
          'all_spell' => 'dongying',
        ),
        14 => 
        array (
          'id' => 68,
          'name' => '钓鱼岛',
          'first_spell' => 'dyd',
          'all_spell' => 'diaoyudao',
        ),
        15 => 
        array (
          'id' => 69,
          'name' => '达州',
          'first_spell' => 'dz',
          'all_spell' => 'dazhou',
        ),
        16 => 
        array (
          'id' => 70,
          'name' => '德州',
          'first_spell' => 'dz',
          'all_spell' => 'dezhou',
        ),
      ),
    ),
    5 => 
    array (
      'title' => 'E',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 71,
          'name' => '鄂尔多斯',
          'first_spell' => 'eeds',
          'all_spell' => 'eerduosi',
        ),
        1 => 
        array (
          'id' => 72,
          'name' => '恩施',
          'first_spell' => 'es',
          'all_spell' => 'enshi',
        ),
        2 => 
        array (
          'id' => 73,
          'name' => '鄂州',
          'first_spell' => 'ez',
          'all_spell' => 'ezhou',
        ),
      ),
    ),
    6 => 
    array (
      'title' => 'F',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 74,
          'name' => '防城港',
          'first_spell' => 'fcg',
          'all_spell' => 'fangchenggang',
        ),
        1 => 
        array (
          'id' => 75,
          'name' => '抚顺',
          'first_spell' => 'fs',
          'all_spell' => 'fushun',
        ),
        2 => 
        array (
          'id' => 76,
          'name' => '佛山',
          'first_spell' => 'fs',
          'all_spell' => 'foshan',
        ),
        3 => 
        array (
          'id' => 77,
          'name' => '阜新',
          'first_spell' => 'fx',
          'all_spell' => 'fuxin',
        ),
        4 => 
        array (
          'id' => 78,
          'name' => '阜阳',
          'first_spell' => 'fy',
          'all_spell' => 'fuyang',
        ),
        5 => 
        array (
          'id' => 79,
          'name' => '抚州',
          'first_spell' => 'fz',
          'all_spell' => 'fuzhou',
        ),
        6 => 
        array (
          'id' => 80,
          'name' => '福州',
          'first_spell' => 'fz',
          'all_spell' => 'fuzhou',
        ),
      ),
    ),
    7 => 
    array (
      'title' => 'G',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 81,
          'name' => '广安',
          'first_spell' => 'ga',
          'all_spell' => 'guangan',
        ),
        1 => 
        array (
          'id' => 82,
          'name' => '格尔木',
          'first_spell' => 'gem',
          'all_spell' => 'geermu',
        ),
        2 => 
        array (
          'id' => 83,
          'name' => '贵港',
          'first_spell' => 'gg',
          'all_spell' => 'guigang',
        ),
        3 => 
        array (
          'id' => 84,
          'name' => '果洛',
          'first_spell' => 'gl',
          'all_spell' => 'guoluo',
        ),
        4 => 
        array (
          'id' => 85,
          'name' => '桂林',
          'first_spell' => 'gl',
          'all_spell' => 'guilin',
        ),
        5 => 
        array (
          'id' => 86,
          'name' => '甘南',
          'first_spell' => 'gn',
          'all_spell' => 'gannan',
        ),
        6 => 
        array (
          'id' => 87,
          'name' => '高雄',
          'first_spell' => 'gx',
          'all_spell' => 'gaoxiong',
        ),
        7 => 
        array (
          'id' => 88,
          'name' => '广元',
          'first_spell' => 'gy',
          'all_spell' => 'guangyuan',
        ),
        8 => 
        array (
          'id' => 89,
          'name' => '固原',
          'first_spell' => 'gy',
          'all_spell' => 'guyuan',
        ),
        9 => 
        array (
          'id' => 90,
          'name' => '贵阳',
          'first_spell' => 'gy',
          'all_spell' => 'guiyang',
        ),
        10 => 
        array (
          'id' => 91,
          'name' => '赣州',
          'first_spell' => 'gz',
          'all_spell' => 'ganzhou',
        ),
        11 => 
        array (
          'id' => 92,
          'name' => '广州',
          'first_spell' => 'gz',
          'all_spell' => 'guangzhou',
        ),
        12 => 
        array (
          'id' => 93,
          'name' => '甘孜',
          'first_spell' => 'gz',
          'all_spell' => 'ganzi',
        ),
      ),
    ),
    8 => 
    array (
      'title' => 'H',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 94,
          'name' => '漯河',
          'first_spell' => 'h',
          'all_spell' => 'he',
        ),
        1 => 
        array (
          'id' => 95,
          'name' => '淮安',
          'first_spell' => 'ha',
          'all_spell' => 'huaian',
        ),
        2 => 
        array (
          'id' => 96,
          'name' => '鹤壁',
          'first_spell' => 'hb',
          'all_spell' => 'hebi',
        ),
        3 => 
        array (
          'id' => 97,
          'name' => '淮北',
          'first_spell' => 'hb',
          'all_spell' => 'huaibei',
        ),
        4 => 
        array (
          'id' => 98,
          'name' => '海北',
          'first_spell' => 'hb',
          'all_spell' => 'haibei',
        ),
        5 => 
        array (
          'id' => 99,
          'name' => '河池',
          'first_spell' => 'hc',
          'all_spell' => 'hechi',
        ),
        6 => 
        array (
          'id' => 100,
          'name' => '海东',
          'first_spell' => 'hd',
          'all_spell' => 'haidong',
        ),
        7 => 
        array (
          'id' => 101,
          'name' => '邯郸',
          'first_spell' => 'hd',
          'all_spell' => 'handan',
        ),
        8 => 
        array (
          'id' => 102,
          'name' => '哈尔滨',
          'first_spell' => 'heb',
          'all_spell' => 'haerbin',
        ),
        9 => 
        array (
          'id' => 103,
          'name' => '合肥',
          'first_spell' => 'hf',
          'all_spell' => 'hefei',
        ),
        10 => 
        array (
          'id' => 104,
          'name' => '鹤岗',
          'first_spell' => 'hg',
          'all_spell' => 'hegang',
        ),
        11 => 
        array (
          'id' => 105,
          'name' => '黄冈',
          'first_spell' => 'hg',
          'all_spell' => 'huanggang',
        ),
        12 => 
        array (
          'id' => 106,
          'name' => '黑河',
          'first_spell' => 'hh',
          'all_spell' => 'heihe',
        ),
        13 => 
        array (
          'id' => 107,
          'name' => '红河',
          'first_spell' => 'hh',
          'all_spell' => 'honghe',
        ),
        14 => 
        array (
          'id' => 108,
          'name' => '怀化',
          'first_spell' => 'hh',
          'all_spell' => 'huaihua',
        ),
        15 => 
        array (
          'id' => 109,
          'name' => '呼和浩特',
          'first_spell' => 'hhht',
          'all_spell' => 'huhehaote',
        ),
        16 => 
        array (
          'id' => 110,
          'name' => '海口',
          'first_spell' => 'hk',
          'all_spell' => 'haikou',
        ),
        17 => 
        array (
          'id' => 111,
          'name' => '呼伦贝尔',
          'first_spell' => 'hlbe',
          'all_spell' => 'hulunbeier',
        ),
        18 => 
        array (
          'id' => 112,
          'name' => '葫芦岛',
          'first_spell' => 'hld',
          'all_spell' => 'huludao',
        ),
        19 => 
        array (
          'id' => 113,
          'name' => '哈密',
          'first_spell' => 'hm',
          'all_spell' => 'hami',
        ),
        20 => 
        array (
          'id' => 114,
          'name' => '海南',
          'first_spell' => 'hn',
          'all_spell' => 'hainan',
        ),
        21 => 
        array (
          'id' => 115,
          'name' => '淮南',
          'first_spell' => 'hn',
          'all_spell' => 'huainan',
        ),
        22 => 
        array (
          'id' => 116,
          'name' => '黄南',
          'first_spell' => 'hn',
          'all_spell' => 'huangnan',
        ),
        23 => 
        array (
          'id' => 117,
          'name' => '黄石',
          'first_spell' => 'hs',
          'all_spell' => 'huangshi',
        ),
        24 => 
        array (
          'id' => 118,
          'name' => '黄山',
          'first_spell' => 'hs',
          'all_spell' => 'huangshan',
        ),
        25 => 
        array (
          'id' => 119,
          'name' => '衡水',
          'first_spell' => 'hs',
          'all_spell' => 'hengshui',
        ),
        26 => 
        array (
          'id' => 120,
          'name' => '和田',
          'first_spell' => 'ht',
          'all_spell' => 'hetian',
        ),
        27 => 
        array (
          'id' => 121,
          'name' => '海西',
          'first_spell' => 'hx',
          'all_spell' => 'haixi',
        ),
        28 => 
        array (
          'id' => 122,
          'name' => '衡阳',
          'first_spell' => 'hy',
          'all_spell' => 'hengyang',
        ),
        29 => 
        array (
          'id' => 123,
          'name' => '河源',
          'first_spell' => 'hy',
          'all_spell' => 'heyuan',
        ),
        30 => 
        array (
          'id' => 124,
          'name' => '菏泽',
          'first_spell' => 'hz',
          'all_spell' => 'heze',
        ),
        31 => 
        array (
          'id' => 125,
          'name' => '湖州',
          'first_spell' => 'hz',
          'all_spell' => 'huzhou',
        ),
        32 => 
        array (
          'id' => 126,
          'name' => '汉中',
          'first_spell' => 'hz',
          'all_spell' => 'hanzhong',
        ),
        33 => 
        array (
          'id' => 127,
          'name' => '杭州',
          'first_spell' => 'hz',
          'all_spell' => 'hangzhou',
        ),
        34 => 
        array (
          'id' => 128,
          'name' => '贺州',
          'first_spell' => 'hz',
          'all_spell' => 'hezhou',
        ),
        35 => 
        array (
          'id' => 129,
          'name' => '惠州',
          'first_spell' => 'hz',
          'all_spell' => 'huizhou',
        ),
      ),
    ),
    9 => 
    array (
      'title' => 'J',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 130,
          'name' => '吉安',
          'first_spell' => 'ja',
          'all_spell' => 'jian',
        ),
        1 => 
        array (
          'id' => 131,
          'name' => '金昌',
          'first_spell' => 'jc',
          'all_spell' => 'jinchang',
        ),
        2 => 
        array (
          'id' => 132,
          'name' => '晋城',
          'first_spell' => 'jc',
          'all_spell' => 'jincheng',
        ),
        3 => 
        array (
          'id' => 133,
          'name' => '景德镇',
          'first_spell' => 'jdz',
          'all_spell' => 'jingdezhen',
        ),
        4 => 
        array (
          'id' => 134,
          'name' => '金华',
          'first_spell' => 'jh',
          'all_spell' => 'jinhua',
        ),
        5 => 
        array (
          'id' => 135,
          'name' => '九江',
          'first_spell' => 'jj',
          'all_spell' => 'jiujiang',
        ),
        6 => 
        array (
          'id' => 136,
          'name' => '吉林',
          'first_spell' => 'jl',
          'all_spell' => 'jilin',
        ),
        7 => 
        array (
          'id' => 137,
          'name' => '江门',
          'first_spell' => 'jm',
          'all_spell' => 'jiangmen',
        ),
        8 => 
        array (
          'id' => 138,
          'name' => '荆门',
          'first_spell' => 'jm',
          'all_spell' => 'jingmen',
        ),
        9 => 
        array (
          'id' => 139,
          'name' => '佳木斯',
          'first_spell' => 'jms',
          'all_spell' => 'jiamusi',
        ),
        10 => 
        array (
          'id' => 140,
          'name' => '济南',
          'first_spell' => 'jn',
          'all_spell' => 'jinan',
        ),
        11 => 
        array (
          'id' => 141,
          'name' => '济宁',
          'first_spell' => 'jn',
          'all_spell' => 'jining',
        ),
        12 => 
        array (
          'id' => 142,
          'name' => '酒泉',
          'first_spell' => 'jq',
          'all_spell' => 'jiuquan',
        ),
        13 => 
        array (
          'id' => 143,
          'name' => '鸡西',
          'first_spell' => 'jx',
          'all_spell' => 'jixi',
        ),
        14 => 
        array (
          'id' => 144,
          'name' => '嘉兴',
          'first_spell' => 'jx',
          'all_spell' => 'jiaxing',
        ),
        15 => 
        array (
          'id' => 145,
          'name' => '揭阳',
          'first_spell' => 'jy',
          'all_spell' => 'jieyang',
        ),
        16 => 
        array (
          'id' => 146,
          'name' => '济源',
          'first_spell' => 'jy',
          'all_spell' => 'jiyuan',
        ),
        17 => 
        array (
          'id' => 147,
          'name' => '嘉峪关',
          'first_spell' => 'jyg',
          'all_spell' => 'jiayuguan',
        ),
        18 => 
        array (
          'id' => 148,
          'name' => '晋中',
          'first_spell' => 'jz',
          'all_spell' => 'jinzhong',
        ),
        19 => 
        array (
          'id' => 149,
          'name' => '焦作',
          'first_spell' => 'jz',
          'all_spell' => 'jiaozuo',
        ),
        20 => 
        array (
          'id' => 150,
          'name' => '锦州',
          'first_spell' => 'jz',
          'all_spell' => 'jinzhou',
        ),
        21 => 
        array (
          'id' => 151,
          'name' => '荆州',
          'first_spell' => 'jz',
          'all_spell' => 'jingzhou',
        ),
      ),
    ),
    10 => 
    array (
      'title' => 'K',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 152,
          'name' => '开封',
          'first_spell' => 'kf',
          'all_spell' => 'kaifeng',
        ),
        1 => 
        array (
          'id' => 153,
          'name' => '克拉玛依',
          'first_spell' => 'klmy',
          'all_spell' => 'kelamayi',
        ),
        2 => 
        array (
          'id' => 154,
          'name' => '昆明',
          'first_spell' => 'km',
          'all_spell' => 'kunming',
        ),
        3 => 
        array (
          'id' => 155,
          'name' => '喀什',
          'first_spell' => 'ks',
          'all_spell' => 'kashi',
        ),
        4 => 
        array (
          'id' => 156,
          'name' => '克州',
          'first_spell' => 'kz',
          'all_spell' => 'kezhou',
        ),
      ),
    ),
    11 => 
    array (
      'title' => 'L',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 157,
          'name' => '六安',
          'first_spell' => 'la',
          'all_spell' => 'liuan',
        ),
        1 => 
        array (
          'id' => 158,
          'name' => '来宾',
          'first_spell' => 'lb',
          'all_spell' => 'laibin',
        ),
        2 => 
        array (
          'id' => 159,
          'name' => '临沧',
          'first_spell' => 'lc',
          'all_spell' => 'lincang',
        ),
        3 => 
        array (
          'id' => 160,
          'name' => '聊城',
          'first_spell' => 'lc',
          'all_spell' => 'liaocheng',
        ),
        4 => 
        array (
          'id' => 161,
          'name' => '乐东',
          'first_spell' => 'ld',
          'all_spell' => 'ledong',
        ),
        5 => 
        array (
          'id' => 162,
          'name' => '娄底',
          'first_spell' => 'ld',
          'all_spell' => 'loudi',
        ),
        6 => 
        array (
          'id' => 163,
          'name' => '临汾',
          'first_spell' => 'lf',
          'all_spell' => 'linfen',
        ),
        7 => 
        array (
          'id' => 164,
          'name' => '廊坊',
          'first_spell' => 'lf',
          'all_spell' => 'langfang',
        ),
        8 => 
        array (
          'id' => 165,
          'name' => '临高',
          'first_spell' => 'lg',
          'all_spell' => 'lingao',
        ),
        9 => 
        array (
          'id' => 166,
          'name' => '丽江',
          'first_spell' => 'lj',
          'all_spell' => 'lijiang',
        ),
        10 => 
        array (
          'id' => 167,
          'name' => '吕梁',
          'first_spell' => 'll',
          'all_spell' => 'lvliang',
        ),
        11 => 
        array (
          'id' => 168,
          'name' => '陇南',
          'first_spell' => 'ln',
          'all_spell' => 'longnan',
        ),
        12 => 
        array (
          'id' => 169,
          'name' => '六盘水',
          'first_spell' => 'lps',
          'all_spell' => 'liupanshui',
        ),
        13 => 
        array (
          'id' => 170,
          'name' => '凉山',
          'first_spell' => 'ls',
          'all_spell' => 'liangshan',
        ),
        14 => 
        array (
          'id' => 171,
          'name' => '丽水',
          'first_spell' => 'ls',
          'all_spell' => 'lishui',
        ),
        15 => 
        array (
          'id' => 172,
          'name' => '拉萨',
          'first_spell' => 'ls',
          'all_spell' => 'lasa',
        ),
        16 => 
        array (
          'id' => 173,
          'name' => '乐山',
          'first_spell' => 'ls',
          'all_spell' => 'leshan',
        ),
        17 => 
        array (
          'id' => 174,
          'name' => '陵水',
          'first_spell' => 'ls',
          'all_spell' => 'lingshui',
        ),
        18 => 
        array (
          'id' => 175,
          'name' => '莱芜',
          'first_spell' => 'lw',
          'all_spell' => 'laiwu',
        ),
        19 => 
        array (
          'id' => 176,
          'name' => '临夏',
          'first_spell' => 'lx',
          'all_spell' => 'linxia',
        ),
        20 => 
        array (
          'id' => 177,
          'name' => '辽阳',
          'first_spell' => 'ly',
          'all_spell' => 'liaoyang',
        ),
        21 => 
        array (
          'id' => 178,
          'name' => '龙岩',
          'first_spell' => 'ly',
          'all_spell' => 'longyan',
        ),
        22 => 
        array (
          'id' => 179,
          'name' => '洛阳',
          'first_spell' => 'ly',
          'all_spell' => 'luoyang',
        ),
        23 => 
        array (
          'id' => 180,
          'name' => '临沂',
          'first_spell' => 'ly',
          'all_spell' => 'linyi',
        ),
        24 => 
        array (
          'id' => 181,
          'name' => '辽源',
          'first_spell' => 'ly',
          'all_spell' => 'liaoyuan',
        ),
        25 => 
        array (
          'id' => 182,
          'name' => '连云港',
          'first_spell' => 'lyg',
          'all_spell' => 'lianyungang',
        ),
        26 => 
        array (
          'id' => 183,
          'name' => '林芝',
          'first_spell' => 'lz',
          'all_spell' => 'linzhi',
        ),
        27 => 
        array (
          'id' => 184,
          'name' => '兰州',
          'first_spell' => 'lz',
          'all_spell' => 'lanzhou',
        ),
        28 => 
        array (
          'id' => 185,
          'name' => '柳州',
          'first_spell' => 'lz',
          'all_spell' => 'liuzhou',
        ),
      ),
    ),
    12 => 
    array (
      'title' => 'M',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 186,
          'name' => '马鞍山',
          'first_spell' => 'mas',
          'all_spell' => 'maanshan',
        ),
        1 => 
        array (
          'id' => 187,
          'name' => '牡丹江',
          'first_spell' => 'mdj',
          'all_spell' => 'mudanjiang',
        ),
        2 => 
        array (
          'id' => 188,
          'name' => '茂名',
          'first_spell' => 'mm',
          'all_spell' => 'maoming',
        ),
        3 => 
        array (
          'id' => 189,
          'name' => '眉山',
          'first_spell' => 'ms',
          'all_spell' => 'meishan',
        ),
        4 => 
        array (
          'id' => 190,
          'name' => '绵阳',
          'first_spell' => 'my',
          'all_spell' => 'mianyang',
        ),
        5 => 
        array (
          'id' => 191,
          'name' => '梅州',
          'first_spell' => 'mz',
          'all_spell' => 'meizhou',
        ),
      ),
    ),
    13 => 
    array (
      'title' => 'N',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 192,
          'name' => '宁波',
          'first_spell' => 'nb',
          'all_spell' => 'ningbo',
        ),
        1 => 
        array (
          'id' => 193,
          'name' => '南昌',
          'first_spell' => 'nc',
          'all_spell' => 'nanchang',
        ),
        2 => 
        array (
          'id' => 194,
          'name' => '南充',
          'first_spell' => 'nc',
          'all_spell' => 'nanchong',
        ),
        3 => 
        array (
          'id' => 195,
          'name' => '宁德',
          'first_spell' => 'nd',
          'all_spell' => 'ningde',
        ),
        4 => 
        array (
          'id' => 196,
          'name' => '南京',
          'first_spell' => 'nj',
          'all_spell' => 'nanjing',
        ),
        5 => 
        array (
          'id' => 197,
          'name' => '怒江',
          'first_spell' => 'nj',
          'all_spell' => 'nujiang',
        ),
        6 => 
        array (
          'id' => 198,
          'name' => '内江',
          'first_spell' => 'nj',
          'all_spell' => 'neijiang',
        ),
        7 => 
        array (
          'id' => 199,
          'name' => '南宁',
          'first_spell' => 'nn',
          'all_spell' => 'nanning',
        ),
        8 => 
        array (
          'id' => 200,
          'name' => '南平',
          'first_spell' => 'np',
          'all_spell' => 'nanping',
        ),
        9 => 
        array (
          'id' => 201,
          'name' => '那曲',
          'first_spell' => 'nq',
          'all_spell' => 'naqu',
        ),
        10 => 
        array (
          'id' => 202,
          'name' => '南沙',
          'first_spell' => 'ns',
          'all_spell' => 'nansha',
        ),
        11 => 
        array (
          'id' => 203,
          'name' => '南通',
          'first_spell' => 'nt',
          'all_spell' => 'nantong',
        ),
        12 => 
        array (
          'id' => 204,
          'name' => '南阳',
          'first_spell' => 'ny',
          'all_spell' => 'nanyang',
        ),
      ),
    ),
    14 => 
    array (
      'title' => 'P',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 205,
          'name' => '平顶山',
          'first_spell' => 'pds',
          'all_spell' => 'pingdingshan',
        ),
        1 => 
        array (
          'id' => 206,
          'name' => '普洱',
          'first_spell' => 'pe',
          'all_spell' => 'puer',
        ),
        2 => 
        array (
          'id' => 207,
          'name' => '盘锦',
          'first_spell' => 'pj',
          'all_spell' => 'panjin',
        ),
        3 => 
        array (
          'id' => 208,
          'name' => '平凉',
          'first_spell' => 'pl',
          'all_spell' => 'pingliang',
        ),
        4 => 
        array (
          'id' => 209,
          'name' => '莆田',
          'first_spell' => 'pt',
          'all_spell' => 'putian',
        ),
        5 => 
        array (
          'id' => 210,
          'name' => '萍乡',
          'first_spell' => 'px',
          'all_spell' => 'pingxiang',
        ),
        6 => 
        array (
          'id' => 211,
          'name' => '攀枝花',
          'first_spell' => 'pzh',
          'all_spell' => 'panzhihua',
        ),
      ),
    ),
    15 => 
    array (
      'title' => 'Q',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 212,
          'name' => '青岛',
          'first_spell' => 'qd',
          'all_spell' => 'qingdao',
        ),
        1 => 
        array (
          'id' => 213,
          'name' => '黔东南',
          'first_spell' => 'qdn',
          'all_spell' => 'qiandongnan',
        ),
        2 => 
        array (
          'id' => 214,
          'name' => '琼海',
          'first_spell' => 'qh',
          'all_spell' => 'qionghai',
        ),
        3 => 
        array (
          'id' => 215,
          'name' => '秦皇岛',
          'first_spell' => 'qhd',
          'all_spell' => 'qinhuangdao',
        ),
        4 => 
        array (
          'id' => 216,
          'name' => '曲靖',
          'first_spell' => 'qj',
          'all_spell' => 'qujing',
        ),
        5 => 
        array (
          'id' => 217,
          'name' => '潜江',
          'first_spell' => 'qj',
          'all_spell' => 'qianjiang',
        ),
        6 => 
        array (
          'id' => 218,
          'name' => '黔南',
          'first_spell' => 'qn',
          'all_spell' => 'qiannan',
        ),
        7 => 
        array (
          'id' => 219,
          'name' => '齐齐哈尔',
          'first_spell' => 'qqhe',
          'all_spell' => 'qiqihaer',
        ),
        8 => 
        array (
          'id' => 220,
          'name' => '七台河',
          'first_spell' => 'qth',
          'all_spell' => 'qitaihe',
        ),
        9 => 
        array (
          'id' => 221,
          'name' => '黔西南',
          'first_spell' => 'qxn',
          'all_spell' => 'qianxinan',
        ),
        10 => 
        array (
          'id' => 222,
          'name' => '清远',
          'first_spell' => 'qy',
          'all_spell' => 'qingyuan',
        ),
        11 => 
        array (
          'id' => 223,
          'name' => '庆阳',
          'first_spell' => 'qy',
          'all_spell' => 'qingyang',
        ),
        12 => 
        array (
          'id' => 224,
          'name' => '钦州',
          'first_spell' => 'qz',
          'all_spell' => 'qinzhou',
        ),
        13 => 
        array (
          'id' => 225,
          'name' => '泉州',
          'first_spell' => 'qz',
          'all_spell' => 'quanzhou',
        ),
        14 => 
        array (
          'id' => 226,
          'name' => '琼中',
          'first_spell' => 'qz',
          'all_spell' => 'qiongzhong',
        ),
      ),
    ),
    16 => 
    array (
      'title' => 'R',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 227,
          'name' => '日喀则',
          'first_spell' => 'rkz',
          'all_spell' => 'rikaze',
        ),
        1 => 
        array (
          'id' => 228,
          'name' => '日照',
          'first_spell' => 'rz',
          'all_spell' => 'rizhao',
        ),
      ),
    ),
    17 => 
    array (
      'title' => 'S',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 229,
          'name' => '深圳',
          'first_spell' => 'sz',
          'all_spell' => 'shenzhen',
        ),
        1 => 
        array (
          'id' => 230,
          'name' => '韶关',
          'first_spell' => 'sg',
          'all_spell' => 'shaoguan',
        ),
        2 => 
        array (
          'id' => 231,
          'name' => '绥化',
          'first_spell' => 'sh',
          'all_spell' => 'suihua',
        ),
        3 => 
        array (
          'id' => 232,
          'name' => '上海',
          'first_spell' => 'sh',
          'all_spell' => 'shanghai',
        ),
        4 => 
        array (
          'id' => 233,
          'name' => '石河子',
          'first_spell' => 'shz',
          'all_spell' => 'shihezi',
        ),
        5 => 
        array (
          'id' => 234,
          'name' => '石家庄',
          'first_spell' => 'sjz',
          'all_spell' => 'shijiazhuang',
        ),
        6 => 
        array (
          'id' => 235,
          'name' => '商洛',
          'first_spell' => 'sl',
          'all_spell' => 'shangluo',
        ),
        7 => 
        array (
          'id' => 236,
          'name' => '三明',
          'first_spell' => 'sm',
          'all_spell' => 'sanming',
        ),
        8 => 
        array (
          'id' => 237,
          'name' => '三门峡',
          'first_spell' => 'smx',
          'all_spell' => 'sanmenxia',
        ),
        9 => 
        array (
          'id' => 238,
          'name' => '山南',
          'first_spell' => 'sn',
          'all_spell' => 'shannan',
        ),
        10 => 
        array (
          'id' => 239,
          'name' => '遂宁',
          'first_spell' => 'sn',
          'all_spell' => 'suining',
        ),
        11 => 
        array (
          'id' => 240,
          'name' => '神农架',
          'first_spell' => 'snj',
          'all_spell' => 'shennongjia',
        ),
        12 => 
        array (
          'id' => 241,
          'name' => '四平',
          'first_spell' => 'sp',
          'all_spell' => 'siping',
        ),
        13 => 
        array (
          'id' => 242,
          'name' => '商丘',
          'first_spell' => 'sq',
          'all_spell' => 'shangqiu',
        ),
        14 => 
        array (
          'id' => 243,
          'name' => '宿迁',
          'first_spell' => 'sq',
          'all_spell' => 'suqian',
        ),
        15 => 
        array (
          'id' => 244,
          'name' => '上饶',
          'first_spell' => 'sr',
          'all_spell' => 'shangrao',
        ),
        16 => 
        array (
          'id' => 245,
          'name' => '汕头',
          'first_spell' => 'st',
          'all_spell' => 'shantou',
        ),
        17 => 
        array (
          'id' => 246,
          'name' => '汕尾',
          'first_spell' => 'sw',
          'all_spell' => 'shanwei',
        ),
        18 => 
        array (
          'id' => 247,
          'name' => '绍兴',
          'first_spell' => 'sx',
          'all_spell' => 'shaoxing',
        ),
        19 => 
        array (
          'id' => 248,
          'name' => '邵阳',
          'first_spell' => 'sy',
          'all_spell' => 'shaoyang',
        ),
        20 => 
        array (
          'id' => 249,
          'name' => '十堰',
          'first_spell' => 'sy',
          'all_spell' => 'shiyan',
        ),
        21 => 
        array (
          'id' => 250,
          'name' => '沈阳',
          'first_spell' => 'sy',
          'all_spell' => 'shenyang',
        ),
        22 => 
        array (
          'id' => 251,
          'name' => '三亚',
          'first_spell' => 'sy',
          'all_spell' => 'sanya',
        ),
        23 => 
        array (
          'id' => 252,
          'name' => '松原',
          'first_spell' => 'sy',
          'all_spell' => 'songyuan',
        ),
        24 => 
        array (
          'id' => 253,
          'name' => '双鸭山',
          'first_spell' => 'sys',
          'all_spell' => 'shuangyashan',
        ),
        25 => 
        array (
          'id' => 254,
          'name' => '随州',
          'first_spell' => 'sz',
          'all_spell' => 'suizhou',
        ),
        26 => 
        array (
          'id' => 255,
          'name' => '朔州',
          'first_spell' => 'sz',
          'all_spell' => 'shuozhou',
        ),
        27 => 
        array (
          'id' => 256,
          'name' => '苏州',
          'first_spell' => 'sz',
          'all_spell' => 'suzhou',
        ),
        28 => 
        array (
          'id' => 257,
          'name' => '宿州',
          'first_spell' => 'sz',
          'all_spell' => 'suzhou',
        ),
        29 => 
        array (
          'id' => 258,
          'name' => '石嘴山',
          'first_spell' => 'szs',
          'all_spell' => 'shizuishan',
        ),
      ),
    ),
    18 => 
    array (
      'title' => 'T',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 259,
          'name' => '泰安',
          'first_spell' => 'ta',
          'all_spell' => 'taian',
        ),
        1 => 
        array (
          'id' => 260,
          'name' => '台北',
          'first_spell' => 'tb',
          'all_spell' => 'taibei',
        ),
        2 => 
        array (
          'id' => 261,
          'name' => '屯昌',
          'first_spell' => 'tc',
          'all_spell' => 'tunchang',
        ),
        3 => 
        array (
          'id' => 262,
          'name' => '塔城',
          'first_spell' => 'tc',
          'all_spell' => 'tacheng',
        ),
        4 => 
        array (
          'id' => 263,
          'name' => '铜川',
          'first_spell' => 'tc',
          'all_spell' => 'tongchuan',
        ),
        5 => 
        array (
          'id' => 264,
          'name' => '通化',
          'first_spell' => 'th',
          'all_spell' => 'tonghua',
        ),
        6 => 
        array (
          'id' => 265,
          'name' => '天津',
          'first_spell' => 'tj',
          'all_spell' => 'tianjin',
        ),
        7 => 
        array (
          'id' => 266,
          'name' => '通辽',
          'first_spell' => 'tl',
          'all_spell' => 'tongliao',
        ),
        8 => 
        array (
          'id' => 267,
          'name' => '铁岭',
          'first_spell' => 'tl',
          'all_spell' => 'tieling',
        ),
        9 => 
        array (
          'id' => 268,
          'name' => '铜陵',
          'first_spell' => 'tl',
          'all_spell' => 'tongling',
        ),
        10 => 
        array (
          'id' => 269,
          'name' => '吐鲁番',
          'first_spell' => 'tlf',
          'all_spell' => 'tulufan',
        ),
        11 => 
        array (
          'id' => 270,
          'name' => '天门',
          'first_spell' => 'tm',
          'all_spell' => 'tianmen',
        ),
        12 => 
        array (
          'id' => 271,
          'name' => '铜仁',
          'first_spell' => 'tr',
          'all_spell' => 'tongren',
        ),
        13 => 
        array (
          'id' => 272,
          'name' => '天水',
          'first_spell' => 'ts',
          'all_spell' => 'tianshui',
        ),
        14 => 
        array (
          'id' => 273,
          'name' => '唐山',
          'first_spell' => 'ts',
          'all_spell' => 'tangshan',
        ),
        15 => 
        array (
          'id' => 274,
          'name' => '太原',
          'first_spell' => 'ty',
          'all_spell' => 'taiyuan',
        ),
        16 => 
        array (
          'id' => 275,
          'name' => '台州',
          'first_spell' => 'tz',
          'all_spell' => 'taizhou',
        ),
        17 => 
        array (
          'id' => 276,
          'name' => '泰州',
          'first_spell' => 'tz',
          'all_spell' => 'taizhou',
        ),
        18 => 
        array (
          'id' => 277,
          'name' => '台中',
          'first_spell' => 'tz',
          'all_spell' => 'taizhong',
        ),
      ),
    ),
    19 => 
    array (
      'title' => 'W',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 278,
          'name' => '文昌',
          'first_spell' => 'wc',
          'all_spell' => 'wenchang',
        ),
        1 => 
        array (
          'id' => 279,
          'name' => '潍坊',
          'first_spell' => 'wf',
          'all_spell' => 'weifang',
        ),
        2 => 
        array (
          'id' => 280,
          'name' => '武汉',
          'first_spell' => 'wh',
          'all_spell' => 'wuhan',
        ),
        3 => 
        array (
          'id' => 281,
          'name' => '乌海',
          'first_spell' => 'wh',
          'all_spell' => 'wuhai',
        ),
        4 => 
        array (
          'id' => 282,
          'name' => '芜湖',
          'first_spell' => 'wh',
          'all_spell' => 'wuhu',
        ),
        5 => 
        array (
          'id' => 283,
          'name' => '威海',
          'first_spell' => 'wh',
          'all_spell' => 'weihai',
        ),
        6 => 
        array (
          'id' => 284,
          'name' => '乌兰察布',
          'first_spell' => 'wlcb',
          'all_spell' => 'wulanchabu',
        ),
        7 => 
        array (
          'id' => 285,
          'name' => '乌鲁木齐',
          'first_spell' => 'wlmq',
          'all_spell' => 'wulumuqi',
        ),
        8 => 
        array (
          'id' => 286,
          'name' => '万宁',
          'first_spell' => 'wn',
          'all_spell' => 'wanning',
        ),
        9 => 
        array (
          'id' => 287,
          'name' => '渭南',
          'first_spell' => 'wn',
          'all_spell' => 'weinan',
        ),
        10 => 
        array (
          'id' => 288,
          'name' => '文山',
          'first_spell' => 'ws',
          'all_spell' => 'wenshan',
        ),
        11 => 
        array (
          'id' => 289,
          'name' => '武威',
          'first_spell' => 'ww',
          'all_spell' => 'wuwei',
        ),
        12 => 
        array (
          'id' => 290,
          'name' => '无锡',
          'first_spell' => 'wx',
          'all_spell' => 'wuxi',
        ),
        13 => 
        array (
          'id' => 291,
          'name' => '梧州',
          'first_spell' => 'wz',
          'all_spell' => 'wuzhou',
        ),
        14 => 
        array (
          'id' => 292,
          'name' => '吴忠',
          'first_spell' => 'wz',
          'all_spell' => 'wuzhong',
        ),
        15 => 
        array (
          'id' => 293,
          'name' => '温州',
          'first_spell' => 'wz',
          'all_spell' => 'wenzhou',
        ),
        16 => 
        array (
          'id' => 294,
          'name' => '五指山',
          'first_spell' => 'wzs',
          'all_spell' => 'wuzhishan',
        ),
      ),
    ),
    20 => 
    array (
      'title' => 'X',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 295,
          'name' => '西安',
          'first_spell' => 'xa',
          'all_spell' => 'xian',
        ),
        1 => 
        array (
          'id' => 296,
          'name' => '兴安盟',
          'first_spell' => 'xam',
          'all_spell' => 'xinganmeng',
        ),
        2 => 
        array (
          'id' => 297,
          'name' => '许昌',
          'first_spell' => 'xc',
          'all_spell' => 'xuchang',
        ),
        3 => 
        array (
          'id' => 298,
          'name' => '宣城',
          'first_spell' => 'xc',
          'all_spell' => 'xuancheng',
        ),
        4 => 
        array (
          'id' => 299,
          'name' => '孝感',
          'first_spell' => 'xg',
          'all_spell' => 'xiaogan',
        ),
        5 => 
        array (
          'id' => 300,
          'name' => '香港',
          'first_spell' => 'xg',
          'all_spell' => 'xianggang',
        ),
        6 => 
        array (
          'id' => 301,
          'name' => '锡林郭勒',
          'first_spell' => 'xlgl',
          'all_spell' => 'xilinguole',
        ),
        7 => 
        array (
          'id' => 302,
          'name' => '厦门',
          'first_spell' => 'xm',
          'all_spell' => 'xiamen',
        ),
        8 => 
        array (
          'id' => 303,
          'name' => '西宁',
          'first_spell' => 'xn',
          'all_spell' => 'xining',
        ),
        9 => 
        array (
          'id' => 304,
          'name' => '咸宁',
          'first_spell' => 'xn',
          'all_spell' => 'xianning',
        ),
        10 => 
        array (
          'id' => 305,
          'name' => '西沙',
          'first_spell' => 'xs',
          'all_spell' => 'xisha',
        ),
        11 => 
        array (
          'id' => 306,
          'name' => '西双版纳',
          'first_spell' => 'xsbn',
          'all_spell' => 'xishuangbanna',
        ),
        12 => 
        array (
          'id' => 307,
          'name' => '仙桃',
          'first_spell' => 'xt',
          'all_spell' => 'xiantao',
        ),
        13 => 
        array (
          'id' => 308,
          'name' => '湘潭',
          'first_spell' => 'xt',
          'all_spell' => 'xiangtan',
        ),
        14 => 
        array (
          'id' => 309,
          'name' => '邢台',
          'first_spell' => 'xt',
          'all_spell' => 'xingtai',
        ),
        15 => 
        array (
          'id' => 310,
          'name' => '新乡',
          'first_spell' => 'xx',
          'all_spell' => 'xinxiang',
        ),
        16 => 
        array (
          'id' => 311,
          'name' => '湘西',
          'first_spell' => 'xx',
          'all_spell' => 'xiangxi',
        ),
        17 => 
        array (
          'id' => 312,
          'name' => '新余',
          'first_spell' => 'xy',
          'all_spell' => 'xinyu',
        ),
        18 => 
        array (
          'id' => 313,
          'name' => '咸阳',
          'first_spell' => 'xy',
          'all_spell' => 'xianyang',
        ),
        19 => 
        array (
          'id' => 314,
          'name' => '信阳',
          'first_spell' => 'xy',
          'all_spell' => 'xinyang',
        ),
        20 => 
        array (
          'id' => 315,
          'name' => '襄阳',
          'first_spell' => 'xy',
          'all_spell' => 'xiangyang',
        ),
        21 => 
        array (
          'id' => 316,
          'name' => '徐州',
          'first_spell' => 'xz',
          'all_spell' => 'xuzhou',
        ),
        22 => 
        array (
          'id' => 317,
          'name' => '忻州',
          'first_spell' => 'xz',
          'all_spell' => 'xinzhou',
        ),
      ),
    ),
    21 => 
    array (
      'title' => 'Y',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 318,
          'name' => '濮阳',
          'first_spell' => 'y',
          'all_spell' => 'yang',
        ),
        1 => 
        array (
          'id' => 319,
          'name' => '雅安',
          'first_spell' => 'ya',
          'all_spell' => 'yaan',
        ),
        2 => 
        array (
          'id' => 320,
          'name' => '延安',
          'first_spell' => 'ya',
          'all_spell' => 'yanan',
        ),
        3 => 
        array (
          'id' => 321,
          'name' => '宜宾',
          'first_spell' => 'yb',
          'all_spell' => 'yibin',
        ),
        4 => 
        array (
          'id' => 322,
          'name' => '延边',
          'first_spell' => 'yb',
          'all_spell' => 'yanbian',
        ),
        5 => 
        array (
          'id' => 323,
          'name' => '伊春',
          'first_spell' => 'yc',
          'all_spell' => 'yichun',
        ),
        6 => 
        array (
          'id' => 324,
          'name' => '银川',
          'first_spell' => 'yc',
          'all_spell' => 'yinchuan',
        ),
        7 => 
        array (
          'id' => 325,
          'name' => '盐城',
          'first_spell' => 'yc',
          'all_spell' => 'yancheng',
        ),
        8 => 
        array (
          'id' => 326,
          'name' => '宜春',
          'first_spell' => 'yc',
          'all_spell' => 'yichun',
        ),
        9 => 
        array (
          'id' => 327,
          'name' => '运城',
          'first_spell' => 'yc',
          'all_spell' => 'yuncheng',
        ),
        10 => 
        array (
          'id' => 328,
          'name' => '宜昌',
          'first_spell' => 'yc',
          'all_spell' => 'yichang',
        ),
        11 => 
        array (
          'id' => 329,
          'name' => '云浮',
          'first_spell' => 'yf',
          'all_spell' => 'yunfu',
        ),
        12 => 
        array (
          'id' => 330,
          'name' => '阳江',
          'first_spell' => 'yj',
          'all_spell' => 'yangjiang',
        ),
        13 => 
        array (
          'id' => 331,
          'name' => '营口',
          'first_spell' => 'yk',
          'all_spell' => 'yingkou',
        ),
        14 => 
        array (
          'id' => 332,
          'name' => '伊犁',
          'first_spell' => 'yl',
          'all_spell' => 'yili',
        ),
        15 => 
        array (
          'id' => 333,
          'name' => '榆林',
          'first_spell' => 'yl',
          'all_spell' => 'yulin',
        ),
        16 => 
        array (
          'id' => 334,
          'name' => '杨凌',
          'first_spell' => 'yl',
          'all_spell' => 'yangling',
        ),
        17 => 
        array (
          'id' => 335,
          'name' => '玉林',
          'first_spell' => 'yl',
          'all_spell' => 'yulin',
        ),
        18 => 
        array (
          'id' => 336,
          'name' => '阳泉',
          'first_spell' => 'yq',
          'all_spell' => 'yangquan',
        ),
        19 => 
        array (
          'id' => 337,
          'name' => '玉树',
          'first_spell' => 'ys',
          'all_spell' => 'yushu',
        ),
        20 => 
        array (
          'id' => 338,
          'name' => '鹰潭',
          'first_spell' => 'yt',
          'all_spell' => 'yingtan',
        ),
        21 => 
        array (
          'id' => 339,
          'name' => '烟台',
          'first_spell' => 'yt',
          'all_spell' => 'yantai',
        ),
        22 => 
        array (
          'id' => 340,
          'name' => '玉溪',
          'first_spell' => 'yx',
          'all_spell' => 'yuxi',
        ),
        23 => 
        array (
          'id' => 341,
          'name' => '益阳',
          'first_spell' => 'yy',
          'all_spell' => 'yiyang',
        ),
        24 => 
        array (
          'id' => 342,
          'name' => '岳阳',
          'first_spell' => 'yy',
          'all_spell' => 'yueyang',
        ),
        25 => 
        array (
          'id' => 343,
          'name' => '扬州',
          'first_spell' => 'yz',
          'all_spell' => 'yangzhou',
        ),
        26 => 
        array (
          'id' => 344,
          'name' => '永州',
          'first_spell' => 'yz',
          'all_spell' => 'yongzhou',
        ),
      ),
    ),
    22 => 
    array (
      'title' => 'Z',
      'citys' => 
      array (
        0 => 
        array (
          'id' => 345,
          'name' => '泸州',
          'first_spell' => 'z',
          'all_spell' => 'zhou',
        ),
        1 => 
        array (
          'id' => 346,
          'name' => '衢州',
          'first_spell' => 'z',
          'all_spell' => 'zhou',
        ),
        2 => 
        array (
          'id' => 347,
          'name' => '儋州',
          'first_spell' => 'z',
          'all_spell' => 'zhou',
        ),
        3 => 
        array (
          'id' => 348,
          'name' => '亳州',
          'first_spell' => 'z',
          'all_spell' => 'zhou',
        ),
        4 => 
        array (
          'id' => 349,
          'name' => '淄博',
          'first_spell' => 'zb',
          'all_spell' => 'zibo',
        ),
        5 => 
        array (
          'id' => 350,
          'name' => '自贡',
          'first_spell' => 'zg',
          'all_spell' => 'zigong',
        ),
        6 => 
        array (
          'id' => 351,
          'name' => '珠海',
          'first_spell' => 'zh',
          'all_spell' => 'zhuhai',
        ),
        7 => 
        array (
          'id' => 352,
          'name' => '镇江',
          'first_spell' => 'zj',
          'all_spell' => 'zhenjiang',
        ),
        8 => 
        array (
          'id' => 353,
          'name' => '湛江',
          'first_spell' => 'zj',
          'all_spell' => 'zhanjiang',
        ),
        9 => 
        array (
          'id' => 354,
          'name' => '张家界',
          'first_spell' => 'zjj',
          'all_spell' => 'zhangjiajie',
        ),
        10 => 
        array (
          'id' => 355,
          'name' => '张家口',
          'first_spell' => 'zjk',
          'all_spell' => 'zhangjiakou',
        ),
        11 => 
        array (
          'id' => 356,
          'name' => '周口',
          'first_spell' => 'zk',
          'all_spell' => 'zhoukou',
        ),
        12 => 
        array (
          'id' => 357,
          'name' => '驻马店',
          'first_spell' => 'zmd',
          'all_spell' => 'zhumadian',
        ),
        13 => 
        array (
          'id' => 358,
          'name' => '重庆',
          'first_spell' => 'zq',
          'all_spell' => 'zhongqing',
        ),
        14 => 
        array (
          'id' => 359,
          'name' => '肇庆',
          'first_spell' => 'zq',
          'all_spell' => 'zhaoqing',
        ),
        15 => 
        array (
          'id' => 360,
          'name' => '舟山',
          'first_spell' => 'zs',
          'all_spell' => 'zhoushan',
        ),
        16 => 
        array (
          'id' => 361,
          'name' => '中山',
          'first_spell' => 'zs',
          'all_spell' => 'zhongshan',
        ),
        17 => 
        array (
          'id' => 362,
          'name' => '昭通',
          'first_spell' => 'zt',
          'all_spell' => 'zhaotong',
        ),
        18 => 
        array (
          'id' => 363,
          'name' => '中卫',
          'first_spell' => 'zw',
          'all_spell' => 'zhongwei',
        ),
        19 => 
        array (
          'id' => 364,
          'name' => '遵义',
          'first_spell' => 'zy',
          'all_spell' => 'zunyi',
        ),
        20 => 
        array (
          'id' => 365,
          'name' => '张掖',
          'first_spell' => 'zy',
          'all_spell' => 'zhangye',
        ),
        21 => 
        array (
          'id' => 366,
          'name' => '资阳',
          'first_spell' => 'zy',
          'all_spell' => 'ziyang',
        ),
        22 => 
        array (
          'id' => 367,
          'name' => '株洲',
          'first_spell' => 'zz',
          'all_spell' => 'zhuzhou',
        ),
        23 => 
        array (
          'id' => 368,
          'name' => '郑州',
          'first_spell' => 'zz',
          'all_spell' => 'zhengzhou',
        ),
        24 => 
        array (
          'id' => 369,
          'name' => '枣庄',
          'first_spell' => 'zz',
          'all_spell' => 'zaozhuang',
        ),
        25 => 
        array (
          'id' => 370,
          'name' => '漳州',
          'first_spell' => 'zz',
          'all_spell' => 'zhangzhou',
        ),
      ),
    ),
  ),
)
;