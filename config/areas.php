<?php 
return array (
  'version' => '2016-03-02 12:04:05',
  'content' => 
  array (
    '上海' => 
    array (
      0 => 
      array (
        'id' => 17,
        'name' => '闵行',
        'city' => '上海',
      ),
      1 => 
      array (
        'id' => 18,
        'name' => '宝山',
        'city' => '上海',
      ),
      2 => 
      array (
        'id' => 19,
        'name' => '嘉定',
        'city' => '上海',
      ),
      3 => 
      array (
        'id' => 20,
        'name' => '南汇',
        'city' => '上海',
      ),
      4 => 
      array (
        'id' => 21,
        'name' => '金山',
        'city' => '上海',
      ),
      5 => 
      array (
        'id' => 22,
        'name' => '青浦',
        'city' => '上海',
      ),
      6 => 
      array (
        'id' => 23,
        'name' => '松江',
        'city' => '上海',
      ),
      7 => 
      array (
        'id' => 24,
        'name' => '奉贤',
        'city' => '上海',
      ),
      8 => 
      array (
        'id' => 25,
        'name' => '崇明',
        'city' => '上海',
      ),
      9 => 
      array (
        'id' => 26,
        'name' => '徐家汇',
        'city' => '上海',
      ),
      10 => 
      array (
        'id' => 27,
        'name' => '浦东',
        'city' => '上海',
      ),
    ),
  ),
)
;