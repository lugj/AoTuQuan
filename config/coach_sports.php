<?php 
return array (
  'version' => '2016-04-09 09:09:56',
  'content' => 
  array (
    0 => 
    array (
      'title' => '热门',
      'sports' => 
      array (
        0 => 
        array (
          'id' => 1,
          'name' => '跑步',
          'type' => 'coach',
        ),
        1 => 
        array (
          'id' => 2,
          'name' => '健身',
          'type' => 'coach',
        ),
        2 => 
        array (
          'id' => 3,
          'name' => '瑜伽',
          'type' => 'coach',
        ),
        3 => 
        array (
          'id' => 4,
          'name' => '舞蹈',
          'type' => 'coach',
        ),
      ),
    ),
    1 => 
    array (
      'title' => '球类',
      'sports' => 
      array (
        0 => 
        array (
          'id' => 5,
          'name' => '足球',
          'type' => 'coach',
        ),
        1 => 
        array (
          'id' => 6,
          'name' => '篮球',
          'type' => 'coach',
        ),
        2 => 
        array (
          'id' => 7,
          'name' => '羽毛球',
          'type' => 'coach',
        ),
        3 => 
        array (
          'id' => 8,
          'name' => '乒乓球',
          'type' => 'coach',
        ),
        4 => 
        array (
          'id' => 9,
          'name' => '网球',
          'type' => 'coach',
        ),
        5 => 
        array (
          'id' => 10,
          'name' => '台球',
          'type' => 'coach',
        ),
        6 => 
        array (
          'id' => 11,
          'name' => '排球',
          'type' => 'coach',
        ),
        7 => 
        array (
          'id' => 12,
          'name' => '棒球',
          'type' => 'coach',
        ),
        8 => 
        array (
          'id' => 13,
          'name' => '保龄球',
          'type' => 'coach',
        ),
        9 => 
        array (
          'id' => 14,
          'name' => '高尔夫',
          'type' => 'coach',
        ),
      ),
    ),
    2 => 
    array (
      'title' => '搏击',
      'sports' => 
      array (
        0 => 
        array (
          'id' => 15,
          'name' => '散打',
          'type' => 'coach',
        ),
        1 => 
        array (
          'id' => 16,
          'name' => '自由搏击',
          'type' => 'coach',
        ),
        2 => 
        array (
          'id' => 17,
          'name' => '跆拳道',
          'type' => 'coach',
        ),
        3 => 
        array (
          'id' => 18,
          'name' => '空手道',
          'type' => 'coach',
        ),
        4 => 
        array (
          'id' => 19,
          'name' => '击剑',
          'type' => 'coach',
        ),
      ),
    ),
    3 => 
    array (
      'title' => '户外',
      'sports' => 
      array (
        0 => 
        array (
          'id' => 20,
          'name' => '自行车',
          'type' => 'coach',
        ),
        1 => 
        array (
          'id' => 21,
          'name' => '登山',
          'type' => 'coach',
        ),
        2 => 
        array (
          'id' => 22,
          'name' => '游泳',
          'type' => 'coach',
        ),
        3 => 
        array (
          'id' => 23,
          'name' => '溜冰',
          'type' => 'coach',
        ),
        4 => 
        array (
          'id' => 24,
          'name' => '滑雪',
          'type' => 'coach',
        ),
      ),
    ),
    4 => 
    array (
      'title' => '射击',
      'sports' => 
      array (
        0 => 
        array (
          'id' => 25,
          'name' => '射击',
          'type' => 'coach',
        ),
        1 => 
        array (
          'id' => 26,
          'name' => '射箭',
          'type' => 'coach',
        ),
      ),
    ),
  ),
)
;