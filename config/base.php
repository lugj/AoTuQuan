<?php

return [
    //分销 金额分配百分比配置
    'level1' => 30, //一级分销获得百分比
    'level2' => 20, //二级分销获得百分比
    'level3' => 10, //三级分销获得百分比

    //金币与人民币兑换比例配置
    'exchange_prop'=>100,

    //订单过期
    'order_cancel_time'=>120,
    //一天未付款自动打款
    'order_pay_plus_time'=>86400,

];
