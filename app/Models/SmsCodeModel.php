<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;


/**
 * App\Models\SmsCodeModel
 *
 * @property integer $code_id
 * @property integer $user_id 用户id
 * @property string $code 短信验证码
 * @property string $code_type 验证码类型，1 注册验证码 2 找回密码验证码
 * @property string $send_time 发送时间
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SmsCodeModel whereCodeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SmsCodeModel whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SmsCodeModel whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SmsCodeModel whereCodeType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SmsCodeModel whereSendTime($value)
 * @property string $tel 手机号
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SmsCodeModel whereTel($value)
 */
class SmsCodeModel extends Model
{
  	protected $table = 'at_sms_code';
	public $timestamps = false;
	public $primaryKey = 'code_id';
}
