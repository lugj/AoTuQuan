<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * App\Models\AppModel
 */
class AppModel extends Model{
	protected $guarded = [];

	protected function after_insert($query,$modified_cols){

	}
	
	protected function after_update($query,$modified_cols){
		
	}	

	protected function after_delete($query,$delete_id){

	}
}