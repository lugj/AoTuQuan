<?php
namespace App\Models;
use App\Http\Components\RedisComponent;
use App\Models\AppModel;
use DB;
/**
 * App\Models\UserSettingModel
 */
class UserGiftModel extends AppModel{
  	protected $table='at_user_gift';
	public $timestamps = false;
	public $primaryKey = 'id';

	public function after_insert($query,$modified_cols){
	}
	public function after_update($query,$modified_cols){
	}

}