<?php
namespace App\Models;
use App\Http\Components\RedisComponent;
use App\Models\AppModel;
use DB;

/**
 * App\Models\UserModel
 */
class UserModel extends AppModel{
  	protected $table='at_user';
	public $timestamps = false;
	public $primaryKey = 'user_id';

}