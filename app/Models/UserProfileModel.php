<?php
namespace App\Models;
use App\Http\Components\RedisComponent;
use App\library\PinYin;
use App\Models\AppModel;
use DB;

use App\library\UtilFunction;
/**
 * App\Models\UserInfoModel
 *
 * @property integer $id 		主键ID
 * @property integer $user_id 	用户表主键ID
 * @property varchar $real_name 短信推送开关
 * @property varchar $nick_name 用户昵称
 * @property varchar $qq 		预约推送开关
 * @property varchar $city 		预约推送开关
 * @property varchar $avatar 	预约推送开关
 * @property text 	 $motto 	预约推送开关
 * @property timestamp $created_at 填写个人信息时间
 * @property timestamp $updated_at 修改时间
 */
class UserProfileModel extends AppModel{
	
  	protected $table='at_user_profile';
	public $timestamps = false;
	public $primaryKey = 'user_id';

	static public function get_profile($user_id){
		$profile = self::firstOrNew(['user_id'=>$user_id]);
		//$profile->birth = UtilFunction::date_age($profile->birth);
		return $profile;
	}
	//添加数据
//	public function after_insert($query,$modified_cols){
//		if (!$query->nick_name && $query->phone) {
//			DB::table('user_profile')->where('user_id',$query->user_id)->update(['nick_name'=>$query->phone]);
//		}
//		if ($query->nick_name) {
//			$Pinyin = new PinYin();
//			$first = $Pinyin->getFirstPY($query->nick_name);
//			DB::table('user_profile')->where('user_id',$query->user_id)->update(['nick_name_first'=>strtoupper(substr($first,0,1))]);
//		}
//	}
	//更新数据
//	public function after_update($query,$modified_cols){
//		if (isset($modified_cols['nick_name'])) {
//			$Pinyin = new PinYin();
//			$first = $Pinyin->getFirstPY($query->nick_name);
//			DB::table('user_profile')->where('user_id',$query->user_id)->update(['nick_name_first'=>strtoupper(substr($first,0,1))]);
//			$corps_update = CorpsModel::where('user_id',$query->user_id)->update(['captain_name' => $query->nick_name]);
//		}
//		elseif (isset($modified_cols['birth'])) {
//			$age = UtilFunction::date_age($query->birth);
//			DB::table('user_profile')->where('user_id',$query->user_id)->update(['age'=>$age]);
//		}
//	}
}