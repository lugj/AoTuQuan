<?php
namespace App\Models;
use App\Models\AppModel;
use DB;
/**
 * App\Models\UserInfoModel
 *
 * @property integer $id 		主键ID
 * @property integer $user_id 	用户表主键ID
 * @property varchar $real_name 短信推送开关
 * @property varchar $nick_name 用户昵称
 * @property varchar $qq 		预约推送开关
 * @property varchar $city 		预约推送开关
 * @property varchar $avatar 	预约推送开关
 * @property text 	 $motto 	预约推送开关
 * @property timestamp $created_at 填写个人信息时间
 * @property timestamp $updated_at 修改时间
 */
class BannersModel extends AppModel{
	
  	protected $table='at_banners';
	public $timestamps = false;
	public $primaryKey = 'id';
}