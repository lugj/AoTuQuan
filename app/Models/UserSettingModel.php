<?php
namespace App\Models;
use App\Http\Components\RedisComponent;
use App\Models\AppModel;
use DB;
/**
 * App\Models\UserSettingModel
 */
class UserSettingModel extends AppModel{
  	protected $table='at_user_setting';
	public $timestamps = false;
	public $primaryKey = 'user_id';

	public function after_insert($query,$modified_cols){
	}
	public function after_update($query,$modified_cols){
	}

}