<?php
namespace App\Models;
use App\Models\AppModel;
use DB;
/**
 * App\Models\DataConfigModel
 */
class ConfigModel extends AppModel{
  	protected $table='at_config';
	public $timestamps = false;
	public $primaryKey = 'id';


	//获取对应的config值
	public function get_value($key){
		$data = ConfigModel::select(['value'])->where("key",$key)->first();
		if($data){
			$data = $data->toArray();
			return $data['value'];
		}else{
			return false;
		}
	}
}