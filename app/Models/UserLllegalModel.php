<?php
namespace App\Models;
use App\Models\AppModel;
use DB;
/**
 * App\Models\UserLllegalModel
 */
class UserLllegalModel extends AppModel{
  	protected $table='user_lllegal';
	public $timestamps = false;
	public $primaryKey = 'id';
}