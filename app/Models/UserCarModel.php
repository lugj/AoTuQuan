<?php
namespace App\Models;
use DB;
/**
 * App\Models\UserIdInfoModel
 */
class UserCarModel extends AppModel{
	
  	protected $table='at_user_car';
	public $timestamps = false;
	public $primaryKey = 'user_id';
}