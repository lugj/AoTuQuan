<?php
namespace App\Models;
use App\Http\Components\RedisComponent;
use App\Models\AppModel;
use DB;
/**
 * App\Models\UserContactListModel
 */
class UserContactListModel extends AppModel{
  	protected $table='user_contact_list';
	public $timestamps = false;
	public $primaryKey = 'user_id';

	public function after_insert($query,$modified_cols){
	}
	public function after_update($query,$modified_cols){
	}

}