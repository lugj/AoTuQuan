<?php
namespace App\Models;
use DB;
/**
 * App\Models\UserIdInfoModel
 */
class UserVideoModel extends AppModel{
	
  	protected $table='at_user_video';
	public $timestamps = false;
	public $primaryKey = 'user_id';
}