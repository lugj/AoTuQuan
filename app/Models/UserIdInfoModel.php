<?php
namespace App\Models;
use DB;
/**
 * App\Models\UserIdInfoModel
 */
class UserIdInfoModel extends AppModel{
	
  	protected $table='at_user_id_info';
	public $timestamps = false;
	public $primaryKey = 'user_id';
}