<?php
/*
|--------------------------------------------------------------------------
| 前台路由设置
|--------------------------------------------------------------------------
*/
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE");

$app->get('/', function () {
    return '此为极客api接口';
});

$app->group(['namespace' => 'App\Http\Controllers\Page','prefix'=>'api/page/','middleware' => 'App\Http\Middleware\ApiSign'], function ($app) {
    // 首页
    $app->post('home',   ['uses'=>'HomeController@version_invoke'      ]);

    $app->post('corps_home',   ['uses'=>'CorpsHomeController@version_invoke'      ]);

    $app->post('weather',['uses'=>'WeatherController@version_invoke'      ]);

    $app->post('publish_list',['uses'=>'PublishListController@version_invoke'      ]);

    $app->post('feedback',['uses'=>'FeedbackController@version_invoke'      ]);
    $app->post('feedback_info',['uses'=>'FeedbackInfoController@version_invoke'      ]);
    $app->post('upload_feedback',['uses'=>'UploadFeedbackController@version_invoke'      ]);
});
/*凹凸圈*/
$app->group(['namespace' => 'App\Http\Controllers\Circle','prefix'=>'api/circle/','middleware' => 'App\Http\Middleware\ApiSign'], function ($app) {
    $app->post('delete',   ['uses'=>'DeleteController@version_invoke'      ]);
    $app->post('send',   ['uses'=>'SendController@version_invoke'      ]);
    $app->post('index',   ['uses'=>'IndexController@version_invoke'      ]);
    $app->post('flower',   ['uses'=>'FlowerController@version_invoke'      ]);
    $app->post('message',   ['uses'=>'MessageController@version_invoke'      ]);
});
/*兼职*/
$app->group(['namespace' => 'App\Http\Controllers\Job','prefix'=>'api/job/','middleware' => 'App\Http\Middleware\ApiSign'], function ($app) {
    $app->post('/order_list/{function}',     ['uses'=>'OrderListController@version_invoke'    ]);
    $app->post('/sub_order/{function}',     ['uses'=>'SubOrderController@version_invoke'    ]);
    $app->post('/place_order/{function}',     ['uses'=>'PlaceOrderController@version_invoke'    ]);
    $app->post('/send/{function}',     ['uses'=>'SendController@version_invoke'    ]);
    $app->post('/detail/{function}',     ['uses'=>'DetailController@version_invoke'    ]);
    $app->post('/user/{function}',     ['uses'=>'UserController@version_invoke'    ]);
    $app->post('/video_chat/{function}',     ['uses'=>'VideoChatController@version_invoke'    ]);
    $app->post('/gift/{function}',     ['uses'=>'GiftController@version_invoke'    ]);
    $app->post('/recommend/{function}',     ['uses'=>'RecommendController@version_invoke'    ]);
    $app->post('/order_operate/{function}',     ['uses'=>'OrderOperateController@version_invoke'    ]);
    $app->post('evaluate', ['uses' => 'EvaluateController@version_invoke']);
    $app->post('order_detail', ['uses' => 'OrderDetailController@version_invoke']);
});
/*首页*/
$app->group(['namespace' => 'App\Http\Controllers\Home','prefix'=>'api/home/','middleware' => 'App\Http\Middleware\ApiSign'], function ($app) {
    $app->post('job_list', ['uses' => 'JobListController@version_invoke']);
    $app->post('index', ['uses' => 'IndexController@version_invoke']);
    $app->post('recommend', ['uses' => 'RecommendController@version_invoke']);
    $app->post('video_chat', ['uses' => 'VideoChatController@version_invoke']);
});
$app->group(['namespace' => 'App\Http\Controllers\Friend','prefix'=>'api/friend/','middleware' => 'App\Http\Middleware\ApiSign'], function ($app) {
    /*好友列表*/
    $app->post('friend_list', ['uses' => 'FriendListController@version_invoke']);
    /*好友申请列表*/
    $app->post('new_friend_list', ['uses' => 'NewFriendListController@version_invoke']);
    /*搜索*/
    $app->post('search', ['uses' => 'SearchController@version_invoke']);
    /*申请*/
    $app->post('apply', ['uses' => 'ApplyController@version_invoke']);
    /*同意好友*/
    $app->post('agree', ['uses' => 'AgreeController@version_invoke']);
    /*删除好友*/
    $app->post('delete', ['uses' => 'DeleteController@version_invoke']);
    /*拉黑*/
    $app->post('black', ['uses' => 'BlackController@version_invoke']);
});
$app->group(['namespace' => 'App\Http\Controllers\User','prefix'=>'api/user/','middleware' => 'App\Http\Middleware\ApiSign'], function ($app) {
    /*我是代理商*/
    $app->post('/agent/{function}',     ['uses'=>'AgentController@version_invoke'    ]);
    /*我是保安员*/
    $app->post('/safety/{function}',     ['uses'=>'SafetyController@version_invoke'    ]);
    /*黑名单列表*/
    $app->post('/account/{function}',     ['uses'=>'AccountController@version_invoke'    ]);
    $app->post('gift',     ['uses'=>'GiftController@version_invoke'    ]);
    /*推广赚钱*/
    $app->post('invite',     ['uses'=>'InviteController@version_invoke'    ]);
    /*黑名单列表*/
    $app->post('/black_list/{function}',     ['uses'=>'BlackListController@version_invoke'    ]);
    /*意见反馈*/
    $app->post('suggest',     ['uses'=>'SuggestController@version_invoke'    ]);
    /*文章*/
    $app->post('/article/{function}',     ['uses'=>'ArticleController@version_invoke'    ]);
    /*充值*/
    $app->post('/recharge/{function}',     ['uses'=>'RechargeController@version_invoke'    ]);
    /*会员*/
    $app->post('/card/{function}',     ['uses'=>'CardController@version_invoke'    ]);
    /*上传视频*/
    $app->post('upload_video',     ['uses'=>'UploadVideoController@version_invoke'    ]);
    /*收藏*/
    $app->post('/auth_deposit/{function}',     ['uses'=>'AuthDepositController@version_invoke'    ]);
    /*收藏*/
    $app->post('/collection/{function}',     ['uses'=>'CollectionController@version_invoke'    ]);
    /*我看过谁 谁看过我*/
    $app->post('visit',     ['uses'=>'VisitController@version_invoke'    ]);
    /*车辆信息查看*/
    $app->post('auth_car_show',     ['uses'=>'AuthCarShowController@version_invoke'    ]);
    /*相册*/
    $app->post('/album/{function}',     ['uses'=>'AlbumController@version_invoke'    ]);
    /*查看微信号*/
    $app->post('/wechat/{function}',     ['uses'=>'WechatController@version_invoke'    ]);
    /*微信号设置*/
    $app->post('set_wechat',     ['uses'=>'SetWechatController@version_invoke'    ]);
    /*设置忽扰*/
    $app->post('disturbed_setting',     ['uses'=>'DisturbedSettingController@version_invoke'    ]);
    /*我的*/
    $app->post('home',     ['uses'=>'HomeController@version_invoke'    ]);
    /*视频认证*/
    $app->post('auth_video',     ['uses'=>'AuthVideoController@version_invoke'    ]);
    /*车辆认证*/
    $app->post('auth_car',     ['uses'=>'AuthCarController@version_invoke'    ]);
    /*身份证认证*/
    $app->post('auth_id_info',     ['uses'=>'AuthIdInfoController@version_invoke'    ]);
    // 认证列表
    $app->post('auth_list',     ['uses'=>'AuthListController@version_invoke'    ]);
    // 注册
    $app->post('/register/{function}',          ['uses'=>'RegisterController@version_invoke'        ]);
    // 上传图片
    $app->post('upload_picture',     ['uses'=>'UploadPictureController@version_invoke'    ]);
    // 上传头像
    $app->post('upload_avatar',     ['uses'=>'UploadAvatarController@version_invoke'    ]);
    // 登录
    $app->post('login',             ['uses'=>'LoginController@version_invoke'           ]);
    // 第三方授权登陆
    $app->post('login_auth',        ['uses'=>'LoginAuthController@version_invoke'       ]);
    // 退出登录
    $app->post('logout',            ['uses'=>'LogoutController@version_invoke'          ]);
    // 找回密码
    $app->post('forget',            ['uses'=>'ForgetController@version_invoke'          ]);
    // 修改密码
    $app->post('password_modify',   ['uses'=>'PasswordModifyController@version_invoke'  ]);
    // 获取用户基本信息
    $app->post('base_info',         ['uses'=>'BaseInfoController@version_invoke'        ]);
    // 获取用户详细信息
    $app->post('profile',           ['uses'=>'ProfileController@version_invoke'         ]);
    // 修改用户详细信息
    $app->post('profile_modify',    ['uses'=>'ProfileModifyController@version_invoke'   ]);

    $app->post('profile_check',    ['uses'=>'ProfileCheckController@version_invoke'   ]);

    $app->post('phone_modify',    ['uses'=>'PhoneModifyController@version_invoke'   ]);
    // 获取短信验证码
    $app->post('sms_check_code',    ['uses'=>'SmsCheckCodeController@version_invoke'    ]);
    // 获取用户设置信息
    $app->post('setting',           ['uses'=>'SettingController@version_invoke'         ]);
    // 修改用户设置信息
    $app->post('setting_modify',    ['uses'=>'SettingModifyController@version_invoke'   ]);

    // 用户实名审核
    $app->post('identify_user',     ['uses'=>'IdentifyUserController@version_invoke'    ]);
    // 上传地理位置
    $app->post('upload_location',   ['uses'=>'UploadLocationController@version_invoke'  ]);    
    // 上传职能证件照
    $app->post('upload_certification_photo', ['uses'=>'UploadCertificationPhotoController@version_invoke']);
    // 职能认证
    $app->post('certification_auth', ['uses'=>'CertificationAuthController@version_invoke'    ]);
    // 职能列表
    $app->post('certification_list', ['uses'=>'CertificationListController@version_invoke'    ]);
    // 用户主页
    $app->post('user_info', ['uses'=>'UserInfoController@version_invoke'    ]);
    // 校验用户登录状态
    $app->post('verify_login', ['uses'=>'VerifyLoginController@version_invoke'    ]);

    $app->post('verify_version', ['uses'=>'VerifyVersionController@version_invoke'    ]);
    // 完善资料
    $app->post('improve_info', ['uses'=>'ImproveInfoController@version_invoke'    ]);
    // 通过手机号查询用户
    $app->post('phone_check', ['uses'=>'PhoneCheckController@version_invoke'    ]);

    $app->post('mission', ['uses'=>'MissionController@version_invoke'    ]);

    $app->post('mission_share', ['uses'=>'MissionShareController@version_invoke'    ]);

    $app->post('mission_sign', ['uses'=>'MissionSignController@version_invoke'    ]);
    //验证完善资料情况
    $app->post('verify_user_info', ['uses'=>'VerifyUserInfoController@version_invoke'    ]);
    //验证手机号
    $app->post('check_phone', ['uses'=>'CheckPhoneController@version_invoke'    ]);
    //更换手机号
    $app->post('change_phone', ['uses'=>'ChangePhoneController@version_invoke'    ]);
    //认证信息
    $app->post('certification_info', ['uses'=>'CertificationInfoController@version_invoke'    ]);
    //获取头像及昵称
    $app->post('get_avatar_name', ['uses'=>'GetAvatarNameController@version_invoke'    ]);
    //收徒赚钱
    $app->post('apprentice', ['uses'=>'ApprenticeController@version_invoke'    ]);
    //收徒赚钱
    $app->post('register_success', ['uses'=>'RegisterSuccessController@version_invoke'    ]);
});

$app->group(['namespace' => 'App\Http\Controllers\Account','prefix'=>'api/account/','middleware' => 'App\Http\Middleware\ApiSign'], function ($app) {
    // 获取账户余额
    $app->post('balance',        ['uses'=>'BalanceController@version_invoke'            ]);
    // 获取账户绑定账号列表
    $app->post('card_list',      ['uses'=>'CardListController@version_invoke'           ]);
    // 绑定账户账号
    $app->post('card_bind',      ['uses'=>'CardBindController@version_invoke'           ]);
    // 解绑
    $app->post('card_unbind',    ['uses'=>'CardUnbindController@version_invoke'         ]);
    // 设置默认
    $app->post('card_set_default',    ['uses'=>'CardSetDefaultController@version_invoke']);
    // 提现申请
    $app->post('withdrawal',     ['uses'=>'WithdrawalController@version_invoke'         ]);

    // 获取账户账单
    $app->post('bill_list',      ['uses'=>'BillListController@version_invoke'           ]);
    // 获取账户账单详细
    $app->post('bill_detail',    ['uses'=>'BillDetailController@version_invoke'         ]);
    // 退款申请
    $app->post('refund',         ['uses'=>'RefundController@version_invoke'             ]);
    
    // 支付密码设置
    $app->post('pay_password_modify',  ['uses'=>'PayPasswordModifyController@version_invoke']);
    $app->post('pay_check_code',  ['uses'=>'PayCheckCodeController@version_invoke']);
    // 充值
    $app->post('recharge',        ['uses'=>'RechargeController@version_invoke']);
    // 通过订单号支付(暂不需要)
    // $app->post('pay',          ['uses'=>'PayController@version_invoke']);
    // 转账
    $app->post('transfer',        ['uses'=>'TransferController@version_invoke']);

    $app->post('withdrawal_info', ['uses'=>'WithdrawalInfoController@version_invoke']);

    $app->post('corps_balance', ['uses'=>'CorpsBalanceController@version_invoke']);

    $app->post('corps_recharge', ['uses'=>'CorpsRechargeController@version_invoke']);

    $app->post('corps_bill_list', ['uses'=>'CorpsBillListController@version_invoke']);

    $app->post('corps_withdrawal', ['uses'=>'CorpsWithdrawalController@version_invoke']);

    //收入列表
    $app->post('income_list', ['uses'=>'IncomeListController@version_invoke']);
    //支出列表
    $app->post('spend_list', ['uses'=>'SpendListController@version_invoke']);
    //任务收支明细
    $app->post('task_bill_list', ['uses'=>'TaskBillListController@version_invoke']);

});
/*不签名的提示*/
$app->group(['namespace' => 'App\Http\Controllers\Account','prefix'=>'api/account/'], function ($app) {
    // 支付回调，更新订单状态
    $app->post('notify',          ['uses'=>'NotifyController@version_invoke']);

});

$app->group(['namespace' => 'App\Http\Controllers\Corps','prefix'=>'api/corps/','middleware' => 'App\Http\Middleware\ApiSign'], function ($app) {
    $app->post('prepare',            ['uses'=>'PrepareController@version_invoke']);

    $app->post('create',            ['uses'=>'CreateController@version_invoke']);

    $app->post('quit',              ['uses'=>'QuitController@version_invoke']);

    $app->post('my_corps_list',     ['uses'=>'MyCorpsListController@version_invoke']);

    $app->post('detail',            ['uses'=>'DetailController@version_invoke']);

    $app->post('detail_modify',     ['uses'=>'DetailModifyController@version_invoke']);

    $app->post('leader_list',       ['uses'=>'LeaderListController@version_invoke']);

    $app->post('set_leader',        ['uses'=>'SetLeaderController@version_invoke']);

    $app->post('member_list',       ['uses'=>'MemberListController@version_invoke']);

    $app->post('member_add',        ['uses'=>'MemberAddController@version_invoke']);

    $app->post('team_list',         ['uses'=>'TeamListController@version_invoke']);

    $app->post('team_request',      ['uses'=>'TeamRequestController@version_invoke']);

    $app->post('team_request_list', ['uses'=>'TeamRequestListController@version_invoke']);

    $app->post('team_request_allow',['uses'=>'TeamRequestAllowController@version_invoke']);

    $app->map(['get', 'post','options','put','delete'],'upload_badge',    ['uses'=>'UploadBadgeController@version_invoke' ]);
    $app->map(['get', 'post','options','put','delete'],'upload_album',    ['uses'=>'UploadAlbumController@version_invoke' ]);
    $app->post('delete_album',    ['uses'=>'DeleteAlbumController@version_invoke' ]);

    $app->post('mission', ['uses'=>'MissionController@version_invoke'    ]);
});


/*基础数据*/
$app->group(['namespace' => 'App\Http\Controllers\Data','prefix'=>'api/data/','middleware' => 'App\Http\Middleware\ApiSign'], function ($app) {
    $app->post('citys',         ['uses'=>'CitysController@version_invoke']);
    $app->post('sports',        ['uses'=>'SportsController@version_invoke']);
    $app->post('areas',         ['uses'=>'AreasController@version_invoke']);
    // 获取用户所在城市的行政区划
    $app->post('get_city_area',        ['uses'=>'GetCityAreaController@version_invoke']);
    $app->post('match_system',         ['uses'=>'MatchSystemController@version_invoke']);
    $app->post('sport_postion',         ['uses'=>'SportPostionController@version_invoke']);
    //免责申明
    $app->post('disclaimer',         ['uses'=>'DisclaimerController@version_invoke']);
    //获取配置信息
    $app->post('get_config',         ['uses'=>'GetConfigController@version_invoke']);
    //消单惩罚规则
    $app->post('cancel_rule',         ['uses'=>'CancelRuleController@version_invoke']);
    //奖励细则
    $app->post('bonus_rule',         ['uses'=>'BonusRuleController@version_invoke']);
    //分享信息
    $app->post('get_share',         ['uses'=>'GetShareController@version_invoke']);
    
});
/*动动圈*/
$app->group(['namespace' => 'App\Http\Controllers\Moments','prefix'=>'api/moments/','middleware' => 'App\Http\Middleware\ApiSign'],function($app){
    /*动动圈*/
    // 上传动动圈图片
    $app->post('upload_moments_photo',        ['uses'=>'UploadMomentsPhotoController@version_invoke']);
    // 提交动动圈
    $app->post('save_moments',        ['uses'=>'SaveMomentsController@version_invoke']);
    // 评论动动圈
    $app->post('comment_moments',        ['uses'=>'CommentMomentsController@version_invoke']);
    // 动动圈列表
    $app->post('moments_list',        ['uses'=>'MomentsListController@version_invoke']);
    // 动动圈详情
    $app->post('moments_detail',        ['uses'=>'MomentsDetailController@version_invoke']);
    // 动动圈点赞
    $app->post('praise_moments',        ['uses'=>'PraiseMomentsController@version_invoke']);
    // 我的发布
    $app->post('my_moments',        ['uses'=>'MyMomentsController@version_invoke']);
    // 动动圈详情-评论
    $app->post('get_moments_comments',        ['uses'=>'GetMomentsCommentsController@version_invoke']);
    // 分享
    $app->post('share',        ['uses'=>'ShareController@version_invoke']);
    // 收藏
    $app->post('collect',        ['uses'=>'CollectController@version_invoke']);
    // 我的收藏
    $app->post('my_collect',        ['uses'=>'MyCollectController@version_invoke']);
    // 我的动动圈
    $app->post('my_moments_list',        ['uses'=>'MyMomentsListController@version_invoke']);
    // 举报
    $app->post('report',        ['uses'=>'ReportController@version_invoke']);
    // 删除评论
    $app->post('delete_moments_comments',        ['uses'=>'DeleteMomentsCommentsController@version_invoke']);
    // 删除动动圈
    $app->post('delete_moments',        ['uses'=>'DeleteMomentsController@version_invoke']);
});
$app->group(['namespace' => 'App\Http\Controllers\Friend','prefix'=>'api/friend/','middleware' => 'App\Http\Middleware\ApiSign'],function($app){
    /*通讯录*/
    // 好友列表
    $app->post('friend_list',       ['uses'=>'FriendListController@version_invoke']);
    // 关注好友列表
    $app->post('follow_friend_list',       ['uses'=>'FollowFriendListController@version_invoke']);
    // 关注战队列表
    $app->post('follow_corps_list',       ['uses'=>'FollowCorpsListController@version_invoke']);
    // 关注/取消关注
    $app->post('follow',       ['uses'=>'FollowController@version_invoke']);
    // 接受/拒绝 好友请求/战队邀请
    $app->post('add_friend',       ['uses'=>'AddFriendController@version_invoke']);
    // 好友请求/战队邀请列表
    $app->post('friend_request_list',       ['uses'=>'FriendRequestListController@version_invoke']);
    // 搜索好友/战队
    $app->post('search_user',       ['uses'=>'SearchUserController@version_invoke']);
    // 请求好友/战队
    $app->post('request_user',       ['uses'=>'RequestUserController@version_invoke']);
    // 通过ID搜索好友/战队
    $app->post('search_user_by_id',       ['uses'=>'SearchUserByIdController@version_invoke']);
    // 手机联系人
    $app->post('user_contact_list',       ['uses'=>'UserContactListController@version_invoke']);
    // 创建群聊
    $app->post('create_group',       ['uses'=>'CreateGroupController@version_invoke']);
    // 管理群聊
    $app->post('manage_group',       ['uses'=>'ManageGroupController@version_invoke']);
    // 群聊添加好友列表
    $app->post('group_add_friend_list',       ['uses'=>'GroupAddFriendListController@version_invoke']);
    // 群聊添加成员
    $app->post('join_group',       ['uses'=>'JoinGroupController@version_invoke']);
    // 退出群聊
    $app->post('quit_group',       ['uses'=>'QuitGroupController@version_invoke']);
    // 移除群聊成员
    $app->post('delete_group_member',       ['uses'=>'DeleteGroupMemberController@version_invoke']);
    // 修改群名称
    $app->post('modify_group_name',       ['uses'=>'ModifyGroupNameController@version_invoke']);

    $app->post('upload_group_avatar',       ['uses'=>'UploadGroupAvatarController@version_invoke']);
    // 获取群聊信息
    $app->post('get_group_info',       ['uses'=>'GetGroupInfoController@version_invoke']);
});


/*信息接口*/
$app->group(['namespace' => 'App\Http\Controllers\Message','prefix'=>'api/message/','middleware' => 'App\Http\Middleware\ApiSign'],function($app){
    $app->post('system_publish_list',        ['uses'=>'SystemPublishListController@version_invoke']);
    $app->post('system_publish',        ['uses'=>'SystemPublishController@version_invoke']);
    $app->post('info',        ['uses'=>'InfoController@version_invoke']);
    //删除接口
    $app->post('delete_system_publish',        ['uses'=>'DeleteSystemPublishController@version_invoke']);
    //消息变更已读
    $app->post('change_status',        ['uses'=>'ChangeStatusController@version_invoke']);
    //获取消息已读未读状态及第一条通知消息
    $app->post('get_base',        ['uses'=>'GetBaseController@version_invoke']);
    // 推送

    $app->post('system_push',        ['uses'=>'SystemPushController@version_invoke']);
    //检测是否可以聊天
    $app->post('check',        ['uses'=>'CheckController@version_invoke']);
    //在线用户
    $app->post('online',        ['uses'=>'OnlineController@version_invoke']);
});


/*服务接口*/
$app->group(['namespace' => 'App\Http\Controllers\Service','prefix'=>'api/service/','middleware' => 'App\Http\Middleware\ApiSign'],function($app){
    // 上传服务封面照
    $app->post('upload_service_pictures',     ['uses'=>'UploadServicePicturesController@version_invoke'    ]);
    //保存服务信息
    $app->post('save_service',     ['uses'=>'SaveServiceController@version_invoke'    ]);
    //获取服务信息
    $app->post('service_info',     ['uses'=>'ServiceInfoController@version_invoke'    ]);
    //获取个人主页
    $app->post('home_page',     ['uses'=>'HomePageController@version_invoke'    ]);
    //获取评价列表
    $app->post('evaluate_list',     ['uses'=>'EvaluateListController@version_invoke'    ]);
    //上传举报图片
    $app->post('upload_report_pictures',     ['uses'=>'UploadReportPicturesController@version_invoke'    ]);
    //提交举报信息
    $app->post('report',     ['uses'=>'ReportController@version_invoke'    ]);
    //上报地理位置 获取周边用户
    $app->post('find',     ['uses'=>'FindController@version_invoke'    ]);
    //验证是否可以聊天
    $app->post('check_chat', ['uses'=>'CheckChatController@version_invoke'    ]);
    //禁用服务信息查询
    $app->post('lllegal', ['uses'=>'LllegalController@version_invoke'    ]);
});

/*订单接口*/
$app->group(['namespace' => 'App\Http\Controllers\Myorder','prefix'=>'api/myorder/','middleware' => 'App\Http\Middleware\ApiSign'],function($app){
    // 我的订单
    $app->post('order_list',     ['uses'=>'OrderListController@version_invoke'    ]);
    //提交订单
    $app->post('save_order',     ['uses'=>'SaveOrderController@version_invoke'    ]);
    //确认订单
    $app->post('sure_order',     ['uses'=>'SureOrderController@version_invoke'    ]);
    //取消、拒绝订单
    $app->post('cancel_order',     ['uses'=>'CancelOrderController@version_invoke'    ]);
    //接单
    $app->post('receive_order',     ['uses'=>'ReceiveOrderController@version_invoke'    ]);
    //获取各个状态下的订单数量
    $app->post('status_num',     ['uses'=>'StatusNumController@version_invoke'    ]);
    //扫描见面二维码
    $app->post('scan_meet_code',     ['uses'=>'ScanMeetCodeController@version_invoke'    ]);
    //扫描收款二维码
    $app->post('scan_cash_code',     ['uses'=>'ScanCashCodeController@version_invoke'    ]);
    //提交评价
    $app->post('evaluate',     ['uses'=>'EvaluateController@version_invoke'    ]);
    //取消原因
    $app->post('cancel_reason',     ['uses'=>'CancelReasonController@version_invoke'    ]);
});

/*后台互通接口*/
$app->group(['prefix'=>'api/admin/'],function($app){
    // 更新
    $app->map(['get', 'post'],'update_citys',  ['uses'=>'App\Http\Controllers\Data\CitysController@update_config']);
    $app->map(['get', 'post'],'update_corps_sports',  ['uses'=>'App\Http\Controllers\Data\SportsController@update_corps_config']);
    $app->map(['get', 'post'],'update_coach_sports',  ['uses'=>'App\Http\Controllers\Data\SportsController@update_coach_config']);

    $app->map(['get', 'post'],'update_areas',  ['uses'=>'App\Http\Controllers\Data\AreasController@update_config']);

    $app->map(['get', 'post'],'update_match_system',  ['uses'=>'App\Http\Controllers\Data\MatchSystemController@update_config']);
    $app->map(['get', 'post'],'update_sport_postion',  ['uses'=>'App\Http\Controllers\Data\SportPostionController@update_config']);
    $app->map(['get', 'post'],'update_corps_lv_exp',  ['uses'=>'App\Http\Controllers\Data\CorpsLvExpController@update_config']);
    $app->map(['get', 'post'],'update_corps_lv_member',  ['uses'=>'App\Http\Controllers\Data\CorpsLvMemberController@update_config']);

    $app->map(['get', 'post'],'update_lv_exp',  ['uses'=>'App\Http\Controllers\Data\LvExpController@update_config']);

    $app->map(['get', 'post'],'update_mission_rule',  ['uses'=>'App\Http\Controllers\Data\MissionRuleController@update_config']);
    $app->map(['get', 'post'],'back_push',  ['uses'=>'App\Http\Controllers\Message\SystemPublishController@back_push']);
    $app->map(['get', 'post'],'back_add_secretary',  ['uses'=>'App\Http\Controllers\Corps\AddSecretaryController@back_add_secretary']);

});

/*
|--------------------------------------------------------------------------
| 测试控制器（TestController）的路由
|--------------------------------------------------------------------------
*/
$app->group(['namespace' => 'App\Http\Controllers\Test','prefix'=>'test'], function($app){
    
    $app->get('/', 'TestController@index');

    $app->get('/{function}', 'TestController@invoke');
});
$app->group(['namespace' => 'App\Http\Controllers\Crontab','prefix'=>'crontab'], function($app){

    $app->get('/cron_service/{function}', 'CronServiceController@version_invoke');
});

/*Html5页面*/
$app->group(['namespace' => 'App\Http\Controllers\H5','prefix'=>'h5'],function($app){
    //免责申明
    $app->get('/disclaimer',        'DisclaimerController@index');
    //用户分享页
    $app->get('/shareUser',        'ShareUserController@index');
    //奖励说明
    $app->get('/bonusRule',        'BonusRuleController@index');
    //违规页面
    $app->get('/lllegal/{function}',        'LllegalController@invoke');
    //用户使用及隐私协议
    $app->get('/view/{function}',        'ViewController@invoke');
    //要请收徒
    $app->get('/apprentice/{function}',        'ApprenticeController@invoke');

});

$app->get('/emptydir', 'PreController@emptydir');
$app->get('/test_function/{function}','TestFunctionController@invoke');