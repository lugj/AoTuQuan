<?php 
namespace App\Http\Components;

use App\Http\Components\AppComponent;

use Illuminate\Http\Request;

use App\library\UtilFunction;
use DB;
use App\Models\CreditCorpsModel;
use App\Models\CreditRuleModel;
use App\Models\CorpsModel;
/**
* 
*/
class CreditComponent extends AppComponent{
	
	public function __construct(){
        parent::__construct();
	}

    // 消费
    public static function consume ($user_id,$corps_id){

    }

    // 战队消费
    public static function corps_consume ($corps_id,$credit_price){
        $corps = CorpsModel::where('corps_id',$corps_id)->first();
        $credit_balance = $corps->credit;
        if ($credit_balance >= $credit_price) {
            $new_credit = new CreditCorpsModel;
            $new_credit->corps_id = $corps_id;
            $new_credit->credit_price = $credit_price * -1;
            if ($new_credit->save()) {
                $corps->credit = $credit_balance - $credit_price;
                $corps->save();
            }
        }else{
            SELF::_json('0','战队积分不足，需要战队积分'.$credit_price);
        }
    }

    // 
    public static function common_pay ($user_id,$corps_id,$amount,$subject,$body = ''){
    }
}
