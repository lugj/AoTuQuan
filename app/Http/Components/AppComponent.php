<?php 
	
namespace App\Http\Components;

use Laravel\Lumen\Routing\Controller as BaseController;

class AppComponent extends BaseController{

	public function __construct(){
	}

	protected static function _json($status, $message='', $data = array(), $other = array(), $status_code = 200){
        // return new JsonResponse([ 'status' => $status, 'message' => $message ], $status_code);
        /*status判断，只能返回1或0字符串*/
        if (is_array($status) && isset($status['status'])) {
            $return_arr = $status;
        }else{
            if (!$status) {
                $status = '0';
            }
            /*message判断，每次都会有message标签*/
            if ($message) {
                $message = trim($message);
            }else{
                $message = '';
            }

            $base_arr = compact('status','message','data');
            $now = microtime(true);
            $base_arr['expire'] = round($now - STARTTIME,3);
            /*data获取不是数组或没有值返回空值*/
            if (!$data) {
                unset($base_arr['data']);
            }
            if (is_array($other) && !empty($other)) {
                $return_arr = array_merge($base_arr,$other);
            }else{
                $return_arr = $base_arr;
            }
        }
        
        array_walk_recursive($return_arr, function (&$value,$key){
	        if (!is_array($value)) {
	            if (strlen($value)) {
	                $value = "$value";
	            }else{
	                $value = '';
	            }
	        }
	    });
        
        $return_json = json_encode($return_arr);
        echo $return_json;
        exit;
    }

    protected function json($status, $message='', $data = array(), $other = array(), $status_code = 200){
        return $this->_json($status, $message, $data, $other, $status_code);
    }
}
