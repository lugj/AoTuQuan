<?php 
namespace App\Http\Components;

use App\Http\Components\AppComponent;
use App\Models\UserLllegalModel;
use Illuminate\Http\Request;
use App\library\UtilFunction;
use DB;
/**
* 
*/
class MessageComponent extends AppComponent{

    private static  $message_tpl = array(
        //卖家拒绝订单满3次
        'seller_refuse_order_limit'=>array(
            'type'=>1,              //等于1  单发 等于2  群发
            'message'=>'限制提供服务通知',      //推送标题
            'desc'=>'您已经拒绝订单满3次，暂时无法提供服务', //推送描述
            'link_type'=>'none',    //none 无法点击进去  href 链接  order 订单
        ),
        //买家取消订单满3次
        'buyer_cancel_order_limit'=>array(
            'type'=>1,
            'message'=>'限制购买服务通知',
            'desc'=>'您已经取消订单3次，暂时无法购买服务',
            'link_type'=>'limit',
        ),
        //卖家取消订单满3次
        'seller_cancel_order_limit'=>array(
            'type'=>1,
            'message'=>'限制提供服务通知',
            'desc'=>'您已经取消订单3次，暂时无法提供服务',
            'link_type'=>'limit'
        ),
        //订单购买成功-给卖家发
        'buyer_order'=>array(
            'type'=>1,
            'message'=>'订单动态',
            'desc'=>'订单付款成功，buyer_name刚刚成功预购了您的服务',
            'link_type'=>'order',
        ),
        //卖家接单-给买家发
        'seller_receive_order'=>array(
            'type'=>1,
            'message'=>'订单动态',
            'desc'=>'seller_name已经接单，订单号:order_id',
            'link_type'=>'order',
        ),
        //卖方未付款状态下 拒绝订单-给买家发
        'seller_refuse_order'=>array(
            'type'=>1,
            'message'=>'订单动态',
            'desc'=>'seller_name已拒绝您的订单，订单号:order_id',
            'link_type'=>'order',
        ),
        //卖方已付款状态下 取消订单-给买方发
        'seller_cancel_order'=>array(
            'type'=>1,
            'message'=>'订单动态',
            'desc'=>'seller_name已取消您的订单，订单号:order_id',
            'link_type'=>'order',
        ),

        //买方已付款/未付款状态下 取消订单-给卖方发
        'buyer_cancel_order'=>array(
            'type'=>1,
            'message'=>'订单动态',
            'desc'=>'buyer_name已取消您的订单，订单号:order_id',
            'link_type'=>'order',
        ),
        //买方扫描见面二维码-给卖方发
        'buyer_scan_meet'=>array(
            'type'=>1,
            'message'=>'订单动态',
            'desc'=>'buyer_name扫描了您的见面二维码，订单号:order_id',
            'link_type'=>'order',
        ),
        //买方扫描收款码-给卖方发
        'buyer_scan_cash'=>array(
            'type'=>1,
            'message'=>'订单动态',
            'desc'=>'buyer_name扫描了您的收款二维码，订单号:order_id',
            'link_type'=>'order',
        ),
        //买方评价订单-给卖方发
        'buyer_evaluate_order'=>array(
            'type'=>1,
            'message'=>'订单动态',
            'desc'=>'buyer_name评价了订单，订单号:order_id',
            'link_type'=>'order',
        ),
        //卖方评价订单-给买方发
        'seller_evaluate_order'=>array(
            'type'=>1,
            'message'=>'订单动态',
            'desc'=>'seller_name评价了您的订单，订单号:order_id',
            'link_type'=>'order',
        ),
        //用户注册
        'register'=>array(
            'type'=>1,
            'message'=>'注册成功通知',
            'desc'=>'去我的“管理服务”完善信息，并开启服务后，异性会员即可预约您的服务',
            'link_type'=>'none'
        ),
           //投诉成功
        'report_success'=>array(
            'type'=>1,
            'message'=>'投诉受理',
            'desc'=>'您的投诉已被受理，平台将认真审查',
            'link_type'=>'none'
        )

    );
	
	public function __construct(){
        parent::__construct();
	}

    /**
     * 统一推送入口
     * limit_time 限制服务时间 单位秒
     */
    public static function unite_push($tpl_model,$data,$limit_time = 0){
        $message_tpl = SELF::$message_tpl[$tpl_model];
        foreach($data AS $index => $item){
            $message_tpl['desc'] = str_replace($index,$item,$message_tpl['desc']);
        }
        $push_data = array_merge($data,$message_tpl);
        //判断是否为限制服务推送，插入限制服务推送信息
        if($tpl_model == 'buyer_cancel_order_limit'){  //买家已接单状态下取消订单
            $user_lllegal = new UserLllegalModel();
            $user_lllegal->user_id = $data['user_id'];
            $user_lllegal->content = "您已经在卖家已接单状态下取消订单满3次。";
            $user_lllegal->type = "取消订单满3次";
            $user_lllegal->result = ($limit_time/60/60)."小时内无法购买服务功能";
            $user_lllegal->open_time = date("Y-m-d H:i:s",time()+$limit_time);
            $user_lllegal->saveOrFail();
            $lllegal_id = $user_lllegal->id;
        }elseif($tpl_model == 'seller_cancel_order_limit'){ //卖家取消订单
            $user_lllegal = new UserLllegalModel();
            $user_lllegal->user_id = $data['user_id'];
            $user_lllegal->content = "您已经在已接单状态下取消订单满3次。";
            $user_lllegal->type = "取消订单满3次";
            $user_lllegal->result = ($limit_time/60/60)."小时内无法出售服务功能";
            $user_lllegal->open_time = date("Y-m-d H:i:s",time()+$limit_time);
            $user_lllegal->saveOrFail();
            $lllegal_id = $user_lllegal->id;
        }
        if(isset($lllegal_id)){
            $push_data['l_id'] = $lllegal_id;
        }

        SELF::get_push_url($push_data);


    }



    /**
     * 调用推送
     */
    public static function get_push_url($push_data = array()){
        app()->configure('ppurl');
        $url = config('ppurl.img_url')."/api/message/system_push";

        $app_token =  config('constants.app_token');
        $random_str = time();
        $sign = md5(md5(md5($app_token.$random_str)));

        $apptype = "android";
        $appversion = config('constants.version');
        $data = array(
            'sign'=>$sign,
            'random_str'=>$random_str,
            'apptype'=>$apptype,
            'appversion'=>$appversion,
        );
        $data = array_merge($data,$push_data);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // post数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // post的变量
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

}
