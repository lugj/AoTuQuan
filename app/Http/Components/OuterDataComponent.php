<?php 
namespace App\Http\Components;

use App\Http\Components\AppComponent;
use App\library\juhe\Weather;
use App\library\UtilRegular;

/**
* 
*/
class OuterDataComponent extends AppComponent{
	
	public function __construct(){
        parent::__construct();
	}

	/**
     * 发起 server 请求
     * @param $action
     * @param $params
     * @param $httpHeader
     * @return mixed
     */
    public function curl($action, $params) {
        // print_r($params);
        $action = self::SERVERAPIURL.$action.'.'.$this->format;
        $httpHeader = $this->createHttpHeader();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $action);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->build_query($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false); //处理http证书问题
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $ret = curl_exec($ch);
        if (false === $ret) {
            $ret =  curl_errno($ch);
        }
        curl_close($ch);
        return $ret;
    }

    /**
     * 发起 server JSON 请求
     * @param $action
     * @param $params
     * @param $httpHeader
     * @return mixed
     */
    public function curl_json($action, $params) {
        // print_r($params);
        if (is_array($params)) {
            $params = json_encode($params);
        }
        $action = self::SERVERAPIURL.$action.'.'.$this->format;
        $httpHeader = $this->createHttpHeader();
        $httpHeader[] = "Content-Type: application/json";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $action);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false); //处理http证书问题
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $ret = curl_exec($ch);
        if (false === $ret) {
            $ret =  curl_errno($ch);
        }
        curl_close($ch);
        return $ret;
    }

	/**
     * 移除禁言群成员
     * @param $userId   用户 Id。（必传）
     * @param $groupId 群组 Id。（必传）
     * @return mixed
     */
    public function weather($city_name) {
        app()->configure('account');
        $juhe_weather_key = config('account.juhe_weather_key');
        $weather = new Weather($juhe_weather_key);
        $result = $weather->easyWeather($city_name);
        return $result;
    }

    public function citys(){
        $content = file_get_contents('http://v.juhe.cn/weather/citys?dtype=&key=18cf3444738a1dba933c24d7214ea50f');
        return json_decode($content,true);
    }

    public static function verify_idcard($card_no,$real_name){
        app()->configure('account');
        $haoservice_key = config('account.haoservice_key');
        $content = file_get_contents('http://apis.haoservice.com/idcard/VerifyIdcardv2?cardNo='.$card_no.'&realName='.$real_name.'&key='.$haoservice_key);
        $return = json_decode($content,true);
        if ($return['error_code'] == 0) {
            return $return;
        }elseif ($return['error_code'] == '206500') {
            $return['reason'] = '身份证号码错误';
            return $return;
        }elseif ($return['error_code'] == '206501') {
            $return['reason'] = '库中无此身份证记录';
            return $return;
        }elseif ($return['error_code'] == '206502') {
            $return['reason'] = '身份证验证中心维护中';
            return $return;
        }else {
            $return['reason'] = '系统错误';
            return $return;
        }
    }
}
