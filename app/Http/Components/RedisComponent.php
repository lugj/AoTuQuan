<?php 
namespace App\Http\Components;

use App\Http\Components\AppComponent;
use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use App\library\UtilFunction;
/**
* 
*/
class RedisComponent extends AppComponent{
	
	public function __construct(){
        parent::__construct();
	}

    /*-------------------------------
       Redis配置
    -------------------------------*/
    //连接Redis
    static public function redis_con(){
        $redis = new \Redis();
        $redis->connect(env('REDIS_HOST', '127.0.0.1'));
        $redis->auth(env('REDIS_PASSWORD', null));

        // 切换redis
        app()->configure('account');
        $redis_server_select = config('account.redis_server_select');
        $redis->select($redis_server_select);
        return $redis;
    }
}
