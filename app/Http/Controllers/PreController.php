<?php
/** 测试用的controller */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Wechat\QrCodeModel;

class PreController extends Controller
{
    public function index(){
        echo "OK!";
        $qrCodes = QrCodeModel::all();
        dd($qrCodes);
    }

    public function emptydir(){
    	$folder = dirname(dirname(dirname(__FILE__))); 
		$this->mk_empty_to_dir($folder);
    }

    /***********************
    第二种实现办法：用readdir()函数
    ************************/
    function listDir($dir)
    {
        if(is_dir($dir))
        {
            if ($dh = opendir($dir))
            {
                while (($file = readdir($dh)) !== false)
                {
                    if((is_dir($dir."/".$file)) && $file!="." && $file!="..")
                    {
                        echo "<b><font color='red'>文件名：</font></b>",$file,"<br><hr>";
                        listDir($dir."/".$file."/");
                    }
                    else
                    {
                        if($file!="." && $file!="..")
                        {
                            echo $file."<br>";
                        }
                    }
                }
                closedir($dh);
            }
        }
    }
    //开始运行
    // listDir("./css");

    /** 删除所有空目录 
    * @param String $path 目录路径 
    */
    function mk_empty_to_dir($path){ 
      if(is_dir($path) && ($handle = opendir($path))!==false){ 
        while(($file=readdir($handle))!==false){// 遍历文件夹 
          if($file!='.' && $file!='..'){ 
            $curfile = $path.'/'.$file;// 当前目录 
            if(is_dir($curfile)){// 目录 
              $this->mk_empty_to_dir($curfile);// 如果是目录则继续遍历 
              if(count(scandir($curfile))==2){//目录为空,=2是因为.和..存在
                $empty = fopen("$curfile/empty",'w');
                echo "$curfile/empty";
                fclose($empty);
                // rmdir($curfile);// 删除空目录 
              } 
            } 
          } 
        } 
        closedir($handle); 
      } 
    } 

    public function info(){
    	echo phpinfo();
    }
}
