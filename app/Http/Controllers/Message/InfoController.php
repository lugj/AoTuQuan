<?php

namespace App\Http\Controllers\Message;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\Models\UserProfileModel;
use App\Models\MessageGroupModel;

/**
 * 获取用户的头像和昵称
 */
class InfoController extends MessageController{

    public function __construct(){
        parent::__construct();        
    }

    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->info($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 向融云注册用户返回token
    * @param $user_id 用户id
    * @return 
    */
    public function info(Request $request){
        // 用户编号
        $request_id = $request->input('request_id');
        // user:用户,gruop:群组
        $type = $request->input('type')?$request->input('type'):'user'; 

        if ($request_id) {
            if ($type == 'user') {
                $profile = UserProfileModel::select([
                    'nick_name',
                    'avatar'
                ])
                ->where('user_id',$request_id)
                ->first();

                if (!$profile)
                    return $this->json('0','获取失败');

                $nick_name = $profile->nick_name;
                $avatar = $profile->avatar;
                return $this->json('1','',compact('nick_name','avatar'));
            }else{
                $group = MessageGroupModel::select([
                    'nick_name',
                    'avatar'
                ])
                ->where('group_id',$request_id)
                ->first();

                if (!$group)
                    return $this->json('0','获取失败');

                $nick_name = $group->nick_name;
                $avatar = $group->avatar;
                return $this->json('1','',compact('nick_name','avatar'));
            }
        }else{
            return $this->json('0','请求参数错误');
        }  
    }
}
