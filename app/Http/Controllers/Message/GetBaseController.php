<?php
namespace App\Http\Controllers\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use DB;

use App\Models\MessageSystemPublishModel;
use App\library\UtilFunction;

/*
* 获取消息已读未读状态及第一条通知消息
*/
class GetBaseController extends MessageController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->get_base($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 获取消息已读未读状态及第一条通知消息
   */
    public function get_base(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $result = array();
        //获取订单动态 第一条消息及状态
        $order_message = MessageSystemPublishModel::select(['desc','created_at'])->where('user_id',$user_id)->where('message_type',2)->orderBy('created_at', 'desc')->first();
        if($order_message){
            $order_message = $order_message->toArray();
            //获取已读未读状态
            $order_read_status = MessageSystemPublishModel::where('user_id',$user_id)->where('message_type',2)->where('status','unread')->first();
            if($order_read_status){
                $order_message['status'] = 0;   //已读
            }else{
                $order_message['status'] = 1;   //未读
            }
        }else{
            $order_message = array();
            $order_message['desc'] = '';
            $order_message['status'] = 1;
            $order_message['created_at'] = '';
        }

        //获取系统消息 第一条消息及状态
        $system_message = MessageSystemPublishModel::select(['desc','created_at'])->where('user_id',$user_id)->where('message_type',1)->orderBy('created_at', 'desc')->first();
        if($system_message){
            $system_message = $system_message->toArray();
            //获取已读未读状态
            $order_read_status = MessageSystemPublishModel::where('user_id',$user_id)->where('message_type',1)->where('status','unread')->first();
            if($order_read_status){
                $system_message['status'] = 0;   //已读
            }else{
                $system_message['status'] = 1;   //未读
            }
        }else{
            $system_message = array();
            $system_message['desc'] = '';
            $system_message['status'] = 1;
            $system_message['created_at'] = '';
        }
        $result['order_message'] = $order_message;
        $result['system_message'] = $system_message;
        return $this->json('1','获取成功',$result);

    }
}
