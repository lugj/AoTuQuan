<?php
namespace App\Http\Controllers\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use DB;

use App\Models\MessageSystemPublishModel;
use App\library\UtilFunction;

/*
* 系统推送消息列表
*/
class SystemPublishListController extends MessageController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->publish_list($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 系统推送列表
    * @param 
    * @return 
   */
    public function publish_list(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $limit = 15;

        $page_index = intval($request->input('page_index'))?intval($request->input('page_index')):0;
        $offest = $page_index * $limit;
        MessageSystemPublishModel::where('user_id', $user_id)->update(array("is_read"=>1));
        //系统通知
        $publish_list = MessageSystemPublishModel::where('user_id', $user_id)
            ->select([
                'publish_id',
                'user_id',
                'message',
                'desc',
                'created_at',
            ])
            ->orderBy('created_at','desc')
            ->skip($offest)->take($limit)
            ->get();

        if ($publish_list) {
            $publish_list = $publish_list->toArray();
            foreach($publish_list AS &$pitem){
                $pitem["created_at"] = date("Y-m-d",strtotime($pitem["created_at"]));
            }
            return $this->json('1','消息获取成功',$publish_list);
        }else{
            return $this->json('1','暂无消息');
        }
    }
}
