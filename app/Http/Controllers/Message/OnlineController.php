<?php
namespace App\Http\Controllers\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use DB;
use App\Models\UserProfileModel;
use App\Models\MessageSystemPublishModel;
use App\Models\UserModel;
use App\Models\JobModel;
use App\Models\ConfigModel;
use App\Models\UserGiftModel;
use App\library\UtilFunction;

/*
* 在线用户
*/
class OnlineController extends MessageController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->online($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }



    /*
    * 在线用户
   */
    public function online(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user["user_id"];
        //获取有兼职的用户
//        $userList = UserModel::where("app_login_time",'!=','0000-00-00 00:00:00')->get()->toArray();
//        $user_ids = array_column($userList,"user_id");

        $jobUserList = JobModel::select([
            'at_job.id',
            'at_user_profile.avatar',
            'at_user.actived_at',
            'at_user.app_login_time',
        ])->join("at_user_profile","at_user_profile.user_id","=","at_job.user_id")
            ->join("at_user","at_user.user_id","=","at_job.user_id")
//            ->where("at_user.app_login_time",'!=','0000-00-00 00:00:00')
//            ->whereIn("at_job.user_id",$user_ids)
                ->where("at_job.model",1)
            ->whereIn("at_job.status",array(0,1))->groupBy('at_job.user_id')->skip(0)
            ->take(30)->orderBy('actived_at', 'DESC')->get()->toArray();

//        if(count($jobUserList)>5){
//            $get_num = 5;
//        }else{
//            $get_num = count($jobUserList);
//        }
//        $result_num = $this->rand_5_user(count($jobUserList),$get_num);
//        foreach($result_num AS $ritem){
//            $result[] = $jobUserList[$ritem];
//        }

        foreach($jobUserList AS &$ritem){
            $ritem["avatar"] = UtilFunction::getUrlImg($ritem["avatar"]);
            $ritem["actived_at"] = UtilFunction::get_active_time(time()-$ritem["actived_at"]);
            if($ritem["app_login_time"] != '0000-00-00 00:00:00'){
                $ritem["login_status"] = "在线";
            }else{
                $ritem["login_status"] = "离线";
            }
        }
        //获取最新消息内容
        $new_message = MessageSystemPublishModel::select(['desc','created_at'])->where('user_id', $user_id)->orderBy("created_at","DESC")->first();
        if($new_message){
            $new_message = $new_message->toArray();
            $new_message["created_at"] = date("m-d H:i",strtotime($new_message["created_at"]));
        }else{
            $new_message = array();
            $new_message["desc"] = "";
            $new_message["created_at"] = "";
        }
        //获取用户头像
        $userProfile = UserProfileModel::where("user_id",$user_id)->first()->toArray();

        $new_message["user_avatar"] = UtilFunction::getUrlImg($userProfile["avatar"]);

        //查看未读消息
        $unread_message = MessageSystemPublishModel::where('user_id', $user_id)->where("is_read",0)->count();

        $result["online_list"] = $jobUserList;
        $result["unread_message"] = $unread_message;
        $result["new_message"] = $new_message;
        return $this->json('1','获取成功',$result);

    }

    /**
     * 获取5个随机在线用户
     */
    public function rand_5_user($max_num,$get_num,$list = array()){
        if(count($list) == $get_num){
            return $list;
        }
        $num = rand(0,$max_num-1);
        if(!in_array($num,$list)){
            $list[] = $num;
        }
        $result = $this->rand_5_user($max_num,$get_num,$list);
        return $result;
    }
}
