<?php
namespace App\Http\Controllers\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use DB;
use App\Models\UserProfileModel;
use App\Models\BlacklistModel;
use App\Models\ConfigModel;
use App\Models\UserGiftModel;
use App\library\UtilFunction;

/*
* 检测是否可以聊天
*/
class CheckController extends MessageController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->check($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }



    /*
    * 检测是否可以聊天
   */
    public function check(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $chat_user_id = $request->input('chat_user_id');
        //检测是否进入黑名单
        $black1 = BlacklistModel::where("user_id",$user_id)->where("black_user_id",$chat_user_id)->first();
        if($black1){
            return $this->json('0','您已将对方拉黑，消息无法发送，请先取消拉黑');
        }
        $black1 = BlacklistModel::where("user_id",$chat_user_id)->where("black_user_id",$user_id)->first();
        if($black1){
            return $this->json('0','对方已将您拉黑，消息无法发送');
        }

        //验证是否为好友
        $result = UserComponent::checkFriend($user_id,$chat_user_id);
        if($result){
            return $this->json('1','可以聊天');
        }else{
            //验证今日是否送礼物了，送了礼物 即可用聊天
            $start_time = strtotime(date("Y-m-d 00:00:00"));
            $end_time = strtotime(date("Y-m-d 23:59:59"));
            $userGift = UserGiftModel::where("give_user_id",$user_id)->where("receive_user_id",$chat_user_id)->whereBetween("create_time",[$start_time,$end_time])->first();
            if($userGift){
                return $this->json('1','可以聊天');
            }

            $userProfile = UserProfileModel::where("user_id",$user_id)->first();
            $config = new ConfigModel();
            if($userProfile->card_level>0){   //是会员
                $gift_limit = $config->get_value('card_hello_chat_limit');
            }else{
                $gift_limit = $config->get_value('hello_chat_limit');
            }
            if($userProfile->today_hello_chat>=$gift_limit){
                return $this->json('0','您今天打招呼数量已经超出限制，请明天再来');
            }else{
                //打招呼 插入今日打招呼记录
                $today_hello_chat_user_list = $userProfile->today_hello_chat_user_list;
                $today_hello_chat_user_list = explode(',',$today_hello_chat_user_list);
                if(!in_array($chat_user_id,$today_hello_chat_user_list)){
                    $userProfile->today_hello_chat = $userProfile->today_hello_chat+1;
                    $today_hello_chat_user_list[] = $chat_user_id;
                    $userProfile->today_hello_chat_user_list = implode(',',$today_hello_chat_user_list);
                    $userProfile->saveOrFail();
                }

                return $this->json('1','可以聊天');
            }

        }

        return $this->json('1','获取成功',$result);

    }
}
