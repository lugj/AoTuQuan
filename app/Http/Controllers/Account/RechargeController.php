<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use App\Http\Components\AccountComponent;

use App\Models\AccountRechargeModel;
use App\Models\OrderModel;
/*
* 充值支付（pingpp）
*/
class RechargeController extends AccountController{

    protected $web_url;
    protected $apipay_config;
    
    public function __construct(){
        parent::__construct();
        $this->web_url = config('constants.web_url');
        $this->apipay_config = config('alipay');
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->recharge($request);
        }else{
            return $this->recharge($request);
            // return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 充值订单并起支付
    * @param $user_id 用户id
    * @return
   */
    public function recharge(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $channel_list = array(
            '2'=>'wx',
            '3'=>'alipay',
        );
        // 参数校验
        $customAttr = [
            'amount' => '金额',
            'order_id'=>'订单号',
        ];
        $this->validate($request, [
            'amount' => 'required',
            'order_id' => 'required',
        ],[ ], $customAttr);

        // 获取参数
        $order_id = strtolower($request->input('order_id'));
        $order = OrderModel::where("order_id",$order_id)->where("pay_user_id",$user_id)->where("status","0")->first();
        if(!$order){
            return $this->json('0','订单号不存在');
        }else{
            $order_arr = $order->toArray();
            if($order_arr['channel'] == 2 || $order_arr['channel'] ==3){
                $channel = $channel_list[$order_arr['channel']];
            }else{
                return $this->json('0','支付方式有误!');
            }
        }
        $amount = $request->input('amount');
        $amount = round($amount,2);
        if($amount != round(($order_arr['price']-$order_arr['balance']),2)){
            return $this->json('0','金额有误!');
        }
        $amount = $amount*100;
        $body = $request->input('body')?$request->input('body'):'购买服务';

        $subject = '购买服务';
        $extra = array();

        $order_data = array(
            'amount'=>$amount,
            'channel'=>$channel,
            'order_no'=>$order_id,
            'currency'=>'cny',
            'subject'=>$subject,
            'body'=>$body,
        );
        $pingpp = new AccountComponent();

        $recharge = $pingpp->pay($order_data);
        echo $recharge;
//        $recharge = json_decode($recharge,true);
//        echo json_encode(array('status'=>'1','message'=>'获取成功','data'=>$recharge));

    }
}
