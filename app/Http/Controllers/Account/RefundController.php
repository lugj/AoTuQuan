<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use DB;
use App\Models\AccountCardModel;

/*
* 退款：（暂不使用）
*/
class RefundController extends AccountController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->refund($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 退款申请
    * @param $user_id 用户id
    * @return 
   */
    public function refund(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        
        
    }
}
