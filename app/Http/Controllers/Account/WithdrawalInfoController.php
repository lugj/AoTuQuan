<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use DB;
use App\Models\UserAccountModel;
use App\Models\DataConfigModel;
use App\Models\AccountWithdrawalModel;

/*
* 获取绑定的提现账户
*/
class WithdrawalInfoController extends AccountController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->withdrawal_info($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 获取绑定的提现账户
    * @param $user_id 用户id
    * @return 
   */
    public function withdrawal_info(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $result = array();
        //获取可提现余额
        $user_account = UserAccountModel::where("user_id",$user_id)->first()->toArray();
        $balance = $user_account['balance']-$user_account['frozen'];
        $result['balance'] = $balance;
        //获取最新一次账户
        $account_with_drawal = AccountWithdrawalModel::select(['card_name','card_number'])
            ->where("user_id",$user_id)
            ->where('status','succeeded')
            ->orderBy('created_at', 'desc')->first();
        if($account_with_drawal){
            $account_with_drawal = $account_with_drawal->toArray();
            $result['account'] = $account_with_drawal;
        }
        $data_config = new DataConfigModel();
        $min_price = $data_config->get_value('withdrawal_min');
        $result['min_price'] = $min_price;
        return $this->json('1','获取成功',$result);
    }
}
