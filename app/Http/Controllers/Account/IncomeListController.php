<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use DB;
use App\Models\AccountRecordModel;
use App\Models\OrderModel;

/*
* 获取收入列表
*/
class IncomeListController extends AccountController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->income_list($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 收入列表
   */
    public function income_list(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $page_index  = intval($request->input('page_index'))?intval($request->input('page_index')):0;

        $limit = 10;
        $offest = $page_index * $limit;
        $billList = AccountRecordModel::select([
            'order_id',
            'price',
            'source',
            'create_time'
        ])->where("account_records.user_id",$user_id)
            ->where("account_records.price",">",0)
            ->whereIn("account_records.source",array(2,4,5,6,7,8,9))
            ->skip($offest)
            ->take($limit)
            ->orderBy('account_records.create_time', 'desc')
            ->get()->toArray();
//        $billList = OrderModel::select([
//            'order.order_id',
//            'order.price',
//            'order.create_time',
//            'user_profile.nick_name',
//        ])
//        ->where('order.user_id',$user_id)
//        ->whereIn('order.status',array(1,2,3,4,5))
//        ->leftJoin("user_profile","user_profile.user_id","=","order.pay_user_id")
//        ->skip($offest)
//        ->take($limit)
//        ->orderBy('order.create_time', 'desc')
//        ->get()->toArray();

        if ($billList) {
            foreach($billList AS $bindex => &$bitem){
               switch($bitem['source']){
                   case 2:  //完成交易给卖家加钱
                       $bitem['title'] = "完成订单:".$bitem['order_id'];
                       break;
                   case 4://徒弟注册奖励
                       $bitem['title'] = "填写邀请码注册奖励";
                       break;
                   case 5://邀请徒弟注册奖励
                       $bitem['title'] = "邀请徒弟注册奖励";
                       break;
                   case 6://师傅获取徒弟百分比金额
                       $bitem['title'] = "徒弟收入提成";
                       break;
                   case 7://实名认证收入
                       $bitem['title'] = "实名认证奖励";
                       break;
                   case 8://退款
                       $bitem['title'] = "订单号:".$bitem['order_id']."退款";
                       break;
                   case 9://拒绝提现 加钱
                       $bitem['title'] = "您的提现审核已被拒绝，金额返还";
                       break;
               }
            }
            $data = $billList;
            if(!empty($data)){
                return $this->json('1','获取成功',$data);
            }else{
                echo json_encode(array('status'=>'1','message'=>'数据为空','data'=>array()));
            }
        }else{
            echo json_encode(array('status'=>'1','message'=>'数据为空','data'=>array()));
        }
    }

}
