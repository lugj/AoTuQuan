<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\SmsComponent;
use App\Http\Components\UserComponent;

use DB;
use App\Models\UserAccountModel;

/*
* 修改支付密码
*/
class PayPasswordModifyController extends AccountController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->pay_password_modify($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 修改支付密码
    * @param $user_id 用户id
    * @return 
   */
    public function pay_password_modify(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $phone = $user['phone'];

        $customAttr = [
            'pay_password' => '支付密码',
            'pay_password_confirm' => '支付密码确认',
            'check_code' => '短信验证码',
        ];
        /*暂时不做校验*/
        $this->validate($request, [
            'pay_password' => 'required|digits:6',
            'pay_password_confirm' => 'required|digits:6|same:pay_password',
            'check_code' => 'required',
        ],[ ], $customAttr);        

        $pay_password = md5($request->input('pay_password'));
        $check_code = $request->input('check_code');

        $sms = new SmsComponent();
        $sms_status = $sms->verify_check_code($phone,$check_code,'pay_password');
        if ($sms_status) {
            $userAccount = UserAccountModel::firstOrNew(['user_id'=>$user_id]);
            $userAccount->pay_password = $pay_password;
            $save_status = $userAccount->saveOrFail();
        }else{
            return $this->json('0','验证码不正确');
        }

        if ($save_status) {
            return $this->json('1','支付密码修改成功');   
        }else{
            return $this->json('0','支付密码修改失败');
        }
    }
}
