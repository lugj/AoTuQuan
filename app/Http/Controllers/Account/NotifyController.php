<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\MessageComponent;
use App\Http\Components\UserComponent;
use App\Models\AccountRecordModel;
use App\Models\UserAccountModel;
use DB;
use App\Models\OrderModel;
use App\Models\UserProfileModel;
use App\Models\LogOrderModel;

/*
* 获取绑定的提现账户
*/
class NotifyController extends AccountController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){

        return $this->notify($request);
    }

    /*
    * 获取绑定的提现账户
    * @param $user_id 用户id
    * @return 
   */
    public function notify(Request $request){
        try {
            $input_data = json_decode(file_get_contents("php://input"), true);
            
            $public_path = dirname(__FILE__);
            $log_data = json_encode($input_data);
            if ($input_data['type'] == 'charge.succeeded') {
                $order_data = $input_data['data']['object'];
                if($order_data['object'] == 'charge' && $order_data['paid'] ==true){
                    $order_no = $order_data['order_no'];
                    $amount = $order_data['amount'];
                    $order = OrderModel::where("order_id",$order_no)->where("status","0")->first();
                    $pay_price = round(($order->price-$order->balance)*100,2) ;
                    if($order && $pay_price == round($amount,2)){
                        $order->status = 1;
                        $order->ping_id = $order_data['id'];
                        $order->uptime = date("Y-m-d H:i:s");
                        if($order->saveOrFail()){
                            //扣除余额
                            if(!empty($order->balance)){
                                $user_account = UserAccountModel::where("user_id",$order->pay_user_id)->first();
                                $user_account->balance = round($user_account->balance-$order->balance,2);
                                $user_account->spending = round($user_account->spending+$order->price,2);
                                $user_account->saveOrFail();
                            }
                            //记录金额日志
                            $account_record = new AccountRecordModel();
                            $account_record->save_records($order->pay_user_id,$order_no,-$order->price,1);

                            //记录订单状态变更日志
                            $log_order = new LogOrderModel();
                            $log_order->save_log($order->pay_user_id,$order_no,0,1);
                            //发送推送
                            $buyer_user = UserProfileModel::where('user_id',$order->pay_user_id)->first()->toArray();
                            $push = array('user_id'=>$order->user_id,'buyer_name'=>$buyer_user['nick_name'],'order_id'=>$order_no);
                            $result = MessageComponent::unite_push('buyer_order',$push);
                        }
                        echo 'success';
                    }else{
                        if($order){
                            $order->status = -1;
                            $order->uptime = date("Y-m-d H:i:s");
                            $order->saveOrFail();
                            $log_data = $log_data."|order_id".$order_no."|pay_price:".$pay_price."|amount:".$amount;
                        }
                        echo 'unsuccess';
                    }
                    if (is_dir($public_path)) {
                        $file = fopen($public_path.'/charge.txt', 'w+');
                        fwrite($file, '[input_data]:'.$log_data);
                        fclose($file);
                    }
//                    $account_recharge = AccountRechargeModel::where('order',$order_no)->first();
//                    if ($account_recharge) {
//                        $user_id = $account_recharge->user_id;
//                        $account_recharge->status = 'succeeded';
//                        $account_recharge->ch_id = $order_data['id'];
//                        if ($account_recharge->saveOrFail()){
//                            DB::table('account_bill')->where('order_no', $order_no)->update(['status' => 'succeeded']);
//                            $userAccount = UserAccountModel::where('user_id',$user_id)->first();
//                            $userAccount->balance = $userAccount->balance + $order_data['amount'];
//                            $userAccount->saveOrFail();
//                        }
//                        //记录充值
//                        UserComponent::log($user_id,'recharge');
//                        //积分经验
//                        UserComponent::mission_exp('once_recharge',$user_id);
//
//                        echo 'success';
//                    }else{
//                        $account_recharge = new AccountRechargeModel;
//                        $account_recharge->status = 'created';
//                        $account_recharge->ch_id = $order_data['id'];
//                        if ($account_recharge->saveOrFail()){
//                        }
//                        echo 'unsuccess';
//                    }
                    exit;
                }
                else{
                    echo 'fail';
                    exit;
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();            
            exit;
        }
    }
}
