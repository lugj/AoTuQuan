<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use DB;
use App\Models\AccountBillModel;
use App\Models\OrderModel;

/*
* 任务收支
*/
class TaskBillListController extends AccountController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->task_bill_list($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 任务收支明细
   */
    public function task_bill_list(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $page_index  = intval($request->input('page_index'))?intval($request->input('page_index')):0;

        $limit = 10;
        $offest = $page_index * $limit;

        $billList = DB::table('x_income_cost')->select([
            'x_income_cost_type.name',
            'x_income_cost.serial_number',
            'x_income_cost.amount',
            'x_income_cost.is_come',
            'x_income_cost.created_timestamp',
        ])
        ->where('x_income_cost.user_id',$user_id)
        ->where('x_income_cost.rec_active_flag','N')
        ->leftJoin("x_income_cost_type","x_income_cost_type.id","=","x_income_cost.income_cost_type_id")
        ->skip($offest)
        ->take($limit)
        ->orderBy('x_income_cost.created_timestamp', 'desc')
        ->get();
        $billList = json_decode(json_encode($billList),true);
            if ($billList) {
            return $this->json('1','获取成功',$billList);
        }else{
                echo json_encode(array('status'=>'1','message'=>'数据为空','data'=>array()));
        }
    }

}
