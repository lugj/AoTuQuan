<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use App\Http\Components\AccountComponent;

use DB;
use App\Models\AccountTransferModel;
use App\Models\UserProfileModel;

/*
* 获取绑定的提现账户
*/
class TransferController extends AccountController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->transfer($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 获取绑定的提现账户
    * @param $user_id 用户id
    * @return 
   */
    public function transfer(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        // 参数校验
        $customAttr = [
            'pay_password' => '支付密码',
            'amount' => '金额',
            // 'body' => '描述',
            'to_user_id' => '支付对象',
            'type' => '类型',
        ];
        $this->validate($request, [
            'pay_password' => 'required',
            'amount' => 'required',
            // 'body' => 'required',
            'to_user_id' => 'required',
            'type' => 'required',
        ],[ ], $customAttr);

        $pay_password = $request->input('pay_password');
        $amount = floatval($request->input('amount')) * 100;
        $body = $request->input('body');
        $to_user_id = $request->input('to_user_id');
        $type = $request->input('type');

        if (!in_array($type, array('to_user'))) {
            return $this->json('0','类型不正确');   
        }

        if (!$amount) {
            return $this->json('0','请填写金额');   
        }

        if($to_user = UserProfileModel::where('user_id',$to_user_id)->first()){
            $to_user_name = $to_user->nick_name;
        }else{
            return $this->json('0','未找到支付对象');   
        }

        // 转账支付
        if($balance = AccountComponent::check_pay_password($user_id,$pay_password)){
            if ($balance >= $amount) {
                $accountTransfer = new AccountTransferModel();
                $accountTransfer->user_id = $user_id;
                $accountTransfer->order_no = AccountComponent::generate_order_no('transfer','dddpocket');
                $accountTransfer->amount = $amount;
                $accountTransfer->channel = 'dddpocket';
                $accountTransfer->subject = '转账';
                $accountTransfer->body = $body?$body:'转账至'.$to_user_name;
                $accountTransfer->to_user_id = $to_user_id;
                $accountTransfer->type = $type;
                $transfer_status = $accountTransfer->saveOrFail();
                if ($transfer_status) {
                    /*操作余额*/
                    $order_data = $accountTransfer->toArray();
                    if ($bill_status = AccountComponent::bill('transfer',$order_data)) {
                        return $this->json('1','转账成功'); 
                    }else{
                        return $this->json('0','记录账单失败');
                    }
                }else{
                    return $this->json('0','转账失败');   
                }
            }else{
                return $this->json('0','余额不足请充值');   
            }
        }else{
            return $this->json('0','余额不足请充值');   
        }
    }
}
