<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use App\Http\Components\PingppComponent;

use DB;
// use App\Models\PayAccountModel;

/*
* 生成支付订单
*/
class AccountPayController extends AccountController{

    protected $web_url;
    protected $apipay_config;
    
    public function __construct(){
        parent::__construct();
        $this->web_url = config('constants.web_url');
        $this->apipay_config = config('alipay');
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->pay($request);
        }else{
            return $this->pay($request);
            // return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 提现
    * @param $user_id 用户id
    * @return 
    */
    public function pay(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        // 参数校验
        $customAttr = [
            'amount' => '金额',
            // 'body' => '描述',
        ];
        $this->validate($request, [
            'amount' => 'required',
            // 'body' => 'required',
        ],[ ], $customAttr);

        $amount = $request->input('amount') * 100;
        $body = $request->input('body');

        // 提现支付
        if($balance = AccountComponent::check_pay_password($user_id,$pay_password)){
            if ($balance >= $amount) {
                $accountWithdrawal = new AccountWithdrawalModel();
                $accountWithdrawal->user_id = $user_id;
                $accountWithdrawal->order_no = AccountComponent::generate_order_no('withdrawal','dddpocket');
                $accountWithdrawal->channel = 'dddpocket';
                $accountWithdrawal->card_number = $card_number;
                $accountWithdrawal->card_name = $card_name;
                $accountWithdrawal->card_type = $card_type;
                $accountWithdrawal->amount = $amount;
                $accountWithdrawal->subject = '提现';
                $accountWithdrawal->body = $body?$body:'提现至支付宝账户';
                $withdrawal_status = $accountWithdrawal->saveOrFail();
                if ($withdrawal_status) {
                    /*操作余额*/
                    $order_data = $accountWithdrawal->toArray();
                    if ($bill_status = AccountComponent::bill('withdrawal',$order_data)) {
                        return $this->json('1','提现成功');   
                    }else{
                        return $this->json('0','记录账单失败');
                    }
                }else{
                    return $this->json('0','提现失败');   
                }
            }else{
                return $this->json('0','余额不足');   
            }
        }else{
            return $this->json('0','余额不足');   
        }
    }
}
