<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use DB;
use App\Models\AccountBillModel;

/*
* 获取绑定的提现账户
*/
class BillDetailController extends AccountController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->bill_detail($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 获取绑定的提现账户
    * @param $user_id 用户id
    * @return 
   */
    public function bill_detail(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        
        $customAttr = [
            'order_no' => '订单号',
        ];
        /*暂时不做校验*/
        $this->validate($request, [
            'order_no' => 'required',
        ],[ ], $customAttr);

        $order_no = $request->input('order_no');
        
        $accountBill = AccountBillModel::select([
            'order_no',
            'channel',
            'amount',
            'subject',
            'body',
            'currency',
            'status',
            'time',
        ])
        ->where('user_id',$user_id)
        ->where('order_no',$order_no)
        ->first();

        if ($accountBill) {
            $data = $accountBill->toArray();
            return $this->json('1','',$data);   
        }else{
            return $this->json('0','未查询到此订单');
        }
    }
}
