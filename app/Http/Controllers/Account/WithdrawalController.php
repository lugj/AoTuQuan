<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use App\Http\Components\AccountComponent;
use App\Http\Components\RongCloudComponent;

use DB;
use App\Models\AccountCardModel;
use App\Models\UserAccountModel;
use App\Models\AccountRecordModel;
use App\Models\DataConfigModel;
use App\Models\AccountWithdrawalModel;

/*
* 提现
*/
class WithdrawalController extends AccountController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->withdrawal($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 提现
    * @param $user_id 用户id
    * @return 
    */
    public function withdrawal(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $card_number = $request->input('card_number');
        $amount = $request->input('amount');
        $card_name = $request->input('card_name');
        if(empty($amount)){
            return $this->json('0','提现金额不能为空!');
        }
        if(empty($card_name)){
            return $this->json('0','姓名不能为空!');
        }
        if(empty($card_number)){
            return $this->json('0','支付宝账号不能为空!');
        }
        $data_config = new DataConfigModel();
        $min_price = $data_config->get_value('withdrawal_min');
        if(round($amount,2)<round($min_price,2)){
            return $this->json('0','提现金额不能少于'.$min_price);
        }

        $accountWithdrawal_last = AccountWithdrawalModel::where("user_id",$user_id)->where("status",'created')->first();
        if($accountWithdrawal_last){
            return $this->json('0','存在待审核的提现记录，请等待审核!');
        }

        $account = UserAccountModel::where("user_id",$user_id)->first();
        $balance = $account->balance;
        $frozen = $account->frozen;
        $price = $balance-$frozen;

        // 提现支付
        if($price){
            if ($price >= $amount) {
                $accountWithdrawal = new AccountWithdrawalModel();
                $accountWithdrawal->user_id = $user_id;
                $accountWithdrawal->order_no = AccountComponent::generate_order_no('withdrawal','dddpocket');
                $accountWithdrawal->channel = 'alipay';
                $accountWithdrawal->card_number = $card_number;
                $accountWithdrawal->card_name = $card_name;
                $accountWithdrawal->amount = $amount;
                $accountWithdrawal->subject = '提现';
                $accountWithdrawal->body = '提现至支付宝账户';
                $withdrawal_status = $accountWithdrawal->saveOrFail();
                if ($withdrawal_status) {
                    $account->frozen = $amount;
                    $account->spending = $account->spending+$amount;
                    $account->saveOrFail();
                    //记录金额日志
                    $account_record = new AccountRecordModel();
                    $account_record->save_records($user_id,'',-$amount,3);
                    return $this->json('1','提现申请已被受理','我们将在24小时内进行转账操作');
                    /*操作余额*/
                    //$order_data = $accountWithdrawal->toArray();
                   //$bill_status = AccountComponent::bill('withdrawal',$order_data);
                   // if ($bill_status) {
                       // RongCloudComponent::system_publish($user_id,'提现申请已被受理','我们将在24小时内进行转账操作','','me');

//                    }else{
//                        return $this->json('0','记录账单失败');
//                    }
                }else{
                    return $this->json('0','提现失败');
                }
            }else{
                return $this->json('0','余额不足');
            }
        }else{
            return $this->json('0','余额不足');
        }
    }
}
