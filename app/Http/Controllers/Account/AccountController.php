<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\ApiController;

/*
* 账单信息接口
*/
class AccountController extends ApiController{

    public function __construct(){

        $this->version = config('constants.version');
        $this->updated = config('constants.updated');
    }
}