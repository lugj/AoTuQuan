<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use DB;
use App\Models\AccountBillModel;
use App\Models\AccountRecordModel;
use App\Models\OrderModel;

/*
* 获取支出列表
*/
class SpendListController extends AccountController{

    public function __construct(){
         parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->spend_list($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 支出列表
   */
    public function spend_list(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $page_index = intval($request->input('page_index')) ? intval($request->input('page_index')) : 0;

        $limit = 10;
        $offest = $page_index * $limit;
        $billList = AccountRecordModel::select([
            'order_id',
            'price',
            'source',
            'create_time'
        ])->where("account_records.user_id", $user_id)
            ->where("account_records.price", "<", 0)
            ->whereIn("account_records.source", array(1,3))
            ->skip($offest)
            ->take($limit)
            ->orderBy('account_records.create_time', 'desc')
            ->get()->toArray();


        if ($billList) {
            foreach ($billList AS $bindex => &$bitem) {
                switch ($bitem['source']) {
                    case 1:  //购买订单 减钱
                        $user_profile = OrderModel::select(['user_profile.nick_name'])->where("order.order_id",$bitem['order_id'])
                        ->leftJoin('user_profile','user_profile.user_id','=','order.user_id')
                        ->first()->toArray();
                        $bitem['title'] = "购买了".$user_profile['nick_name']."的服务";
                        break;
                    case 3://提现
                        $bitem['title'] = "申请提现到支付宝";
                        break;
                }
            }
            $data = $billList;
            if (!empty($data)) {
                return $this->json('1', '获取成功', $data);
            } else {
                echo json_encode(array('status' => '1', 'message' => '数据为空', 'data' => array()));
            }
        } else {
            echo json_encode(array('status' => '1', 'message' => '数据为空', 'data' => array()));
        }
    }

}
