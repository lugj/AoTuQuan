<?php

namespace App\Http\Controllers\Myorder;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Service\ServiceController;
use App\Models\OrderModel;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;

use App\Models\ServiceModel;

use App\library\UploadFile;


/**
 * 各个状态的订单数量
 */
class StatusNumController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->status_num($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 各个状态的订单数量
     */
    public function status_num(Request $request){
        $result = array();
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $status = array(1,2,3,4,5,6,7);
        $result = array();
        //我的订单
        $my_order_list = OrderModel::where('user_id',$user_id)
            ->whereIn('status',$status)
            ->get();
        if($my_order_list){
            $temp_data = array(
                1=>0,
                2=>0,
                3=>0,
                4=>0,
                5=>0,
            );
            $my_order_list = $my_order_list->toArray();
            foreach($my_order_list AS $oitem){
                if($oitem['status'] == 6 || $oitem['status'] == 7){
                    $temp_data[5] = $temp_data[5]+1;
                }elseif($oitem['status'] != 5){
                    $temp_data[$oitem['status']] = $temp_data[$oitem['status']]+1;
                }
            }
            $result['myorder'] = $temp_data;
        }

        //我的约单
        $yue_order_list = OrderModel::where('pay_user_id',$user_id)
            ->whereIn('status',$status)
            ->get();
        if($yue_order_list){
            $temp_data = array(
                1=>0,
                2=>0,
                3=>0,
                4=>0,
                5=>0,
            );
            $yue_order_list = $yue_order_list->toArray();
            foreach($yue_order_list AS $oitem){
                if($oitem['status'] == 6 || $oitem['status'] == 7){
                    $temp_data[5] = $temp_data[5]+1;
                }elseif($oitem['status'] != 5){
                    $temp_data[$oitem['status']] = $temp_data[$oitem['status']]+1;
                }
            }
            $result['yueorder'] = $temp_data;
        }
        if($result){
            return $this->json('1','获取成功',$result);
        }else{
            return $this->json('1','数据为空');
        }
    }
}