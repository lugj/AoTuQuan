<?php

namespace App\Http\Controllers\Myorder;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;

use App\Models\ServiceModel;
use App\Models\OrderModel;
use App\Models\UserSettingModel;
use App\Models\UserProfileModel;
use App\library\UploadFile;

use App\library\UtilFunction;

/**
 * 我的订单
 */
class OrderListController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->order_list($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 我的订单
     */
    public function order_list(Request $request){
        $customAttr = [
            'status' => '状态',
            'type' => '类型',
        ];
        /*暂时不做校验*/
        $this->validate($request, [
            'status' => 'required',
            'type' => 'required|numeric',
        ],[ ], $customAttr);
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $status = $request->input('status');
        $type = $request->input('type');
        if(!in_array($status,array(1,2,3,4,5))){
            return $this->json('0','订单状态有误!');
        }
        if($status == 4){
            $status = array(4,5);
        }elseif($status == 5){
            $status = array(6,7);
        }else{
            $status = array($status);
        }

        /**
         * type=1 我的订单
         * type=2 我的约单
         */
        if($type == 1){
            $type_condition = "order.user_id";
            $type_condition2 = "order.pay_user_id";
        }elseif($type == 2){
            $type_condition = "order.pay_user_id";
            $type_condition2 = "order.user_id";
        }else{
            return $this->json('0','类型输入有误!');
        }


        $page_index  = intval($request->input('page_index'))?intval($request->input('page_index')):0;

        $limit = 10;
        $offest = $page_index * $limit;

        $order_list = OrderModel::select([
            'user_profile.avatar',
            'user_profile.nick_name',
            'user_profile.job',
            'user_profile.gender',
            'order.order_id',
            'order.create_time',
            'order.price',
            'order.channel',
            'order.balance',
            'order.hours',
            'order.hour_price',
            'order.service_time',
            'order.message',
            'order.commission_rate',
            'order.commission_price',
            'order.status',
            'order.address',
            'order.user_id',
            'order.pay_user_id',
            'order.id',
        ])
            ->where($type_condition,$user_id)
            ->whereIn("order.status",$status)
            ->leftJoin("user_profile","user_profile.user_id","=",$type_condition2)
            ->skip($offest)
            ->take($limit)
            ->orderBy('create_time', 'desc')
            ->get();

        if($order_list){
            $order_list = $order_list->toArray();
            foreach($order_list as $oindex => $oitem){
                $order_list[$oindex]['avatar'] = UserComponent::get_avatar($oitem['avatar']);
                $order_list[$oindex]['create_time'] = date("Y-m-d,H:i",strtotime($oitem['create_time']));
                $service_time_str = strtotime($oitem['service_time']);
                $order_list[$oindex]['service_date'] = date("Y-m-d",$service_time_str);
                if(date('Ymd') == date('Ymd',$service_time_str) ){
                    $order_list[$oindex]['service_time'] = "今天 ".date("H:i",$service_time_str);
                }elseif(date('Ymd',(86400+time())) == date('Ymd',$service_time_str)){
                    $order_list[$oindex]['service_time'] = "明天 ".date("H:i",$service_time_str);
                }else{
                    $order_list[$oindex]['service_time'] = date("H:i",$service_time_str);
                }
                $order_list[$oindex]['actual_price'] = $oitem['price']-$oitem['commission_price'];
                //获取距离
                $user_setting = UserSettingModel::where("user_id",$oitem['user_id'])->first();
                $pay_user_setting = UserSettingModel::where("user_id",$oitem['pay_user_id'])->first();
                if($user_setting && $pay_user_setting){
                    $user_setting = $user_setting->toArray();
                    $pay_user_setting = $pay_user_setting->toArray();
                    $distance = UtilFunction::get_distance($user_setting['lat'],$user_setting['lon'],$pay_user_setting['lat'],$pay_user_setting['lon']);
                    if($distance<1000){
                        $distance = $distance."米";
                    }else{
                        $distance = round(($distance/1000),2)."公里";
                    }
                    $order_list[$oindex]['distance'] = $distance;
                }
                //实际支付金额
                $order_list[$oindex]['pay_price'] = round($oitem['price']-$oitem['balance'],2);

            }
            if(!empty($order_list)){
                return $this->json('1','获取成功',$order_list);
            }else{
                echo json_encode(array('status'=>'1','message'=>'数据为空','data'=>array()));
            }

        }else{
            echo json_encode(array('status'=>'1','message'=>'数据为空','data'=>array()));
        }
    }
}