<?php

namespace App\Http\Controllers\Myorder;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Service\ServiceController;
use App\library\Pingpp\Error\Api;
use App\Models\OrderModel;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;

use App\Models\OrderCancelModel;
use App\Models\DataConfigModel;
use App\library\UploadFile;


/**
 * 取消、拒绝订单
 */
class CancelReasonController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->cancel_reason($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 取消、拒绝订单
     */
    public function cancel_reason(Request $request){

        $list = OrderCancelModel::select(['content'])->get();
        if($list){
            $list = $list->toArray();
            return $this->json('1','获取成功!',$list);
        }else{
            return $this->json('1','数据为空!');
        }

    }
}