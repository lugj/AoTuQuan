<?php

namespace App\Http\Controllers\Myorder;

use App\Http\Controllers\Service\ServiceController;
use App\Models\AccountRecordModel;
use App\Models\OrderModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;
use App\Http\Components\MessageComponent;
use App\Models\UserSettingModel;
use App\Models\ServiceModel;
use App\Models\UserAccountModel;
use App\Models\DataConfigModel;
use App\library\UploadFile;


/**
 * 提交订单
 */
class SaveOrderController extends ServiceController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->save_order($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 保存订单
     */
    public function save_order(Request $request){
        $customAttr = [
            'service_user_id'=>'服务方ID',
            'service_time' => '服务时间',
            'hours' => '购买时长',
            'channel' => '支付方式',
            'balance' => '余额使用金额',
            'price' => '订单总金额',
        ];
        $this->validate($request, [
            'service_user_id'=>'required',
            'service_time' => 'required',
            'hours' => 'required',
            'channel' => 'required',
            'balance' => 'required',
            'price' => 'required',
        ],[ ], $customAttr);


        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $order_id = date("YmdHis").rand(10000,99999);
        $service_user_id = $request->input('service_user_id');
        $address = $request->input('address');
        $service_time = $request->input('service_time');
        $hours = $request->input('hours');
        $message = $request->input('message');
        $channel = $request->input('channel');
        $balance = $request->input('balance');
        $price = $request->input('price');

        if(!in_array($channel,array(1,2,3))){
            return $this->json('0','支付方式有误!');
        }
        //获取卖家服务信息
        $service_user = ServiceModel::where('user_id',$service_user_id)->first();
        if($service_user){
            $service_user = $service_user->toArray();
            $hour_price = $service_user['price'];
            if(round($hour_price*$hours,2) != round($price,2)){
                return $this->json('0','价格有误!');
            }

        }else{
            return $this->json('0','服务信息不存在!');
        }
        //验证买家是否可以购买服务
        $buyer_setting = UserSettingModel::where('user_id',$user_id)->first();
        if(!$buyer_setting || $buyer_setting->buyer_limit_time>time()){
            return $this->json('0','您的账户暂时无法预约服务!');
        }
        //验证卖家是否可以购买服务
        $seller_setting = UserSettingModel::where('user_id',$service_user_id)->first();
        if(!$seller_setting || $seller_setting->seller_limit_time>time() || $seller_setting->switch_service == 0){
            return $this->json('0','该用户暂时无法提供服务!');
        }
        //验证余额支付信息是否正确
        if($channel == 1){
            if(round($balance,2) != round($price,2)){
                return $this->json('0','价格有误!');
            }
        }

        $user_account = UserAccountModel::where('user_id',$user_id)->first();
        if($user_account){
            $user_account_arr = $user_account->toArray();
            if($balance>$user_account_arr['balance']){
                return $this->json('0','您的余额不足!');
            }
        }

        $config = new DataConfigModel();
        $commission_rate = $config->get_value('commission_rate');
        $order = new OrderModel();
        $order->order_id = $order_id;
        $order->pay_user_id = $user_id;
        $order->user_id = $service_user_id;
        $order->address = $address;
        $order->service_time = $service_time;
        $order->hours = $hours;
        $order->hour_price = $hour_price;
        $order->message = $message;
        $order->balance = $balance;
        $order->channel = $channel;
        $order->price = $price;
        $order->commission_rate = $commission_rate;
        $order->commission_price = round(($commission_rate/100)*$price,2);
        if($channel == 1 && round($price,2) == round($balance,2)){
            $order->status = 1;
        }
        $result = $order->saveOrFail();
        if($result){
            if($channel == 1 && round($price,2) == round($balance,2)){
                //扣除余额
                $user_account->balance = round($user_account_arr['balance']-$price,2);
                $user_account->spending = round($user_account_arr['spending']+$price,2);
                $user_account->saveOrFail();
                //余额支付 记录金额操作
                $account_record = new AccountRecordModel();
                $account_record->save_records($user_id,$order_id,-$price,1);
                $buyer_user = UserProfileModel::where('user_id',$user_id)->first()->toArray();

                MessageComponent::unite_push('buyer_order',array('user_id'=>$service_user_id,'buyer_name'=>$buyer_user['nick_name'],'order_id'=>$order_id));
            }
            return $this->json('1','提交成功',$order_id);
        }else{
            return $this->json('0','提交失败');
        }
    }
}