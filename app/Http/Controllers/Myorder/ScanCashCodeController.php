<?php

namespace App\Http\Controllers\Myorder;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Service\ServiceController;
use App\Models\OrderModel;
use App\Models\AccountRecordModel;
use App\Models\UserAccountModel;
use App\Models\DataConfigModel;
use App\Http\Components\MessageComponent;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;

use App\library\UploadFile;
use App\Models\LogOrderModel;
use App\Models\UserProfileModel;


/**
 * 扫描收款二维码
 */
class ScanCashCodeController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->scan_cash_code($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 扫描收款二维码
     */
    public function scan_cash_code(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $customAttr = [
            'order_id'=>'订单号',
        ];
        $this->validate($request, [
            'order_id'=>'required',
        ],[ ], $customAttr);
        $status = 4;
        $order_id = $request->input('order_id');
        $order = OrderModel::where("order_id",$order_id)
            ->where('order.pay_user_id','=',$user_id)
            ->first();

        if($order){
            $previous_status = $order->status;
            if($order->status != 3 && $order->status != 7){
                return $this->json('0','订单状态有误!');
            }
            $order->status = $status;
            $order->uptime = date("Y-m-d H:i:s");
            $result = $order->saveOrFail();
            if($result){


                //操作服务者金额
                $service_user = UserAccountModel::where("user_id",$order->user_id)->first();
                //查看是否存在师傅的金额
                $UserProfile = UserProfileModel::where("user_id",$order->user_id)->first()->toArray();
                $get_price = round($order->price-$order->commission_price,2);
                if(!empty($UserProfile['master_id'])){
                    $data_config = new DataConfigModel();
                    $rate = $data_config->get_value('master_get_rate');
                    $source_price = $get_price;
                    $get_price = round($get_price-$get_price*$rate/100,2);
                    $master_price = round($source_price-$get_price,2);
                    //记录师傅金额
                    $masterAccount = UserAccountModel::where("user_id",$UserProfile['master_id'])->first();
                    if($masterAccount){
                        $masterAccount->balance = $masterAccount->balance+$master_price;
                        $masterAccount->income = $masterAccount->income+$master_price;
                        $masterAccount->master_income = $masterAccount->master_income+$master_price;
                        $masterAccount->saveOrFail();
                        $account_record = new AccountRecordModel();
                        $account_record->save_records($UserProfile['master_id'],$order_id,$master_price,6);
                    }

                }
                $cur_price = round($service_user->balance+$get_price,2);
                $service_user->balance = $cur_price;
                $service_user->income = round($service_user->income+$get_price,2);;
                $service_user->saveOrFail();
                //记录金额日志
                $account_record = new AccountRecordModel();
                $account_record->save_records($order->user_id,$order_id,$get_price,2);

                //记录日志
                $log_order = new LogOrderModel();
                $log_order->save_log($user_id,$order_id,$previous_status,$status);
                $buyer_user = UserProfileModel::where('user_id',$user_id)->first()->toArray();
                MessageComponent::unite_push('buyer_scan_cash',array('user_id'=>$order->user_id,'buyer_name'=>$buyer_user['nick_name'],'order_id'=>$order_id));
                return $this->json('1','操作成功');
            }else{
                return $this->json('0','操作失败');
            }
        }else{
            return $this->json('0','该订单不存在!');
        }
    }
}