<?php

namespace App\Http\Controllers\Myorder;

use App\Http\Controllers\Service\ServiceController;
use App\Models\OrderModel;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;

use App\Models\ServiceModel;
use App\Models\UserAccountModel;
use App\Models\UserProfileModel;
use App\Models\DataConfigModel;
use App\library\UploadFile;


/**
 * 确认订单
 */
class SureOrderController extends ServiceController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->sure_order($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 确认订单
     */
    public function sure_order(Request $request){
        $result = array();
        $customAttr = [
            'service_user_id' => '服务者ID',
        ];
        $this->validate($request, [
            'service_user_id' => 'required|numeric',
        ],[ ], $customAttr);
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $service_user_id = $request->input('service_user_id');
        $data = UserComponent::get_base_info($service_user_id);
        if($data){
            $result['avatar'] = $data['avatar'];
            $result['nick_name'] = $data['nick_name'];
            $result['job'] = $data['job'];

            $service = ServiceModel::select(['price'])->where("user_id",$service_user_id)->first();
            if($service) {
                $service = $service->toArray();
                $result['price'] = $service['price'];
            }

            $user_account = UserAccountModel::where('user_id',$user_id)->first()->toArray();
            $result['balance'] = $user_account['balance']-$user_account['frozen'];  //可用余额
            //获取配置信息
            $config_list = DataConfigModel::select(['title','desc','key','value'])
                ->orwhere('key','order_aftertime')
                ->orwhere('key','order_time_min')
                ->orwhere('key','order_time_max')
                ->get();
            foreach($config_list AS $citem){
                $result[$citem['key']] = $citem['value'];
            }
        }

        if($result){
            return $this->json('1','获取成功',$result);
        }else{
            return $this->json('0','数据获取失败');
        }
    }
}