<?php

namespace App\Http\Controllers\Myorder;

use App\Http\Controllers\ApiController;
use App\Models\OrderModel;
use App\Models\EvaluateModel;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;
use App\Http\Components\MessageComponent;
use App\Models\UserProfileModel;
use App\Models\LogOrderModel;
use App\library\UploadFile;


/**
 * 提交评价
 */
class EvaluateController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->evaluate($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 提交评价
     */
    public function evaluate(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $customAttr = [
            'star'=>'评星',
            'content'=>'评价内容',
            'order_id'=>'订单号'
        ];
        $this->validate($request, [
            'star'=>'required|numeric',
            'content'=>'required',
            'order_id'=>'required',
        ],[ ], $customAttr);
        $status = 5;
        $star = $request->input('star');
        $content = $request->input('content');
        $order_id = $request->input('order_id');
        $order = OrderModel::where('order_id',$order_id)
            ->where(function($query) use ($user_id){
                $query->where('order.user_id','=',$user_id)
                    ->orwhere('order.pay_user_id','=',$user_id);
            })->first();
        if($order){
            $previous_status = $order->status;
            if($previous_status != 4){
                return $this->json('0','订单状态有误!');
            }
            $order_arr = $order->toArray();
            $evaluate = new EvaluateModel();
            if($order_arr['user_id'] == $user_id){
                $be_evaluater = $order_arr['pay_user_id'];
            }else{
                $be_evaluater = $order_arr['user_id'];
            }
            $evaluate->user_id = $be_evaluater;
            $evaluate->evaluate_user = $user_id;
            $evaluate->star = $star;
            $evaluate->content = $content;
            $evaluate->order_id = $order_id;
            $result = $evaluate->saveOrFail();
            if($result){
                //更新订单状态
                $order->status = $status;
                $order->uptime = date("Y-m-d H:i:s");
                $order->saveOrFail();
                //更新用户评分
                $evaluate_count = EvaluateModel::where("user_id",$be_evaluater)->count();
                $evaluate_sum  = EvaluateModel::where("user_id",$be_evaluater)->sum('star');
                $user_profile = UserProfileModel::where('user_id',$be_evaluater)->first();
                $user_profile->star = round($evaluate_sum/$evaluate_count);
                $user_profile->saveOrFail();
                //记录日志
                $log_order = new LogOrderModel();
                $log_order->save_log($user_id,$order_id,$previous_status,$status);
                //发推送
                $user = UserProfileModel::where("user_id",$user_id)->first()->toArray();
                if($order_arr['user_id'] == $user_id) { //卖家评价买家
                    MessageComponent::unite_push('seller_evaluate_order', array('user_id' => $order->pay_user_id, 'seller_name' => $user['nick_name'], 'order_id' => $order_id));
                }else{
                    MessageComponent::unite_push('buyer_evaluate_order', array('user_id' => $order->user_id, 'buyer_name' => $user['nick_name'], 'order_id' => $order_id));
                }
                return $this->json('1','保存成功');
            }else{
                return $this->json('0','保存失败!');
            }
        }else{
            return $this->json('0','订单不存在!');
        }
    }
}