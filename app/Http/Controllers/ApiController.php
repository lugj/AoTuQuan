<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Validator;

use DB;
use App\Models\UserModel;
use App\Http\Controllers\Controller;
use App\library\RongCloudServer;

use App\Http\Components\UserComponent;
/**
 * Api 
 * 说明：
 * 不使用JsonResponse
 * 对错误的请求返回json串
 */

class ApiController extends Controller
{
    protected $user_id;
    protected $user_type;
    protected $err_msg = '';

    public function __construct(){
        // $this->user_component = new UserComponent;
        // $this->user_component->get_user();
    }

    protected function update_app($apptype,$appversion){
        $db_version = DB::table('other_app_update')->where(['apptype'=>$apptype])->first();
        if ($db_version) {
            /*若版本小于最低版就强制更新*/
            if (version_compare( $appversion, $db_version->appversion_lowest, '<')) {
                $message = $db_version->version_desc;
                return $this->json('103',$message,'',array('download_link'=>$db_version->download_link));
            }
            /*若版本小于最新版，则提示更新*/
            if (version_compare($appversion, $db_version->appversion, '<')) {
                $message = $db_version->version_desc;
                return $this->json('102',$message,'',array('download_link'=>$db_version->download_link));
            }
        }
    }

    public function redis_con(){
        $redis =  new \Redis();
        $redis->connect(env('REDIS_HOST', '127.0.0.1'),env('REDIS_PORT', 6379));
        $redis->auth(env('REDIS_PASSWORD', null));
        return $redis;
    }

    public static function now(){
        return date('Y-m-d H:i:s');
    }

    public static function ip(){
        static $ip = null;
        if (is_null($ip)) {
            $ip = CCCommonUtil::getClientIp();
        }
        return $ip;
    }

    /**
     * {@inheritdoc}
     * 系统回复信息调整，只返回第一条信息的错误
     */
    protected function formatValidationErrors(Validator $validator)
    {
        $first_error = $validator->errors()->first();
        $errors = $validator->errors();

        return ['status'=>'0','message'=>$first_error,'errors'=>$errors];
    }

    protected static function _json($status, $message='', $data = array(), $other = array(), $status_code = 200){
        // return new JsonResponse([ 'status' => $status, 'message' => $message ], $status_code);
        /*status判断，只能返回1或0字符串*/
        if (is_array($status) && isset($status['status'])) {
            $return_arr = $status;
        }else{
            if (!$status) {
                $status = '0';
            }
            /*message判断，每次都会有message标签*/
            if ($message) {
                $message = trim($message);
            }else{
                $message = '';
            }

            $base_arr = compact('status','message','data');
            $now = microtime(true);
            $base_arr['expire'] = round($now - STARTTIME,3);
            /*data获取不是数组或没有值返回空值*/
            if (!$data && $status == '0') {
                unset($base_arr['data']);
            }
            if (is_array($other) && !empty($other)) {
                $return_arr = array_merge($base_arr,$other);
            }else{
                $return_arr = $base_arr;
            }
        }
        
        array_walk_recursive($return_arr, function (&$value,$key){
            if (!is_array($value)) {
                if (strlen($value)) {
                    $value = "$value";
                }else{
                    $value = '';
                }
            }
        });
        
        $return_json = json_encode($return_arr,JSON_UNESCAPED_UNICODE);
        header('content-type:application/json');
        echo $return_json;
        exit;
    }

    protected function json($status, $message='', $data = array(), $other = array(), $status_code = 200){
        return $this->_json($status, $message, $data, $other, $status_code);
    }
}
