<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Circle;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\CircleModel;
use App\Models\CircleMessageModel;
use App\Models\UserModel;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 凹凸圈删除
 */
class DeleteController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->delete($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 凹凸圈删除
     */
    public function delete(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $cid = $request->input('cid');
        if(empty($cid)){
            return $this->json('0','参数为空');
        }
        $circle = CircleModel::where("id",$cid)->first();
        if(!$circle){
            return $this->json('0','该记录不存在');
        }elseif($circle->user_id == $user_id){
            $result = CircleModel::where("id",$cid)->where("user_id",$user_id)->delete();
            if($result){
                CircleMessageModel::where("cid",$cid)->delete();
                return $this->json('1','删除成功');
            }else{
                return $this->json('0','保存失败');
            }
        }else{
            return $this->json('0','您无法删除别人的凹凸圈');
        }


    }



}