<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Circle;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\CircleModel;
use App\Models\CircleMessageModel;
use App\Models\UserModel;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 留言
 */
class MessageController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->message($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 留言
     */
    public function message(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $model = $request->input('model');
        $cid = $request->input('cid');
        $message = $request->input('message');
        $reply_user_id = $request->input('reply_user_id');
        if(empty($cid)){
            return $this->json('0','凹凸圈ID不能为空');
        }
        if(empty($message)){
            return $this->json('0','留言内容不能为空');
        }
        $circle = CircleModel::where("id",$cid)->first();
        if(!$circle){
            return $this->json('0','该凹凸圈记录不存在');
        }
        $model = empty($model)?0:$model;
        $reply_user_id = empty($reply_user_id)?0:$reply_user_id;
        $circleMessage = new CircleMessageModel();
        $circleMessage->cid = $cid;
        $circleMessage->user_id = $user_id;
        $circleMessage->message = $message;
        $circleMessage->model = $model;
        $circleMessage->reply_user_id = $reply_user_id;
        $circleMessage->create_time = time();
        $result = $circleMessage->saveOrFail();
        if($result){
            CircleModel::where("id",$cid)->increment("message_num",1);
            return $this->json('1','留言成功');
        }else{
            return $this->json('0','保存失败');
        }

    }



}