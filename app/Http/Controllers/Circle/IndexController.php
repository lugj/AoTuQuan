<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Circle;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\CircleModel;
use App\Models\CircleMessageModel;
use App\Models\UserModel;
use App\Models\UserSettingModel;
use App\Models\UserProfileModel;
use App\Models\FriendModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 首页
 */
class IndexController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->index($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 首页
     */
    public function index(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $model = $request->input('model');  //1 同城 2好友 3我的
        if(!in_array($model,array(1,2,3))){
            $model = 1;
        }
        $page_index  = intval($request->input('page_index'))?intval($request->input('page_index')):1;
        $limit = 10;
        $offest = ($page_index-1) * $limit;
        $list = CircleModel::select([
            'at_circle.id',
            'at_circle.user_id',
            'at_circle.model',
            'at_circle.pictures',
            'at_circle.video',
            'at_circle.desc',
            'at_circle.city',
            'at_circle.read_num',
            'at_circle.flower_num',
            'at_circle.message_num',
            'at_circle.create_time',

            'at_user_profile.avatar',
            'at_user_profile.nick_name',
        ]);

        switch($model){
            case 1: //同城
                $userSetting  = UserSettingModel::where("user_id",$user_id)->first()->toArray();
                $user_city = $userSetting["city"];

                $list = $list->where("at_circle.city",$user_city)->where("at_circle.who_see",1)
                    ->orwhere(function($query) use ($user_id){
                        $query->where("at_circle.who_see",3)->havingRaw("FIND_IN_SET(".$user_id.",at_circle.remind_see_list)");
                    });
                break;
            case 2: //好友
                $friend_user_id_list = array();
                $friend_list = FriendModel::where(function($query) use ($user_id){
                    $query->where('apply_user_id','=',$user_id)
                        ->where("status",1);
                })->orwhere(function($query) use ($user_id){
                    $query->where('agree_user_id','=',$user_id)
                        ->where("status",1);})
                    ->get()->toArray();

                foreach($friend_list AS $fitem){
                    if($fitem["apply_user_id"]!=$user_id){
                        if(!in_array($fitem["apply_user_id"],$friend_user_id_list)){
                            $friend_user_id_list[] = $fitem["apply_user_id"];
                        }
                    }
                    if($fitem["agree_user_id"]!=$user_id){
                        if(!in_array($fitem["agree_user_id"],$friend_user_id_list)){
                            $friend_user_id_list[] = $fitem["agree_user_id"];
                        }
                    }
                }
                $list = $list->whereIn("at_circle.user_id",$friend_user_id_list)->where("at_circle.who_see",1)
                    ->orwhere(function($query) use ($user_id){
                        $query->where("at_circle.who_see",3)->havingRaw("FIND_IN_SET(".$user_id.",at_circle.remind_see_list)");
                    })->where("at_circle.user_id","!=",$user_id);
                break;
            case 3: //我的
                $list = $list->where("at_circle.user_id",$user_id);
                break;
        }
        $list = $list->leftJoin("at_user_profile","at_user_profile.user_id","=","at_circle.user_id")
            ->skip($offest)
            ->take($limit)
            ->orderBy('at_circle.create_time', 'desc')->get()->toArray();

        foreach($list AS $key => &$item){
            if(date("Y") == date("Y",$item["create_time"])){
                $item["year"] = "";
            }else{
                $item["year"] = date("Y年",$item["create_time"]);
            }

            $item["month"] = date("m月",$item["create_time"]);
            $item["day"] = date("d",$item["create_time"]);
            $item["message_list"] =  $this->_get_message($item,$user_id);
            $item["avatar"] = UtilFunction::getUrlImg($item["avatar"]);
            if($item["model"] == 1){
                $picture_list = explode(',',$item["pictures"]);
                $pic_list = array();
                foreach ($picture_list as &$pitem) {
                    $pitem = UtilFunction::getUrlImg($pitem);
                    $temp = array();
                    $temp["pic"] = $pitem;
                    $pic_list[] = $temp;
                }
                $item["pictures"] = $pic_list;
            }else{
                $item["video"] = UtilFunction::getUrlImg($item["video"]);
            }
            CircleModel::where("id",$item["id"])->increment("read_num",1);
        }

        return $this->json('1','获取成功',$list);
    }

    /**
     * 留言获取
     */
    public function _get_message($item,$user_id){
        $c_id = $item["id"];
        $message_list = CircleMessageModel::select([
            'at_circle_message.id',


            'at_circle_message.message',
            'at_circle_message.model',
            'at_circle_message.create_time',
            'at_circle_message.user_id',
            'at_user_profile.avatar',
            'at_user_profile.nick_name',
            'at_circle_message.reply_user_id',
        ])->where("at_circle_message.cid",$c_id)
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_circle_message.user_id")
            ->orderBy('at_circle_message.create_time', 'asc')->get()->toArray();
        if(!empty($message_list)){
            foreach($message_list AS $mkey => &$mitem){
                if($mitem["model"] == 1){   //悄悄话
                    if(!($user_id == $mitem["user_id"] || $user_id == $item["user_id"] || $user_id == $mitem["reply_user_id"])){
                        unset($message_list[$mkey]);
                        continue;
                    }
                }
                $mitem["avatar"] = UtilFunction::getUrlImg($mitem["avatar"]);
                if(!empty($mitem["reply_user_id"])){
                    $replyProfile = UserProfileModel::where("user_id",$mitem["reply_user_id"])->first()->toArray();
                    $mitem["reply_nick_name"] = $replyProfile["nick_name"];
                }
                $mitem["create_time"] = date("Y-m-d H:i:s",$mitem["create_time"]);

            }
            return $message_list;

        }else{
            return $message_list;
        }
    }




}