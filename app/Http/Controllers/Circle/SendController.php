<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Circle;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\CircleModel;
use App\Models\UserModel;
use App\Models\UserSettingModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 发布
 */
class SendController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->send($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 发布
     */
    public function send(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $model = $request->input('model');//1 图片 2视频 3文字
        $pictures = $request->input('pictures');
        $video = $request->input('video');
        $desc = $request->input('desc');
        $lon = $request->input('lon');
        $lat = $request->input('lat');
        $who_see = $request->input('who_see');  //1 公开 所有人可见 2 私密 只有自己可以看 3 部分可见
        $who_see_list = $request->input('who_see_list');
        $remind_see_list = $request->input('remind_see_list');
        if(empty($pictures) && empty($video)){
            return $this->json('0','文件不能为空！');
        }
        if($model == 1){
            if(empty($pictures)){
                return $this->json('0','文件不能为空！');
            }else{
                if(count(explode(',',$pictures))>9){
                    return $this->json('0','图片最多只能上传9张！');
                }
            }
        }elseif($model == 2){
            if(empty($video)){
                return $this->json('0','文件不能为空！');
            }
        }elseif($model !=3){
            return $this->json('0','文件类型有误！');
        }


        $circle = new CircleModel();
        $circle->user_id = $user_id;
        $circle->model = $model;
        $circle->desc = $desc;
        $circle->lon = $lon;
        $circle->lat = $lat;
        $city = UtilFunction::getCityByLatLon($lat,$lon);
        $circle->city = $city;
        $circle->who_see = $who_see;
        $circle->who_see_list = $who_see_list;
        $circle->remind_see_list = $remind_see_list;
        if($model == 1){
            $pictures_arr = explode(',',$pictures);
            foreach($pictures_arr AS &$pitem){
                $pitem = UtilFunction::getRootImg($pitem);
            }
            $pictures = implode(',',$pictures_arr);
            $circle->pictures = $pictures;
        }elseif($model == 2){
            $circle->video = UtilFunction::getRootImg($video);
        }

        $result = $circle->saveOrFail();
        if($result){
            return $this->json('1','保存成功',$result);
        }else{
            return $this->json('0','保存失败');
        }

    }



}