<?php
/** 测试用的controller */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\library\PinYin;
use App\library\juhe\Sms;
use App\library\AES;
use App\Http\Components\SmsComponent;
use App\Http\Components\RongCloudComponent;

// use App\library\Qiniu\Auth;
// use App\library\Qiniu\Storage\BucketManager;

use DB;

use App\library\Pingpp\Pingpp;
use App\library\Pingpp\Charge;

use Illuminate\Support\Facades\Config;
use Qiniu\Auth;
use Qiniu\Storage\BucketManager;

class TestFunctionController extends Controller
{
    public function invoke($function,Request $request){
        if (method_exists($this, $function)) {
            call_user_func_array(array($this,$function), array($request));
        }else{
            echo "function does not exists";
        }
    }

    public function sort_citys(){
        app()->configure('all_citys');
        app()->configure('hot_citys');
        $citys_arr = config('all_citys');
        $hot_citys = config('hot_citys');
        $PinYin = new PinYin();
        $citys = array();
        foreach ($citys_arr as $city_arr) {
            $city_name = $city_arr['city'];
            $citys[$city_name] = $PinYin->getFirstPY($city_name);
        }
        array_multisort($citys);
        $return_citys = array();
        $dictionarys = array();
        $count = 0;
        foreach ($citys as $city_name => $value) {
            $py = $PinYin->getFirstPY($city_name);
            $all_spell = $PinYin->getAllPY($city_name);
            $dictionary = substr($py, 0,1);
            $new_city['id'] = ++$count;
            $new_city['name'] = $city_name;
            $new_city['first_spell'] = $py;
            $new_city['all_spell'] = $all_spell;
            // $dictionarys[$new_city['dictionary']][] = $new_city['id'];
            $return_citys[$dictionary][] = $new_city;

            if (in_array($count,$hot_citys)) {
                $data_hot_citys[] = $new_city;
            }
        }

        $data[] = array('title'=>'热门城市','citys'=>$data_hot_citys);
        foreach ($return_citys as $dictionary => $dic_arr) {
            $data[] = array('title'=>$dictionary,'citys'=>$dic_arr);
        }

        // $data['citys'] = $return_citys;
        
        echo(json_encode($data));
        echo "<br/>end<br/>";
    }

    public function testsms(){
        app()->configure('account');
        $sms_key = config('account.juhe_sms_key');
        $sms = new Sms($sms_key);
        $sms->send('18662627927',array('code'=>'3306'));
    }

    public function testcomponent(){
        $sms = new SmsComponent();
        $return = $sms->send_check_code('18662627927','register');
    }

    public function current_week(){ 
        //开学第一天的时间戳 
        
        $week = date('W');
        var_dump($week);
    }

    public function image_list(){


        $accessKey = 'Access_Key';
        $secretKey = 'Secret_Key';
        $auth = new Auth($accessKey, $secretKey);
        $bucketMgr = new BucketManager($auth);

        $bucket = 'Bucket_Name';
        $prefix = '';
        $marker = '';
        $limit = 3;

        list($iterms, $marker, $err) = $bucketMgr->listFiles($bucket, $prefix, $marker, $limit);
        if ($err !== null) {
            echo "\n====> list file err: \n";
            var_dump($err);
        } else {
            echo "Marker: $marker\n";
            echo "\nList Iterms====>\n";
            var_dump($iterms);
        }
    }

    public function pingpp_list(){
        Pingpp::setApiKey('sk_live_z988mD1K48i5PmPaj5P4ujbD');

        $charge_list = Charge::all();
        // $char = $charge_list->__toArray();
        $generate_list = array();
        //   'id' => string 'ch_XDSCW9mjXH4K1mH4G8OmTWLS' (length=27)
        //   'object' => string 'charge' (length=6)
        //   'created' => int 1460529485
        //   'livemode' => boolean true
        //   'paid' => boolean true
        //   'refunded' => boolean false
        //   'app' => string 'app_fPyzv5Gqb1mDizPO' (length=20)
        //   'channel' => string 'wx' (length=2)
        //   'order_no' => string 'Rwx20160413143804600' (length=20)
        //   'client_ip' => string '58.208.66.246' (length=13)
        //   'amount' => int 1
        //   'amount_settle' => int 1
        //   'currency' => string 'cny' (length=3)
        //   'subject' => string '鍏呭€煎姩鍔ㄥ竵' (length=15)
        //   'body' => string '鍏呭€�' (length=6)
        foreach ($charge_list->data as $_data) {
            // $data = $_data->__toArray();
            // $data_arr = array();
            $data_arr['id'] = $_data->id;
            $data_arr['object'] = $_data->object;
            $data_arr['created'] = $_data->created;
            $data_arr['livemode'] = $_data->livemode;
            $data_arr['paid'] = $_data->paid;
            $data_arr['refunded'] = $_data->refunded;
            $data_arr['app'] = $_data->app;
            $data_arr['channel'] = $_data->channel;
            $data_arr['order_no'] = $_data->order_no;
            $data_arr['client_ip'] = $_data->client_ip;
            $data_arr['amount'] = $_data->amount;
            $data_arr['amount_settle'] = $_data->amount_settle;
            $data_arr['currency'] = $_data->currency;
            $data_arr['subject'] = $_data->subject;
            $data_arr['body'] = $_data->body;
            $generate_list[$_data->order_no] = $data_arr;
            // var_dump($data);
            // echo "$ch_id";
        }
        var_dump($generate_list);
    }

    public function aes_test(){
        // $aes_test = new AES();
        // $aes_test->set_key('apptest');
        // $aec = $aes_test->encrypt('abc');
        // $dec = $aes_test->decrypt($aec);
        $ent_arr = array(
            'sign'=>'d30121200c8bace80a69935b2d49447f',
            'random_str'=>'1461567861',
            'apptype'=>'android',
            'appversion'=>'1.0',
            'user_id'=>'10007',
            'user_token'=>'8d8e5f4e1dc2154bfc89b6b0e8d8b799',
        );
        ksort($ent_arr);
        $query = http_build_query($ent_arr);

        $aec = AES::encode($query);
        $dec = AES::decode('sZowsEJOxoSKynbE13wpxvBvjD0E6VfnrRO+h9RciRtPAbnAyjNbDWBZTZX2RVlYxYxU3HWGSIoi1lfPVTc3UHhdCfwmm0wbQVI7T4wHOXN8k7m0IMH2lAIPeU2MB4GpPxxFNLNNfHn7L+vcklvmW9af+JhvaXMEUN8G9MW15nr44etqvQG72ySXUXzT2YJltuV9foXLakC2lU9Epf6hHw==');
        var_dump($aec);
        var_dump($dec);

    }

}
