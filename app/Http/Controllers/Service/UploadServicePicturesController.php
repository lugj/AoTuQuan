<?php

namespace App\Http\Controllers\Service;

use Illuminate\Http\Request;
use App\Http\Components\UserComponent;

use App\Models\ServiceModel;
use App\library\UploadFile;


/**
 * 上传我的服务封面照片
 */
class UploadServicePicturesController extends ServiceController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->upload_service_pictures($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 上传用户照片到相册
     * @param Request $request
     * @author paulLu
     */
    public function upload_service_pictures(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $upload = new UploadFile();// 实例化上传类
        $upload->savePath   =  'UserService/';// 设置附件上传目录
        $upload->maxSize    = 10240000;
        $upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg','bmp');
        $upload->autoSub    = true;
        $upload->subType    = 'custom';
        $upload->subDir     = $user_id.'/';
        $upload->thumb      = true;//是否开启图片文件缩略图
        $upload->thumbPrefix    ='';
        $upload->thumbMaxWidth  ='300';
        $upload->thumbMaxHeight ='300';

        if(!$upload->upload()) {
            return $this->json('0',$upload->getErrorMsg());
        }else{
            $info_arr = $upload->getUploadFileInfo();
            foreach($info_arr as $info){
                list($filepath,$filename) = explode('/', $info['savename']);
                app()->configure('ppurl');
                $img_url = config('ppurl.img_url');
                $src =  $img_url.'/'.$info['savepath'].$info['savename'];
                $thumb = $img_url.'/'.$info['savepath'].$filepath.'/'.$upload->thumbPrefix.$filename;

                // $img_thumb = $img_url.$thumb;

                return $this->json('1','上传成功',$src);
            }
        }
    }
}