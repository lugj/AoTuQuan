<?php

namespace App\Http\Controllers\Service;

use Illuminate\Http\Request;
use App\Http\Components\UserComponent;
use App\Http\Components\MessageComponent;
use App\Models\ServiceModel;
use App\Models\OrderModel;
use App\Models\LogOrderModel;
use App\Models\OtherReportModel;
use App\library\UploadFile;


/**
 * 提交举报信息
 */
class ReportController extends ServiceController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->report($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     *提交举报或投诉信息
     */
    public function report(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $customAttr = [
            'report_id' => '被举报者',
            'content' => '举报内容',
            'pictures' => '封面图片',
        ];
        $this->validate($request, [
            'report_id' => 'required|numeric',
            'content' => 'required',
            'pictures' => 'required',
        ],[ ], $customAttr);

        $report_id = $request->input('report_id');
        $order_id = $request->input('order_id');
        $content = $request->input('content');
        $pictures = $request->input('pictures');
        $status = 7;

        if(!empty($order_id)){
            $order = OrderModel::where('order_id',$order_id)->first();
            if($order){
                $order_arr = $order->toArray();
                $previous_status = $order_arr['status'];
                if($order_arr['status'] != 3){
                    return $this->json('0','订单状态有误');
                }
            }else{
                return $this->json('0','订单号不存在');
            }
        }


        if($report_id == $user_id){
            return $this->json('0','信息有误!');
        }
        $report = new OtherReportModel();
        $report->user_id = $user_id;
        $report->report_id = $report_id;
        $report->content = $content;
        $report->pictures = $pictures;
        if(!empty($order_id)){
            $report->order_id = $order_id;
        }
        $result = $report->saveOrFail();

        if($result){
            if(!empty($order_id)) {
                //更新订单状态为投诉
                $order->status = $status;
                $order->saveOrFail();
                //订单状态变成日志
                $log_order = new LogOrderModel();
                $log_order->save_log($user_id, $order_id, $previous_status, $status);
            }
            //投诉成功 发推送
            MessageComponent::unite_push('report_success',array('user_id'=>$user_id));
            return $this->json('1','提交成功');
        }else{
            return $this->json('0','提交失败');
        }
    }
}