<?php

namespace App\Http\Controllers\Service;

use App\Http\Components\RedisComponent;
use App\Models\CertificationModel;
use App\Models\FriendModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use DB;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use App\Models\UserSettingModel;
use App\Models\ServiceModel;
use App\Models\EvaluateModel;

use App\library\UtilFunction;

/**
 * 个人主页
 */
class HomePageController extends ServiceController{

    public function __construct(){
        parent::__construct();        
    }

    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->home_page($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 我的主页
    */
    public function home_page(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $service_user_id = $request->input('service_user_id');  //服务者用户ID
        //用户基本信息
        $info = UserComponent::get_base_info($service_user_id);

        //封装数据
        if ($info) {
            unset($info['switch_push']);
            unset($info['switch_service']);
            unset($info['balance']);
            unset($info['income']);
            unset($info['spending']);
            //计算活跃时间
            $actived_at = time()-$info['actived_at'];
            $info['actived_at'] = UtilFunction::get_active_time($actived_at);

            //计算距离
            $user = UserSettingModel::where("user_id",$user_id)->first();
            if ($user) {
                $user = $user->toArray();
            }

            $distance = UtilFunction::get_distance($user['lat'],$user['lon'],$info['lat'],$info['lon']);
            if($distance<1000){
                $distance = $distance."米";
            }else{
                $distance = round(($distance/1000),2)."公里";
            }

            $info['distance'] = $distance;
            $data['info'] = $info;

            $service = ServiceModel::where("user_id",$service_user_id)->first();
            if($service) {
                $service = $service->toArray();
                $service['pictures'] = explode(',',$service['pictures']);
                $data['service'] = $service;
            }

            //获取评价总数

            $evaluate_num = EvaluateModel::where("user_id",$service_user_id)->count();

            if($evaluate_num){
                $data['evaluate_num'] = $evaluate_num;
            }else{
                $data['evaluate_num'] = 0;
            }

            return $this->json('1','数据获取成功',$data);
        }else{
            return $this->json('0','未查询到用户信息');
        }
    }


}
