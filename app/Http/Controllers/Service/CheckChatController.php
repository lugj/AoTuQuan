<?php

namespace App\Http\Controllers\Service;

use Illuminate\Http\Request;
use DB;
use App\Models\UserModel;
use App\Models\OrderModel;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

/**
 * 验证是否可以聊天
 */
class CheckChatController extends ServiceController{

    public function __construct(){
        parent::__construct();        
    }

    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->check_chat($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 验证是否可以聊天
     */
    public function check_chat(Request $request){
        // error_reporting(E_ALL);
        // ini_set('display_errors','1');
        $customAttr = [
            'chat_user_id' => '用户ID',
        ];
        $this->validate($request, [
            'chat_user_id' => 'required|numeric',
        ],[ ], $customAttr);

        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $chat_user_id = $request->input('chat_user_id');

        // 判断订单是否在执行中
        $order= OrderModel::where('user_id',$chat_user_id)->where('pay_user_id',$user_id)->whereIn("status",array('1','2','3','4'))->first();
        
        if(!empty($order)){
            return $this->json('1','可以聊天');
        }else{
            $order= OrderModel::where('user_id',$user_id)->where('pay_user_id',$chat_user_id)->whereIn("status",array('1','2','3','4'))->first();
            if(!empty($order)){
                return $this->json('1','可以聊天');
            }else{
                return $this->json('0','下单才可以跟我聊天哦!');
            }
        }
    }
}
