<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Service\ServiceController;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;

use App\Models\ServiceModel;
use App\Models\EvaluateModel;
use App\Models\UserProfileModel;
use App\library\UploadFile;


/**
 * 评价列表
 */
class EvaluateListController extends ServiceController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->evaluate_list($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 保存服务
     */
    public function evaluate_list(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];


        $page_index  = intval($request->input('page_index'))?intval($request->input('page_index')):0;

        $limit = 10;
        $offest = $page_index * $limit;

        $service_user_id = $request->input('service_user_id');  //服务者用户ID
        $evaluate_list = EvaluateModel::select([
            'user_profile.avatar',
            'user_profile.nick_name',
            'user_profile.gender',
            'user_evaluate.star',
            'user_evaluate.content',
            'user_evaluate.create_time',
        ])
            ->where("user_evaluate.user_id",$service_user_id)
            ->leftJoin("user_profile","user_profile.user_id","=","user_evaluate.evaluate_user")
            ->skip($offest)
            ->take($limit)
            ->orderBy('create_time', 'desc')
            ->get();
        if($evaluate_list){
            $evaluate_list = $evaluate_list->toArray();
            foreach($evaluate_list as $eindex => $eitem){
                $evaluate_list[$eindex]['avatar'] = UserComponent::get_avatar($eitem['avatar']);
            }
            $data['list'] = $evaluate_list;
            $data['evaluate_num'] = EvaluateModel::where("user_id",$service_user_id)->count();
            return $this->json('1','获取成功',$data);
        }else{
            return $this->json('0','暂无数据',array());
        }
    }
}