<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Service\ServiceController;
use App\Models\UserLllegalModel;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;

use App\Models\ServiceModel;
use App\library\UploadFile;
use App\Models\DataConfigModel;

/**
 * 服务禁用信息查询
 */
class lllegalController extends ServiceController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->lllegal($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 服务禁用信息查询
     */
    public function lllegal(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $l_id = $request->input('l_id');
        if(empty($l_id)){
            return $this->json('0','参数有误!');
        }
        $lllegal = UserLllegalModel::where("id",$l_id)->where("user_id",$user_id)->first();
        if($lllegal){
            $lllegal = $lllegal->toArray();
            return $this->json('1','获取成功',$lllegal);
        }else{
            return $this->json('0','获取成功');
        }
    }
}