<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Service\ServiceController;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;

use App\Models\ServiceModel;
use App\library\UploadFile;
use App\Models\DataConfigModel;

/**
 * 保存我的服务
 */
class ServiceInfoController extends ServiceController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->service_info($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 保存服务
     */
    public function service_info(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $service = ServiceModel::where("user_id",$user_id)->first();

        if($service){
            $service = $service->toArray();
            $service['pictures'] = explode(',',$service['pictures']);
            //获取配置信息
            $config_list = DataConfigModel::select(['title','desc','key','value'])
                ->orwhere('key','service_price_min')
                ->orwhere('key','service_price_max')
                ->get();
            foreach($config_list AS $citem){
                $service[$citem['key']] = $citem['value'];
            }
            return $this->json('1','获取成功',$service);
        }else{
            return $this->json('1','获取成功');
        }
    }
}