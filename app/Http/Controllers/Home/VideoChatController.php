<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Home;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\BannersModel;
use App\Models\UserVideoChatModel;
use App\Models\JobModel;
use App\Models\UserModel;
use App\Models\UserSettingModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 主页 视频聊天
 */
class VideoChatController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->video_chat($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 主页 视频聊天
     */
    public function video_chat(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        //获取视频聊天列表
        $list = UserProfileModel::select(['user_id','avatar','nick_name','age','height','weight'])->where("open_video_chat",1)->where("video_chat_recommend",1)->get()->toArray();
        foreach($list AS &$ritem){
            $ritem["avatar"] = UtilFunction::getUrlImg($ritem["avatar"]);
        }
        return $this->json('1','获取成功',$list);
    }



}