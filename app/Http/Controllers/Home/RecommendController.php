<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Home;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\BannersModel;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\UserModel;
use App\Models\UserSettingModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 推荐
 */
class RecommendController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->recommend($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 推荐
     */
    public function recommend(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $userSetting = UserSettingModel::where("user_id",$user_id)->first()->toArray();
        //获取顶部新鲜人物
        $fresh_list = JobModel::select([
            'at_job.id',
            'at_user_profile.avatar',
            'at_user_profile.nick_name',
            'at_user_setting.lon',
            'at_user_setting.lat',
        ])
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_job.user_id")
            ->leftJoin("at_user_setting","at_user_setting.user_id","=","at_job.user_id")
            ->where("at_job.model",1)->where("at_job.is_fresh",1)->orderBy("at_job.create_time","DESC")->get()->toArray();
        foreach($fresh_list AS &$fitem){
            $fitem["avatar"] = UtilFunction::getUrlImg($fitem["avatar"]);
            $fitem["distance"] = UtilFunction::get_distance($userSetting['lat'],$userSetting['lon'],$fitem["lat"],$fitem["lon"]);
        }

        //获取推荐的兼职
        $recommend_list = JobModel::select([
            'at_job.id',
            'at_job.job_class_name',
            'at_user_profile.avatar',
            'at_user_profile.nick_name',
            'at_user_profile.credit_num',
            'at_user_setting.lon',
            'at_user_setting.lat',
        ])
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_job.user_id")
            ->leftJoin("at_user_setting","at_user_setting.user_id","=","at_job.user_id")
            ->where("at_job.model",1)->where("at_job.is_recommend2",1)->orderBy("at_job.create_time","DESC")->get()->toArray();
        foreach($recommend_list AS &$ritem){
            $ritem["avatar"] = UtilFunction::getUrlImg($ritem["avatar"]);
            $ritem["distance"] = UtilFunction::get_distance($userSetting['lat'],$userSetting['lon'],$ritem["lat"],$ritem["lon"]);
        }
        $result = array();
        $result["fresh_list"] = $fresh_list;
        $result["recommend_list"] = $recommend_list;
        return $this->json('1','获取成功',$result);
    }



}