<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Home;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\UserModel;
use App\Models\UserSettingModel;
use App\Models\UserProfileModel;
use App\Models\UserCarModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 兼职/需求列表
 */
class JobListController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->job_list($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 兼职/需求列表
     */
    public function job_list(Request $request){

        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $sort = $request->input('sort');    //排序 1 智能排序 2最新发布  3距离最近 4人气最高
        $gender = $request->input('gender');   //性别 0不限制  1男 2女
        $age = $request->input('age');   //年龄 0不限制  1 25以下 2 25~35  3 35以上
        $height = $request->input('height'); //身高 0不限制 1 160以下  2 160-168 3 168以上
        $distance = $request->input('distance'); //距离 0不限制 1 5公里以内 2  10公里以内
        $model = $request->input('model');   //1 找服务方  2找需求方
//        $auth_video = $request->input('auth_video'); //是否视频认证  0关闭 1开启
        $job_class_id = $request->input('job_class_id'); //分类ID
        if(empty($job_class_id)){
            return $this->json('0','分类ID不能为空');
        }

        $page_index  = intval($request->input('page_index'))?intval($request->input('page_index')):1;
        $limit = 10;
        $offest = ($page_index-1) * $limit;

        $sort = empty($sort)?1:$sort;
        $gender = empty($gender)?0:$gender;
        $age = empty($age)?0:$age;
        $height = empty($height)?0:$height;
        $distance = empty($distance)?0:$distance;
        $model = empty($model)?1:$model;
//        $auth_video = empty($auth_video)?0:$auth_video;
        $userSetting = UserSettingModel::where("user_id",$user_id)->first()->toArray();


        $list = JobModel::select([
            'at_job.id',
            'at_job.user_id',
            'at_user_profile.avatar',
            'at_user_profile.nick_name',
            'at_user_profile.deposit_auth',
            'at_user_profile.card_level',
            'at_user_profile.gender',
            'at_user_profile.height',
            'at_user_profile.weight',
            'at_user_profile.age',
            'at_user_profile.credit_num',
            'at_user_profile.car_auth',

            'at_user_car.car_logo',

            'at_job.price',
            'at_job.service_hourse',
            'at_job.introduce',
            'at_job.voice',
            'at_job.voice_time',

            'at_user_setting.lon',
            'at_user_setting.lat',
        ])->whereIn("at_job.status",array(0,1))->where("at_job.model",$model)->where("at_job.job_class_id",$job_class_id);


        if(!empty($gender)){
            $list = $list->where("at_user_profile.gender",$gender);
        }

        switch($age){
            case 1:
                $list = $list->where("at_user_profile.age",'<',25);
                break;
            case 2:
                $list = $list->whereBetween("at_user_profile.age",[25,35]);
                break;
            case 3:
                $list = $list->where("at_user_profile.age",'>',35);
                break;
        }

        switch($height){
            case 1:
                $list = $list->where("at_user_profile.height",'<',160);
                break;
            case 2:
                $list = $list->whereBetween("at_user_profile.height",[160,168]);
                break;
            case 3:
                $list = $list->where("at_user_profile.height",'>',168);
                break;
        }

//        if($auth_video == 1){
//            $list->where("at_user_profile.video_auth",1);
//        }

        $list = $list->leftJoin("at_user_profile","at_user_profile.user_id","=","at_job.user_id")
            ->leftJoin("at_user_setting","at_user_setting.user_id","=","at_job.user_id")
            ->leftJoin("at_user_car","at_user_car.user_id","=","at_job.user_id")
            ->skip($offest)
            ->take($limit);

        switch($sort){
            case 1:
                $list = $list->orderBy('at_job.is_recommend', 'desc')->orderBy('at_job.is_recommend2', 'desc')->orderBy('at_job.is_fresh', 'desc');
                break;
            case 2:
                $list = $list->orderBy('at_job.create_time', 'desc');
                break;
            case 4:
                $list = $list->orderBy('at_user_profile.credit_num', 'desc');
                break;

        }

        $list = $list->get()->toArray();

        foreach($list AS $key => &$item){
            $item["avatar"] = UtilFunction::getUrlImg($item["avatar"]);
            if(!empty($item["voice"])){
                $item["voice"] = UtilFunction::getUrlImg($item["voice"]);
            }
            if($item["car_auth"] == 1 && !empty($item["car_logo"])){
                $item["car_logo"] = UtilFunction::getUrlImg($item["car_logo"]);
            }

            $item["distance"] = UtilFunction::get_distance($userSetting["lat"],$userSetting["lon"],$item["lat"],$item["lon"]);
            $item["distance_true"] = UtilFunction::get_distance_true($userSetting["lat"],$userSetting["lon"],$item["lat"],$item["lon"]);
            if($distance == 1){ //只获取5公里以内的
                if($item["distance_true"]>5000){
                    unset($list[$key]);
                }
            }elseif($distance == 2){    //只获取10公里以内的
                if($item["distance_true"]>10000){
                    unset($list[$key]);
                }
            }
        }

        if($sort  == 3){    //按距离排序
            /*根据实际距离排序*/
            if(!empty($list)){
                foreach ( $list as $key => $row ){
                    $sort_keys[$key] = $row['distance_true'];
                }
                array_multisort($sort_keys, SORT_ASC, $list);
            }

        }

        return $this->json('1','获取成功',$list);


    }



}