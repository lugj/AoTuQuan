<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Home;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\BannersModel;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\UserModel;
use App\Models\UserSettingModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 首页
 */
class IndexController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->index($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 首页
     */
    public function index(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $lon = $request->input('lon');
        $lat = $request->input('lat');
        //更新地理位置及活跃时间
        $userSetting = UserSettingModel::where("user_id",$user_id)->first();
        if(!empty($lon) && !empty($lat)){
            $userSetting->lon = $lon;
            $userSetting->lat = $lat;
            $city = UtilFunction::getCityByLatLon($lat,$lon);
            $userSetting->city = $city;
            $userSetting->saveOrFail();
        }

        $user_active = UserModel::where("user_id",$user_id)->first();
        $user_active->actived_at = time();
        $user_active->saveOrFail();

        $result = array();
        //获取顶部轮播图
        $banner = array();
        $banner_list = BannersModel::select(['pic'])->where("type","main")->get()->toArray();
        foreach($banner_list AS &$litem){
            $litem['pic'] = UtilFunction::getUrlImg($litem["pic"]);
        }
        $result["banner"] = $banner_list;

        //获取分类菜单
        $job_class = JobClassModel::select(['id','title','is_hot','picture'])->orderBy("sort","ASC")->get()->toArray();
        foreach($job_class AS &$jitem){
            $jitem["picture"] = UtilFunction::getUrlImg($jitem["picture"]);
        }
        $result["job_class"] = $job_class;

        //获取推荐的兼职
        $recommend_list = JobModel::select([
            'at_job.id',
            'at_job.job_class_name',
            'at_user_profile.avatar',
            'at_user_profile.video_auth',
            'at_user_profile.nick_name',
            'at_user_profile.age',
            'at_user_profile.gender',
            'at_user.actived_at',

            ])
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_job.user_id")
            ->leftJoin("at_user","at_user.user_id","=","at_job.user_id")
            ->where("at_job.model",1)->where("at_job.status","!=",2)->where("at_job.is_recommend",1)->orderBy("at_job.create_time","DESC")->get()->toArray();
        foreach($recommend_list AS &$ritem){
            $ritem["avatar"] = UtilFunction::getUrlImg($ritem["avatar"]);
            $ritem["actived_at"] = UtilFunction::get_active_time(time()-$ritem['actived_at']);
        }
        $result["recommend_list"] = $recommend_list;
        return $this->json('1','获取成功',$result);
    }



}