<?php
namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use App\Http\Components\RongCloudComponent;
use App\Http\Components\SmsComponent;

use DB;
use App\Models\UserModel;
use App\Models\UserSettingModel;
use App\Models\UserProfileModel;
/**
 * 用户登录
 */
class LoginController extends UserController{
    private $register_url = "/api/user/register"; //注册
    public function __construct(){
        parent::__construct();
    }

    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->login($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 登录
     */
    private function login(Request $request){

        $customAttr = [
            'username' => '手机号',
            'check_code' => '验证码',
        ];
        $this->validate($request, [
            'username' => 'required|numeric',
            'check_code' => 'required',
        ],[ ], $customAttr);
        /*app类型保存到source字段*/
        $apptype_source = array('ios','android','h5');

        $username = $request->input('username');
        $check_code = $request->input('check_code');
        $apptype = strtolower($request->input('apptype'));
        $app_login_time = date('Y-m-d H:i:s',time());
        if (!in_array($apptype, $apptype_source)) {
            return $this->json('0',"登录失败，APP类型不正确：$apptype");
        }
        //判断验证码是否正确
        $sms = new SmsComponent();
        $sms_status = $sms->verify_check_code($username,$check_code,'login');

        if($sms_status){
            $user = UserModel::where('phone',$username)->first();
            // 正常的验证后操作
            if (!empty($user) && $user->status == 1) {

                $user->app_login_time = $app_login_time;
                $user->app_type = $apptype;
                $user->saveOrFail();

                // 将登录信息存入日志
                $user_token = UserComponent::get_token($username,$user->app_login_time,$apptype);
                $user_id = $user->user_id;
                $data['user_id'] = $user_id;
                $data['user_token'] = $user_token;
                //获取性别
                $user_profile = UserProfileModel::where("user_id",$user_id)->first();
                if($user_profile){
                    $user_profile_arr = $user_profile->toArray();
                    if(empty($user_profile_arr['message_token'])){
                        $message_token = RongCloudComponent::register($user_id,$user_profile_arr['nick_name'],$user_profile_arr['avatar']);
                        $user_profile->message_token = $message_token;
                        $user_profile->saveOrFail();
                        $data['message_token'] = $message_token;
                    }else{
                        $data['message_token'] = $user_profile_arr['message_token'];
                    }
                }else{
                    return $this->json('0','登录失败，未获取到用户信息');
                }

                return $this->json('1','登陆成功',$data);
            }elseif(empty($user)){  //没有注册 调用注册接口
                return $this->json('0',"请先注册");
            }elseif(!empty($user) && $user->status == 3 && time()<$user->login_fobit_time){
                return $this->json('0',"该账号已被禁止登陆！");
            }
        }else{
            return $this->json('0',$sms->error_msg);
        }


    }



    
}
