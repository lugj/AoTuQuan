<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserCarModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 车辆认证
 */
class AuthCarController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
//                if (version_compare($appversion, '1.0') > 0) {
//                    return $this->identify_user_haoservice($request);
//                }else{
                    return $this->auth_car($request);
               // }
            
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 车辆认证
     * @param Request $request
     * @author paulLu
     */
    public function auth_car(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $driving_license = $request->input('driving_license');  //行驶证
        $driver_license = $request->input('driver_license'); //驾驶证
        $car_picture = $request->input('car_picture'); //车辆认证

        if(empty($driving_license)){
            return $this->json('0','请上传行驶证');
        }
        if(empty($driver_license)){
            return $this->json('0','请上传驾驶证');
        }
        if(empty($car_picture)){
            return $this->json('0','请上传车辆照片');
        }

        $profile = UserProfileModel::firstOrNew(['user_id'=>$user_id]);
        if($profile && $profile->car_auth == 1){
            return $this->json('0','您的车辆已认证成功，无需提交');
        }

        $userCar = UserCarModel::where("user_id",$user_id)->first();
        if($userCar && $userCar->status == 0){
            return $this->json('0','您提交的车辆认证正在审核，请耐心等待');
        }
        if(!$userCar){
            $userCar = new UserCarModel();
            $userCar->user_id = $user_id;
        }
        $userCar->driving_license = UtilFunction::getRootImg($driving_license);
        $userCar->driver_license = UtilFunction::getRootImg($driver_license);
        $userCar->car_picture =  UtilFunction::getRootImg($car_picture);
        $userCar->status = 0;
        if($userCar && $userCar->status == 2){
            $userCar->updated_at = date("Y-m-d H:i:s");
        }
        $result = $userCar->saveOrFail();
        if($result){
            return $this->json('1','提交成功，请等待审核!');
        }else{
            return $this->json('0','提交失败!');
        }
    }

}