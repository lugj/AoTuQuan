<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use DB;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use App\Models\UserSettingModel;

use App\library\UtilFunction;

/**
 * 获取用户基本信息
 */
class BaseInfoController extends UserController{

    public function __construct(){
        parent::__construct();        
    }

    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->base_info($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 获取用户基本信息
    * @param $user_id 用户id
    * @return 
    */
    public function base_info(Request $request){

        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $data = UserProfileModel::select(['avatar','nick_name','phone','card_level','deposit_auth','age','gender','height','weight'])->where("user_id",$user_id)->first()->toArray();

        if ($data) {
            $data["avatar"] = UtilFunction::getUrlImg($data["avatar"]);
            return $this->json('1','获取成功',$data);
        }else{
            return $this->json('0','未查询到用户信息');
        }
    }
}
