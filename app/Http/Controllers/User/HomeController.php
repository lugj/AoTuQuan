<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserCarModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 我的主页
 */
class HomeController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                    return $this->home($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 我的主页
     * @param Request $request
     * @author paulLu
     */
    public function home(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $info = UserProfileModel::select(['at_user_profile.aotu_id','at_user_profile.avatar','at_user_profile.nick_name','at_user_profile.is_agent','at_user_profile.is_safety','at_user_profile.deposit_auth','at_user_profile.card_level','at_user_setting.disturbed_open','at_user_setting.disturbed_time'])->where("at_user_profile.user_id",$user_id)
            ->leftJoin('at_user_setting','at_user_profile.user_id','=','at_user_setting.user_id')
            ->first();
        if($info){
            $infoArr = $info->toArray();
            $infoArr["avatar"] = UtilFunction::getUrlImg($infoArr["avatar"]);
            return $this->json('1','获取成功',$infoArr);
        }
    }

}