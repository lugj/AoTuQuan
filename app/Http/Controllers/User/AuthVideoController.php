<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserVideoModel;
use App\Models\UserProfileModel;
use App\Models\UserModel;
use App\User;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 视频认证
 */
class AuthVideoController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
//                if (version_compare($appversion, '1.0') > 0) {
//                    return $this->identify_user_haoservice($request);
//                }else{
                    return $this->auth_video($request);
               // }
            
            }catch(\Exception $e){
                return $this->json('0','系统错误44'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 实名认证
     * @param Request $request
     * @author paulLu
     */
    public function auth_video(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $profile = UserProfileModel::firstOrNew(['user_id'=>$user_id]);
        if($profile && $profile->video_auth == 1){
            return $this->json('0','您已通过视频认证，无需提交1');
        }

        $userVideo = UserVideoModel::where("user_id",$user_id)->first();

        if($userVideo && $userVideo->status == 0){
            return $this->json('0','您提交的视频认证正在审核，请耐心等待12');
        }

        $video = $request->input('video');
        $result = UtilFunction::uploadFile("video","video",$video);
        if($result){
            if(!$userVideo){
                $userVideo = new UserVideoModel();
                $userVideo->user_id = $user_id;
            }
            $userVideo->video_url = $result;
            $userVideo->status = 0;
            $userVideo->saveOrFail();
            return $this->json('1','提交成功，请等待审核!3');
        }else{
            return $this->json('0','视频上传失败!5');
        }
    }

}