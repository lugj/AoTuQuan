<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserCarModel;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 设置忽扰时间段
 */
class DisturbedSettingController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                    return $this->disturbed_setting($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 忽扰设置
     * @param Request $request
     * @author paulLu
     */
    public function disturbed_setting(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $userSetting = UserSettingModel::where("user_id",$user_id)->first();
        $status = $request->input('status');  //1 打开  0 关闭
        $time = $request->input('time');    //时间段

        if(!in_array($status,array(0,1))){
            return $this->json('0','参数输入有误');
        }else{
            $userSetting->disturbed_open = $status;
        }


        if(!empty($time)){
            $userSetting->disturbed_time = $time;
        }
        $result = $userSetting->saveOrFail();
        if($result){
            return $this->json('1','设置成功');
        }else{
            return $this->json('0','设置失败');
        }
    }

}