<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserAccountModel;
use App\Models\UserProfileModel;
use App\Models\AccountRecordsModel;
use App\Models\AccountWithdrawalModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 我的账户
 */
class AccountController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{

                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 我的账户
     */
    public function info(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        app()->configure('base');
        $exchange_prop = config('base.exchange_prop');
        $userAccount = UserAccountModel::select([
            'at_user_account.balance',
            'at_user_account.gift_num',
            'at_user_account.flower_num',
            'at_user_account.today_income',
            'at_user_account.frozen',
            'at_user_account.income',
            'at_user_account.withdraw',
            'at_user_account.recharge',

            'at_user_profile.card_level',
        ])->where("at_user_account.user_id",$user_id)
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_user_account.user_id")->first()->toArray();
        $userAccount["balance_rmb"] = round($userAccount["balance"]/$exchange_prop,2);
        $userAccount["today_income_rmb"] = round($userAccount["today_income"]/$exchange_prop,2);
        $userAccount["frozen_rmb"] = round($userAccount["frozen"]/$exchange_prop,2);
        $userAccount["can_withdraw_rmb"] = round(($userAccount["balance"]-$userAccount["frozen"])/$exchange_prop,2);
        $userAccount["income_rmb"] = round($userAccount["income"]/$exchange_prop,2);
        return $this->json('1','获取成功!',$userAccount);
    }

    /**
     * 我的账单
     */
    public function bill(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $accountRecords = AccountRecordsModel::select([
            'user_id',
            'payment_code',
            'source_remark',
            'price',
            'create_time',
        ])->where("user_id",$user_id)->get()->toArray();
        $list = array();
        foreach($accountRecords AS $aitem){
            $create_time = strtotime($aitem["create_time"]);
            $aitem["create_time"] = date("m-d H:i",$create_time);
            $list[date("Y-m",$create_time)][] = $aitem;
        }
        return $this->json('1','获取成功!',$list);
    }

    /**
     * 提现 提交
     */
    public function withdraw(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $bank_name = $request->input('bank_name');  //银行名称
        $card_number = $request->input('card_number');  //卡号
        $price = $request->input('price');  //提现金额
        if(empty($bank_name)){
            return $this->json('0','请选择到账银行!');
        }
        if(empty($card_number)){
            return $this->json('0','请输入银行卡卡号!');
        }
        if(empty($price)){
            return $this->json('0','请输入提现金额!');
        }
        if(!is_numeric($price)){
            return $this->json('0','提现金额输入有误!');
        }
        $userAccount = UserAccountModel::where("user_id",$user_id)->first();
        $userAccountArr = $userAccount->toArray();
        app()->configure('base');
        $exchange_prop = config('base.exchange_prop');
        $balance_rmb = round(($userAccountArr["balance"]-$userAccountArr["frozen"])/$exchange_prop,2);

        if($price>$balance_rmb){
            return $this->json('0','您的余额不足!');
        }
        $accountWithDraw = new AccountWithdrawalModel();
        $accountWithDraw->user_id = $user_id;
        $accountWithDraw->order_sn = date("YmdHis").rand(1000,9999);
        $accountWithDraw->price = $price;
        $accountWithDraw->card_number = $card_number;
        $accountWithDraw->bank_name = $bank_name;
        $result = $accountWithDraw->saveOrFail();
        if($result){
            $userAccount->frozen = $userAccount->frozen+($price*$exchange_prop);
            $userAccount->saveOrFail();
            return $this->json('1','申请提现成功!');
        }else{
            return $this->json('0','申请提现失败!');
        }
    }



}