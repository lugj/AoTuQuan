<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use App\Models\UserLogModel;

/**
 * 用户相关接口 控制器
 * 
 * 1.通过条件生成用户令牌 get_token
 * 2.通过请求数据判断token有效性 check_token
 * 3.校验是否登陆的用户已经填写过个人信息 check_is_profiled
 * 4.登录、注册操作日志 doLog
 */
class UserController extends ApiController{

    public function __construct(){
        parent::__construct();
        $this->version = config('app.version');
        $this->updated = config('app.updated');
    }
}
