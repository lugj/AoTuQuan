<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\WechatSeeModel;
use App\Models\ConfigModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 微信号
 */
class WechatController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{

                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 微信号设置
     * @param Request $request
     * @author paulLu
     */
    public function setting(Request $request){

        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $wechat = $request->input('wechat');  //微信号

        if(empty($wechat)){
            return $this->json('0','请输入您的微信号');
        }

        $profile = UserProfileModel::where("user_id",$user_id)->first();
        $profile->wechat = $wechat;
        $result = $profile->saveOrFail();
        if($result){
            return $this->json('1','设置成功!');
        }else{
            return $this->json('0','提交失败!');
        }
    }

    /**
     * 查看自己的微信信息
     */
    public function info(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $config = new ConfigModel();
        $price = $config->get_value("see_wechat_price");

        $userProfile = UserProfileModel::select(['at_user_profile.wechat','at_user_account.wechat_income'])->where("at_user_profile.user_id",$user_id)
            ->leftJoin('at_user_account','at_user_account.user_id','=','at_user_profile.user_id')
            ->first();
        if($userProfile){

            $userProfile = $userProfile->toArray();
            $userProfile['price'] = $price;
            return $this->json('1','获取成功',$userProfile);
        }else{
            return $this->json('0','提交失败!');
        }
    }

    /**
     * 微信
     * model =1  谁查看我的微信
     * model=2 已查看的微信
     */
    public function see_list(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $model = $request->input('model'); //1  谁查看我的微信   2 已查看的微信
        if(!in_array($model,array(1,2))){
            return $this->json('0','参数输入有误!');
        }
        if($model == 1){
            $list = WechatSeeModel::select(['at_user_profile.user_id','at_user_profile.avatar','at_user_profile.nick_name'])->where("at_wechat_see.cover_user_id",$user_id)
                ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_wechat_see.see_user_id")
                ->get();
        }elseif($model == 2){
            $list = WechatSeeModel::select(['at_user_profile.user_id','at_user_profile.avatar','at_user_profile.nick_name','at_user_profile.wechat'])->where("at_wechat_see.see_user_id",$user_id)->leftJoin("at_user_profile","at_user_profile.user_id","=","at_wechat_see.cover_user_id")
                ->get();
        }
        if($list){
            $list = $list->toArray();
            foreach($list AS &$item){
                $item["avatar"] = UtilFunction::getUrlImg($item["avatar"]);
                if($model == 1){
                    $result = UserComponent::checkFriend($item["user_id"],$user_id);
                    if($result){
                        $item["is_friend"] = "已是好友";
                    }else{
                        $item["is_friend"] = "不是好友";
                    }
                }
            }
            if($list){
                return $this->json('1','获取成功',$list);
            }else{
                return $this->json('1','数据为空!',array());
            }

        }else{
            return $this->json('1','数据为空!',array());
        }

    }

}