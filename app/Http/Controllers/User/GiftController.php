<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use App\Http\Components\RongCloudComponent;

use DB;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use App\Models\UserGiftModel;

use App\library\UtilFunction;
/*
* 我的礼物
*/
class GiftController extends UserController{

    public function __construct(){
        parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->gift($request);
            }catch(\Exception $e){
                return $this->json('0',$e->getMessage() );
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 我的礼物
    * @param $user_id 用户id
    * @return 
   */
    public function gift(Request $request){
        // 检验当前登录是否有效
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $list = DB::select("select gift_id,gift_title,gift_picture,COUNT(*) AS gift_num from at_user_gift where `receive_user_id`=".$user_id." GROUP BY gift_id");
        foreach($list AS &$uitem){
            $uitem = (array)$uitem;
            $uitem["gift_picture"] = UtilFunction::getUrlImg($uitem["gift_picture"]);
        }

        if($list){

            return $this->json('1','获取成功',$list);
        }else{
            return $this->json('0','修改数据失败');
        }
    }
}
