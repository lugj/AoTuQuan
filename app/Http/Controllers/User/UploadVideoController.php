<?php
namespace App\Http\Controllers\User;

use App\library\UtilFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use App\library\UploadFile;

/**
 * 上传视频
 */
class UploadVideoController extends UserController{

    public function __construct(){
        parent::__construct();        
    }

    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->upload_video($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 上传视频
     * @param $coder_id 用户id
     * @return 
     */
    public function upload_video(Request $request){
        $video_type_list = array("video_chat","circle");
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $video_type = $request->input('video_type');    //类型

        if(!in_array($video_type,$video_type_list)){
            return $this->json('0','视频类型失败');
        }

        $video =  $request->input('video');
        if(empty($video)){
            return $this->json('0','视频文件为空!');
        }

        $video_url = UtilFunction::uploadFile("video",$video_type,$video);
        if($video_url){
            $video_url = UtilFunction::getUrlImg($video_url);
            return $this->json('1','上传成功',$video_url);
        }else{
            return $this->json('0','上传失败');
        }
    }
}
