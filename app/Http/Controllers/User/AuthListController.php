<?php
namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use DB;
use App\Models\UserModel;
use App\Models\UserProfileModel;
/**
 * 认证列表
 */
class AuthListController extends UserController{
    private $register_url = "/api/user/register"; //注册
    public function __construct(){
        parent::__construct();
    }

    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->auth_list($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 认证列表
     */
    private function auth_list(Request $request)
    {
        $result = array(
            "phone_auth"=>1,
        );
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $user_profile= UserProfileModel::where('user_id',$user_id)->first();
        if($user_profile){
            $result["id_info_auth"] = $user_profile->id_info_auth;
            $result["video_auth"] = $user_profile->video_auth;
            $result["car_auth"] = $user_profile->car_auth;
            $result["avatar_auth"] = $user_profile->avatar_auth;
            $result["id_info_auth"] = $user_profile->id_info_auth;
            $result["deposit_auth"] = $user_profile->deposit_auth;
            return $this->json('1','列表获取成功',$result);
        }else{
            return $this->json('0','无法查询到对应用户信息');
        }



    }



    
}
