<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\SuggestModel;
use App\Models\UserProfileModel;
use App\Models\VisitModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 意见反馈
 */
class SuggestController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
//                if (version_compare($appversion, '1.0') > 0) {
//                    return $this->identify_user_haoservice($request);
//                }else{
                    return $this->suggest($request);
               // }
            
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 意见反馈
     * @param Request $request
     * @author paulLu
     */
    public function suggest(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $model = $request->input('model');  //反馈类型
        $content = $request->input('content');  //反馈内容
        $pictures = $request->input('pictures');  //反馈类型
        if(empty($model)){
            return $this->json('0','反馈类型不能为空');
        }
        if(empty($content)){
            return $this->json('0','反馈内容不能为空');
        }
        if(!empty($pictures)){
            $pictures = explode(',',$pictures);
            foreach($pictures AS &$pitem){
                $pitem = UtilFunction::getRootImg($pitem);
            }
            $pictures = implode(',',$pictures);
        }

        $suggest = new SuggestModel();
        $suggest->user_id = $user_id;
        $suggest->model = $model;
        $suggest->content = $content;
        $suggest->pictures = $pictures;
        $result = $suggest->saveOrFail();
        if($result){
            return $this->json('1','保存成功，感谢您的反馈');
        }else{
            return $this->json('0','操作失败，请稍后重试');
        }


    }

}