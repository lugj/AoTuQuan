<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserCarModel;
use App\Models\UserProfileModel;
use App\Models\VisitModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 我看过的人/谁看过我
 */
class VisitController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
//                if (version_compare($appversion, '1.0') > 0) {
//                    return $this->identify_user_haoservice($request);
//                }else{
                    return $this->visit($request);
               // }
            
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 我看过的人/谁看过我
     * @param Request $request
     * @author paulLu
     */
    public function visit(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $is_card = 1;
        $model = $request->input('model');  //1 来访者  2我看过的人
        if(!in_array($model,array(1,2))){
            return $this->json('0','类别输入有误');
        }
        $userProfile = UserProfileModel::where("user_id",$user_id)->first();
        if($userProfile->card_level == 0){
            $is_card = 0;
        }else{
            $is_card = 1;
        }

        if($model == 1){
            if($userProfile->card_level == 0){
               $list = array();
            }else{
                $list = VisitModel::select(['at_user_profile.user_id','at_user_profile.avatar','at_user_profile.nick_name','at_user_profile.gender','at_user_profile.age','at_user_profile.gender','at_user_profile.card_level','at_visit.create_time'])->where("at_visit.user_id",$user_id)
                    ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_visit.visit_user_id")->get();
                $list  = $list->toArray();
            }
        }else{
            $list = VisitModel::select(['at_user_profile.user_id','at_user_profile.avatar','at_user_profile.nick_name','at_user_profile.gender','at_user_profile.age','at_user_profile.gender','at_user_profile.card_level','at_visit.create_time'])->where("at_visit.visit_user_id",$user_id)
                ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_visit.user_id")->get();
            $list  = $list->toArray();
        }

        foreach($list AS &$item){
            $item["avatar"] =UtilFunction::getUrlImg($item["avatar"]);
            if(date("Ymd") == date("Ymd",$item["create_time"])){
                $item["visit_time"] = "今天 ".date("H:i",$item["create_time"]);
            }else{
                $item["visit_time"] = date("Y-m-d H:i",$item["create_time"]);
            }
        }
        $result = array();
        $result["is_card"] = $is_card;
        $result["list"] = $list;
        return $this->json('1','获取成功',$result);

    }

}