<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserIdInfoModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 身份证认证
 */
class AuthIdInfoController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
//                if (version_compare($appversion, '1.0') > 0) {
//                    return $this->identify_user_haoservice($request);
//                }else{
                    return $this->auth_id_info($request);
               // }
            
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 实名认证
     * @param Request $request
     * @author paulLu
     */
    public function auth_id_info(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $number = $request->input('id_number');
        $id_positive = $request->input('id_positive'); //身份证正面
        $id_hold = $request->input('hold_url'); //身份证反面

        if(strlen($number)!=18){
            return $this->json('0','请填写正确的身份证号码');
        }
        if(empty($id_positive)){
            return $this->json('0','请上传您的身份证正面');
        }
        if(empty($id_hold)){
            return $this->json('0','请上传您的手持身份证');
        }

        $profile = UserProfileModel::firstOrNew(['user_id'=>$user_id]);
        if($profile && $profile->id_info_auth == 1){
            return $this->json('0','您已经身份证认证成功，无需提交');
        }

        $userIdInfo = UserIdInfoModel::where("user_id",$user_id)->first();
        if($userIdInfo && $userIdInfo->status == 0){
            return $this->json('0','您提交的身份证认证正在审核，请耐心等待');
        }
        if(!$userIdInfo){
            $userIdInfo = new UserIdInfoModel();
            $userIdInfo->user_id = $user_id;
        }
        $userIdInfo->id_number = $number;
        $userIdInfo->positive_url =  UtilFunction::getRootImg($id_positive);
        $userIdInfo->hold_url = UtilFunction::getRootImg($id_hold);
        $userIdInfo->status = 0;
        if($userIdInfo->status == 2){
            $userIdInfo->updated_at = date("Y-m-d H:i:s");
        }
        $result = $userIdInfo->saveOrFail();
        if($result){
            return $this->json('1','提交成功，请等待审核!');
        }else{
            return $this->json('0','提交失败!');
        }
    }

}