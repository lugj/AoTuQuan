<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;;
use App\Models\ConfigModel;
use App\Models\UserAccountModel;
use App\Models\UserProfileModel;
use App\Models\CardPackageModel;
use App\Models\CardBuyModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 会员
 */
class CardController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{

                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 查看会员充值信息
     */
    public function buy_show(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $result = array();
        $userProfile = UserProfileModel::select(['avatar','nick_name','card_level','card_endtime'])->where("user_id",$user_id)->first()->toArray();
        if($userProfile["card_level"]>0){
            $plus_day = ceil(($userProfile["card_endtime"]-strtotime(date("Y-m-d 23:59:59")))/86400);
            $plus_day = $plus_day<=0?0:$plus_day;
            $userProfile['plus_day'] = $plus_day;
            $userProfile['card_endtime'] = date("Y-m-d",$userProfile['card_endtime']);
        }
        $userProfile["avatar"] = UtilFunction::getUrlImg($userProfile["avatar"]);
        $result["user"] = $userProfile;
        $package = CardPackageModel::get()->toArray();
        $result["packagelist"] = $package;

        return $this->json('1','获取成功!',$result);
    }

    /**
     * 购买会员
     */
    public function buy(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $package_id = $request->input('package_id');
        $card_auto = $request->input('card_auto');  //等于1 为会员到期自动续费
        if(empty($package_id)){
            return $this->json('0','请选择需要购买的会员套餐');
        }
        $package = CardPackageModel::where("id",$package_id)->first();
        if(!$package){
            return $this->json('0','该套餐不存在');
        }

        $userAccount = UserAccountModel::where("user_id",$user_id)->first()->toArray();
        if($userAccount["balance"]<$package->price){
            return $this->json('0','余额不足，购买失败!');
        }

        $userProfile = UserProfileModel::where("user_id",$user_id)->first();

        $userProfile->card_level = $package->level;
        if($userProfile->card_endtime>time()){
            $start_time = $userProfile->card_endtime;
        }else{
            $start_time = time();
        }

        $end_time = strtotime(date('Y-m-d 23:59:59',$start_time).'+'.$package->month.' month');
        $cardBuy = new CardBuyModel();
        $cardBuy->user_id = $user_id;
        $cardBuy->card_package_id = $package_id;
        $cardBuy->month = $package->month;
        $cardBuy->card_level = $package->level;
        $cardBuy->price = $package->price;
        $cardBuy->source = 1;
        $cardBuy->card_endtime = $end_time;
        $result = $cardBuy->saveOrFail();


        if($result){
            $userProfile->card_endtime = $end_time;
            if($card_auto == 1){
                $userProfile->card_auto = $card_auto;
            }
            $result = $userProfile->saveOrFail();
            UserComponent::price_save('buy_card',$user_id,$package->price,-1);
            return $this->json('1','购买成功');
        }else{
            return $this->json('0','购买失败，请稍后再试');
        }
    }



}