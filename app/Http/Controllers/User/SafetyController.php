<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserAccountModel;
use App\Models\UserProfileModel;
use App\Models\UserSettingModel;
use App\Models\AccountRecordsModel;
use App\Models\JobOrderModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 我是保安员
 */
class SafetyController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{

                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 我是保安员列表
     */
    public function data(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $page_index  = intval($request->input('page_index'))?intval($request->input('page_index')):1;
        $limit = 10;
        $offest = ($page_index-1) * $limit;
        $list = array();
        $userSetting = UserSettingModel::where("user_id",$user_id)->first()->toArray();
        $jobOrder = JobOrderModel::select([
            'id',
            'user_id',
            'job_user_id',
            'job_class_id',
            'job_class_name',
            'job_model',
            'unit_price',
            'service_time',
            'start_time',
            'status',
            'safety_status',
            'address',
            'lat',
            'lon',
        ])->where("safety_user_id",$user_id)->skip($offest)->take($limit)->orderBy('create_time', 'ASC')->get()->toArray();
        foreach($jobOrder AS $oitem){
            if($oitem["job_model"] == 1){
                $pro_user_id = $oitem["job_user_id"];
            }else{
                $pro_user_id = $oitem["user_id"];
            }
            $userProfile = UserProfileModel::select([
                'at_user_profile.avatar',
                'at_user_profile.nick_name',
                'at_user_profile.gender',
                'at_user_profile.age',
                'at_user_profile.id_info_auth',
                'at_user_profile.video_auth',
                'at_user_profile.car_auth',
                'at_user_profile.face_auth',
                'at_user_setting.lat',
                'at_user_setting.lon',
            ])
                ->leftJoin("at_user_setting","at_user_setting.user_id","=","at_user_profile.user_id")->where("at_user_profile.user_id",$pro_user_id)->first()->toArray();
            $userProfile["avatar"] = UtilFunction::getUrlImg($userProfile["avatar"]);
            $userProfile["phone_auth"] = 1;
            $userProfile["id"] = $oitem["id"];
            $userProfile["job_class_name"] = $oitem["job_class_name"];
            $userProfile["start_time"] = $oitem["start_time"];
            $userProfile["address"] = $oitem["address"];
            $userProfile["safety_status"] = $oitem["safety_status"];
            $userProfile["distance"] = UtilFunction::get_distance($userSetting["lat"],$userSetting["lon"],$oitem["lat"],$oitem["lon"]);
            $list[] = $userProfile;
        }
        return $this->json('1','获取成功!',$list);
    }

    /**
     * 确认保驾
     */
    public function confirm(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $order_id = $request->input('order_id');
        if(empty($order_id)){
            return $this->json('0','请先传入订单号');
        }
        $order = JobOrderModel::where("id",$order_id)->first();
        if(!$order){
            return $this->json('0','订单不存在');
        }
        if(!in_array($order->status,array(0,1))){
            return $this->json('0','订单状态有误，无法确认保驾');
        }
        if($order->safety_status != 0 && $order->safety_user_id == $user_id){
            return $this->json('0','状态有误');
        }
        $order->safety_status = 1;
        $result = $order->saveOrFail();
        if($result){
            return $this->json('1','操作成功');
        }else{
            return $this->json('0','操作失败');
        }
    }

    /**
     * 取消
     */
    public function cancel(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $order_id = $request->input('order_id');
        if(empty($order_id)){
            return $this->json('0','请先传入订单号');
        }
        $order = JobOrderModel::where("id",$order_id)->first();
        if(!$order){
            return $this->json('0','订单不存在');
        }
        if($order->safety_status != 0 && $order->safety_user_id == $user_id){
            return $this->json('0','状态有误');
        }
        $order->safety_status = 2;
        $result = $order->saveOrFail();
        if($result){
            return $this->json('1','操作成功');
        }else{
            return $this->json('0','操作失败');
        }
    }



}