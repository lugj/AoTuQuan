<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;;
use App\Models\ConfigModel;
use App\Models\UserAccountModel;
use App\Models\UserProfileModel;
use App\Models\RechargeSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 充值
 */
class RechargeController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{

                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 金额
     */
    public function price_list(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $result = array();
        $userProfile = UserAccountModel::where("user_id",$user_id)->first()->toArray();
        $balance = $userProfile["balance"];
        $list = RechargeSettingModel::select(['id','picture','gold','desc','price','desc_sign'])->orderBy('sort', 'asc')->get()->toArray();
        foreach($list AS &$item){
            $item["picture"] = UtilFunction::getUrlImg($item["picture"]);
        }
        $result["balance"] = $balance;
        $result["list"] = $list;
        return $this->json('1','获取成功!',$result);
    }

    /**
     * 购买会员
     */
    public function buy(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

    }



}