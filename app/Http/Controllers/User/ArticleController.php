<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\ArticleModel;
use App\Models\ConfigModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 文章-使用手册
 */
class ArticleController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{

                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 使用手册列表
     */
    public function handbook_list(Request $request){
        $list = ArticleModel::select(['id','title'])->where("model",1)->orderBy('sort', 'ASC')->get()->toArray();
        return $this->json('1','获取成功!',$list);

    }

    /**
     * 详情页
     */
    public function detail(Request $request){
        $id = $request->input('id');
        $item = ArticleModel::select(['id','title','content'])->where("id",$id)->first()->toArray();
        return $this->json('1','获取成功!',$item);

    }



}