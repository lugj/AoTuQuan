<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;
use App\Http\Components\RongCloudComponent;

use DB;
use App\Models\UserModel;
use App\Models\UserProfileModel;

use App\library\UtilFunction;
/*
* 修改用户基本信息
*/
class ProfileModifyController extends UserController{

    public function __construct(){
        parent::__construct();
    }
    
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->profile_modify($request);
            }catch(\Exception $e){
                return $this->json('0',$e->getMessage() );
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /*
    * 修改（新增）用户基本信息
    * @param $user_id 用户id
    * @return 
   */
    public function profile_modify(Request $request){
        // 检验当前登录是否有效
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        /*获取参数*/
        $param_name = $request->input('param_name');
        $param_value = $request->input('param_value');

        /*校验参数*/
        // 允许的修改参数名
        $allow_modify = array(
            'nick_name',
            'age',
            'height',
            'weight',
            'gender',
        );
        if (!$param_name || !in_array($param_name, $allow_modify)) {
            return $this->json('0','修改参数错误');
        }

        if(empty($param_value)){
            if ($param_name == 'nick_name') {
                return $this->json('0','请填写昵称！');
            }elseif ($param_name == 'height') {
                return $this->json('0','请填写身高！');
            }elseif ($param_name == 'weight') {
                return $this->json('0','请填写体重！');
            }elseif($param_name == 'age') {
                return $this->json('0','请填写年龄！');
            }elseif($param_name == 'gender'){
                 return $this->json('0','请选择性别！');
            }
        }

        switch ($param_name) {
            case 'nick_name':
                $param_value = trim($param_value);
                break;
            case 'weight':
            case 'height':
                if(!(is_numeric($param_value)&& $param_value>0&&$param_value<999)){
                    return $this->json('0','请填写0-999的数字');
                }
                break;
            case 'gender':
                if(!in_array($param_value,array(1,2))){
                    return $this->json('0','性别输入有误!');
                }
                break;
            default:
                break;
        }


        $userProfile = UserProfileModel::where('user_id',$user_id)->first();
        if (!$userProfile) {
            $userProfile = new UserProfileModel();
            $userProfile->user_id = $user_id;
        }

        $userProfile->$param_name = $param_value;

        if($userProfile->saveOrFail()){

           // $return_data = UserProfileModel::where('user_id',$user_id)->first()->toArray();

            if ($param_name == 'nick_name') {
                $avatar = UtilFunction::getUrlImg($userProfile->avatar);
                $nick_name = $param_value;

                RongCloudComponent::update($user_id,$nick_name,$avatar);
            }

            return $this->json('1','修改成功');
        }else{
            return $this->json('0','修改数据失败');
        }
    }
}
