<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use DB;
use App\Models\UserModel;
use App\Models\UserSettingModel;

/**
 * 退出登录接口
 */
class LogoutController extends UserController{

    public function __construct(){
        parent::__construct();        
    }

    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->logout($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    public function logout(Request $request){
        $user_id = $request->input('user_id');
        $logout = UserModel::find($user_id);
        if ($logout) {
            $logout->app_login_time = '';
            $logout->saveOrFail();
        }
        return $this->json('1','已退出登录');
    }
    
}
