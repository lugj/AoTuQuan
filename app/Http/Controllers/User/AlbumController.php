<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\AlbumModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 相册
 */
class AlbumController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{

                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 保存这批上传的照片
     * @param Request $request
     * @author paulLu
     */
    public function save(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $picture = $request->input('picture');  //图片
        $model = $request->input('model');  //1为私密照
        if(empty($picture)){
            return $this->json('0','上传照片不能为空!');
        }
        if($model!=1){
            $model = 0;
        }

        $all_num = AlbumModel::where("user_id",$user_id)->whereIn("status",array(0,1))->count();
        if(($all_num+1)>12){
            return $this->json('0','我的相册最多只能有12张照片!');
        }
        $all_sm_num = AlbumModel::where("user_id",$user_id)->where("model",1)->whereIn("status",array(0,1))->count();
        if($model == 1 && $all_sm_num+1>4){
            return $this->json('0','私密照片最多只能上传4张!');
        }
        $picture_url = UtilFunction::uploadFile("picture","album",$picture);
        if(!$picture_url){
            return $this->json('0','图片上传失败!');
        }
        $album = new AlbumModel();
        $album->user_id = $user_id;
        $album->model = $model;
        $album->picture = $picture_url;
        $album->saveOrFail();

        return $this->json('1','保存成功!',UtilFunction::getUrlImg($picture_url));
    }

    /**
     * 查看图片
     */
    public function show(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $album_list = AlbumModel::select(['id','picture','model'])->where("user_id",$user_id)->whereIn("status",array(0,1))->get();
        if($album_list){
            $album_list = $album_list->toArray();
            foreach($album_list AS &$aitem){
                $aitem["picture"] = UtilFunction::getUrlImg($aitem["picture"]);
            }
        }else{
            $album_list = array();
        }
        return $this->json('1','获取成功!',$album_list);
    }

    /**
     * 删除
     */
    public function delete(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $id  = $request->input('id');
        $result =  AlbumModel::where("user_id",$user_id)->where("id",$id)->delete();
        if($result){
            return $this->json('1','删除成功!');
        }else{
            return $this->json('0','删除失败!');
        }

    }

}