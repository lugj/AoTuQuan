<?php
namespace App\Http\Controllers\User;

use App\library\UtilFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Components\UserComponent;

use App\library\UploadFile;

/**
 * 上传图片
 */
class UploadPictureController extends UserController{

    public function __construct(){
        parent::__construct();        
    }

    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->upload_picture($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 上传图片
     * @param $coder_id 用户id
     * @return 
     */
    public function upload_picture(Request $request){
        $picture_type = array("avatar",'car','id_info','video_chat','circle','suggest','complaint');
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $type = $request->input('picture_type');    //类型

        if(!in_array($type,$picture_type)){
            return $this->json('0','图片类型失败');
        }

        $picture =  $request->input('picture');
        if(empty($picture)){
            return $this->json('0','图片文件为空!');
        }

        $picture_url = UtilFunction::uploadFile("picture",$type,$picture);
        if($picture_url){
            $picture_url = UtilFunction::getUrlImg($picture_url);
            return $this->json('1','上传成功',$picture_url);
        }else{
            return $this->json('0','上传失败');
        }
    }
}
