<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserCarModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 车辆认证信息查看
 */
class AuthCarShowController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
//                if (version_compare($appversion, '1.0') > 0) {
//                    return $this->identify_user_haoservice($request);
//                }else{
                    return $this->auth_car_show($request);
               // }
            
            }catch(\Exception $e){
                return $this->json('0','系统错误');
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 车辆认证
     * @param Request $request
     * @author paulLu
     */
    public function auth_car_show(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];


        $userCar = UserCarModel::where("user_id",$user_id)->first();
        if(!$userCar){
            return $this->json('0','您尚未提交车辆信息');
        }
        if($userCar){
            $userCar = $userCar->toArray();
            return $this->json('1','获取成功',$userCar);
        }
    }

}