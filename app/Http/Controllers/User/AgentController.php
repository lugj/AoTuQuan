<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserAccountModel;
use App\Models\UserProfileModel;
use App\Models\UserSettingModel;
use App\Models\AccountRecordsModel;
use App\Models\JobOrderModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 我是代理商
 */
class AgentController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{

                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 我是代理商
     */
    public function data(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $result = array();
        $userAccount = UserAccountModel::where("user_id",$user_id)->first()->toArray();
        $userProfile = UserProfileModel::where("user_id",$user_id)->first()->toArray();
        if($userProfile["is_agent"] != 1){
            return $this->json('0','您还不是代理商!');
        }
        $result["agent_income"] = round($userAccount["agent_income"]/100,2);
        $accontRecords = AccountRecordsModel::where("user_id",$user_id)->where("source",9)->get()->toArray();
        $today_order_num = 0;
        $today_income = 0;
        $month_order_num = 0;
        $month_income = 0;
        foreach($accontRecords AS $aitem){
            $create_time = strtotime($aitem["create_time"]);
            $start_month_time = strtotime(date("Y-m-01 00:00:00"));
            $end_month_time = strtotime(date("Y-m-01 00:00:00")." +1 month")-1;
            if($create_time<=$end_month_time && $create_time>=$start_month_time){
                $month_income = $month_income+$aitem["price"];
                $month_order_num++;
                $start_today_time = strtotime(date("Y-m-d 00:00:00"));
                $end_today_time = strtotime(date("Y-m-d 23:59:59"));
                if($create_time<=$end_today_time && $create_time>=$start_today_time){
                    $today_income = $today_income+$aitem["price"];
                    $today_order_num++;
                }
            }

        }

        $result["today_order_num"] = $today_order_num;
        $result["today_income"] = round($today_income/100,2);
        $result["month_order_num"] = $month_order_num;
        $result["month_income"] = round($month_income/100,2);
        $result["area"] = $userProfile["agent_province_name"].$userProfile["agent_city_name"].$userProfile["agent_district_name"];
        return $this->json('1','获取成功!',$result);
    }




}