<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\CollectionModel;
use App\Models\UserCarModel;
use App\Models\UserProfileModel;
use App\Models\UserSettingModel;
use App\Models\VisitModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 收藏
 */
class CollectionController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{

                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 收藏列表
     * @param Request $request
     * @author paulLu
     */
    public function data(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $list = CollectionModel::select(['at_job.id','at_user_profile.user_id','at_user_profile.avatar','at_user_profile.nick_name','at_user_profile.age','at_user_profile.gender','at_job.job_class_name','at_user_setting.lon','at_user_setting.lat'])
            ->where("at_collection.user_id",$user_id)
            ->leftJoin("at_job","at_collection.job_id","=","at_job.id")
            ->leftJoin("at_user_setting","at_user_setting.user_id","=","at_job.user_id")
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_job.user_id")->get()->toArray();
        if($list){
            $userSetting = UserSettingModel::where("user_id",$user_id)->first()->toArray();
            foreach($list AS &$item){
                $distance = UtilFunction::get_distance($userSetting['lat'],$userSetting['lon'],$item['lat'],$item['lon']);
                $item["distance"] = $distance;
                $item["avatar"] = UtilFunction::getUrlImg($item["avatar"]);
                unset($item["lat"]);
                unset($item["lon"]);
            }
        }

        return $this->json('1','获取成功',$list);

    }

    /**
     * 取消收藏
     */
    public function cancel(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $job_id = $request->input('job_id');
        if(empty($job_id)){
            return $this->json('0','参数有误');
        }
        $result = CollectionModel::where("job_id",$job_id)->where("user_id",$user_id)->delete();
        if($result){
            return $this->json('1','取消成功');
        }else{
            return $this->json('0','取消失败');
        }
    }

}