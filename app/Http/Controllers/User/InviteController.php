<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserCarModel;
use App\Models\UserProfileModel;
use App\Models\VisitModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 推广赚钱
 */
class InviteController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
//                if (version_compare($appversion, '1.0') > 0) {
//                    return $this->identify_user_haoservice($request);
//                }else{
                    return $this->invite($request);
               // }
            
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 推广赚钱
     * @param Request $request
     * @author paulLu
     */
    public function invite(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $result = array();
        $userProfile = UserProfileModel::where("user_id",$user_id)->first()->toArray();
        $result["invite_code"] = $userProfile["invite_code"];   //推荐码
        $master_num_list = $this->_getLevelNum(array($user_id));

        $result = array_merge($result,$master_num_list);

        $all_level_num = $master_num_list["level1_num"]+$master_num_list["level2_num"]+$master_num_list["level3_num"];
        $result["all_level_num"] = $all_level_num;

        if($all_level_num>0){
            $result["level1_per"] = floor($master_num_list["level1_num"]/$all_level_num*100);
            $result["level2_per"] = floor($master_num_list["level2_num"]/$all_level_num*100);
            $result["level3_per"] = 100-$result["level1_per"]-$result["level2_per"];
        }else{
            $result["level1_per"] = 100;
            $result["level2_per"] = 0;
            $result["level3_per"] = 0;
        }

        return $this->json('1','获取成功',$result);

    }

    /**
     * 获取用户下级人数
     */
    public function _getLevelNum($user_arr,$level = 1,$result = array()){
        if($level>3){
            return $result;
        }

        $level_num = UserProfileModel::whereIn("master_id",$user_arr)->count();

        if($level_num){
            $result["level".$level."_num"] = $level_num;
            $level_list = UserProfileModel::select(['user_id'])->whereIn("master_id",$user_arr)->get()->toArray();
            $user_list = array_column($level_list,"user_id");
            $result = $this->_getLevelNum($user_list,$level+1,$result);
            return $result;
        }else{
            for($i=$level;$i<=3;$i++){
                $result["level".$i."_num"] = 0;
            }
            return $result;
        }
    }

}