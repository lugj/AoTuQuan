<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\User;

use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\CollectionModel;
use App\Models\UserCarModel;
use App\Models\UserProfileModel;
use App\Models\UserSettingModel;
use App\Models\BlacklistModel;
use App\Models\VisitModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 黑名单列表
 */
class BlackListController extends UserController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{

                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 黑名单列表
     * @param Request $request
     * @author paulLu
     */
    public function data(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $list = BlacklistModel::select(['at_user_profile.user_id','at_user_profile.avatar','at_user_profile.nick_name','at_user_profile.age','at_user_profile.gender'])
            ->where("at_blacklist.user_id",$user_id)
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_blacklist.user_id")->get()->toArray();
        foreach($list AS &$item){
            $item["avatar"] = UtilFunction::getUrlImg($item["avatar"]);
        }
        return $this->json('1','获取成功',$list);

    }

    /**
     * 取消拉黑
     */
    public function cancel(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $black_user_id = $request->input('black_user_id');
        if(empty($black_user_id)){
            return $this->json('0','参数有误');
        }
        $result = BlacklistModel::where("user_id",$user_id)->where("black_user_id",$black_user_id)->delete();
        if($result){
            return $this->json('1','取消成功');
        }else{
            return $this->json('0','取消失败');
        }
    }

}