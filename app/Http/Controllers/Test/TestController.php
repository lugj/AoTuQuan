<?php
/** 测试用的controller */

namespace App\Http\Controllers\Test;

use App\Http\Components\CorpsComponent;
use App\Http\Components\RedisComponent;
use App\library\juhe\Weather;
use App\Models\CorpsModel;
use App\Models\JudgeModel;
use App\Models\MatchModel;
use App\Models\MatchRequestModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\UserModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

use App\Http\Components\RongCloudComponent;

/*
* 测试接口
* 1、测试接口的界面  index
* 2、过验证  get_sign
* 参数接口*****************************
* 3、测试获取全部参数  param_allparam_all
* 4、测试参数模糊查询  param_fuzzy
* 用户接口*****************************
* 5、测试注册  user_register
* 6、测试找回密码  user_forget
* 7、测试修改密码  user_password_modify
* 8、测试获取用户基本信息  user_base_info
* 9、测试修改（新增）用户基本信息  user_base_info_modify
* 10、测试获取用户详细信息  user_detail
* 11、测试登录  user_login
* 12、测试退出登录  user_logout
* 13、测试获取短信验证  user_sms_check_code
* 14、测试获取用户设置信息  user_setting
* 15、测试修改用户设置信息  user_setting_modify
* 账单接口*****************************
* 16、测试获取用户当前账户（支付宝）  account_pay
* 17、测试修改用户设置信息  account_pay_modify
* 极客接口*****************************
* 18、测试获取当前用户极客信息  geek_info
* 19、测试修改当前用户极客信息  geek_info_modify
* 需求接口*****************************
* 20、测试获取极客信息列表  geek_random_list
* 21、修改当前用户需求信息 demand_info_modify
* 预约接口*****************************
* 21、测试提交预约接口 bespeak_send_to
* 22、测试获取我的预约列表 bespeak_for_boss
* 23、测试获取预约我的列表 bespeak_for_geek
* 辅助接口*****************************
* 24、HTTP POST Request  post_url
*/
class TestController extends Controller
{
    public $with_html = false;
    public $with_header = false;
    public $with_array = false;
    public $with_server = false;
    public $all_params = array();

    // public $api_host = 'http://api.test.looip.com';
    public $api_host = 'http://luman.com';


    public function __construct(Request $request){
        $this->all_params = $request->all();
        $view = $request->input('view');
        switch ($view) {
            case 'html':
                $this->with_html = true;
                break;
            case 'header':
                $this->with_header = true;
                break;
            case 'array':
                $this->with_array = true;
                break;
            case 'all':
                $this->with_html = true;
                $this->with_header = true;
                $this->with_array = true;
                $this->with_server = true;
                break;
        }

        $this->api_host = "http://".$_SERVER['HTTP_HOST'];
        // $this->get_sign();
    }

    public function invoke($function,Request $request){
        if (method_exists($this, $function)) {
            return call_user_func_array(array($this,$function), array($request));
        }else{
            echo "function does not exists";
        }
    }

    /*测试接口的界面*/
    public function index(Request $request){
        $arrlist = array(
            '页面接口' => array(
                '获取首页数据|/api/page/home' =>array('user_id','city_name'),
                '获取战队首页数据|/api/page/corps_home' =>array('user_id'),
                '获取首页天气数据|/api/page/weather' =>array('user_id','city_name'),
                '反馈|/api/page/feedback' =>array('user_id','content'),
            ),
            '用户接口' => array(
                '获取短信验证码|/api/user/sms_check_code' =>array(
                    'phone','code_type'
                    ),
                '用户注册|/api/user/register' =>array(
                    'username','password','check_code'
                    ),
                '用户登录|/api/user/login' =>array(
                    'username','password'
                    ),
                '用户授权登录|/api/user/login_auth' =>array(
                    'auth_type','open_id','access_token','nick_name','avatar'
                    ),
                '找回密码|/api/user/forget' =>array(
                    'username','check_code','password','confirmpass'
                    ),
                '退出登录|/api/user/logout' =>array(
                    'user_id'
                    ),
                '获取用户基本信息|/api/user/base_info' =>array(
                    'user_id',
                    'profile_id'
                    ),
                '获取用户信息|/api/user/profile' =>array(
                    'user_id'
                    ),
                '修改手机|/api/user/phone_modify' =>array(
                    'user_id',
                    'phone',
                    'check_code',
                    'password',
                    'password_confirm'
                    ),
                '修改密码|/api/user/password_modify' =>array(
                    'user_id',
                    'oldpass','password','confirmpass'
                    ),
                '上传头像|/api/user/upload_avatar'=>array(
                    'user_id',
                    'file'
                    ),
                '上传地理位置|/api/user/upload_location'=>array(
                    'user_id',
                    'lon','lat','city'
                    ),
                '修改用户信息|/api/user/profile_modify' =>array(
                    'user_id',
                    'param_name',
                    'param_value'
                    ),
                '获取用户设置信息|/api/user/setting' =>array(
                    'user_id'
                    ),
                '修改用户设置信息|/api/user/setting_modify' =>array(
                    'user_id',
                    'switch_sms','switch_bespeak','switch_push'
                    ),
                '上传身份证照片|/api/user/upload_id_photo'=>array(
                    'user_id',
                    'type','file'
                    ),
                '提交实名信息|/api/user/identify_user'=>array(
                    'user_id',
                    'real_name','id_number'
                    ),
                '上传职能证书|/api/user/upload_certification_photo'=>array(
                    'user_id',
                    'file'
                ),
                '提交职能认证申请|/api/user/certification_auth'=>array(
                    'user_id',
                    'certificate','sport','type','validity_start_time','validity_end_time'
                ),
                '职能认证列表|/api/user/certification_list'=>array(
                    'user_id'
                ),
                '用户主页|/api/user/user_info'=>array(
                    'user_id',
                    'target_id'
                ),
                '测试用户登录状态|/api/user/verify_login'=>array(
                    'user_id'
                ),
                '测试app更新|/api/user/verify_version'=>array(
                ),
                '完善资料|/api/user/improve_info'=>array(
                    'user_id'
                ),
                '通过手机号检验是否好友|/api/user/phone_check'=>array(
                    'user_id',
                    'phone'
                ),
                '检验是否需要完善资料|/api/user/profile_check'=>array(
                    'user_id',
                ),
                '任务|/api/user/mission'=>array(
                    'user_id',
                ),
                '任务完成分享|/api/user/mission_share'=>array(
                    'user_id',
                ),
                '任务签到|/api/user/mission_sign'=>array(
                    'user_id',
                ),
                '验证用户是否完善资料|/api/user/verify_user_info'=>array(
                    'user_id',
                    'target_id',
                ),
            ),
            '账单接口' => array(
                '获取账户余额|/api/account/balance' =>array(
                    'user_id',
                ),
                '获取账户绑定账号列表|/api/account/card_list' =>array(
                    'user_id',
                ),
                '绑定账户账号|/api/account/card_bind' =>array(
                    'user_id',
                    'card_number','card_name','type'
                ),
                '解绑账户账号|/api/account/card_unbind' =>array(
                    'user_id',
                    'card_id',
                ),
                '设置默认账号|/api/account/card_set_default' =>array(
                    'user_id',
                    'card_id',
                ),
                '提现申请|/api/account/withdrawal' =>array(
                    'user_id',
                    'card_id','pay_password','amount','body',
                ),
                '提现申请-信息|/api/account/withdrawal_info' =>array(
                    'user_id',
                ),
                '获取账户账单|/api/account/bill_list' =>array(
                    'user_id',
                    'page_index'
                ),
                // '获取账户账单详细|/api/account/bill_detail' =>array(
                //     'user_id',
                // ),
                // '退款申请|/api/account/refund' =>array(
                    // 'user_id',
                // ),
                '支付密码设置|/api/account/pay_password_modify' =>array(
                    'user_id',
                    'pay_password', 'pay_password_confirm', 'check_code'
                ),
                '支付密码设置短信|/api/account/pay_check_code' =>array(
                    'user_id',
                    'pay_password',
                    'pay_password_confirm',
                ),
                '充值|/api/account/recharge' =>array(
                    'user_id',
                    'channel','amount','body'
                ),
                '转账|/api/account/transfer' =>array(
                    'user_id',
                    'pay_password','amount','body','to_user_id','type',
                ),
                '支付测试|/api/account/pay' =>array(
                    'user_id',
                    'order_no','channel','amount','subject','body'
                ),
                '支付通知测试|/api/account/notify' =>array(
                    'input'
                ),
                '战队钱包余额|/api/account/corps_balance' =>array(
                    'user_id',
                    'corps_id',
                ),
                '战队钱包充值|/api/account/corps_recharge' =>array(
                    'user_id',
                    'corps_id',
                    'pay_password',
                    'amount',
                    'body',
                ),
                '战队钱包账单|/api/account/corps_bill_list' =>array(
                    'user_id',
                    'corps_id',
                ),
                '战队钱包提现|/api/account/corps_withdrawal' =>array(
                    'user_id',
                    'corps_id',
                    'amount',
                    'body',
                ),
            ),
            '实时约接口' => array(
                '上传实时约图片|/api/realtime/upload_realtime_album' =>array(
                    'user_id',
                    'file'
                ),
                '发布实时约找学员|/api/realtime/release_realtime' =>array(
                    'user_id',
                    'subject','sport','sport_authed','pick_up_type','hourly_pay','city','area','allow_start_time',
                    'allow_end_time','allow_gender','type','content','img_url1','img_url2','img_url3'
                ),
                '发布实时约找教练|/api/realtime/release_realtime_student' =>array(
                    'user_id',
                    'subject','sport','sport_authed','pick_up_type','city','area','start_time',
                    'time_long','allow_gender','type','content','img_url1','img_url2','img_url3'
                ),
                '获取实时约详情|/api/realtime/get_realtime_detail'=>array(
                    'user_id',
                    'publisher_id','realtime_id'
                )
            ),
            '战队接口' => array(
                '创建战队|/api/corps/create' =>array(
                    'user_id',
                    'badge','name','sport','slogan'
                ),
                '退出战队|/api/corps/quit' =>array(
                    'user_id',
                    'corps_id'
                ),
                '我的战队列表|/api/corps/my_corps_list' =>array(
                    'user_id',
                    'type',
                ),
                '战队详情|/api/corps/detail' =>array(
                    'user_id',
                    'corps_id',
                ),
                '战队详情修改|/api/corps/detail_modify' =>array(
                    'user_id',
                    'corps_id',
                    'param_name',
                    'param_value',
                ),
                '战队领队列表|/api/corps/leader_list' =>array(
                    'user_id',
                    'corps_id',
                ),
                '设置领队|/api/corps/set_leader' =>array(
                    'user_id',
                    'corps_id',
                    'leader_user_id',
                ),
                '战队添加好友到战队列表|/api/corps/member_list' =>array(
                    'user_id',
                    'corps_id',
                ),
                '战队添加好友到战队|/api/corps/member_add' =>array(
                    'user_id',
                    'corps_id',
                    'friend_user_id',
                ),
                '战队队员列表|/api/corps/team_list' =>array(
                    'user_id',
                    'corps_id',
                ),
                '申请成为队员|/api/corps/team_request' =>array(
                    'user_id',
                    'corps_id',
                ),
                '新申请的队员列表|/api/corps/team_request_list' =>array(
                    'user_id',
                    'corps_id',
                ),
                '战队同意队员申请|/api/corps/team_request_allow' =>array(
                    'user_id',
                    'corps_id',
                    'request_user_id'
                ),
                '删除相册图片|/api/corps/delete_album' =>array(
                    'album_id',
                ),
                '更新战队信息|/api/corps/update_detail' =>array(
                    'corps_id',
                ),
                '战队进度条|/api/corps/mission' =>array(
                    'corps_id',
                ),
                '战队赛事列表|/api/corps/match_list' =>array(
                    'user_id',
                    'corps_id','page'
                ),
                '战队列表|/api/corps/corps_list' =>array(
                    'user_id',
                    'page'
                ),
                '我的可管理战队列表|/api/corps/my_admin_corps_list' =>array(
                    'user_id',
                    'page'
                ),
                '球秘列表|/api/corps/secretary_list' =>array(
                    'user_id',
                    'page','corps_id'
                ),
                '添加球秘|/api/corps/add_secretary' =>array(
                    'user_id',
                    'corps_id','secretary_id'
                ),
            ),
            '约战接口' => array(
                '发起者召集令发布|/api/match/enroll_requester_public'=>array(
                    'user_id',
                    'corps_id', 'match_start_time', 'match_end_time', 'match_system', 'match_site_type', 'match_site_address', 'match_site_pay_type'
                    ),
                '响应者召集令发布|/api/match/enroll_responser_public'=>array(
                    'user_id',
                    'corps_id', 'request_id', 'match_site_address'
                    ),
                '召集令详情|/api/match/enroll_detail'=>array(
                    'user_id',
                    'enroll_id'
                    ),
                '报名召集令|/api/match/enroll_member'=>array(
                    'user_id',
                    'enroll_id'
                    ),
                '召集令报名者列表|/api/match/enroll_member_list'=>array(
                    'user_id',
                    'enroll_id'
                    ),
                '发布约战前置|/api/match/request_pre'=>array(
                    'user_id',
                    ),
                '发布约战|/api/match/request'=>array(
                    'user_id',
                    'enroll_id', 'request_member_ids', 'team_color', 'age_min', 'age_max', 'judge_require', 'judge_pay_type', 'request_amount', 'solgen','surance_id'
                    ),
                '约战列表|/api/match/request_list'=>array(
                    'user_id',
                    'page_index', 'order_status'
                    ),
                '约战详情|/api/match/request_detail'=>array(
                    'user_id',
                    'request_id'
                    ),
                '约战详情-比赛结果|/api/match/request_detail_finish_match'=>array(
                    'user_id',
                    'match_id'
                ),
                '约战详情-裁判|/api/match/request_detail_judge'=>array(
                    'user_id',
                    'match_id'
                ),
                '约战详情-参赛队员|/api/match/request_detail_members'=>array(
                    'user_id',
                    'match_id'
                ),
                '应战前置|/api/match/response_pre'=>array(
                    'user_id',
                    'request_id'
                    ),
                '应战|/api/match/response'=>array(
                    'user_id',
                    'enroll_id', 'response_member_ids', 'surance_id',
                    ),
                '挑战书列表|/api/match/challenge_list'=>array(
                    'user_id',
                    ),
                '挑战书详情|/api/match/challenge_detail'=>array(
                    'user_id',
                    'request_id','type'
                    ),
                '挑战书应战|/api/match/challenge_response'=>array(
                    'user_id',
                    'response_id','request_id'
                    ),
                '我的约战列表|/api/match/my_match_list'=>array(
                    'user_id',
                    'type','page_index'
                    ),
                '已结束约战详情|/api/match/my_finish_match'=>array(
                    'user_id',
                    'match_id'
                ),
                '保险|/api/match/surance'=>array(
                ),
                '保险校验战队积分|/api/match/credit_verify'=>array(
                    'user_id',
                    'enroll_id','surance_id','member_ids'
                ),
            ),
            '招募接口' => array(
                '发布战队招募|/api/recruit/public_corps'=>array(
                    'user_id',
                    'corps_id',
                    'sport_postion',
                ),
                '战队招募列表|/api/recruit/corps_list'=>array(
                    'user_id',
                    'search_param'
                ),
                '发布球员自荐|/api/recruit/public_member'=>array(
                    'user_id',
                    'sport_postion',
                    'recommend',
                    'images',
                ),
                '球员自荐列表|/api/recruit/member_list'=>array(
                    'user_id',
                    'search_param'
                ),
            ),
            '基础数据接口' => array(
                '城市|/api/data/citys' =>array(
                    'version'
                ),
                '区域|/api/data/areas' =>array(
                    'version'
                ),
                '运动类型|/api/data/sports' =>array(
                    'user_id',
                    'type',
                    'version'
                ),
                '获取区域|/api/data/get_city_area' =>array(
                    'user_id'
                ),
                '比赛赛制|/api/data/match_system' =>array(
                    'sport','version'
                ),
                '球赛位置|/api/data/sport_postion' =>array(
                    'sport','version'
                ),
            ),
            '动动圈接口' => array(
                '上传动动圈照片|/api/moments/upload_moments_photo' =>array(
                    'user_id',
                    'file',
                ),
                '发布动动圈动态|/api/moments/save_moments' =>array(
                    'user_id',
                    'text','images'
                ),
                '动动圈列表|/api/moments/moments_list' =>array(
                    'user_id',
                    'keywords','page'
                ),
                '动动圈详情|/api/moments/moments_detail' =>array(
                    'user_id',
                    'moment_id',
                ),
                '动动圈详情-评论|/api/moments/get_moments_comments' =>array(
                    'user_id',
                    'moment_id','page'
                ),
                '动动圈评论|/api/moments/comment_moments' =>array(
                    'user_id',
                    'moment_id','text'
                ),
                '动动圈点赞|/api/moments/praise_moments' =>array(
                    'user_id',
                    'moment_id',
                ),
                '我的发布|/api/moments/my_moments' =>array(
                    'user_id',
                    'page','target_id'
                ),
                '分享|/api/moments/share' =>array(
                    'user_id',
                    'text', 'share_type', 'title', 'desc', 'image', 'link',
                ),
                '收藏/删除收藏|/api/moments/collect' =>array(
                    'user_id',
                    'moment_id',
                ),
                '我的收藏|/api/moments/my_collect' =>array(
                    'user_id',
                    'page',
                ),
                '我的动动圈列表|/api/moments/my_moments_list' =>array(
                    'user_id',
                    'keywords','page'
                ),
                '举报|/api/moments/report' =>array(
                    'user_id',
                    'moment_id'
                ),
                '删除评论|/api/moments/delete_moments_comments' =>array(
                    'user_id',
                    'comment_id'
                ),
                '删除动动圈|/api/moments/delete_moments' =>array(
                    'user_id',
                    'moment_id'
                ),
            ),
            '消息接口'=>array(
                '系统消息列表|/api/message/system_publish_list'=>array(
                    'user_id',
                    'type',
                    'page_index',
                ),
                '发送系统消息|/api/message/system_publish'=>array(
                    'user_id',
                    'message',
                    'desc',
                    'link',
                    'link_type',
                ),
                '获取用户昵称和头像|/api/message/info'=>array(
                    'request_id',
                    'type',
                ),
                '删除系统消息|/api/message/delete_system_publish'=>array(
                    'user_id',
                    'publish_id',
                ),
                '消息变更为已读|/api/message/change_status'=>array(
                    'user_id',
                    'publish_ids',
                ),
            ),
            '通讯录接口' => array(
                '好友列表|/api/friend/friend_list' =>array(
                    'user_id',
                    'keywords'
                ),
                '关注好友列表|/api/friend/follow_friend_list' =>array(
                    'user_id',
                    'keywords'
                ),
                '关注战队列表|/api/friend/follow_corps_list' =>array(
                    'user_id',
                ),
                '关注/取消关注|/api/friend/follow' =>array(
                    'user_id',
                    'target_id','type'
                ),
                '搜索战队/用户|/api/friend/search_user' =>array(
                    'user_id',
                    'keywords'
                ),
                '接受/拒绝 好友请求/战队邀请|/api/friend/add_friend' =>array(
                    'user_id',
                    'request_id','type','status'
                ),
                '好友请求/战队邀请列表|/api/friend/friend_request_list' =>array(
                    'user_id',
                    'page'
                ),
                '请求好友/战队|/api/friend/request_user' =>array(
                    'user_id',
                    'target_id','type'
                ),
                '通过ID搜索好友/战队|/api/friend/search_user_by_id' =>array(
                    'user_id',
                    'target_id'
                ),
                '手机联系人|/api/friend/user_contact_list' =>array(
                    'user_id',
                    'contact'
                ),
                '创建群聊|/api/friend/create_group' =>array(
                    'user_id',
                    'members'
                ),
                '管理群聊|/api/friend/manage_group' =>array(
                    'user_id',
                    'group_id'
                ),
                '群聊添加好友列表|/api/friend/group_add_friend_list' =>array(
                    'user_id',
                    'group_id'
                ),
                '群聊添加成员|/api/friend/join_group' =>array(
                    'user_id',
                    'group_id','members'
                ),
                '退出群聊|/api/friend/quit_group' =>array(
                    'user_id',
                    'group_id',
                ),
                '移除群聊成员|/api/friend/delete_group_member' =>array(
                    'user_id',
                    'group_id','members'
                ),
                '修改群聊名称|/api/friend/modify_group_name' =>array(
                    'user_id',
                    'group_id','nick_name'
                ),
                '上传群聊头像|/api/friend/upload_group_avatar' =>array(
                    'user_id',
                    'group_id','avatar'
                ),
                '获取群聊信息|/api/friend/get_group_info' =>array(
                    'user_id',
                    'group_id'
                ),
            ),
            '裁判中心' => array(
                '判断用户是否为裁判|/api/judge/is_judge' =>array(
                    'user_id',
                ),
                '裁判列表|/api/judge/judge_list' =>array(
                    'user_id',
                    'page'
                ),
                '执裁单列表|/api/judge/judge_match_list' =>array(
                    'user_id',
                    'page','status'
                ),
                '结束比赛|/api/judge/finish_match' =>array(
                    'user_id',
                    'match_id','request_score','response_score'
                ),
            ),
            '后台互通' => array(
                '更新任务规则|/api/admin/update_mission_rule' =>array(),
                '更新比赛赛制|/api/admin/update_match_system' =>array(),
                '更新运动位置|/api/admin/update_sport_postion' =>array(),
                '更新战队信息|/api/corps/update_detail' =>array(),
                '更新战队信息|/api/admin/update_citys' =>array(),
            )
        );

        return view('api.index',[
            'arrlist' => $arrlist,
            'jsarrlist' => json_encode($arrlist)
            ]);
    }

    /*测试修改用户设置信息*/
    public function user_upload_avatar(){
        $params = $this->get_sign();
        $params['api_url'] = $this->api_host.'/api/user/upload_avatar';
        return view('api.upload_avatar',$params);
    }

    /*测试修改用户设置信息*/
    public function corps_upload_album(){
        $params = $this->get_sign();

        $user_token = $this->get_token(10009);
        $params['user_token'] = $user_token;

        $params = array_merge($params,$this->all_params);

        $params['api_url'] = $this->api_host.'/api/corps/upload_album';
        $params['user_id'] = 10009;
        return view('api.upload_test',$params);
    }
    /*测试修改用户设置信息*/
    public function corps_upload_badge(){
        $params = $this->get_sign();

        $user_token = $this->get_token(10009);
        $params['user_token'] = $user_token;

        $params = array_merge($params,$this->all_params);

        $params['api_url'] = $this->api_host.'/api/corps/upload_badge';
        $params['user_id'] = 10009;
        return view('api.upload_test',$params);
    }


    /*过验证*/
    private function get_sign(){
        $app_token = config('constants.app_token');
        $random_str = time();
        $sign = md5(md5(md5($app_token.$random_str)));
        $apptype = 'android';
        $appversion = '1.0.1';
        return compact('sign','random_str','apptype','appversion');
    }

    /*获取token*/
    private function get_token($user_id){
        if ($user = UserModel::where('user_id',$user_id)->first()) {
            $username = $user->phone;
            $password = $user->password;
            $app_login_time = $user->app_login_time;
            $app_type = "$user->app_type";
            $api_arr = compact('username','password','app_login_time');
            ksort($api_arr);
            $api_token = md5(md5(md5(implode($app_type, $api_arr))));

            return $api_token;
        }else{
            return 'df560853e78cce759ec01b496a4dc87a';
        }
    }

    /*测试找回密码*/
    public function test_request(){
        $params = $this->get_sign();
        $api_url = $this->api_host.($this->all_params['api_url']);
        if (isset($this->all_params['user_id'])) {
            $user_token = $this->get_token($this->all_params['user_id']);
            $params['user_token'] = $user_token;
        }

        $params = array_merge($params,$this->all_params);
        ($this->post_url($api_url,$params));
    }

    /*测试找回密码*/
    public function test_system_publish(){
        // var_dump(RongCloudComponent::push_rong(10009,'xxxxx'));
        var_dump(RongCloudComponent::system_publish(array('10009','10027'),'message','desc','link'));
    }

    /*测试找回密码*/
    public function system_register(){
        // var_dump(RongCloudComponent::push_rong(10009,'xxxxx'));
        var_dump(RongCloudComponent::system_register(10000,'系统消息','/avatar/system_message.png'));
    }

    /*
     * HTTP POST Request
    */
    private  function post_url($url, $params) {
        $ch = curl_init();
        if(stripos($url, "https://") !== false) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $response = curl_exec($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);
        if(intval($header["http_code"]) == 200) {
            if ($this->with_html) {
                echo "[html]:<br/>";
                echo $response;
                echo "<hr>";
            }
            if ($this->with_array) {
                echo "[array]:<br/>";
                var_dump(json_decode($response));
                echo "<hr>";
            }
            if ($this->with_server) {
                echo "[server]:<br/>";
                var_dump($_SERVER);
                echo "<hr>";
            }
            if ($this->with_header) {
                echo "[header]:<br/>";
                var_dump($params);
                echo "<hr>";
            }
            echo '<br/>'.$response;
        } else {
            echo $response;
            // if ($this->with_html) {
            // }
            // if ($this->with_array) {
            //     var_dump(json_decode($response));
            // }
            // if ($this->with_server) {
            //     var_dump($_SERVER);
            // }
            // if ($this->with_header) {
            //     return ['header'=>$header,'response'=>$response];
            // }else{
            //     return $response;
            // }
        }
    }
    //每天执行
    public function updateCorpsInfo(){
        $all_corp_ids = CorpsModel::lists('corps_id');
        foreach ($all_corp_ids as $all_corps_id){
            CorpsComponent::update_corps($all_corps_id);
            echo '战队'.$all_corps_id.'更新完成';

        }
    }

    public function upload_id_photo(){
        $params = $this->get_sign();
        $params['api_url'] = $this->api_host.'/api/user/upload_id_photo';
        return view('api.upload_id_photo',$params);
    }

    public function identify_user(){
        $params = $this->get_sign();
        $params['api_url'] = $this->api_host.'/api/user/identify_user';
        return view('api.identify_user',$params);
    }

    public function get_city_area(){
        $params = $this->get_sign();
        $params['api_url'] = $this->api_host.'/api/realtime/get_city_area';
        return view('api.identify_user',$params);
    }

    public function upload_realtime_album(){
        $params = $this->get_sign();
        $params['api_url'] = $this->api_host.'/api/realtime/upload_realtime_album';
        return view('api.upload_id_photo',$params);
    }

    public function release_realtime(){
        $params = $this->get_sign();
        $params['api_url'] = $this->api_host.'/api/realtime/release_realtime';
        return view('api.release_realtime',$params);
    }

    public function upload_certification(){
        $params = $this->get_sign();
        $params['api_url'] = $this->api_host.'/api/user/upload_certification_photo';
        return view('api.upload_certification',$params);
    }

    public function upload_moments(){
        $params = $this->get_sign();
        $params['api_url'] = $this->api_host.'/api/moments/upload_moments_photo';
        return view('api.upload_certification',$params);
    }


    /**
     * 将城市区划数据保存到数据库
     * @return boolean
     */
    public function save_areas(){
        app()->configure('account');
        $juhe_city_key = config('account.juhe_city_key');
        $weather = new Weather($juhe_city_key);
        $result = $weather->getCitys();
        $areas = array();
        $area = array();
        if($result['resultcode'] == 200){
            $city_arr = $result['result'];
            foreach($city_arr as $city){
                if($city['city'] == $city['district']){
                    continue;
                }else{
                    $area['id'] = $city['id'];
                    $area['name'] = $city['district'];
                    $area['city'] = $city['city'];
                    $areas[] = $area;
                }
            }
        }
        $save_areas_flag = DB::table('data_areas')->insert($areas);
        var_dump($save_areas_flag);
    }

    public function redis_con(){
        $redis =  new \Redis();
        $redis->connect(env('REDIS_HOST', '127.0.0.1'),env('REDIS_PORT', 6379));
        $redis->auth(env('REDIS_PASSWORD', null));
        return $redis;
    }

    public function redis_flush(){
        $redis = RedisComponent::redis_con();
        $redis->flushDB();
    }

    /*测试REDIS*/
    public function testRedis(){
        // $redis = new \Redis();
        // // $redis->connection('default');
        // $redis->connect('127.0.0.1');
        // $redis->auth('namespace redis');
        
        $redis = $this->redis_con();

        var_dump(env('REDIS_HOST'));
        var_dump(env('REDIS_PORT'));
        var_dump(env('REDIS_DATABASE'));
        var_dump(env('REDIS_PASSWORD'));

        $redis->set('api:user:10009','test');
        $value = $redis->get('api:user:10009');
        var_dump($value);
    }


    public function editRequestRedis(){
        $match_requests = MatchRequestModel::all();
        foreach($match_requests as $match_request){
            $match = MatchModel::where('request_id',$match_request->request_id)->first();
            //数据存入redis
            if($match){
                $redis = RedisComponent::redis_con();
                $member_ids = explode(',',$match_request->request_member_ids);
                foreach($member_ids as $member_id){
                    var_dump($match->match_id);
                    var_dump($member_id);
                    $redis->sAdd('gojaya:user:'.$member_id.':match',$match->match_id);
                }
            }
        }
    }

    public function testRong(){
        $cloud = RongCloudComponent::server();
        $content = json_encode(array('content'=>'g1007620160411225108','extra'=>''));
        echo $cloud->messagePublish('10000','10076','app:ungroup',$content);
    }


    public function register_system_users(){
        app()->configure('ppurl');
        $img_url = config('ppurl.img_url');
        $system_user_arr = array(
            array(
                'user_id' => '1111',
                'nick_name' => '与我相关',
                'avatar' => $img_url.'/avatar/system_me.png',
            ),
            array(
                'user_id' => '2222',
                'nick_name' => '好友关系',
                'avatar' => $img_url.'/avatar/system_friend.png',
            ),
            array(
                'user_id' => '3333',
                'nick_name' => '入队申请',
                'avatar' => $img_url.'/avatar/system_team.png',
            ),
            array(
                'user_id' => '4444',
                'nick_name' => '战队操作',
                'avatar' => $img_url.'/avatar/system_corps.png',
            ),
            array(
                'user_id' => '5555',
                'nick_name' => '约战相关',
                'avatar' => $img_url.'/avatar/system_match.png',
            ),
        );
        foreach ($system_user_arr as $arr) {
            RongCloudComponent::system_register($arr['user_id'],$arr['nick_name'],$arr['avatar']);
            $user_p = UserProfileModel::firstOrNew(['user_id'=>$arr['user_id']]);
            $user_p->nick_name = $arr['nick_name'];
            $user_p->avatar = $arr['avatar'];
            $user_p->save();
        }
    }
}
