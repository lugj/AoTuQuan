<?php

namespace App\Http\Controllers\Crontab;
use App\Http\Components\UserComponent;
use App\Models\UserAccountModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;
use DB;
use App\Models\JobOrderModel;
use App\Models\UserSettingModel;
use App\Models\ConfigModel;
use App\Models\UserModel;
use App\Http\Controllers\Controller;

/**
 * 定时服务
 */
class CronServiceController extends Controller{

    public function version_invoke($function,Request $request){

        try{
            return call_user_func_array(array($this,$function), array($request));
        }catch(\Exception $e){

        }
    }

    /**
     * 每分钟执行
     */
    public function min(){
        $cur_time = time();
        $log = "";
        //禁止交易到期
        $transaction_user_list = UserModel::where("status",2)->where("transaction_fobit_time",'<=',$cur_time)->get()->toArray();
        $transaction_user_ids = array_column($transaction_user_list,'user_id');
        if(!empty($transaction_user_ids)){
            UserModel::whereIn("user_id",$transaction_user_ids)->update(array("status"=>1));
        }
        //永久封号到期
        $login_forbit_user_list = UserModel::where("status",3)->where("login_fobit_time",'<=',$cur_time)->get()->toArray();
        $login_forbit_user_ids = array_column($login_forbit_user_list,'user_id');
        if(!empty($login_forbit_user_ids)){
            UserModel::whereIn("user_id",$login_forbit_user_ids)->update(array("status"=>1));
        }

        //会员到期
        $user_expire_list = UserProfileModel::where("card_level",">",0)->where("card_endtime",'<=',$cur_time)->get()->toArray();
        $user_expire_ids = array_column($user_expire_list,'user_id');
        if(!empty($user_expire_ids)){
            UserModel::whereIn("user_id",$user_expire_ids)->update(array("card_level"=>0));
        }

        //订单20分钟到期
        app()->configure('base');
        $order_cancel_time = config('base.order_cancel_time');
        $job_order_list = JobOrderModel::where("create_time",'<',$cur_time-$order_cancel_time)->where("status",0)->get()->toArray();
        $job_order_ids = array_column($job_order_list,'id');
        if(!empty($job_order_ids)){
            JobOrderModel::whereIn("id",$job_order_ids)->update(array("status"=>-1));
        }

        //1天未付款 自动打款
        $order_pay_plus_time = config('base.order_pay_plus_time');
        $job_pay_order_list = JobOrderModel::where("accept_time",'<',$cur_time-$order_pay_plus_time)->where("accept_time",">",0)->where("status",1)->get()->toArray();
        foreach($job_pay_order_list AS $jitem){
                $update_data = array(
                    "pay_time"=>time(),
                    "status"=>2,
                );

                $order_id = $jitem["id"];
                $jobOrderArr["price"] = $jitem["price"]*100;
                if($jitem["job_model"] == 1){   //发布的兼职
                    $user_id = $jitem["user_id"];
                }else{
                    $user_id = $jitem["job_user_id"];
                }

                UserComponent::price_save('job_order',$user_id,$jobOrderArr["price"],-1,'gold',$order_id);
                $user_income = $jobOrderArr["price"];

                if($jobOrderArr["safety_user_id"]>0 && $jobOrderArr["safety_status"] == 1){
                    $config = new ConfigModel();
                    $safety_income_pre = $config->get_value('safety_income_pre');
                    if($safety_income_pre>0){
                        $safety_income = round($jobOrderArr["price"]/100*$safety_income_pre,2);
                        $user_income = $jobOrderArr["price"]-$safety_income;
                        //保安员的收入
                        UserComponent::price_save('safety',$jobOrderArr["safety_user_id"],$safety_income,1,'gold',$order_id);
                        $update_data["safety_price"] = $safety_income;
                    }
                }
                //代理商收入
                if($jobOrderArr["agent_user_id"]>0){
                    $config = new ConfigModel();
                    $agent_income_pre = $config->get_value('agent_income_pre');
                    if($agent_income_pre>0){
                        $agent_income = round($jobOrderArr["price"]/100*$agent_income_pre,2);
                        $user_income = $user_income-$agent_income;
                        UserComponent::price_save('agent',$jobOrderArr["agent_user_id"],$agent_income,1,'gold',$order_id);
                        $update_data["agent_price"] = $agent_income;
                    }
                }

                if($jobOrderArr["user_id"] == $user_id){
                    UserComponent::price_save('job_order',$jobOrderArr["job_user_id"],$user_income,1,'gold',$order_id);
                }else{
                    UserComponent::price_save('job_order',$jobOrderArr["user_id"],$user_income,1,'gold',$order_id);
                }

                JobOrderModel::where("id",$order_id)->update($update_data);
        }

        echo "success";exit;
    }

    /**
     * 每天执行
     */
    public function day(){
        UserProfileModel::where("user_id",'>',0)->update(array("today_hello_chat"=>0,'today_hello_chat_user_list'=>""));
        UserAccountModel::where("user_id",'>',0)->update(array("today_income"=>0));
    }

    /**
     * 添加日志
     */
    public function addLog(){

    }
}
