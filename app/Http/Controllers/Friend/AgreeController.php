<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Friend;

use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\FriendModel;
use App\Models\FollowModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 *同意好友申请
 */
class AgreeController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                    return $this->agree($request);
            
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 同意好友申请
     */
    public function agree(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $apply_user_id = $request->input('apply_user_id');
        if(empty($apply_user_id)){
            return $this->json('0','参数为空');
        }
        $friend = FriendModel::where("apply_user_id",$apply_user_id)->where("agree_user_id",$user_id)->first();
        if(!$friend){
            return $this->json('0','好友申请记录不存在');
        }else{
            $friend->agree_time = time();
            $friend->status =1;
            $friend->source =1;
            $result = $friend->saveOrFail();
        }
        if(!empty($result)){
            return $this->json('1','操作成功');
        }else{
            return $this->json('0','操作失败');
        }

    }

}