<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Friend;

use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\FriendModel;
use App\Models\FollowModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 搜索
 */
class SearchController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                    return $this->search($request);
            
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 搜索用户
     */
    public function search(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $aotu_id = $request->input('aotu_id');
        $list = UserProfileModel::select(['user_id','nick_name','avatar','age','gender'])->where("aotu_id","like","%".$aotu_id."%")->get()->toArray();

        if(!empty($list)){
            foreach($list AS &$item){
                $item["avatar"] = UtilFunction::getUrlImg($item["avatar"]);
                $is_friend = UserComponent::checkFriend($user_id,$item["user_id"]);
                if($is_friend || $item["user_id"] == $user_id){
                    $item["is_friend"] = 1;
                }else{
                    $item["is_friend"] = 0;
                }
            }
            return $this->json('1','获取成功',$list);
        }else{
            return $this->json('1','为搜索到用户');
        }

    }

}