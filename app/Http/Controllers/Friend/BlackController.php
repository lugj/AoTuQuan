<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Friend;

use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\FriendModel;
use App\Models\BlacklistModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 *拉黑好友
 */
class BlackController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                    return $this->black($request);
            
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 拉黑好友
     */
    public function black(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $black_user_id = $request->input('black_user_id');  //被拉黑的用户ID
        $blackList = BlacklistModel::where("user_id",$user_id)->where("black_user_id",$black_user_id)->first();
        if($blackList){
            return $this->json('0','该用户您已经拉黑');
        }else{
            $blackList = new BlacklistModel();
            $blackList->user_id = $user_id;
            $blackList->black_user_id = $black_user_id;
            $result = $blackList->saveOrFail();
            if($result){
                return $this->json('1','拉黑成功');
            }else{
                return $this->json('0','操作失败');
            }
        }


    }

}