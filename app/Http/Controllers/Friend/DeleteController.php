<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Friend;

use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\FriendModel;
use App\Models\FollowModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 *删除好友
 */
class DeleteController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                    return $this->delete($request);
            
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 删除好友
     */
    public function delete(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $delete_user_id = $request->input('delete_user_id');
        if(empty($delete_user_id)){
            return $this->json('0','参数为空');
        }
        //删除好友记录
        $result  = FriendModel::where(function($query) use ($user_id,$delete_user_id){
            $query->where('apply_user_id','=',$user_id)
                ->where('agree_user_id','=',$delete_user_id)
                ->where("status",1);
        })->orwhere(function($query) use ($user_id,$delete_user_id){
            $query->where('apply_user_id','=',$delete_user_id)
                ->where('agree_user_id','=',$user_id)
                ->where("status",1);
        })->delete();

        if(!empty($result)){
            //取消对对方的关注
            FollowModel::where("fans_user_id",$user_id)->where("follow_user_id",$delete_user_id)->delete();
            return $this->json('1','删除成功');
        }else{
            return $this->json('0','操作失败');
        }

    }

}