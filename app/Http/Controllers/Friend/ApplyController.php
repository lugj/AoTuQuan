<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Friend;

use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\FriendModel;
use App\Models\FollowModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 申请
 */
class ApplyController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                    return $this->apply($request);
            
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 申请
     */
    public function apply(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $friend_user_id = $request->input('friend_user_id');
        if(empty($friend_user_id)){
            return $this->json('0','参数为空');
        }
        if($user_id == $friend_user_id){
            return $this->json('0','不能自己给自己申请好友');
        }

        $is_friend = UserComponent::checkFriend($user_id,$friend_user_id);
        if($is_friend){
            return $this->json('0','你们已经是好友，无需申请');
        }

        $friend = FriendModel::where("apply_user_id",$user_id)->where("agree_user_id",$friend_user_id)->first();
        if($friend){
            $friend->apply_time = time();
        }else{
            $friend = new FriendModel();
            $friend->apply_user_id = $user_id;
            $friend->agree_user_id = $friend_user_id;
            $friend->apply_time = time();
        }
        $result = $friend->saveOrFail();
        if(!empty($result)){
            return $this->json('1','申请成功，请等待回复');
        }else{
            return $this->json('0','操作失败');
        }

    }

}