<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Friend;

use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\FriendModel;
use App\Models\FollowModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 新朋友申请列表
 */
class NewFriendListController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                    return $this->new_friend_list($request);
            
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 新朋友申请列表
     * @param Request $request
     * @author paulLu
     */
    public function new_friend_list(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $list = FriendModel::select([
            'at_user_profile.user_id',
            'at_user_profile.nick_name',
            'at_user_profile.avatar',
            'at_user_profile.gender',
            'at_user_profile.age',
            'at_friend.status',
        ])->where("agree_user_id",$user_id)
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_friend.apply_user_id")
            ->get()->toArray();

        if(!empty($list)){
            foreach($list AS &$item){
                $item["avatar"] = UtilFunction::getUrlImg($item["avatar"]);
            }
            $friend_num = FriendModel::where("status",1)
                ->where(function($query) use ($user_id){
                    $query->where('apply_user_id','=',$user_id)
                        ->orwhere('agree_user_id','=',$user_id);
                })->count();
            $result = array();
            $result["new_friend"] = $list;
            $result["friend_num"] = $friend_num;
            return $this->json('1','获取成功',$result);
        }else{
            return $this->json('1','获取成功');
        }

    }

}