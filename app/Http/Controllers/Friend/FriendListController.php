<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Friend;

use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\FriendModel;
use App\Models\FollowModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 好友列表
 */
class FriendListController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                    return $this->friend_list($request);
            
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 列表
     * @param Request $request
     * @author paulLu
     */
    public function friend_list(Request $request){
        $user_list = array();
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $model = $request->input('model'); //1 好友  2 关注  3 粉丝
        if(!in_array($model,array(1,2,3))){
            return $this->json('0','参数输入有误!');
        }
        if($model == 1){
            $list = FriendModel::where("status",1)
                ->where(function($query) use ($user_id){
                    $query->where('apply_user_id','=',$user_id)
                        ->orwhere('agree_user_id','=',$user_id);
                })->get();
            $list = $list->toArray();
            foreach($list AS $item){
                if($item["apply_user_id"] == $user_id){
                    $user_list[] = $item["agree_user_id"];
                }else{
                    $user_list[] = $item["apply_user_id"];
                }
            }
        }elseif($model == 2){
            $list = FollowModel::where("fans_user_id",$user_id)->get();
            if($list){
                $list = $list->toArray();
                foreach($list AS $item){
                    $user_list[] = $item["follow_user_id"];
                }
            }
        }elseif($model == 3){
            $list = FollowModel::where("follow_user_id",$user_id)->get();
            if($list){
                $list = $list->toArray();
                foreach($list AS $item){
                    $user_list[] = $item["fans_user_id"];
                }
            }
        }
        if(!empty($user_list)){
            $list = array();
            $user_list = UserProfileModel::select(['user_id','nick_name','avatar'])->whereIn("user_id",$user_list)->get()->toArray();

            foreach($user_list AS &$uitem){
                $uitem["avatar"] = UtilFunction::getUrlImg($uitem["avatar"]);
                $char = UtilFunction::getFirstChar($uitem["nick_name"]);
                $list[$char][] = $uitem;
            }
            return $this->json('1','获取成功',$list);
        }else{
            return $this->json('1','获取成功');
        }

    }

}