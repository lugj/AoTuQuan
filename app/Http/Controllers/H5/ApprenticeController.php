<?php

namespace App\Http\Controllers\H5;

use App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use App\Models\OtherFeedbackModel;
use Illuminate\Http\Request;
use App\Http\Components\UserComponent;
use App\Models\UserProfileModel;
use App\Models\UserAccountModel;
use App\Models\DataConfigModel;
use DB;

use App\Models\OtherAppUpdateModel;
use App\library\UtilFunction;

/*
* 邀请收徒
*/
class ApprenticeController extends Controller{

    private $send_code_url = "/api/user/sms_check_code";    //发送验证码

    private $register_url = "/api/user/register"; //注册

    public function __construct(){
    }

    public function invoke($function,Request $request){
        if (method_exists($this, $function)) {
            return call_user_func_array(array($this,$function), array($request));
        }else{
            echo "function does not exists";
        }
    }

    /**
     * 用户收徒主页
     */
    public function index(Request $request){
        $user_id =  $request->input('user_id');
        if(empty($user_id)){
            die("用户ID为空!");
        }
        //获取师傅收入
        $userAccount = UserAccountModel::where("user_id",$user_id)->first();
        if(!$userAccount){
            die("用户不存在！");
        }
        $userAccount = $userAccount->toArray();
        $master_income = $userAccount['master_income'];
        //徒弟数量
        $apprentice_num = UserProfileModel::where("master_id",$user_id)->count();
        //邀请码
        $userProfile = UserProfileModel::where("user_id",$user_id)->first()->toArray();
        $invite_code = $userProfile['invite_code'];
        //获取师傅获得比例
        $data_config = new DataConfigModel();
        $master_get_rate = $data_config->get_value('master_get_rate');
        return view('h5.apprentice_index', [
            'master_income'=>$master_income,
            'apprentice_num'=>$apprentice_num,
            'invite_code'=>$invite_code,
            'master_get_rate'=>$master_get_rate

        ]);
    }

    /**
     * 分享出去的注册页
     */
    public function share(Request $request){
        $user_id =  $request->input('user_id');
        if(empty($user_id)){
            die("用户ID为空!");
        }
        $user = UserProfileModel::select(['nick_name','invite_code','avatar'])->where("user_id",$user_id)->first()->toArray();
        return view('h5.apprentice_share', [
            'user'=>$user
        ]);

    }

    /**
     * 注册
     */
    public function register(Request $request){
        $username =  $request->input('af_phone');
        $check_code =  $request->input('af_code');
        $gender =  $request->input('af_sex_input');
        $invite_code =  $request->input('invite_code');

        if(strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone')||strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')){
            $app = OtherAppUpdateModel::where("apptype","ios")->orderBy("appversion","DESC")->first()->toArray();
        }else{
            $app = OtherAppUpdateModel::where("apptype","android")->orderBy("appversion", "DESC")->first()->toArray();
        }


        if(empty($username) || empty($check_code) || empty($gender)){
//            die(json_encode(array('code'=>-1,'message'=>'数据为空!')));
            echo "<script>alert('数据为空!');location.go(-1)</script>";exit;
        }else{

            $data = array();
            $data['username'] = $username;
            $data['check_code'] = $check_code;
            $data['gender'] = $gender;
            $data['invite_code'] = $invite_code;
            $result = $this->_get_post_url($this->register_url,$data);
            $result = json_decode($result,true);
            if($result['status'] == 1){
                echo "<script>alert('注册成功!')</script>";
                header("Location:".$app['download_link']);exit;
                exit;
            }else{
                $message = $result['message'];
                echo "<script>alert('{$message}');window.history.go(-1)</script>";exit;
            }
        }
    }

    /**
     * 发送验证码
     */
    public function send_code(Request $request){
        $phone =  $request->input('phone');
        if(empty($phone)){
            die("手机号不能为空！");
        }
        $result = $this->_get_post_url($this->send_code_url,array("code_type"=>'register',"phone"=>$phone));
        $result = json_decode($result,true);
        if($result['status'] == 1){
            die(json_encode(array('code'=>1)));
        }else{
            die(json_encode(array('code'=>-1,'message'=>$result['message'])));
        }
    }


    /**
     * 调用接口
     */
    public function _get_post_url($url,$push_data = array()){
        app()->configure('ppurl');
        $url = config('ppurl.img_url').$url;
        $app_token =  config('constants.app_token');
        $random_str = time();
        $sign = md5(md5(md5($app_token.$random_str)));

        $apptype = "android";
        $appversion = config('constants.version');
        $data = array(
            'sign'=>$sign,
            'random_str'=>$random_str,
            'apptype'=>$apptype,
            'appversion'=>$appversion,
        );
        $data = array_merge($data,$push_data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // post数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // post的变量
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}
