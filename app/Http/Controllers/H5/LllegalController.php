<?php

namespace App\Http\Controllers\H5;

use App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserLllegalModel;
use App\Models\OtherH5Model;
use DB;

use App\Http\Components\OuterDataComponent;
use App\library\UtilFunction;

/*
* 违规页面
*/
class LllegalController extends Controller{

    public function __construct(){
    }
    public function invoke($function,Request $request){
        if (method_exists($this, $function)) {
            return call_user_func_array(array($this,$function), array($request));
        }else{
            echo "function does not exists";
        }
    }

    public function index(Request $request){
        $l_id =  $request->input('l_id');
        if(empty($l_id)){
            die("参数有误!");
        }
        $lllegal = UserLllegalModel::where("id",$l_id)->first();
        if($lllegal) {
            $lllegal = $lllegal->toArray();
            return view('h5.lllegal', [
                'data'=>$lllegal
            ]);
        }else{
            die("数据为空!");
        }


    }

    /**
     * 平台行为规范
     */
    public function rule(Request $request){
        $data = OtherH5Model::where('id',9)->first()->toArray();
        return view('h5.lllegal_rule',[
            'data'=>$data,
            'title'=>'平台行为规范'
        ]);
    }

}
