<?php

namespace App\Http\Controllers\H5;

use App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OtherH5Model;
use DB;

use App\Http\Components\OuterDataComponent;
use App\Http\Components\UserComponent;

/*
* 免责声明
*/
class DisclaimerController extends Controller{

    public function __construct(){
    }


    public function index(Request $request){
        $data = OtherH5Model::where('id',1)->first()->toArray();
        return view('h5.disclaimer',[
            'data'=>$data
        ]);
    }
}
