<?php

namespace App\Http\Controllers\H5;

use App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OtherH5Model;
use DB;

use App\Http\Components\OuterDataComponent;
use App\Http\Components\UserComponent;

/*
* H5页面
*/
class ViewController extends Controller{

    public function __construct(){
    }
    public function invoke($function,Request $request){
        if (method_exists($this, $function)) {
            return call_user_func_array(array($this,$function), array($request));
        }else{
            echo "function does not exists";
        }
    }

    /**
     *用户使用及隐私协议
     */
    public function agreement(Request $request){
        $data = OtherH5Model::where('id',7)->first()->toArray();
        return view('h5.view',[
            'data'=>$data,
            'title'=>'用户使用及隐私协议'
        ]);
    }

    /**
     * 取消订单惩罚规则
     */
    public function cancel_rule(Request $request){
        $data = OtherH5Model::where('id',8)->first()->toArray();
        return view('h5.view',[
            'data'=>$data,
            'title'=>'取消订单惩罚规则'
        ]);
    }

    /**
     * 服务发布细则
     */
    public function service_detail(Request $request){
        $data = OtherH5Model::where('id',4)->first()->toArray();
        return view('h5.view',[
            'data'=>$data,
            'title'=>'服务发布细则'
        ]);
    }
    /**
     * 平台行为规范
     */
    public function lllegal(Request $request){
        $data = OtherH5Model::where('id',9)->first()->toArray();
        return view('h5.view',[
            'data'=>$data,
            'title'=>'平台行为规范'
        ]);
    }
}
