<?php

namespace App\Http\Controllers\H5;

use App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserProfileModel;
use App\Http\Components\UserComponent;
use App\Models\ServiceModel;
use App\Models\EvaluateModel;
use DB;

use App\Http\Components\OuterDataComponent;
use App\library\UtilFunction;

/*
* 用户分享页
*/
class ShareUserController extends Controller{

    public function __construct(){
    }


    public function index(Request $request){
        $service_user_id =  $request->input('user_id');
        if(empty($service_user_id)){
            die("用户ID为空!");
        }
        //用户基本信息
        $info = UserComponent::get_base_info($service_user_id);

        //封装数据
        if ($info) {
            unset($info['switch_push']);
            unset($info['switch_service']);
            unset($info['balance']);
            unset($info['income']);
            unset($info['spending']);
            //计算活跃时间
            $actived_at = time() - $info['actived_at'];
            $info['actived_at'] = UtilFunction::get_active_time($actived_at);

            $data['info'] = $info;

            $service = ServiceModel::where("user_id", $service_user_id)->first();
            if ($service) {
                $service = $service->toArray();
                $service['pictures'] = explode(',', $service['pictures']);
                $data['service'] = $service;
            }
            //获取评价总数
            $evaluate_num = EvaluateModel::where("user_id", $service_user_id)->count();

            if ($evaluate_num) {
                $data['evaluate_num'] = $evaluate_num;
            } else {
                $data['evaluate_num'] = 0;
            }
            return view('h5.share_user', [
                'data'=>$data
            ]);
        }else{
            die("用户不存在!");
        }
    }

}
