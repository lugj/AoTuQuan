<?php

namespace App\Http\Controllers\H5;

use App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OtherH5Model;
use DB;

use App\Http\Components\OuterDataComponent;
use App\Http\Components\UserComponent;

/*
* 奖励细则
*/
class BonusRuleController extends Controller{

    public function __construct(){
    }


    public function index(Request $request){
        $data = OtherH5Model::where('id',6)->first()->toArray();
        return view('h5.bonus_rule',[
            'data'=>$data
        ]);
    }
}
