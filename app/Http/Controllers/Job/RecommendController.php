<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Job;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\Http\Controllers\Myorder\EvaluateController;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\CollectionModel;
use App\Models\ConfigModel;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\FollowModel;
use App\Models\UserGiftModel;
use App\Models\UserProfileModel;
use App\Models\UserCarModel;
use App\Models\EvaluateModel;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 我是服务方/我是需求方 推荐
 */
class RecommendController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 我是服务方
     * 发布的需求推荐
     */
    public function demand(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $demand = JobModel::where("user_id",$user_id)->where("model",2)->first();
        $userSetting = UserSettingModel::where("user_id",$user_id)->first()->toArray();
        //获取推荐的兼职
        $list = JobModel::select([
            'at_job.id',
            'at_job.user_id',
            'at_job.job_class_id',
            'at_job.job_class_name',
            'at_job.price',
            'at_job.service_hourse',
            'at_job.introduce',
            'at_user_profile.avatar',
            'at_user_profile.nick_name',
            'at_user_profile.age',
            'at_user_profile.gender',
            'at_user_profile.deposit_auth',
            'at_user_profile.card_level',
            'at_user_profile.car_auth',
            'at_user_profile.credit_num',

            'at_user_setting.lat',
            'at_user_setting.lon',
        ])
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_job.user_id")

            ->leftJoin("at_user_setting","at_user_setting.user_id","=","at_job.user_id")
            ->where("at_job.model",2)->where("at_job.user_id",'!=',$user_id);
        if($demand){
           // $list = $list->where("at_job.job_class_id",$demand->job_class_id);
        }
        $list = $list->skip(0)->take(10)->get()->toArray();
        foreach($list AS &$item){
            $item["distance"] = UtilFunction::get_distance($userSetting["lat"],$userSetting["lon"],$item["lat"],$item["lon"]);
            if($item["car_auth"] == 1){
                $car = UserCarModel::where("user_id",$item["user_id"])->first()->toArray();
                if(!empty($car["car_logo"])){
                    $item["car_logo"] = UtilFunction::getUrlImg($car["car_logo"]);
                }else{
                    $item["car_logo"] = 0;
                }

            }
            $item["avatar"] = UtilFunction::getUrlImg($item["avatar"]);
        }

        return $this->json('1','获取成功!',$list);
    }


    /**
     * 我是需求方
     * 发布的兼职推荐
     */
    public function work(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $demand = JobModel::where("user_id",$user_id)->where("model",1)->first();
        $userSetting = UserSettingModel::where("user_id",$user_id)->first()->toArray();
        //获取推荐的兼职
        $list = JobModel::select([
            'at_job.id',
            'at_job.user_id',
            'at_job.job_class_id',
            'at_job.job_class_name',
            'at_job.price',
            'at_job.service_hourse',
            'at_job.voice',
            'at_job.voice_time',

            'at_user_profile.avatar',
            'at_user_profile.nick_name',
            'at_user_profile.height',
            'at_user_profile.weight',
            'at_user_profile.age',
            'at_user_profile.gender',
            'at_user_profile.deposit_auth',
            'at_user_profile.card_level',
            'at_user_profile.credit_num',
            'at_user_profile.video_auth',

            'at_user_setting.lat',
            'at_user_setting.lon',
        ])
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_job.user_id")
            ->leftJoin("at_user_setting","at_user_setting.user_id","=","at_job.user_id")
            ->where("at_job.model",1)->where("at_job.user_id",'!=',$user_id);
        if($demand){
            $list = $list->where("at_job.job_class_id",$demand->job_class_id);
        }
        $list = $list->skip(0)->take(10)->get()->toArray();
        foreach($list AS &$item){
            $item["distance"] = UtilFunction::get_distance($userSetting["lat"],$userSetting["lon"],$item["lat"],$item["lon"]);
            $item["avatar"] = UtilFunction::getUrlImg($item["avatar"]);
            $item["voice"] = UtilFunction::getUrlImg($item["voice"]);
        }
        return $this->json('1','获取成功!',$list);
    }


}