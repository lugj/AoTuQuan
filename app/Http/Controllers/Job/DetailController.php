<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Job;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\Http\Controllers\Myorder\EvaluateController;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\CollectionModel;
use App\Models\ConfigModel;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\FollowModel;
use App\Models\UserGiftModel;
use App\Models\UserProfileModel;
use App\Models\UserCarModel;
use App\Models\EvaluateModel;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 查看详情
 */
class DetailController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 验证是否已经关注
     */
    public function _check_follow($fans_user_id,$follow_user_id){
        $follow = FollowModel::where("fans_user_id",$fans_user_id)->where("follow_user_id",$follow_user_id)->first();
        if($follow){
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * 验证是否收藏
     */
    public function _check_collection($model,$user_id,$job_id = 0){
        $collection = CollectionModel::where("user_id",$user_id)->where("model",$model)->where("job_id",$job_id)->first();
        if($collection){
            return 1;
        }else{
            return 0;
        }
    }


    /**
     * 兼职详情页
     */
    public function work(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $job_id = $request->input('job_id');
        if(empty($job_id)){
            return $this->json('0','参数为空!');
        }
        $job = JobModel::select(['user_id','job_class_id','job_class_name','price','service_hourse','introduce','voice','voice_time'])->where("id",$job_id)->first();
        if(!$job){
            return $this->json('0','该兼职详情不存在!');
        }
        $result = array();
        $job_arr = $job->toArray();

        $job_arr["voice"] = UtilFunction::getUrlImg($job_arr["voice"]);

        //相册
        $album_list = UserComponent::getAlbum($user_id,0);
        $result["album_list"] = $album_list;
        //是否收藏
        $is_collection = $this->_check_collection(1,$user_id,$job_id);
        $result["is_collection"] = $is_collection;
        //是否关注
        $is_follow = $this->_check_follow($user_id,$job->user_id);
        $result["is_follow"] = $is_follow;
        //查看用户基础信息
        $userProfile = UserProfileModel::select(['nick_name','deposit_auth','card_level','age','gender','height','weight','credit_num'])->where("user_id",$job->user_id)->first()->toArray();
        $result["user_profile"] = $userProfile;

        //发布的其他技能
        $skill_list = array();
        $repeat_list = array();
        $other_skill = JobModel::where("job_class_id","!=",$job->job_class_id)->where("user_id",$job->user_id)->get();
        if($other_skill){
            $other_skill = $other_skill->toArray();
            foreach($other_skill AS $oitem){
                if(!in_array($oitem["job_class_id"],$repeat_list)){
                    $repeat_list[] = $oitem["job_class_id"];
                    $temp = array("job_class_id"=>$oitem["job_class_id"],"job_class_name"=>$oitem["job_class_name"]);
                    $skill_list[] = $temp;
                }
            }
        }
        $result["skill_list"] = $skill_list;
        //获取收到的礼物列表
        $gift_list = UserGiftModel::select(['gift_picture'])->where("receive_user_id",$job->user_id)->get()->toArray();
        foreach($gift_list AS &$gitem){
            $gitem["gift_picture"] = UtilFunction::getUrlImg($gitem["gift_picture"]);
        }
        $result["gift_list"] = $gift_list;

        //获取评价
        $evaluate_list = EvaluateModel::select(['at_user_profile.avatar','at_user_profile.nick_name','at_evaluate.content'])->where("at_evaluate.job_id",$job_id)->where("at_evaluate.model",1)->where("at_evaluate.user_id",$user_id)
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_evaluate.evaluate_user_id")->get()->toArray();
        foreach($evaluate_list AS &$eitem){
            $eitem["avatar"] = UtilFunction::getUrlImg($eitem["avatar"]);
        }
        $result["evaluate_list"] = $evaluate_list;

        //获取距离
        $userSetting1 = UserSettingModel::where("user_id",$job->user_id)->first()->toArray();
        $userSetting2 = UserSettingModel::where("user_id",$user_id)->first()->toArray();
        $distance = UtilFunction::get_distance($userSetting1["lat"],$userSetting1["lon"],$userSetting2["lat"],$userSetting2["lon"]);
        $result["distance"] = $distance;
        $result["job"] = $job_arr;
        return $this->json('1','获取成功!',$result);
    }

    /**
     * 需求详情
     */
    public function demand(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $job_id = $request->input('job_id');
        if(empty($job_id)){
            return $this->json('0','参数为空!');
        }
        $job = JobModel::select(['user_id','job_class_id','job_class_name','price','service_hourse','introduce','start_time','service_address','age','gender'])->where("id",$job_id)->first();
        if(!$job){
            return $this->json('0','该兼职详情不存在!');
        }
        $result = array();
        $job_arr = $job->toArray();


        //查看用户基础信息
        $userProfile = UserProfileModel::select(['nick_name','avatar','deposit_auth','card_level','age','gender','credit_num','car_auth'])->where("user_id",$job->user_id)->first()->toArray();
        if($userProfile["car_auth"] == 1){
            $car = UserCarModel::where("user_id",$job->user_id)->first()->toArray();
            if(!empty($car["car_logo"])){
                $userProfile["car_logo"] = UtilFunction::getUrlImg($car["car_logo"]);
            }else{
                $userProfile["car_logo"] = 0;
            }

        }
        $userProfile["avatar"] = UtilFunction::getUrlImg($userProfile["avatar"]);
        $result["user_profile"] = $userProfile;

        //获取距离
        $userSetting1 = UserSettingModel::where("user_id",$job->user_id)->first()->toArray();
        $userSetting2 = UserSettingModel::where("user_id",$user_id)->first()->toArray();
        $distance = UtilFunction::get_distance($userSetting1["lat"],$userSetting1["lon"],$userSetting2["lat"],$userSetting2["lon"]);
        $result["distance"] = $distance;
        $result["job"] = $job_arr;
        return $this->json('1','获取成功!',$result);
    }


    /**
     * 视频详情
     */
    function video_chat(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $video_user_id = $request->input('video_user_id');
        if(empty($video_user_id)){
            return $this->json('0','参数获取失败!');
        }
        $result = array();


        //相册
        $album_list = UserComponent::getAlbum($user_id,0);
        $result["album_list"] = $album_list;
        //是否收藏
        $is_collection = $this->_check_collection(2,$user_id,0);
        $result["is_collection"] = $is_collection;
        //是否关注
        $is_follow = $this->_check_follow($user_id,$video_user_id);
        $result["is_follow"] = $is_follow;
        //查看用户基础信息
        $userProfile = UserProfileModel::select(['nick_name','deposit_auth','card_level','age','gender','height','weight','height','weight','credit_num'])->where("user_id",$user_id)->first()->toArray();
        $result["user_profile"] = $userProfile;

        //发布的其他技能
        $skill_list = array();
        $repeat_list = array();
        $other_skill = JobModel::where("user_id",$video_user_id)->get();
        if($other_skill){
            $other_skill = $other_skill->toArray();
            foreach($other_skill AS $oitem){
                if(!in_array($oitem["job_class_id"],$repeat_list)){
                    $repeat_list[] = $oitem["job_class_id"];
                    $temp = array("job_class_id"=>$oitem["job_class_id"],"job_class_name"=>$oitem["job_class_name"]);
                    $skill_list[] = $temp;
                }
            }
        }
        $result["skill_list"] = $skill_list;
        //获取收到的礼物列表
        $gift_list = UserGiftModel::select(['gift_picture'])->where("receive_user_id",$video_user_id)->get()->toArray();
        foreach($gift_list AS &$gitem){
            $gitem["gift_picture"] = UtilFunction::getUrlImg($gitem["gift_picture"]);
        }
        $result["gift_list"] = $gift_list;

        //获取评价
        $evaluate_list = EvaluateModel::select(['at_user_profile.avatar','at_user_profile.nick_name','at_evaluate.content'])->where("at_evaluate.model",2)->where("at_evaluate.user_id",$video_user_id)
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_evaluate.evaluate_user_id")->get()->toArray();
        foreach($evaluate_list AS &$eitem){
            $eitem["avatar"] = UtilFunction::getUrlImg($eitem["avatar"]);
        }
        $result["evaluate_list"] = $evaluate_list;

        //获取每分钟价格
        $config = new ConfigModel();
        $video_chat_price = $config->get_value('video_chat_price');
        $result["video_chat_price"] = $video_chat_price;
        return $this->json('1','获取成功!',$result);


    }

}