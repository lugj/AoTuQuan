<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Job;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\ConfigModel;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use App\Models\GiftModel;
use App\Models\UserAccountModel;
use App\Models\UserGiftModel;
use App\Models\AccountRecordsModel;
use App\User;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 礼物
 */
class GiftController extends ApiController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function, Request $request)
    {
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try {
                return call_user_func_array(array($this, $function), array($request));
            } catch (\Exception $e) {
                return $this->json('0', '系统错误' . $e->getMessage());
            }
        } else {
            return $this->json('102', '该app版本下无此接口，请更新app');
        }
    }

    /**
     * 礼物列表
     */
    public function show(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $list = GiftModel::select(['id','title','picture','card_price','price'])->orderBy('create_time', 'desc')->get()->toArray();
        foreach($list AS &$item){
            $item["picture"] = UtilFunction::getUrlImg($item["picture"]);
        }
        if($list){
            return $this->json('1','获取成功',$list);
        }else{
            return $this->json('0','获取失败');
        }
    }

    /**
     * 送礼物
     */
    public function send(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $gift_id = $request->input('gift_id');
        $receive_user_id = $request->input('receive_user_id');
        if(empty($gift_id)){
            return $this->json('0','请选择您需要送的礼物');
        }
        if(empty($receive_user_id)){
            return $this->json('0','请选择您要送的人');
        }
        if($user_id == $receive_user_id){
            return $this->json('0','自己不能给自己送礼物');
        }
        $gift = GiftModel::select(['id','title','picture','card_price','price'])->first();
        if(!$gift){
            return $this->json('0','您送的礼物不存在');
        }
        $receiveUser = UserModel::where("user_id",$receive_user_id)->first();
        if(!$receiveUser){
            return $this->json('0','接受者用户不存在');
        }
        $giftArr = $gift->toArray();
        $userProfile = UserProfileModel::where("user_id",$user_id)->first()->toArray();
        if($userProfile["card_level"]>0){
            $price = $giftArr["card_price"];
        }else{
            $price = $giftArr["price"];
        }

        $userAccount = UserAccountModel::where("user_id",$user_id)->first()->toArray();
        UserComponent::checkBalance($userAccount,$price);

        $userGift = new UserGiftModel();
        $userGift->give_user_id = $user_id;
        $userGift->receive_user_id = $receive_user_id;
        $userGift->gift_id = $gift_id;
        $userGift->gift_title = $giftArr["title"];
        $userGift->gift_picture = $giftArr["picture"];
        $userGift->gift_price =$price;
        $userGift->create_time = time();
        $result = $userGift->saveOrFail();
        if($result){
            $user_gift_id = $userGift->getQueueableId();
            UserComponent::price_save('gift',$user_id,$price,-1,'gold',$user_gift_id);
            UserComponent::price_save('gift',$receive_user_id,$price,1,'gold',$user_gift_id);
            UserAccountModel::where("user_id",$receive_user_id)->increment("gift_num",1);
            return $this->json('1','送礼物成功');
        }else{
            return $this->json('0','礼物发送失败');
        }
    }

}