<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Job;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\Http\Controllers\Myorder\EvaluateController;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserCarModel;
use App\Models\ConfigModel;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\JobOrderModel;
use App\Models\UserProfileModel;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 订单列表
 */
class OrderListController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 我是服务方
     */
    public function service(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $model = $request->input('model');  //1  全部 2 交易中  3已完成
        $model = empty($model)?1:$model;
        $page_index  = intval($request->input('page_index'))?intval($request->input('page_index')):1;
        $limit = 10;
        $offest = ($page_index-1) * $limit;
        $list = JobOrderModel::select([
            'id',
            'user_id',
            'job_user_id',
            'job_class_id',
            'job_class_name',
            'job_model',
            'unit_price',
            'service_time',
            'status',
            'create_time',
        ])->where(function($query) use ($user_id){   //发布需求的人
            $query->where('job_user_id','=',$user_id)
                ->where('job_model','=',2);
        })->orwhere(function($query) use ($user_id){    //约她下单的人
            $query->where('user_id','=',$user_id)
                ->where('job_model','=',1);
        });
        if($model == 2){
            $list = $list->whereIn("status",array(2,3));
        }elseif($model == 3){
            $list = $list->where("status",4);
        }
        $list = $list->skip($offest)->take($limit)->orderBy('create_time', 'ASC')->get()->toArray();
        foreach($list AS &$item){
            $status_title = "";
            if($item["job_user_id"] == $user_id){   //我是发布需求的人
                $profile_user_id = $item["user_id"];
                switch($item["status"]){
                    case 0:
                        $status_title = "待您确认";
                        break;
                    case 1:
                        $status_title = "待您付款";
                        break;
                    case 2:
                        $status_title = "待您确认";
                        break;
                    case 3:
                        $status_title = "服务中";
                        break;
                    case 4:
                        $status_title = "已完成";
                        break;
                    case -1:
                        $status_title = "您已取消";
                        break;
                    case -2:
                        $status_title = "您已拒绝";
                        break;
                }
            }else{  //我是约她下单的人
                $profile_user_id = $item["job_user_id"];
                switch($item["status"]){
                    case 0:
                        $status_title = "待对方确认";
                        break;
                    case 1:
                        $status_title = "待您付款";
                        break;
                    case 2:
                        $status_title = "待您确认";
                        break;
                    case 3:
                        $status_title = "服务中";
                        break;
                    case 4:
                        $status_title = "已完成";
                        break;
                    case -1:
                        $status_title = "您已取消";
                        break;
                    case -2:
                        $status_title = "对方已拒绝";
                        break;
                }
            }
            $item["status_title"] = $status_title;
            //获取用户信息
            $userProfile = UserProfileModel::where("user_id",$profile_user_id)->first()->toArray();
            $item["avatar"] = UtilFunction::getUrlImg($userProfile["avatar"]);
            $item["nick_name"] = $userProfile["nick_name"];
            $item["gender"] = $userProfile["gender"];
            $item["age"] = $userProfile["age"];
            $item["deposit_auth"] = $userProfile["deposit_auth"];
            $item["card_level"] = $userProfile["card_level"];
            $item["car_logo"] = "";
            $item["plus_time"] = "";
            if($userProfile["car_auth"] == 1){
                $userCar = UserCarModel::where("user_id",$profile_user_id)->first();
                if($userCar){
                    $item["car_logo"] = UtilFunction::getUrlImg($userCar->car_logo);
                }
            }
            //待确认订单获取剩余时间
            if($item["status"] == 0) {
                $plus_time = UtilFunction::get_order_plus_time($item["create_time"]);
                if ($plus_time > 0) {
                    $item["plus_time"] = $plus_time . "后过期";
                }
            }


            $item["create_time"] = date("Y-m-d",$item["create_time"]);

        }
        return $this->json('1','获取成功',$list);
    }

    /**
     * 我是需求方
     */
    public function demand(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $model = $request->input('model');  //1  全部 2 交易中  3已完成
        $model = empty($model)?1:$model;
        $page_index  = intval($request->input('page_index'))?intval($request->input('page_index')):1;
        $limit = 10;
        $offest = ($page_index-1) * $limit;
        $list = JobOrderModel::select([
            'id',
            'user_id',
            'job_user_id',
            'job_class_id',
            'job_class_name',
            'job_model',
            'unit_price',
            'service_time',
            'status',
            'create_time',
        ])->where(function($query) use ($user_id){   //应邀下单的人
            $query->where('user_id','=',$user_id)
                ->where('job_model','=',2);
        })->orwhere(function($query) use ($user_id){    //发布兼职的人
            $query->where('job_user_id','=',$user_id)
                ->where('job_model','=',1);
        });
        if($model == 2){
            $list = $list->whereIn("status",array(2,3));
        }elseif($model == 3){
            $list = $list->where("status",4);
        }
        $list = $list->skip($offest)->take($limit)->orderBy('create_time', 'ASC')->get()->toArray();
        foreach($list AS &$item){
            $status_title = "";
            if($item["user_id"] == $user_id){   //应邀下单的人
                $profile_user_id = $item["job_user_id"];
                switch($item["status"]){
                    case 0:
                        $status_title = "待对方确认";
                        break;
                    case 1:
                        $status_title = "待对方付款";
                        break;
                    case 2:
                        $status_title = "已支付等待服务";
                        break;
                    case 3:
                        $status_title = "服务中";
                        break;
                    case 4:
                        $status_title = "已完成";
                        break;
                    case -1:
                        $status_title = "对方已取消";
                        break;
                    case -2:
                        $status_title = "对方已拒绝";
                        break;
                }
            }else{  //我是约她下单的人
                $profile_user_id = $item["user_id"];
                switch($item["status"]){
                    case 0:
                        $status_title = "待您确认";
                        break;
                    case 1:
                        $status_title = "待对方付款";
                        break;
                    case 2:
                        $status_title = "已支付等待服务";
                        break;
                    case 3:
                        $status_title = "服务中";
                        break;
                    case 4:
                        $status_title = "已完成";
                        break;
                    case -1:
                        $status_title = "对方已取消";
                        break;
                    case -2:
                        $status_title = "您已拒绝";
                        break;
                }
            }
            $item["status_title"] = $status_title;
            //获取用户信息
            $userProfile = UserProfileModel::where("user_id",$profile_user_id)->first()->toArray();
            $item["avatar"] = UtilFunction::getUrlImg($userProfile["avatar"]);
            $item["nick_name"] = $userProfile["nick_name"];
            $item["gender"] = $userProfile["gender"];
            $item["age"] = $userProfile["age"];
            $item["height"] = $userProfile["height"];
            $item["weight"] = $userProfile["weight"];
            $item["deposit_auth"] = $userProfile["deposit_auth"];
            $item["card_level"] = $userProfile["card_level"];
            $item["plus_time"] = "";

            //待确认订单获取剩余时间
            if($item["status"] == 0) {
                $plus_time = UtilFunction::get_order_plus_time($item["create_time"]);
                if ($plus_time > 0) {
                    $item["plus_time"] = $plus_time . "后过期";
                }
            }

            $item["create_time"] = date("Y-m-d",$item["create_time"]);

        }
        return $this->json('1','获取成功',$list);


    }





}