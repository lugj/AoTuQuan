<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Job;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\ConfigModel;
use App\Models\CollectionModel;
use App\Models\JobModel;
use App\Models\FollowModel;
use App\Models\FriendModel;
use App\Models\UserAccountModel;
use App\Models\UserProfileModel;
use App\Models\WechatSeeModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 用户操作相关
 */
class UserController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }
    /**
     * 关注
     */
    public function follow(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $follow_user_id = $request->input('follow_user_id');
        if(empty($follow_user_id)){
            return $this->json('0','被关注者ID不能为空!');
        }
        if($user_id == $follow_user_id){
            return $this->json('0','自己不能关注自己!');
        }

        $follow = FollowModel::where("fans_user_id",$user_id)->where("follow_user_id",$follow_user_id)->first();
        if($follow){
            return $this->json('0','您已经关注了对方!');
        }else{
            $follow = new FollowModel();
            $follow->fans_user_id = $user_id;
            $follow->follow_user_id = $follow_user_id;
            $follow->create_time = time();
            $result = $follow->saveOrFail();
            if($result){
                //验证双方是否互相关注 是的话 成为好友
                $follow = FollowModel::where("fans_user_id",$follow_user_id)->where("follow_user_id",$user_id)->first();
                $is_friend = UserComponent::checkFriend($user_id,$follow_user_id);
                if(!$is_friend && $follow){
                    $cur_time = time();
                    //判断是否已存在好友申请记录
                    $friend = FriendModel::where(function($query) use ($user_id,$follow_user_id){
                        $query->where('apply_user_id','=',$user_id)
                            ->where('agree_user_id','=',$follow_user_id);
                    })->orwhere(function($query) use ($user_id,$follow_user_id){
                        $query->where('apply_user_id','=',$follow_user_id)
                            ->where('agree_user_id','=',$user_id);
                    })->first();
                    if($friend){
                        $friend->agree_time = $cur_time;
                        $friend->status = 1;
                        $friend->source = 2;
                        $friend->saveOrFail();
                    }else{
                        $friend = new FriendModel();
                        $friend->apply_user_id = $user_id;
                        $friend->agree_user_id = $follow_user_id;
                        $friend->agree_time = $cur_time;
                        $friend->apply_time = $cur_time;
                        $friend->status = 1;
                        $friend->source = 2;
                        $friend->saveOrFail();
                    }

                }
                //关注成功 发推送
                $send_data = array(
                    'user_id'=>$follow_user_id,
                    'message'=>"关注提醒",
                    "desc"=>"有人关注你了，赶紧去看看吧",
                );
                UtilFunction::sendPush($send_data);

                return $this->json('1','关注成功!');
            }else{
                return $this->json('0','关注失败，请稍后再试!');
            }
        }
    }

    /**
     * 取消关注
     */
    public function cancel_follow(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $follow_user_id = $request->input('follow_user_id');
        if(empty($follow_user_id)){
            return $this->json('0','被关注者ID不能为空!');
        }
        if($user_id == $follow_user_id){
            return $this->json('0','自己不能取消关注自己!');
        }
        $result = FollowModel::where("fans_user_id",$user_id)->where("follow_user_id",$follow_user_id)->delete();
        if($result){
            $is_friend = UserComponent::checkFriend($user_id,$follow_user_id);
            if($is_friend){
                FriendModel::where("apply_user_id",$user_id)->where("agree_user_id",$follow_user_id)->delete();
            }
            return $this->json('1','取关成功!');
        }else{
            return $this->json('0','取消关注失败，请稍后再试!');
        }
    }

    /**
     * 收藏
     */
    public function collection(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $model = $request->input('model');  //1 兼职收藏  2视频详情收藏
        if($model == 1){
            $job_id = $request->input('job_id');
            if(empty($job_id)){
                return $this->json('0','参数为空!');
            }
            $collection = CollectionModel::where("user_id",$user_id)->where("job_id",$job_id)->first();
            if($collection){
                return $this->json('0','您已经收藏过了!');
            }
            $job = JobModel::where("id",$job_id)->first();
            if(!$job){
                return $this->json('0','您收藏的兼职不存在!');
            }
            $job = $job->toArray();
            $job_class_name = $job['job_class_name'];
        }elseif($model == 2){
            $job_id = 0;
            $job_class_name = "";
        }else{
            return $this->json('0','分类有误!');
        }



        $collection = new CollectionModel();
        $collection->job_id = $job_id;
        $collection->model = $model;
        $collection->job_class_name = $job_class_name;
        $collection->user_id = $user_id;
        $result = $collection->saveOrFail();
        if($result){
            return $this->json('1','收藏成功!');
        }else{
            return $this->json('0','收藏失败!');
        }

    }

    /**
     * 取消收藏
     */
    public function cancel_collection(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $job_id = $request->input('job_id');
        if(empty($job_id)){
            return $this->json('0','参数为空!');
        }
        $result = CollectionModel::where("user_id",$user_id)->where("job_id",$job_id)->delete();
        if($result){
            return $this->json('1','取消收藏成功!');
        }else{
            return $this->json('0','取消收藏失败!');
        }
    }

    /**
     * 查看微信号
     */
    public function show_wechat(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $result = array();
        $wechat_user_id = $request->input('wechat_user_id');

        $userProfile = UserProfileModel::where("user_id",$wechat_user_id)->first();
        if(!$userProfile || empty($userProfile->wechat)){
            return $this->json('0','该用户或微信号不存在!');
        }
        if($user_id == $wechat_user_id){
            $result["already_see"] = 1;
            $result["price"] = 0;
            $result["wechat"] = $userProfile->wechat;
            $result["deposit_auth"] = $userProfile->deposit_auth;
            return $this->json('1','查看成功!',$result);
        }

        $alreadySee = WechatSeeModel::where("see_user_id",$user_id)->where("cover_user_id",$wechat_user_id)->first();
        if($alreadySee){
            $result["already_see"] = 1;
            $result["price"] = $alreadySee->price;
            $result["wechat"] = $userProfile->wechat;
            $result["deposit_auth"] = $userProfile->deposit_auth;
            return $this->json('1','查看成功!',$result);
        }else{
            $userData = UserAccountModel::select([
                'at_user_profile.card_level',
                'at_user_account.balance',
            ])
                ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_user_account.user_id")
                ->where("at_user_account.user_id",$user_id)->first()->toArray();
            $config = new ConfigModel();
            if($userData["card_level"]>0){  //会员
                $price = $config->get_value("card_see_wechat_price");
            }else{  //非会员
                $price = $config->get_value("see_wechat_price");
            }

            $result["already_see"] = 0;
            $result["price"] = $price;
            $result["wechat"] = "******";
            $result["deposit_auth"] = $userProfile->deposit_auth;
            return $this->json('1','未查看过!',$result);
        }

    }

    /**
     * 查看微信号
     */
    public function see_wechat(Request $request)
    {
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $result = array();
        $wechat_user_id = $request->input('wechat_user_id');

        $userProfile = UserProfileModel::where("user_id",$wechat_user_id)->first();
        if(!$userProfile || empty($userProfile->wechat)){
            return $this->json('0','该用户或微信号不存在!');
        }
        if($user_id == $wechat_user_id){
            $result["price"] = 0;
            $result["wechat"] = $userProfile->wechat;
            return $this->json('1','查看成功!',$result);
        }

        $alreadySee = WechatSeeModel::where("see_user_id",$user_id)->where("cover_user_id",$wechat_user_id)->first();
        if($alreadySee){
            $result["price"] = $alreadySee->price;
            $result["wechat"] = $userProfile->wechat;
            return $this->json('1','查看成功!',$result);
        }

        $userData = UserAccountModel::select([
            'at_user_profile.card_level',
            'at_user_account.balance',
            'at_user_account.frozen',
        ])
            ->leftJoin("at_user_profile","at_user_profile.user_id","=","at_user_account.user_id")
            ->where("at_user_account.user_id",$user_id)->first()->toArray();
        $config = new ConfigModel();
        if($userData["card_level"]>0){  //会员
            $price = $config->get_value("card_see_wechat_price");
        }else{  //非会员
            $price = $config->get_value("see_wechat_price");
        }
        UserComponent::checkBalance($userData,$price);

        $wechatSee = new WechatSeeModel();
        $wechatSee->see_user_id = $user_id;
        $wechatSee->cover_user_id = $wechat_user_id;
        $wechatSee->see_time = time();
        $wechatSee->price = $price;
        if($wechatSee->saveOrFail()){
            UserComponent::price_save("see_wechat",$user_id,$price,-1);
            UserComponent::price_save("see_wechat",$wechat_user_id,$price,1);
            $result["price"] = $price;
            $result["wechat"] = $userProfile->wechat;
            return $this->json('1','查看成功!',$result);
        }else{
            return $this->json('0','操作失败!');
        }


    }
}