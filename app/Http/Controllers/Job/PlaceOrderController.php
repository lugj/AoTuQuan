<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Job;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\Http\Controllers\Myorder\EvaluateController;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\CollectionModel;
use App\Models\ConfigModel;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\FollowModel;
use App\Models\UserGiftModel;
use App\Models\UserProfileModel;
use App\Models\UserAccountModel;
use App\Models\UserCarModel;
use App\Models\EvaluateModel;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 下单页面
 */
class PlaceOrderController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 兼职
     * 下单约她
     */
    public function work_about_her(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $job_id = $request->input('job_id');
        if(empty($job_id)){
            return $this->json('0','参数为空!');
        }
        $job = JobModel::select(['user_id','job_class_id','job_class_name','price','service_hourse','introduce','voice','voice_time'])->where('model',1)->where("id",$job_id)->first();
        if(!$job){
            return $this->json('0','该兼职详情不存在!');
        }




        $result = array();
        $job_arr = $job->toArray();
        $job_arr["voice"] = UtilFunction::getUrlImg($job_arr["voice"]);
//        $job_arr["voice"] = UtilFunction::getUrlImg($job_arr["voice"]);

        //查看用户基础信息
        $userProfile = UserProfileModel::select(['nick_name','avatar','deposit_auth','card_level','age','gender','height','weight','credit_num'])->where("user_id",$job->user_id)->first()->toArray();
        $userProfile["avatar"] = UtilFunction::getUrlImg($userProfile["avatar"]);
        $result["user_profile"] = $userProfile;

        //获取距离
        $userSetting1 = UserSettingModel::where("user_id",$job->user_id)->first()->toArray();
        $userSetting2 = UserSettingModel::where("user_id",$user_id)->first()->toArray();
        $distance = UtilFunction::get_distance($userSetting1["lat"],$userSetting1["lon"],$userSetting2["lat"],$userSetting2["lon"]);

        $result["job"] = $job_arr;
        $result["distance"] = $distance;
        //我的押金认证
        $myProfile = UserProfileModel::select(['deposit_auth'])->where("user_id",$user_id)->first()->toArray();
        $result["my_deposit_auth"] = $myProfile["deposit_auth"];
        $config = new ConfigModel();
        $result["trip_mode"] = explode(',',$config->get_value('trip_mode'));
        return $this->json('1','获取成功!',$result);
    }

    /**
     * 需求
     * 下单应邀
     */
    public function demand_invite(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $job_id = $request->input('job_id');
        if(empty($job_id)){
            return $this->json('0','参数为空!');
        }
        $job = JobModel::select(['user_id','job_class_id','job_class_name','price','service_hourse','start_time','service_address','order_price'])->where("model",2)->where("id",$job_id)->first();
        if(!$job){
            return $this->json('0','该兼职详情不存在!');
        }
        $result = array();
        $job_arr = $job->toArray();


        //查看用户基础信息
        $userProfile = UserProfileModel::select(['nick_name','avatar','age','gender','deposit_auth'])->where("user_id",$job->user_id)->first()->toArray();

        $userProfile["avatar"] = UtilFunction::getUrlImg($userProfile["avatar"]);
        $result["user_profile"] = $userProfile;

        //获取距离
        $userSetting1 = UserSettingModel::where("user_id",$job->user_id)->first()->toArray();
        $userSetting2 = UserSettingModel::where("user_id",$user_id)->first()->toArray();
        $distance = UtilFunction::get_distance($userSetting1["lat"],$userSetting1["lon"],$userSetting2["lat"],$userSetting2["lon"]);
        $result["job"] = $job_arr;
        $result["distance"] = $distance;
        //我的押金认证
        $myProfile = UserProfileModel::select(['deposit_auth'])->where("user_id",$user_id)->first()->toArray();
        $result["my_deposit_auth"] = $myProfile["deposit_auth"];
        return $this->json('1','获取成功!',$result);
    }

}