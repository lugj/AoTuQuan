<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Job;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\Http\Controllers\Myorder\EvaluateController;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\UserCarModel;
use App\Models\ConfigModel;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\JobOrderModel;
use App\Models\UserProfileModel;
use App\Models\ComplainModel;
use App\Models\UserAccountModel;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 订单操作
 */
class OrderOperateController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 接受
     */
    public function accept(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $order_id = $request->input('order_id');
        if(empty($order_id)){
            return $this->json('0','参数为空');
        }
        $jobOrder = JobOrderModel::where("id",$order_id)->first();
        if(!$jobOrder){
            return $this->json('0','订单不存在!');
        }

        $jobOrderArr = $jobOrder->toArray();
        if($jobOrderArr["job_user_id"] != $user_id){
            return $this->json('0','您无权限操作!');
        }
        if($jobOrderArr["status"] != '0'){
            return $this->json('0','订单状态有误');
        }
        $jobOrder->status = 1;
        $jobOrder->accept_time = time();
        $result = $jobOrder->saveOrFail();
        if($result){
            $send_data = array(
                'user_id'=>$jobOrderArr["user_id"],
                'message'=>"订单通知",
                "desc"=>"你的订单有新动态，赶紧去看看吧",
            );
            UtilFunction::sendPush($send_data);
            return $this->json('1','操作成功');
        }else{
            return $this->json('0','操作失败，请稍后再试');
        }

    }

    /**
     * 拒绝
     */
    public function refuse(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $order_id = $request->input('order_id');
        if(empty($order_id)){
            return $this->json('0','参数为空');
        }
        $jobOrder = JobOrderModel::where("id",$order_id)->first();
        if(!$jobOrder){
            return $this->json('0','订单不存在!');
        }
        $jobOrderArr = $jobOrder->toArray();
        if($jobOrderArr["job_user_id"] != $user_id){
            return $this->json('0','您无权限操作!');
        }
        if($jobOrderArr["status"] != '0'){
            return $this->json('0','订单状态有误');
        }
        $jobOrder->status = -2;
        $jobOrder->refuse_time = time();
        $result = $jobOrder->saveOrFail();
        if($result){
            $send_data = array(
                'user_id'=>$jobOrderArr["user_id"],
                'message'=>"订单通知",
                "desc"=>"你的订单有新动态，赶紧去看看吧",
            );
            UtilFunction::sendPush($send_data);
            return $this->json('1','操作成功');
        }else{
            return $this->json('0','操作失败，请稍后再试');
        }

    }

    /**
     * 付款
     */
    public function pay(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $order_id = $request->input('order_id');
        if(empty($order_id)){
            return $this->json('0','参数为空');
        }
        $jobOrder = JobOrderModel::where("id",$order_id)->first();
        if(!$jobOrder){
            return $this->json('0','订单不存在!');
        }
        $jobOrderArr = $jobOrder->toArray();
        if(!(($jobOrderArr["job_user_id"] == $user_id && $jobOrderArr["job_model"] == 2) || ($jobOrderArr["user_id"] == $user_id && $jobOrderArr["job_model"] == 1))){
            return $this->json('0','您无权限操作!');
        }
        if($jobOrderArr["status"] != 1){
            return $this->json('0','订单状态有误');
        }

        $jobOrder->status = 2;
        $jobOrder->pay_time = time();
        $jobOrder->is_pay = 1;
        $result = $jobOrder->saveOrFail();
        if($result){
            $jobOrderArr["price"] = $jobOrderArr["price"]*100;
            UserComponent::price_save('job_order',$user_id,$jobOrderArr["price"],-1,'gold',$order_id);
            $user_income = $jobOrderArr["price"];

            if($jobOrderArr["safety_user_id"]>0 && $jobOrderArr["safety_status"] == 1){
                $config = new ConfigModel();
                $safety_income_pre = $config->get_value('safety_income_pre');
                if($safety_income_pre>0){
                    $safety_income = round($jobOrderArr["price"]/100*$safety_income_pre,2);
                    $user_income = $jobOrderArr["price"]-$safety_income;
                    //保安员的收入
                    UserComponent::price_save('safety',$jobOrderArr["safety_user_id"],$safety_income,1,'gold',$order_id);
                    $jobOrder->safety_price = $safety_income;
                    $jobOrder->saveOrFail();
                }
            }
            //代理商收入
            if($jobOrderArr["agent_user_id"]>0){
                $config = new ConfigModel();
                $agent_income_pre = $config->get_value('agent_income_pre');
                if($agent_income_pre>0){
                    $agent_income = round($jobOrderArr["price"]/100*$agent_income_pre,2);
                    $user_income = $user_income-$agent_income;
                    UserComponent::price_save('agent',$jobOrderArr["agent_user_id"],$agent_income,1,'gold',$order_id);
                    $jobOrder->agent_price = $agent_income;
                    $jobOrder->saveOrFail();
                }
            }




            if($jobOrderArr["user_id"] == $user_id){
                $push_user_id = $jobOrderArr["job_user_id"];
                UserComponent::price_save('job_order',$jobOrderArr["job_user_id"],$user_income,1,'gold',$order_id);
            }else{
                $push_user_id = $jobOrderArr["user_id"];
                UserComponent::price_save('job_order',$jobOrderArr["user_id"],$user_income,1,'gold',$order_id);
            }


            $send_data = array(
                'user_id'=>$push_user_id,
                'message'=>"订单通知",
                "desc"=>"你的订单有新动态，赶紧去看看吧",
            );
            UtilFunction::sendPush($send_data);
            return $this->json('1','操作成功');
        }else{
            return $this->json('0','操作失败，请稍后再试');
        }

    }

    /**
     * 取消
     */
    public function cancel(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $order_id = $request->input('order_id');
        if(empty($order_id)){
            return $this->json('0','参数为空');
        }
        $jobOrder = JobOrderModel::where("id",$order_id)->first();
        if(!$jobOrder){
            return $this->json('0','订单不存在!');
        }
        $jobOrderArr = $jobOrder->toArray();
        if(!(($jobOrderArr["job_user_id"] == $user_id && $jobOrderArr["job_model"] == 2) || ($jobOrderArr["user_id"] == $user_id && $jobOrderArr["job_model"] == 1))){
            return $this->json('0','您无权限操作!');
        }
        if($jobOrderArr["status"] != '1' && $jobOrderArr["status"] != '2'){
            return $this->json('0','订单状态有误');
        }
        $jobOrder->status = -1;
        $jobOrder->cancel_time = time();
        $result = $jobOrder->saveOrFail();
        if($result){
            if($jobOrderArr["user_id"] == $user_id){
                $push_user_id = $jobOrderArr["job_user_id"];
            }else{
                $push_user_id = $jobOrderArr["user_id"];
            }
            $send_data = array(
                'user_id'=>$push_user_id,
                'message'=>"订单通知",
                "desc"=>"你的订单有新动态，赶紧去看看吧",
            );
            UtilFunction::sendPush($send_data);
            return $this->json('1','操作成功');
        }else{
            return $this->json('0','操作失败，请稍后再试');
        }

    }

    /**
     * 开始服务
     */
    public function start_service(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $order_id = $request->input('order_id');
        if(empty($order_id)){
            return $this->json('0','参数为空');
        }
        $jobOrder = JobOrderModel::where("id",$order_id)->first();
        if(!$jobOrder){
            return $this->json('0','订单不存在!');
        }
        $jobOrderArr = $jobOrder->toArray();
        if(!(($jobOrderArr["job_user_id"] == $user_id && $jobOrderArr["job_model"] == 2) || ($jobOrderArr["user_id"] == $user_id && $jobOrderArr["job_model"] == 1))){
            return $this->json('0','您无权限操作!');
        }
        if($jobOrderArr["status"] != '2'){
            return $this->json('0','订单状态有误');
        }
        $jobOrder->status = 3;
        $jobOrder->start_service_time = time();
        $result = $jobOrder->saveOrFail();
        if($result){
            if($jobOrderArr["user_id"] == $user_id){
                $push_user_id = $jobOrderArr["job_user_id"];
            }else{
                $push_user_id = $jobOrderArr["user_id"];
            }
            $send_data = array(
                'user_id'=>$push_user_id,
                'message'=>"订单通知",
                "desc"=>"你的订单有新动态，赶紧去看看吧",
            );
            UtilFunction::sendPush($send_data);
            return $this->json('1','操作成功');
        }else{
            return $this->json('0','操作失败，请稍后再试');
        }

    }

    /**
     * 完成服务
     */
    public function complete_service_time(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $order_id = $request->input('order_id');
        if(empty($order_id)){
            return $this->json('0','参数为空');
        }
        $jobOrder = JobOrderModel::where("id",$order_id)->first();
        if(!$jobOrder){
            return $this->json('0','订单不存在!');
        }
        $jobOrderArr = $jobOrder->toArray();
        if(!(($jobOrderArr["job_user_id"] == $user_id && $jobOrderArr["job_model"] == 2) || ($jobOrderArr["user_id"] == $user_id && $jobOrderArr["job_model"] == 1))){
            return $this->json('0','您无权限操作!');
        }
        if($jobOrderArr["status"] != '3'){
            return $this->json('0','订单状态有误');
        }
        $jobOrder->status = 4;
        $jobOrder->complete_service_time = time();
        $result = $jobOrder->saveOrFail();
        if($result){
            UserProfileModel::where("user_id",$jobOrderArr["user_id"])->increment("credit_num",1);
            UserProfileModel::where("user_id",$jobOrderArr["job_user_id"])->increment("credit_num",1);
            if($jobOrderArr["user_id"] == $user_id){
                $push_user_id = $jobOrderArr["job_user_id"];
            }else{
                $push_user_id = $jobOrderArr["user_id"];
            }
            $send_data = array(
                'user_id'=>$push_user_id,
                'message'=>"订单通知",
                "desc"=>"你的订单有新动态，赶紧去看看吧",
            );
            UtilFunction::sendPush($send_data);
            return $this->json('1','操作成功');
        }else{
            return $this->json('0','操作失败，请稍后再试');
        }

    }

    /**
     * 投诉
     */
    public function complaint(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $order_id = $request->input('order_id');
        if(empty($order_id)){
            return $this->json('0','参数为空');
        }
        $jobOrder = JobOrderModel::where("id",$order_id)->first();
        if(!$jobOrder){
            return $this->json('0','订单不存在!');
        }
        $jobOrderArr = $jobOrder->toArray();
        if(!(($jobOrderArr["job_user_id"] == $user_id && $jobOrderArr["job_model"] == 2) || ($jobOrderArr["user_id"] == $user_id && $jobOrderArr["job_model"] == 1))){
            return $this->json('0','您无权限操作!');
        }
        if($jobOrderArr["status"] != '3'){
            return $this->json('0','订单状态有误');
        }

        $reason = $request->input('reason');
        $content = $request->input('content');
        $pictures = $request->input('pictures');
        if(empty($reason)){
            return $this->json('0','请选择举报原因');
        }
        $pictures = explode(',',$pictures);
        foreach($pictures AS &$pitem){
            $pitem = UtilFunction::getRootImg($pitem);
        }
        $pictures = implode(',',$pictures);
        $complain = new ComplainModel();
        $complain->user_id = $user_id;
        $complain->order_id = $order_id;
        $complain->reason = $reason;
        $complain->content = $content;
        $complain->pictures = $pictures;
        $result = $complain->saveOrFail();
        if($result){
            return $this->json('1','操作成功');
        }else{
            return $this->json('0','操作失败，请稍后再试');
        }

    }





}