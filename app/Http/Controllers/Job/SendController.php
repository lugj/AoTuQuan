<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Job;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\ConfigModel;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 发布
 */
class SendController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke($function,Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return call_user_func_array(array($this,$function), array($request));
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 发布页 信息查看
     */
    public function info(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $result = array();
        //获取分类菜单
        $job_class = JobClassModel::select(['id','title'])->orderBy("sort","ASC")->get()->toArray();
        $result["job_class"] = $job_class;
        $userProfile = UserProfileModel::where("user_id",$user_id)->first();
        $result["deposit_auth"] = $userProfile->deposit_auth;
        $config = new ConfigModel();
        $result["trip_mode"] = explode(',',$config->get_value('trip_mode'));
        return $this->json('1','获取成功!',$result);
    }

    /**
     * 验证限制
     */
    public function _check_limit($model,$user){
        $config = new ConfigModel();
        $user = UserProfileModel::where("user_id",$user["user_id"])->first()->toArray();
        if($model == 'work'){    //兼职

            if($user["card_level"]>0){  //会员
                $num = $config->get_value('card_send_work_limit');
            }else{
                $num = $config->get_value('send_work_limit');
            }
            //获取该用户当前以及发布的兼职数量
            $job_num = JobModel::where("user_id",$user["user_id"])->where("model",1)->whereIn('status',array(0,1))->count();
            if($job_num>=$num){
                return false;
            }else{
                return true;
            }
        }elseif($model == 'demand'){
            if($user["card_level"]>0){  //会员
                $num = $config->get_value('card_send_demand_limit');
            }else{
                $num = $config->get_value('send_demand_limit');
            }
            //获取该用户当前以及发布的兼职数量
            $demand_num = JobModel::where("user_id",$user["user_id"])->where("model",2)->whereIn('status',array(0,1))->count();
            if($demand_num>=$num){
                return false;
            }else{
                return true;
            }
        }
    }

    /**
     * 发布兼职
     */
    public function work(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $customAttr = [
            'class_id'=>'服务类型',
            'introduce'=>'服务介绍',
            'price'=>'每小时价格',
            'voice'=>'语音',
        ];
        $this->validate($request, [
            'class_id'=>'required',
            'introduce'=>'required',
            'price'=>'required',
            'voice'=>'required',
        ],[ ], $customAttr);
        $check_num = $this->_check_limit('work',$user);
        if($check_num){
            $class_id = $request->input('class_id');
            $introduce = $request->input('introduce');
            $price = $request->input('price');
            $service_hourse = $request->input('service_hourse');
            $remark = $request->input('remark');
            $voice = $request->input('voice');
            $voice_time = $request->input("voice_time");
            if($class_id != 7){ //发布不是旅游,必须填起约时间
                if(empty($service_hourse)){
                    return $this->json('0','起约时间不能为空！');
                }
            }

            $voice_upload = UtilFunction::uploadFile("voice","voice",$voice);
            if($voice_upload){
                $job_class = JobClassModel::where("id",$class_id)->first();
                $job = new JobModel();
                $job->model = 1;
                $job->job_class_id = $class_id;
                $job->job_class_name = $job_class->title;
                $job->user_id = $user_id;
                $job->price = $price;
                $job->service_hourse = $service_hourse;
                $job->introduce = $introduce;
                $job->remark = $remark;
                $job->voice = $voice_upload;
                $job->voice_time = $voice_time;
                $job->create_time = time();
                if($job->saveOrFail()){
                    return $this->json('1','上传成功!');
                }else{
                    return $this->json('0','上传失败');
                }
            }else{
                return $this->json('0','语音上传失败');
            }


        }else{
            return $this->json('0','上传数量超出限制');
        }


    }

    /**
     * 发布需求
     */
    public function demand(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $customAttr = [
            'class_id'=>'服务类型',
            'service_address'=>'服务地址',
            'price'=>'每小时价格',
            'service_hourse'=>'服务时长',
            'start_time'=>'开始时间',
            'introduce'=>'需求说明',
            'age'=>'年龄要求',
            'gender'=>'性别',
        ];
        $this->validate($request, [
            'class_id'=>'required',
            'service_address'=>'required',
            'price'=>'required',
            'service_hourse'=>'required',
            'start_time'=>'required',
            'introduce'=>'required',
            'age'=>'required',
            'gender'=>'required',
        ],[ ], $customAttr);
        $check_num = $this->_check_limit('demand',$user);
        if($check_num){
            $class_id = $request->input('class_id');
            $service_address = $request->input('service_address');
            $price = $request->input('price');
            $service_hourse = $request->input('service_hourse');
            $start_time = $request->input('start_time');
            $introduce = $request->input('introduce');
            $age = $request->input('age');
            $gender = $request->input('gender');
            $trip_mode = $request->input('trip_mode');
            $lon = $request->input('lon');  //经度
            $lat = $request->input('lat');  //纬度
            if($class_id == 7){
                if(empty($trip_mode)){
                    return $this->json('0','请选择出行方式！');
                }
            }

            $job_class = JobClassModel::where("id",$class_id)->first();
            $job = new JobModel();
            $job->model = 2;
            $job->job_class_id = $class_id;
            $job->job_class_name = $job_class->title;
            $job->user_id = $user_id;
            $job->service_address = $service_address;
            $job->price = $price;
            $job->service_hourse = $service_hourse;
            $job->introduce = $introduce;
            $job->start_time = $start_time;
            $job->age = $age;
            $job->gender = $gender;
            $job->trip_mode = $trip_mode;
            $job->order_price = round($price*$service_hourse,2);
            $job->lon = $lon;
            $job->lat = $lat;
            $job->create_time = time();
            if($job->saveOrFail()){
                return $this->json('1','发布成功!');
            }else{
                return $this->json('0','发布失败');
            }

        }else{
            return $this->json('0','上传数量超出限制');
        }
    }



}