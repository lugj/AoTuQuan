<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Job;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\EvaluateModel;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\UserModel;
use App\Models\JobOrderModel;
use App\Models\VideoChatModel;
use App\Models\UserProfileModel;
use App\Models\ComplainModel;
use App\Models\UserAccountModel;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 订单详情
 */
class OrderDetailController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->order_detail($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 订单详情
     */
    public function order_detail(Request $request){
        $user = UserComponent::check_token($request);
        $result = array();
        $user_id = $user['user_id'];
        $order_id = $request->input('order_id');
        if(empty($order_id)){
            return $this->json('0','参数为空');
        }
        $jobOrder = JobOrderModel::where("id",$order_id)->first();
        if(!$jobOrder){
            return $this->json('0','订单不存在!');
        }

        $jobOrderArr = $jobOrder->toArray();
        if($jobOrderArr["user_id"] == $user_id){
            $profile_user_id = $jobOrderArr["job_user_id"];
        }else{
            $profile_user_id = $jobOrderArr["user_id"];
        }
        $userProfile = UserProfileModel::where("user_id",$profile_user_id)->first()->toArray();
        $user_data = array();
        $user_data["avatar"] = UtilFunction::getUrlImg($userProfile["avatar"]);
        $user_data["nick_name"] = $userProfile["nick_name"];
        $user_data["gender"] = $userProfile["gender"];
        $user_data["age"] = $userProfile["age"];
        $user_data["height"] = $userProfile["height"];
        $user_data["weight"] = $userProfile["weight"];
        $user_data["deposit_auth"] = $userProfile["deposit_auth"];
        $user_data["card_level"] = $userProfile["card_level"];
        $user_data["credit_num"] = $userProfile["credit_num"];

        $userSetting1 = UserSettingModel::where("user_id",$profile_user_id)->first()->toArray();
        $userSetting2 = UserSettingModel::where("user_id",$user_id)->first()->toArray();
        $distance = UtilFunction::get_distance($userSetting1["lat"],$userSetting1["lon"],$userSetting2["lat"],$userSetting2["lon"]);
        $user_data["distance"] = $distance;
        if(!empty($jobOrderArr["voice"])){
            $jobOrderArr["voice"] = UtilFunction::getUrlImg($jobOrderArr["voice"]);
        }
        unset($jobOrderArr["order_sn"]);
        unset($jobOrderArr["create_time"]);
        unset($jobOrderArr["accept_time"]);
        unset($jobOrderArr["refuse_time"]);
        unset($jobOrderArr["pay_time"]);
        unset($jobOrderArr["cancel_time"]);
        unset($jobOrderArr["start_service_time"]);
        unset($jobOrderArr["complete_service_time"]);
        $jobOrderArr["order_status_title"] = $this->_get_order_status_title($jobOrderArr,$user_id);
        $result["user_data"] = $user_data;
        $result["job_order"] = $jobOrderArr;
        return $this->json('1','获取成功',$result);
    }

    /**
     * 获取订单状态名称
     */
    public function _get_order_status_title($order,$user_id){
        $status_title = "";
        if($order["job_model"] == 1){   //兼职
            if($order["user_id"] == $user_id){
                switch($order["status"]){
                    case 0:
                        $status_title = "待对方确认";
                        break;
                    case 1:
                        $status_title = "待您付款";
                        break;
                    case 2:
                        $status_title = "待您确认";
                        break;
                    case 3:
                        $status_title = "服务中";
                        break;
                    case 4:
                        $status_title = "已完成";
                        break;
                    case -1:
                        $status_title = "您已取消";
                        break;
                    case -2:
                        $status_title = "对方已拒绝";
                        break;
                }
            }else{
                switch($order["status"]){
                    case 0:
                        $status_title = "待您确认";
                        break;
                    case 1:
                        $status_title = "待对方付款";
                        break;
                    case 2:
                        $status_title = "已支付等待服务";
                        break;
                    case 3:
                        $status_title = "服务中";
                        break;
                    case 4:
                        $status_title = "已完成";
                        break;
                    case -1:
                        $status_title = "对方已取消";
                        break;
                    case -2:
                        $status_title = "您已拒绝";
                        break;
                }
            }
        }elseif($order["job_model"] == 2){  //需求
            if($order["user_id"] == $user_id){
                switch($order["status"]){
                    case 0:
                        $status_title = "待对方确认";
                        break;
                    case 1:
                        $status_title = "待对方付款";
                        break;
                    case 2:
                        $status_title = "已支付等待服务";
                        break;
                    case 3:
                        $status_title = "服务中";
                        break;
                    case 4:
                        $status_title = "已完成";
                        break;
                    case -1:
                        $status_title = "对方已取消";
                        break;
                    case -2:
                        $status_title = "对方已拒绝";
                        break;
                }
            }else{
                switch($order["status"]){
                    case 0:
                        $status_title = "待您确认";
                        break;
                    case 1:
                        $status_title = "待您付款";
                        break;
                    case 2:
                        $status_title = "待您确认";
                        break;
                    case 3:
                        $status_title = "服务中";
                        break;
                    case 4:
                        $status_title = "已完成";
                        break;
                    case -1:
                        $status_title = "您已取消";
                        break;
                    case -2:
                        $status_title = "您已拒绝";
                        break;
                }
            }
        }
        return $status_title;
    }



}