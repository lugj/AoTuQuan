<?php
/**
 * Created by PhpStorm.
 * User: paulLu
 * Date: 2016/2/22
 * Time: 13:57
 */
namespace App\Http\Controllers\Job;
use App\Http\Controllers\ApiController;
use App\Http\Components\UserComponent;
use App\library\juhe\Identify;
use App\library\UtilFunction;
use App\Models\EvaluateModel;
use App\Models\JobClassModel;
use App\Models\JobModel;
use App\Models\UserModel;
use App\Models\JobOrderModel;
use App\Models\VideoChatModel;
use App\Models\UserProfileModel;
use App\Models\ComplainModel;
use App\Models\UserAccountModel;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;

use App\Http\Components\OuterDataComponent;
use App\library\UtilRegular;

/**
 * 评价
 */
class EvaluateController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            try{
                return $this->evaluate($request);
            }catch(\Exception $e){
                return $this->json('0','系统错误'.$e->getMessage());
            }
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }


    /**
     * 评价
     */
    public function evaluate(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];
        $model = $request->input('model'); //1 兼职评价 2视频评价
        $video_chat_id = $request->input('video_chat_id');    //被评价人用户ID
        $order_id = $request->input('order_id');
        $skill_star = $request->input('skill_star');    //兼职技能
        $attitude_star = $request->input('attitude_star');  //服务态度
        $photo_star = $request->input('photo_star');    //照片真实
        $content = $request->input('content');  //评价内容
        $evaluate = new EvaluateModel();
        $evaluate->model = $model;
        $evaluate->evaluate_user_id = $user_id;
        $evaluate->skill_star = $skill_star;
        $evaluate->attitude_star = $attitude_star;
        $evaluate->photo_star = $photo_star;
        $evaluate->content = $content;

        if($model == 1){
            if(!empty($order_id)){
                $jobOrder = JobOrderModel::where("id",$order_id)->first();
                if(!$jobOrder){
                    return $this->json('0','订单不存在!');
                }
                $jobOrderArr = $jobOrder->toArray();
                if(!(($jobOrderArr["job_user_id"] == $user_id && $jobOrderArr["job_model"] == 2) || ($jobOrderArr["user_id"] == $user_id && $jobOrderArr["job_model"] == 1))){
                    return $this->json('0','您无权限操作!');
                }
                if($jobOrderArr["status"] != '4'){
                    return $this->json('0','订单状态有误');
                }
                $evaluate->order_id = $order_id;
                $evaluate->job_id = $jobOrderArr["job_id"];
                if($jobOrderArr["job_user_id"] == $user_id){
                    $evaluate->user_id = $jobOrderArr["user_id"];
                    $b_user_id = $jobOrderArr["user_id"];
                }else{
                    $evaluate->user_id = $jobOrderArr["job_user_id"];
                    $b_user_id = $jobOrderArr["job_user_id"];
                }
                $evaluate->job_id = $jobOrderArr["job_id"];
            }else{
                return $this->json('0','订单号不能为空');
            }
        }elseif($model == 2){
            if(empty($video_chat_id)){
                return $this->json('0','视频聊天ID不存在');
            }
            $videoChat = VideoChatModel::where("id",$video_chat_id)->first();

            if(!$videoChat){
                return $this->json('0','视频聊天记录不存在');
            }
            $videoChat = $videoChat->toArray();
            $evaluate->video_chat_id = $video_chat_id;
            $evaluate->user_id = $videoChat["receive_user_id"];
            $b_user_id = $videoChat["receive_user_id"];
        }else{
            return $this->json('0','参数有误');
        }

        $result = $evaluate->saveOrFail();
        if($result){
            if($model == 1 && !empty($order_id)){
                $jobOrder->is_evaluate = 1;
                $jobOrder->saveOrFail();
            }
            $all_star = $skill_star+$attitude_star+$photo_star;
            if($all_star>=8){
                UserProfileModel::where("user_id",$b_user_id)->increment("credit_num",1);
            }else{
                UserProfileModel::where("user_id",$b_user_id)->decrement("credit_num",1);
            }
            //评价通知
            $userProfile = UserProfileModel::where("user_id",$user_id)->first()->toArray();
            $send_data = array(
                'user_id'=>$b_user_id,
                'message'=>"评价通知",
                "desc"=>$userProfile["nick_name"]."评价了您，赶紧去看看吧",
            );
            UtilFunction::sendPush($send_data);
            return $this->json('1','评价成功');
        }else{
            return $this->json('0','评价失败');
        }




    }



}