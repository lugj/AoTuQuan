<?php

namespace App\Http\Controllers\Data;
use App\Http\Components\UserComponent;
use App\Models\OtherShareModel;
use App\Models\UserSettingModel;
use App\Models\DataConfigModel;
use Illuminate\Http\Request;


/**
 * 获取分享信息
 */
class GetShareController extends DataController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->get_share($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 获取分享信息
     * @param Request $request
     * @author paulLu
     */
    public function get_share(Request $request){
        $type = $request->input('type');
        if(!in_array($type,array(1,2))){
            return $this->json('0','数据参数有误');
        }
        $data = OtherShareModel::select(['title','desc','picture'])->where("type",$type)->first()->toArray();
        app()->configure('ppurl');
        $img_url = config('ppurl.admin_url');
        $data['picture'] = $img_url.$data['picture'];
        return $this->json('1','获取数据成功',$data);
    }
}