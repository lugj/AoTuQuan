<?php

namespace App\Http\Controllers\Data;
use App\Http\Components\UserComponent;
use App\Models\UserSettingModel;
use App\Models\DataConfigModel;
use Illuminate\Http\Request;


/**
 * 获取配置配置信息
 */
class GetConfigController extends DataController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->get_config($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 获取配置配置信息
     * @param Request $request
     * @author paulLu
     */
    public function get_config(Request $request){
        $data = array();
        $list = DataConfigModel::select(['title','desc','key','value'])
            ->where('key','service_price_min')
            ->orwhere('key','service_price_max')
            ->orwhere('key','order_aftertime')
            ->orwhere('key','order_time_min')
            ->orwhere('key','order_time_max')
            ->orwhere('key','withdrawal_min')
            ->orwhere('key','find_actived_time')
            ->orwhere('key','find_distance')
            ->orwhere('key','boot_page_img')
            ->orwhere('key','boot_page_img_url')
            ->get();
        if($list){
            $list = $list->toArray();
            $value = array();
            app()->configure('ppurl');
            $admin_url = config('ppurl.admin_url');
            foreach($list AS $litem){
                if($litem['key'] == 'boot_page_img' || $litem['key'] == 'boot_page_img_url'){
                    if($litem['key'] == 'boot_page_img'){
                        $value['img'] = $admin_url."/".$litem['value'];
                        $data['boot_page_img']['title'] = $litem['title'];
                        $data['boot_page_img']['desc'] = $litem['desc'];
                    }elseif($litem['key'] == 'boot_page_img_url'){
                        $value['url'] = $litem['value'];
                    }

                    $data['boot_page_img']['value'] = $value;
                }else{
                    $data[$litem['key']] = array(
                        'title'=>$litem['title'],
                        'desc'=>$litem['desc'],
                        'value'=>$litem['value']
                    );
                }

            }
        }
        return $this->json('1','获取数据成功',$data);
    }
}