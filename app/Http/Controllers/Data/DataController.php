<?php

namespace App\Http\Controllers\Data;

use App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

/**
 * 数据字典 控制器
 * 
 */
class DataController extends ApiController{

    public function __construct(){
        parent::__construct();
    }

    protected function check_version($file_name,$request_version){
    	// app端保存的缓存数据版本
        app()->configure($file_name);
        if ($request_version == config($file_name.'.version')) {
        	return $request_version;
        }else{
        	return false;
        }
    }

    protected function check_db_version($file_name){
    	// app端保存的缓存数据版本
        $data_version = DB::table('data_version')->where('name',$file_name)->first();
        $version = ($data_version->updated_at)?$data_version->updated_at:$data_version->created_at;

    	return $version;
        // app()->configure($file_name);
        // if ($version == config($file_name.'.version')) {
        // }else{
        // 	return false;
        // }
    }

    protected function set_config($file_name,$data){
    	// 写入到data
        ob_start();
        var_export($data);
        $con = ob_get_contents();
        ob_end_clean();
        $config_dir = dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config/';
        $content = '<?php '.PHP_EOL.'return '.$con.PHP_EOL.';';
        file_put_contents($config_dir.$file_name.'.php', $content);
    }
}
