<?php

namespace App\Http\Controllers\Data;
use App\Http\Components\UserComponent;
use App\Models\UserSettingModel;
use Illuminate\Http\Request;


/**
 * 获取行政区划
 */
class GetCityAreaController extends DataController{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 版本控制，对应app版本跳转对应接口
     * @param Request $request
     * @author paulLu
     */
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->get_city_area($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    /**
     * 获取所在市的行政区划
     * @param Request $request
     * @author paulLu
     */
    public function get_city_area(Request $request){
        $user = UserComponent::check_token($request);
        $user_id = $user['user_id'];

        $setting = UserSettingModel::firstOrNew(['user_id'=>$user_id]);
        $city = $setting->city;
        app()->configure('areas');
        $areas = config('areas.content');
        $data = array();
        $data['city'] = $city;
        foreach($areas[$city] as $area){
            $data['area'][] = $area['name'];
        }
        return $this->json('1','获取数据成功',$data);
    }
}