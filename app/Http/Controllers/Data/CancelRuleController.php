<?php

namespace App\Http\Controllers\Data;

use App\library\juhe\Weather;
use App\Models\OtherH5Model;
use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;

/**
 * 消单惩罚规则
 */
class CancelRuleController extends DataController{

    public function __construct(){
        parent::__construct();
    }

    /*版本控制，对应app版本跳转对应接口*/
    public function version_invoke(Request $request){
        $appversion = $request->input('appversion');
        $apptype = $request->input('apptype');
        if ($appversion && $apptype) {
            return $this->cancel_rule($request);
        }else{
            return $this->json('102','该app版本下无此接口，请更新app');
        }
    }

    public function cancel_rule(Request $request){
        $data = OtherH5Model::where('id',5)->first()->toArray();

        return $this->json('1','获取成功',$data);
    }
}
