<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use Route;

class ApiSign{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $app_token = config('constants.app_token');
        $app_sign = $request->input('sign');
        if ($app_sign && $random_str = $request->input('random_str')) {
            $api_sign = md5(md5(md5($app_token.$random_str)));
            if ($api_sign == $app_sign) {
                return $next($request);
            }
        }
        elseif ($request->input('debug')) {
            return $next($request);
        }
        exit(json_encode(array('status'=>'201','message'=>'APP签名验证未通过')));
    }
}
 