<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

use App\Models\UserProfile;

class UserEvent extends Event{

    use SerializesModels;

    public $event_obj;

    /**
     * 创建新的事件实例
     *
     * @param  UserProfile  $user_profile
     * @return void
     */
    public function __construct(UserProfile $event_obj)
    {
        $this->event_obj = $event_obj;
    }
}