<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;
use \Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
// use App\Models\UserProfileModel;

class UserServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\PodcastWasPurchased' => [
            'App\Listeners\EmailPurchaseConfirmation',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        UserProfileModel::updated(function($user_profile){
            $corps = CorpsModel::where('user_id',$user_profile->user_id)->get();
            foreach ($corps as $corps_row) {
                $corps_row->captain_name = $user_profile['nick_name'];
                $corps_row->save();
            }
        });
    }
}
