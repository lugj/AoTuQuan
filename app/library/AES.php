<?php 
namespace App\library;
/**
 * php AES加解密类
 * 如果要与java共用，则密钥长度应该为16位长度
 * 因为java只支持128位加密，所以php也用128位加密，可以与java互转。
 * 同时AES的标准也是128位。只是RIJNDAEL算法可以支持128，192和256位加密。
 * java 要使用AES/CBC/NoPadding标准来加解密
 * 
 * @author Terry
 *
 */
class AES{  
    /** 
     * 算法,另外还有192和256两种长度 
     */  
    const CIPHER = MCRYPT_RIJNDAEL_128;  
    /** 
     * 模式  
     */  
    const MODE = MCRYPT_MODE_ECB;  
  
    private static $key = 'apptest';

    /** 
     * 加密 
     * @param string $key   密钥 
     * @param string $str   需加密的字符串 
     * @return type  
     */  
    static public function encode($str){  
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(self::CIPHER,self::MODE),MCRYPT_RAND);  
        $encrypt_str = mcrypt_encrypt(self::CIPHER, self::$key, self::addPKCS7Padding($str), self::MODE, $iv);  
        return base64_encode($encrypt_str);
    }  
      
    /** 
     * 解密 
     * @param type $key 
     * @param type $str 
     * @return type  
     */  
    static public function decode($encrypt_str){  
        $encrypt_str = base64_decode($encrypt_str);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(self::CIPHER,self::MODE),MCRYPT_RAND);  
        $str = mcrypt_decrypt(self::CIPHER, self::$key, $encrypt_str, self::MODE, $iv);  
        return trim($str);
    }

    /**
     * 填充算法
     * @param string $source
     * @return string
     */
    static public function addPKCS7Padding($source){
        $source = trim($source);
        $block = mcrypt_get_block_size('rijndael-128', 'ecb');
        $pad = $block - (strlen($source) % $block);
        if ($pad <= $block) {
            $char = chr($pad);
            $source .= str_repeat($char, $pad);
        }
        return $source;
    }
    /**
     * 移去填充算法
     * @param string $source
     * @return string
     */
    static public function stripPKSC7Padding($source){
        $source = trim($source);
        $char = substr($source, -1);
        $num = ord($char);
        if($num==62)return $source;
        $source = substr($source,0,-$num);
        return $source;
    }
}