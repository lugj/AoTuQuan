<?php
namespace App\library;
/**
 * 公共类
 *
 * @package library
 * @author  Hong <hong1070@qq.com>
 * @version CCCommonUtil.class.php 2012-12-06 by Hong
 */
class CCCommonUtil
{

    public static function object_to_array($obj)
    {
        $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
        foreach ($_arr as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? self::object_to_array($val) : $val;
            $arr[$key] = $val;
        }
        return $arr;
    }

    /**
     *
     * 包含中英文长度的长度限制判断，utf8在判断时算成两个字符 ...
     * @param String $str
     */
    public static function  getLength($str)
    {
        return strlen((preg_replace("/[\xe0-\xef][\x80-\xbf]{2}/", 'xx', $str)));
    }

    /**
     * 过滤脏词
     *
     * Enter description here ...
     * @param string $word
     */
    public static function filterWord($word)
    {
        return htmlspecialchars($word);
    }

    /**
     * 日期格式化
     */
    public static function  getDate()
    {
        return date('Y-m-d');
    }

    /**
     * 时间格式化
     */
    public static function  getTime()
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * 多维数组排序
     * @param string $data
     * @param string $column1
     * @param string $sort1
     * @param string $column2
     * @param string $sort2
     */
    public static function multisort(&$data, $column1, $sort1, $column2, $sort2)
    {
        foreach ($data as $key => $row) {
            $volume[$key] = $row[$column1];
            $edition[$key] = $row[$column2];
        }
        array_multisort($volume, $sort1, $edition, $sort2, $data);
    }

    /**
     * 两个日期直接的天数差
     * Enter description here ...
     * @param unknown_type $date1
     * @param unknown_type $date2
     */
    public static function datediff($date1, $date2)
    {
        $d1 = strtotime($date1);
        $d2 = strtotime($date2);
        return round(($d2 - $d1) / 3600 / 24);
    }

    /**
     * 获取真实的Ip地址
     * @return String
     */
    public static function getClientIp()
    {
        if (isset ($_SERVER ['HTTP_QVIA'])) {
            $ip = qvia2ip($_SERVER ['HTTP_QVIA']);
            if ($ip) {
                return $ip;
            }
        }
        if (isset ($_SERVER ['HTTP_CLIENT_IP']) and !empty ($_SERVER ['HTTP_CLIENT_IP'])) {
            return CCCommonUtil::filterIp($_SERVER ['HTTP_CLIENT_IP']);
        }
        if (isset ($_SERVER ['HTTP_X_FORWARDED_FOR']) and !empty ($_SERVER ['HTTP_X_FORWARDED_FOR'])) {
            $ip = strtok($_SERVER ['HTTP_X_FORWARDED_FOR'], ',');
            return CCCommonUtil::filterIp($ip);
        }
        if (isset ($_SERVER ['HTTP_PROXY_USER']) and !empty ($_SERVER ['HTTP_PROXY_USER'])) {
            return CCCommonUtil::filterIp($_SERVER ['HTTP_PROXY_USER']);
        }
        if (isset ($_SERVER ['REMOTE_ADDR']) and !empty ($_SERVER ['REMOTE_ADDR'])) {
            return CCCommonUtil::filterIp($_SERVER ['REMOTE_ADDR']);
        } else {
            return "0.0.0.0";
        }
    }

    /**
     * Filter the ip string
     *
     * @param  string $key the ip string
     * @return boolean              whether the ip is correct
     */
    public static function filterIp($key)
    {
        $key = preg_replace("/[^0-9.]/", "", $key);
        return preg_match("/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/", $key) ? $key : "0.0.0.0";
    }
}