<?php
namespace App\library\alidayu;
include "TopSdk.php";
//阿里大于短信发送
class Sms{

    private $appkey ='';
    private $secretkey = '';
    private $code_tpl = array(
        'login'=>'SMS_100900160',
        'register'=>'SMS_100900160',
        'change'=>'SMS_100900160',
    );
    public function __construct($appkey,$secretkey){
        $this->appkey = $appkey;
        $this->secretkey = $secretkey;
    }

    public function sendCheckCode($code,$phone,$code_type){
        $c = new \TopClient();
        $c->appkey = $this->appkey;
        $c->secretKey = $this->secretkey;
        $req = new \AlibabaAliqinFcSmsNumSendRequest();
        $req->setExtend("23370671");
        $req->setSmsType("normal");
        $req->setSmsFreeSignName("凹凸圈");
        $message = array(
            'code'=>$code,
        );
        $message = json_encode($message);
        $req->setSmsParam($message);
        $req->setRecNum($phone);
        $req->setSmsTemplateCode($this->code_tpl[$code_type]);
        $resp = $c->execute($req);
        return $resp;
    }
}
?>