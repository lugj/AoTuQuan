<?php
namespace App\library\juhe;
use Exception;
// +----------------------------------------------------------------------
// | JuhePHP [ NO ZUO NO DIE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2010-2015 http://juhe.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: Juhedata <info@juhe.cn>
// +----------------------------------------------------------------------
 
//----------------------------------
// 聚合数据
//----------------------------------
class Sms{
    private $appkey = false; //申请的聚合天气预报APPKEY
 
    private $sendUrl = 'http://v.juhe.cn/sms/send';

    // private $checkCodeTpl = '9947';
    private $checkCodeTpl = '10186';

    public function __construct($appkey){
        $this->appkey = $appkey;
    }
 
    /**
     * 发送验证码
     * @param $phone   用户 Id。（必传）
     * @param $param 群组 Id。（必传）
     * @return mixed
     */
    public function send($phone,$template_id,$param = array()) {
        $http_param = array();
        foreach ($param as $param_key => $param_value) {
            $http_param['#'.$param_key.'#'] = $param_value;
        }
        $smsParam = http_build_query($http_param);
        $smsConf = array(
            'key'       =>  $this->appkey,      //您申请的APPKEY
            'mobile'    =>  "$phone",           //接受短信的用户手机号码
            'tpl_id'    =>  "$template_id",     //您申请的短信模板ID，根据实际情况修改
            'tpl_value' =>  $smsParam           //您设置的模板变量，根据实际情况修改
        );
         
        $content = $this->juhecurl($this->sendUrl,$smsConf,1); //请求发送短信
        return $this->_returnArray($content);
    }

    /**
     * 发送验证码
     * @param $phone   用户 Id。（必传）
     * @param $param 群组 Id。（必传）
     * @return mixed
     */
    public function sendCheckCode($phone,$code,$message='') {
        $tpl_value = '#message#='.$message.'&#code#='.$code;
        $smsConf = array(
            'key'       =>  $this->appkey,      //您申请的APPKEY
            'mobile'    =>  "$phone",           //接受短信的用户手机号码
            'tpl_id'    =>  $this->checkCodeTpl,//您申请的短信模板ID，根据实际情况修改
            'tpl_value' =>  $tpl_value,//您设置的模板变量，根据实际情况修改
        );
         
        $content = $this->juhecurl($this->sendUrl,$smsConf,1); //请求发送短信
        return $this->_returnArray($content);
    }
 
    /**
     * 将JSON内容转为数据，并返回
     * @param string $content [内容]
     * @return array
     */
    public function _returnArray($content){
        return json_decode($content,true);
    }
 
    /**
     * 请求接口返回内容
     * @param  string $url [请求的URL地址]
     * @param  string $params [请求的参数]
     * @param  int $ipost [是否采用POST形式]
     * @return  string
     */
    public function juhecurl($url,$params=false,$ispost=0){
        $httpInfo = array();
        $ch = curl_init();
 
        curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
        curl_setopt( $ch, CURLOPT_USERAGENT , 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36' );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 30 );
        curl_setopt( $ch, CURLOPT_TIMEOUT , 30);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
        if( $ispost )
        {
            curl_setopt( $ch , CURLOPT_POST , true );
            curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
            curl_setopt( $ch , CURLOPT_URL , $url );
        }
        else
        {
            if($params){
                curl_setopt( $ch , CURLOPT_URL , $url.'?'.$params );
            }else{
                curl_setopt( $ch , CURLOPT_URL , $url);
            }
        }
        $response = curl_exec( $ch );
        // var_dump($response);
        if ($response === FALSE) {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
        $httpInfo = array_merge( $httpInfo , curl_getinfo( $ch ) );
        curl_close( $ch );
        return $response;
    }
 
}