<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/2/22
 * Time: 14:04
 */
namespace App\library\juhe;

/**
 * 证件识别接口
 * 提供两种方式，请根据您的PHP版本、服务器环境等因素选择适合的方式
 * 推荐使用第一种(PHP 5 -->= 5.5.0)
 * 示例中的身份证图片来自网络，用真实的身份证图片会有更佳的识别效果
 */
class Identify{
    private $appkey = false; //申请的聚合证件识别的APPKEY

    private $url = 'http://api2.juheapi.com/cardrecon/upload';

    public function __construct($appkey){
        $this->appkey = $appkey;
    }

    /**
     * @param $cardType
     * @param $img_url
     * @param $img_type
     * @return mixed
     */
    public function getIdInfo($cardType,$img_url,$img_type){
        $ch = curl_init($this->url);
        $cfile = curl_file_create($img_url, $img_type, '1.jpg');
        $data = array(
            'cardType' => $cardType,
            'key' =>$this->appkey,
            'pic' => $cfile,
        );
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response,true);
        return $result;
    }

}
//$config = array(
//    'key' => '14230622bca05bea115d1d95f39b5403',
//    'url' => 'http://api2.juheapi.com/cardrecon/upload', //聚合数据证件识别接口的URL地址
//    'cardType' => '2', //证件的类型
//    'type' => 'image/jpg', //证件图片的类型
//);
//
//$ch = curl_init($config['url']);
//$cfile = curl_file_create('1.jpg', $config['type'], '1.jpg');
//$data = array(
//    'cardType' => $config['cardType'],
//    'key' => $config['key'],
//    'pic' => $cfile,
//);
//curl_setopt($ch, CURLOPT_POST,1);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//curl_exec($ch);
//curl_close($ch);