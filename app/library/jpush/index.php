<?php

	include_once(__DIR__ .'/vendor/autoload.php');

	use JPush\Model as M;
	use JPush\JPushClient;
	use JPush\JPushLog;
	use Monolog\Logger;
	use Monolog\Handler\StreamHandler;

	use JPush\Exception\APIConnectionException;
	use JPush\Exception\APIRequestException;

	$master_secret = '571bfb66af3ac589950ac7f3';
	$app_key = '640ecf535cb949cdb943ff8d';

	$master_secret_02 = '27d24a4334551b4bc6747ef0';
	$app_key_02 = '9b09ed86fda0d7feb7aa3e0a';

	JPushLog::setLogHandlers(array(new StreamHandler(__DIR__.'/jpush.log', Logger::DEBUG)));
	$client = new JPushClient($app_key, $master_secret);
	$client_02 = new JPushClient($app_key_02, $master_secret_02);

	function push_notification($user, $title, $content, $link) {
		global $client;
		
		$br = '<br/>';
		$spilt = ' - ';

		// $str_01 = $user == 'all' ? 'M\all' : is_array($user) ? M\audience(M\alias($user) : '';
		
		$link = htmlspecialchars_decode($link);
		try {

		    $result = $client->push();
		    $result->setPlatform(M\all);
		    if ($user == 'all')
		    	$result->setAudience(M\all);
		    else
		    	$result->setAudience(M\audience(M\alias($user)));

		    $result->setNotification(M\notification($title, M\android($content, $title, null, array('url'=>$link)), M\ios($content, 'happy', null, false, array('url'=>$link), null)));
		    $result->setOptions(M\options(time(), 864000, null, true, 0));
		    $result->printJSON();
		    $re = $result->send();
		    
		    echo 'Push Success.' . $br;
		    echo 'sendno : ' . $re->sendno . $br;
		    echo 'msg_id : ' . $re->msg_id . $br;
		    echo 'Response JSON : ' . $re->json . $br;
		} catch (APIRequestException $e) {
		    echo 'Push Fail.' . $br;
		    echo 'Http Code : ' . $e->httpCode . $br;
		    echo 'code : ' . $e->code . $br;
		    echo 'Error Message : ' . $e->message . $br;
		    echo 'Response JSON : ' . $e->json . $br;
		    echo 'rateLimitLimit : ' . $e->rateLimitLimit . $br;
		    echo 'rateLimitRemaining : ' . $e->rateLimitRemaining . $br;
		    echo 'rateLimitReset : ' . $e->rateLimitReset . $br;
		} catch (APIConnectionException $e) {
		    echo 'Push Fail: ' . $br;
		    echo 'Error Message: ' . $e->getMessage() . $br;
		    //response timeout means your request has probably be received by JPUsh Server,please check that whether need to be pushed again.
		    echo 'IsResponseTimeout: ' . $e->isResponseTimeout . $br;
		}

		echo $br . '-------------' . $br;

		// header('Refresh:5;url=?m=activity&s=app_info_push.php');
		// exit;
	}

	function push_notification_02($user, $title, $content, $link) {
		global $client_02;
		
		$br = '<br/>';
		$spilt = ' - ';

		// $str_01 = $user == 'all' ? 'M\all' : is_array($user) ? M\audience(M\alias($user) : '';
		
		$link = htmlspecialchars_decode($link);
		try {

		    $result = $client_02->push();
		    $result->setPlatform(M\all);
		    if ($user == 'all')
		    	$result->setAudience(M\all);
		    else
		    	$result->setAudience(M\audience(M\alias($user)));

		    $result->setNotification(M\notification($title, M\android($content, $title, null, array('url'=>$link)), M\ios($content, 'happy', null, false, array('url'=>$link), null)));
		    $result->setOptions(M\options(time(), 864000, null, true, 0));
		    $result->printJSON();
		    $re = $result->send();
		    
		    echo 'Push Success.' . $br;
		    echo 'sendno : ' . $re->sendno . $br;
		    echo 'msg_id : ' . $re->msg_id . $br;
		    echo 'Response JSON : ' . $re->json . $br;
		} catch (APIRequestException $e) {
		    echo 'Push Fail.' . $br;
		    echo 'Http Code : ' . $e->httpCode . $br;
		    echo 'code : ' . $e->code . $br;
		    echo 'Error Message : ' . $e->message . $br;
		    echo 'Response JSON : ' . $e->json . $br;
		    echo 'rateLimitLimit : ' . $e->rateLimitLimit . $br;
		    echo 'rateLimitRemaining : ' . $e->rateLimitRemaining . $br;
		    echo 'rateLimitReset : ' . $e->rateLimitReset . $br;
		} catch (APIConnectionException $e) {
		    echo 'Push Fail: ' . $br;
		    echo 'Error Message: ' . $e->getMessage() . $br;
		    //response timeout means your request has probably be received by JPUsh Server,please check that whether need to be pushed again.
		    echo 'IsResponseTimeout: ' . $e->isResponseTimeout . $br;
		}

		echo $br . '-------------' . $br;

		header('Refresh:5;url=?m=activity&s=app_info_push.php');
		exit;
	}


?>