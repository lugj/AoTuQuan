<?php 
namespace App\library;

class UtilRegular { 

	// 判断是否是身份证
	public static function idcard($id_card_no){
		$is_legal = false;
		if (strlen($id_card_no) == 15) {
            $is_legal = preg_match("/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/", $id_card_no);
        }elseif (strlen($id_card_no) == 18) {
            $is_legal = preg_match("/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}(\d|x|X)$/", $id_card_no);
        }
        return $is_legal;
	}

	// 判断是否都是中文
	public static function chinese($character){
		$is_legal = false;
        $is_legal = preg_match("/^[\x{4e00}-\x{9fa5}]+$/u",$character);
        return $is_legal;
	}
}