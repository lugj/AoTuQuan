<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="title" content="极客邦SOHO-技能确认">
<meta name="description" content="极客邦SOHO-技能确认">
<meta name="keywords" content="极客邦SOHO-技能确认">
<meta name="apple-mobile-web-app-capable" content="yes" />
<title>极客邦SOHO-技能确认</title>

<link href="/home/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="/home/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
<script type="text/javascript" src="/home/js/jquery-1.9.1.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#examTest" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand"><img alt="" src="/home/img/soho_logo.png" class="img-responsive" width="150" style="display: inline-block;">测评系统</a>
	</div>
  </div>
</nav>

<!-- content starts -->
<div id="content">			
	@yield('content')
</div>
			<!-- content ends -->
<div class="row-fluid">
	<div class="logfooter"  style='display:none;'>
		<ul class="inline unstyled">
			<li><a href="http://www.looip.cn/">网站首页</a></li>
			<li>|</li>
			<li><a href="#">用户须知</a></li>
			<li>|</li>
			<li><a href="#">隐私协议</a></li>
			<li>|</li>
			<li><a href="#">网站论坛</a></li>
			<li>|</li>
			<li>© 2009-2015 持创研发中心 备案号 xxxx</li>
		</ul>
	</div>
</div>
</body>
</html>