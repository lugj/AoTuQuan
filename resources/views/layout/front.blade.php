<!DOCTYPE html>
<html lang="en">

<head>
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="author" content="">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="qc:admins" content="17161452403707645346477106375" />
	<!--<meta property="qc:admins" content="1716145227647710636" />-->
	<meta name="keywords" content="程序员,程序员兼职,兼职程序员,程序员兼职信息,独立程序员，PHP兼职,IOS ios兼职, Android  android兼职, 前端开发 html5兼职,开发兼职,兼职众包，" />
	<meta name="description" content="极客邦SOHO是独立程序员兼职任务协同平台，服务区域于北京、上海、广州、深圳、苏州、杭州、重庆、成都、武汉、西安、大连、天津、南京专业服务程序员，IT技术的兼职，众包服务，交易服务，兼职服务，帮助程序员赚钱 looip.cn  coding.net，外快" />
	<title> 极客邦SOHO 为程序员发现能赚钱的兼职机会</title>

	<link rel="stylesheet" href="/home/css/style.min.css">
	@yield('style')

	<script type="text/javascript">
		var _paq = _paq || [];
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function() {
			var u="//piwik.test.looip.com/";
			_paq.push(['setTrackerUrl', u+'piwik.php']);
			_paq.push(['setSiteId', 1]);
			var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
			g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
		})();
	</script>

	<script type="text/javascript">
		var SOHO_CONF = {!!json_encode(config("jsmap.".\Route::currentRouteName())) !!};
		if (SOHO_CONF) {
			SOHO_CONF.usertype = '{{ session("user_info") ? session("user_info")->user_type : '' }}';
			SOHO_CONF.isfirs = '{{ isset($userInfo->first_register) && $userInfo->first_register ? "true" : "false" }}';
		}
		var timestamp = "{{ date('Y-m-d H:i:s') }}",
				_token = "{{ csrf_token() }}";
				statistics = "{{ route('ua-statistics') }}",
				aboutlinks = "{{route('ua-linkstatistics')}}";
				getCodeUrl = "{{route('get-code')}}",
				doResetUrl = "{{route('do-reset')}}",
				loginUrl = "{{route('login')}}",
				qqLoginUrl = "{{route('qq-login')}}",
				doRegisterGeek = "{{route('do-register-geek')}}",
				doRegisterBoss = "{{route('do-register-boss')}}",
				guidecUrl = "{{ route('guidec') }}",
				guidebUrl = "{{ route('guideb') }}",
				doInviteAnswer = "{{ route('do-invite-answer') }}";
	</script>
	<!--[if lt IE 9]>
	<script>
	   (function() {
			if (!
			/*@cc_on!@*/
			0) return;
			var e = "abbr, article, aside, audio, canvas, datalist, details, dialog, eventsource, figure, footer, header, hgroup, mark, menu, meter, nav, output, progress, section, time, video".split(', ');
			var i= e.length;
			while (i--) { document.createElement(e[i]) }
		})()
	</script>
	<![endif]-->
</head>

<body>
	<!-- <div class="loading-wrap"><div class="loading"><div class="logo"></div><div class="shadow"></div></div></div> -->
	<div class="header-wrap animated">
		<header class="soho-header">
			<a class="logo" href="/">
				<img src="/home/images/logo_blue_medium.png">
			</a>
			<!--menutrigger-->
			<div class="min-menu">
				<a class="menu-trigger" data-flag="trigger" href="javascript:void(0);"></a>
			</div>
			<div class="right-col clearfix" id="trigger">
				<ul class="sub-nav" id="sohoMenu">
					@if (empty(session('user_info')))
					<!-- <li><a href="{{ route('find') }}">发现大牛</a></li> -->
					<li><a class="j-login" href="{{route('login')}}" data-type="3">登录</a></li>
					<li><a class="j-register" href="{{route('register')}}" data-type="2">加入</a></li>
					@else @if (session('user_info')->user_type == 'B')
					<li><a class="j-find" href="{{ route('find') }}" data-type="7">发现</a></li>
					@endif
					<li><a href="{{ route('invitation') }}">约单</a></li>
					@if (session('user_info')->user_type == 'C')
					<li><a href="{{ route('user-purse') }}">钱包</a></li>
					@endif
					<li><a href="{{ route('user-settings') }}">设置</a></li>
					<li><a class="j-logout" href="{{route('logout')}}" data-type="14">退出</a></li>
					@endif
				</ul>
				</ul>
			</div>
		</header>
	</div>

	<!--内容-->
	@yield('content')
	<!--内容end-->

	<footer class="footer">
		<nav class="navbar">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="site-info clearfix">
							<li class=""><a class="soho-tel" href="javascript:void(0);">400-018-8883</a></li>
							<!-- <li class="col-md-4"><a class="" href="tel:4000188883">400-018-8883</a></li> -->
							<li class="">
								<a class="about-us" href="/about">关于我们</a>
								<a class="about-us" href="/help" target="_blank">帮助中心</a>
								<a class="about-us" href="http://ghj.looip.cn?q=2" target="_blank">极客微刊</a>
								<a class="" href="/about#support" target="_blank">合作伙伴</a>
							</li>
							<li class="">
								<a href="javascript:;" class="btn-ico btn-wechat">
									<div class="share-qc">
										<img src="/home/images/ico_qc.jpg" width="100%">
									</div>
								</a>
								<a class="btn-ico btn-weibo" id="weiboLink" href="http://weibo.com/5675277720/profile?topnav=1&amp;wvr=6" data-link-pc="http://weibo.com/5675277720/profile?topnav=1&amp;wvr=6" data-link-phone="http://m.weibo.cn/u/5675277720" target="_blank"></a>
							</li>
						</ul>
					</div>
					<div class="col-sm-6 foot-version">
						<p class="soho-font-12 text-right">
							沪&nbsp;ICP&nbsp;备&nbsp;12021414&nbsp;号-1
							<!-- <span class="interval">|</span> -->
							2003-2015 looip.cn, All Rights Reserved
						</p>
						<div class="soho-font-12 soho-color-gray text-center">版本号:V2.0.0</div>
					</div>

				</div>
			</div>
		</nav>
	</footer>
	<div class="loading-wrap">
		<div class="loading">
			<div class="logo"></div>
			<div class="shadow"></div>
		</div>
	</div>
	<!--[if lt IE 9]>
	<script src="/home/lib/respond.js"></script>
	<script src="/home/lib/jquery.placeholder.js"></script>
	<script type="text/javascript">
		$('input, textarea').placeholder();
	</script>
	<![endif]-->
	<script type="text/javascript" src="/home/lib/seajs/sea.js"></script>
	<script type="text/javascript" src="/home/page/config.js"></script>
	<script type="text/javascript"> seajs.use("/home/page/" + SOHO_CONF.module + "/" + SOHO_CONF.page); </script>
	@yield('script')
	<script type="text/javascript">
		seajs.use(['dialog'], function() {
			@if (session('message'))
				var message = "{!! session('message') !!}";
				@elseif(isset($message))
			var message = "{!! $message !!}";
			@elseif(isset($errors) && count($errors))
			var message = "{{implode('', $errors->all())}}";
			@endif
			if (typeof message != 'undefined' && message.length) {
				UI.prompt(message);
			}
		})
	</script>
	@if( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false ) @include('wechat.jsshare') @endif @if (! config('app.debug'))
	<script type="text/javascript">
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "//hm.baidu.com/hm.js?5f050a120cb23c2d8229e82e91c33bb7";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
	@endif


</body>

</html>
