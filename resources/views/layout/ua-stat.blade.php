<script>
    @if( $userInfo = session('user_info'))
        @if ($userInfo->user_type == 'B')
            $('a[href="{{route('find')}}"]').click(function(){
                $.ajax({
                    url: '{{route('ua-statistics')}}',
                    type: 'get',
                    async: false,
                    data: {'type':7, '_token':"{{csrf_token()}}"},
                    error: function(e){
                      console.log(e);
                    }
                });
            });
        @endif
        @if (isset($userInfo->first_register) && $userInfo->first_register)
            $('a[href="{{route('find')}}"]').click(function(){
                $.ajax({
                    url: '{{route('ua-statistics')}}',
                    type: 'get',
                    async: false,
                    data: {'type':13, '_token':"{{csrf_token()}}"}
                });

            });
            $('a[href="{{route('logout')}}"]').click(function(){
                $.ajax({
                    url: '{{route('ua-statistics')}}',
                    type: 'get',
                    async: false,
                    data: {'type':14, '_token':"{{csrf_token()}}"}
                });
            });
         @endif
    @else
     $('a[href="{{route('login')}}"]').click(function(){
        $.ajax({
            url: '{{route('ua-statistics')}}',
            type: 'get',
            async: false,
            data: {'type':3, '_token':"{{csrf_token()}}"}
        });
    });
    $('a[href="{{route('register')}}"]').click(function(){
        $.ajax({
            url: '{{route('ua-statistics')}}',
            type: 'get',
            async: false,
            data: {'type':2, '_token':"{{csrf_token()}}"}
        });
    });
    @endif

</script>
