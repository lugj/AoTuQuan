<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title;?></title>
<meta name="viewport" content="width=device-width, user-scalable=no">
<link rel="stylesheet" href="/h5/css/style.css">
<script type="text/javascript" src="/h5/js/jquery.min.js"></script>
<script type="text/javascript" src="/h5/js/public.js"></script>
</head>
<body>
<div id="subject">
	<div class="statement">
		<div class="statement_p">

			<?php echo $data['content']?>
		</div>
	</div>
</div>

<!--页面涉及脚本-->
<script type="text/javascript">
$(document).ready(function(){
});
</script>
</body>
</html>