<?php if ( $_SERVER['REQUEST_METHOD'] == 'POST' ): ?>
	<?php echo json_encode(array('status'=>'101','message'=>'接口请求地址不正确')); ?>
<?php else: ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="author" content="">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="qc:admins" content="1716145227647710636" />
	<meta name="keywords" content="程序员兼职,兼职程序员,程序员兼职信息,独立程序员，PHP兼职,IOS ios兼职, Android  android兼职, 前端开发 html5兼职,开发兼职,兼职众包，" />
	<meta name="description" content="极客邦SOHO是独立程序员兼职任务协同平台，服务区域于北京、上海、广州、深圳、苏州、杭州、重庆、成都、武汉、西安、大连、天津、南京专业服务程序员，IT技术的兼职，众包服务，交易服务，兼职服务，帮助程序员赚钱 looip.cn  coding.net，外快" />
	<title>404</title>
	<style type="text/css">
		body, html {
			padding: 0;
			margin: 0;
		}
		.back-home {
			display: block;
			width: 200px;
			height: 40px;
			line-height: 40px;
			background-color: #15304B;
			color: #FFF;
			text-align: center;
			text-decoration: none;
			position: absolute;
			left: 50%;
			margin-left: -100px;
			bottom: 70px;
			border: 1px solid #FFF;
			border-radius: 20px;
		}
		.box {
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background: url(/home/images/404.jpg) no-repeat center center;
			background-size: cover;
		}
	</style>

</head>
<body>
	<div class="wrap">
		<div class="box">
			
		</div>
		<a href="/" class="back-home">返回首页</a>
	</div>
</body>
</html>
<?php endif ?>