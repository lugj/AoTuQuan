<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>GeekApp接口调试</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="/api/css/bootstrap.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    </style>
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h2>GeekApp接口调试界面</h2>
        <h3>使用说明：</h3>
        <h4>（1）选择合适的接口。</h4>
        <h4>（2）系统会生成该接口的参数表，您可以直接在文本框内填入对应的参数值。</h4>
        <h4>（3）点击检查问题按钮，即可得到相应的调试信息。</h4>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <form class=" form-horizontal" action="{{$api_url}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="sign" value="{{$sign}}">
                <input type="hidden" name="random_str" value="{{$random_str}}">
                <input type="hidden" name="appversion" value="{{$appversion}}">
                <input type="hidden" name="apptype" value="{{$apptype}}">
                <div class="form-group">
                    <label class="col-sm-2 control-label">主题(subject)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="subject">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">运动类型(sport)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="sport">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">运动认证(sport_authed)</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="sport_authed">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">见面方式</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="pick_up_type">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">价格</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="hourly_pay">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">城市</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="city">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">区域</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="area">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">开始时间</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="allow_start_time">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">结束时间</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="allow_end_time">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">性别要求</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="allow_gender">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">需求</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="type">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">图片1</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="img_url1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">图片2</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="img_url2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">图片3</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="img_url3">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-10">
                        <button class="btn btn-default">提交</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>