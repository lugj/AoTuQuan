<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="author" content="">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="qc:admins" content="17161452403707645346477106375" />
	<!--<meta property="qc:admins" content="1716145227647710636" />-->
	<meta name="keywords" content="程序员,程序员兼职,兼职程序员,程序员兼职信息,独立程序员，PHP兼职,IOS ios兼职, Android  android兼职, 前端开发 html5兼职,开发兼职,兼职众包，" />
	<meta name="description" content="极客邦SOHO是独立程序员兼职任务协同平台，服务区域于北京、上海、广州、深圳、苏州、杭州、重庆、成都、武汉、西安、大连、天津、南京专业服务程序员，IT技术的兼职，众包服务，交易服务，兼职服务，帮助程序员赚钱 looip.cn  coding.net，外快" />
	<title> 极客邦SOHO 为程序员发现能赚钱的兼职机会</title>
	<base href="http://www.looip.cn"/>
	<link rel="stylesheet" href="/home/css/style.min.css?v=201512101732" media="screen" title="no title" charset="utf-8">
</head>

<body style="padding-top: 0!important">
	<!--内容-->
<div class="about-wrap">
	<div class="banner">
		<div class="banner-inner row">
			<div class="col-md-3 banner-font">

			</div>
		</div>
	</div>
	<div class="content">
		<p>“我所擅长的技能还能做什么？”作为一个高端程序员，您会不会有这样的困扰？</p>
		<p>一个处在上升期且精力充沛的程序员，利用业余时间从事兼职，凭借自己的能力攻坚更多的项目，获得更多的劳动报酬，同时帮助创业者快速、保质地完成产品研发及迭代，持续创造自身及社会价值。极客邦SOHO正是为帮助程序员实现最大兼职价值，致力于提供专业的服务、自由的环境，打造新时代的工作方式，将全球最好的项目交给最好的极客程序员来实现。</p>
		<p>如果说全球的创业家是梦想者，全球的极客程序员是实践者，那极客邦SOHO愿意成为连接全球梦想者和实践者的桥梁。</p>
		<p>极客邦SOHO，向所有的脑力劳动者致敬，向所有伟大的极客程序员致敬！</p>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 contact">
				<h3>地址</h3>
				<ul>
					<li><span>北京</span><p>朝阳区四惠东通惠河畔产业园1111号国投尚科大厦六层</p></li>
					<li><span>上海</span><p>浦东新区郭守敬路498号1号楼416-412室</p></li>
					<li><span>苏州</span><p>工业园区企鸿路39号博济智造1-2层</p></li>
				</ul>
			</div>
			<div class="col-md-6 address clearfix">
				<div class="col-xs-12 col-md-12">
					<h3>联系我们</h3>
					<div class="row">
						<div class="col-xs-6 col-md-6 clearfix">
							<span class="about-icon about-icon-serve"></span>
							<div class="nav-item"><span>客服电话</span><span>400-018-8883</span></div>
						</div>
						<div class="col-xs-6 col-md-6 clearfix">
							<span class="about-icon about-icon-wechat"></span>
							<div class="nav-item"><span>客服微信</span><span>jikesoho</span></div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-12">
					<h3>商务合作</h3>
					<div class="row">
						<div class="col-xs-6 col-md-6 clearfix"><span class="about-icon about-icon-tel"></span><div class="nav-item"><span>电话</span><span>400-000-2457</span></div></div>
						<div class="col-xs-6 col-md-6 clearfix"><span class="about-icon about-icon-mail"></span><div class="nav-item"><span>EMAIL</span><span>sales@looip.cn</span></div></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

</html>
