
<!DOCTYPE html>
<html lang="en">

<head>
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="author" content="">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="qc:admins" content="17161452403707645346477106375" />
	<!--<meta property="qc:admins" content="1716145227647710636" />-->
	<meta name="keywords" content="程序员,程序员兼职,兼职程序员,程序员兼职信息,独立程序员，PHP兼职,IOS ios兼职, Android  android兼职, 前端开发 html5兼职,开发兼职,兼职众包，" />
	<meta name="description" content="极客邦SOHO是独立程序员兼职任务协同平台，服务区域于北京、上海、广州、深圳、苏州、杭州、重庆、成都、武汉、西安、大连、天津、南京专业服务程序员，IT技术的兼职，众包服务，交易服务，兼职服务，帮助程序员赚钱 looip.cn  coding.net，外快" />
	<title> 极客邦SOHO 为程序员发现能赚钱的兼职机会</title>
	<base href="http://www.looip.cn"/>
	<link rel="stylesheet" href="/home/css/style.min.css?v=201512101732" media="screen" title="no title" charset="utf-8">
	
</head>

<body style="padding-top: 0;">


	<!--内容-->
	<!--begin -->
<div class="help-wrap">
  <div class="hlep-banner">
    <div class="banner-inner">
      <div class="help-search">
        <h3>欢迎您，我们如何能够帮助您?</h3>
        <div class="search-wrap">
        <form  method="get" action="http://www.looip.cn/help">
          <input class="help-search-input ico" type="search" name="search" placeholder="搜索任何内容(预约,接单,付款)" value="">
          <input class="help-btn-search" type="button" data-search="http://www.looip.cn/help" name="btn_search" value=""></input>
        </form>
        </div>
      </div>

    </div>
    <div class="help-nav-wrap">
      <ul class="help-nav">
        <li class="active help-center-btn"><a href="http://www.looip.cn/help">帮助中心</a></li>
          <li class="how-2use-btn menu-bar"><a href='javascript:history.go(-1);'><span class="ico-left-lt">&lt;</span>
                              平台使用
                              </a>
          </li>
      </ul>
    </div>
      <ul class="min-help-nav" id="helpTabs">
        <li class="active" style="width: 100%;"><a href="#" data-tab="helpgeek">极客邦用户须知</a></li>
      </ul>
  </div>
  <div class="help-content">
    <div class="content-inner clearfix">
      <div class="content-info" style="display:block!important">
        <div class="content-list">
                  <div class="contnt-item">
            <h3>如何使用极客邦SOHO</h3>
            <p>
              <p>极客邦SOHO让分享变得简单、愉快、安全。我们验证用户的个人资料和需求信息，为极客和企业提供一个智能消息系统让他们可以放心沟通，同时我们还拥有一个值得信赖的收款和转款平台。</p>
            </p>
          </div>
                  <div class="contnt-item">
            <h3>使用你们的产品、服务收费吗？有没有中间手续费呢？</h3>
            <p>
              <p>目前平台对于企业用户是完全免费的。</p>
            </p>
          </div>
                  <div class="contnt-item">
            <h3>我每天可以邀约多少程序员？</h3>
            <p>
              <p>目前您每天可以邀约5次程序员，当天额度用完后需等隔天邀约。</p>
            </p>
          </div>
                  <div class="contnt-item">
            <h3>如何看到程序员信息？</h3>
            <p>
              <p>填写完您的项目及用人需求后，就可以看到平台内的程序员信息了。</p>
            </p>
          </div>
                  <div class="contnt-item">
            <h3>我如何沟通我想联系的程序员？</h3>
            <p>
              <p>邀约您想联系的程序员，如果他同意了邀约，您就可以看到他的QQ联系方式，与其沟通。</p>
            </p>
          </div>
        </div>
      </div>
      <!-- <div class="menu-bar">菜单</div> -->
    </div>
  </div>
</div>
<!--end -->
</body>

</html>
