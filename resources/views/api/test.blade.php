<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>GeekApp接口调试</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="/api/css/bootstrap.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    </style>
  </head>
  <body>
	<div class="container">
		<div class="jumbotron">
		    <h2>GeekApp接口调试界面</h2>
			<h3>使用说明：</h3>
			<h4>（1）选择合适的接口。</h4>
			<h4>（2）系统会生成该接口的参数表，您可以直接在文本框内填入对应的参数值。</h4>
			<h4>（3）点击检查问题按钮，即可得到相应的调试信息。</h4>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<div class=" form-horizontal">
					<div class="form-group">
						<span class="col-sm-2 control-label"><span class="badge">一、接口类型:</span></span>
						<div class="col-sm-5">
							<select class="form-control" onchange="getjiekou(this.value)">
								<option value=''>--请选择--</option>
								@foreach ($arrlist as $key => $value)
					        	<option value='{{$key}}'>{{$key}}</option>
					        	@endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<span class="col-sm-2 control-label"><span class="badge">二、接口列表:</span></span>
						<div class="col-sm-5">
							<span id="jiekou">
							<select class="form-control">
								<option>--请选择--</option>
							</select>
						</span>
						</div>
					</div>	
					<div class="form-group">			
						<span class="col-sm-2 control-label"><span class="badge">三、参数列表:</span></span>
					</div>	
					<form class="form-horizontal"  id="canshu" >
						<div id="" class="alert alert-warning" role="alert">
							请选择接口类型和接口列表
						</div>
					</form>
					<div class="form-group">
						<span class="col-sm-2 control-label"><span class="badge">四、显示方式:</span></span>
						<div class="col-sm-5">
							<select id="view" class="form-control">
								<option>html</option>
								<option>header</option>
								<option>array</option>
								<option>all</option>
							</select>
						</div>
					</div>	
					<input class="btn btn-info" type='button' value="提交" onclick="getjieguo()">
				</div>
			</div>
		</div>

		<div class="embed-responsive embed-responsive-4by3">
	  		<iframe class="embed-responsive-item" id="iframe_id" src=""  style="width:1000px;height:800px;"></iframe>
		</div>	    
	</div>

	<div id="text-html" class="hide">
		<div class="form-group">
		    <label class="col-sm-2 control-label">{label}</label>
		    <div class="col-sm-10">
		      <input type="{type}" class="form-control" name="{name}" id="{id}" placeholder="{placeholder}">
		    </div>
		  </div>
	</div>

	<script type="text/javascript">
		function getjiekou(obj){
			var jiekou ="<select onchange='getcanshu(this.value)' class='form-control' id='list'><option value=''>--请选择--</option>";
			arrjiekou = <?php print_r($jsarrlist) ?>;
			//console.log(arrjiekou); 
		    var i;
			for( i in arrjiekou){
				if(obj==i){
					var j;
					for(j in arrjiekou[i]){
						jiekou+="<option value='"+j+"'>"+j+"</option>";
					}
				}
			}

		    jiekou += "</select>";
			document.getElementById("jiekou").innerHTML = jiekou;
		}

		function getcanshu(obj){
			var canshu ="";
			arrjiekou = <?php print_r($jsarrlist) ?>;
		    //console.log(arrjiekou);
		    var i;
			for( i in arrjiekou){
				var j;
				for(j in arrjiekou[i]){
					if(obj==j){
						var k;
						for(k in arrjiekou[i][j]){
							if (k == 'file') {
								var canshu_html = document.getElementById('text-html').innerHTML;
								canshu_html = canshu_html.replace('{type}','file');
								canshu_html = canshu_html.replace('{name}',arrjiekou[i][j][k]);
								canshu_html = canshu_html.replace('{placeholder}',arrjiekou[i][j][k]);
								canshu_html = canshu_html.replace('{label}',arrjiekou[i][j][k]);
								canshu_html = canshu_html.replace('{id}',arrjiekou[i][j][k]);
								canshu+=canshu_html;
							}else{
								var canshu_html = document.getElementById('text-html').innerHTML;
								canshu_html = canshu_html.replace('{type}','text');
								canshu_html = canshu_html.replace('{name}',arrjiekou[i][j][k]);
								canshu_html = canshu_html.replace('{placeholder}',arrjiekou[i][j][k]);
								canshu_html = canshu_html.replace('{label}',arrjiekou[i][j][k]);
								canshu_html = canshu_html.replace('{id}',arrjiekou[i][j][k]);
								canshu+=canshu_html;
							}
							// canshu+="<span class='label label-default' style='width:150px; height30px;'>"+arrjiekou[i][j][k]+"</span><input name='"+arrjiekou[i][j][k]+"' id='"+arrjiekou[i][j][k]+"'/><br>";
						}
					}
				}
			}

			document.getElementById("canshu").innerHTML = canshu;
		}

		function getjieguo(){

			var list = document.getElementById("list").value;
			var arrlist=[];
			arrlist=list.split("|");

			var view = document.getElementById("view").value;
			var src_canshu ="";
			arrjiekou = <?php print_r($jsarrlist) ?>;	    
		    var i;
			for( i in arrjiekou){
				var j;
				for(j in arrjiekou[i]){
					if(list==j){
						var k;
						for(k in arrjiekou[i][j]){
							var canshu_value=document.getElementById(arrjiekou[i][j][k]).value;
							src_canshu+="&"+arrjiekou[i][j][k]+"="+canshu_value;
						}
					}
				}
			}

			var iframe_src="/"+arrlist[1]+"?view="+view+src_canshu
			console.log(iframe_src);

			document.getElementById("iframe_id").src=iframe_src;
		}
	</script>

  </body>
</html>