@if ($paginator->lastPage() > 1)
<div class="soho-page">
	<div class="dataTables_paginate paging_bs_normal">
        <?php
            $from = 10 * floor(($paginator->currentPage() - 1) / 10);
            $to = min($paginator->lastPage(), 10 + $from);
        ?>
		<ul class="pagination">
			@for ($i = max(1,$from); $i <= $to; $i++)
		        <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
		            <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
		        </li>
		    @endfor
            @if ($to < $paginator->lastPage())
			<li class="next">
		        <a href="{{ $paginator->url($to+1) }}" >
		        	<span>...</span>
		        </a>
		    </li>
            @endif
		</ul>
	</div>
</div>
@endif
